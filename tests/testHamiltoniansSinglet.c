/* test of hamiltonians against known results */
/* compiles with
* cc -o tests/testHamiltoniansSinglet tests/testHamiltoniansSinglet.c cobjs/* objs/singlet* objs/onescalar_* -Iinclude -lm -lgsl -lgslcblas -DHAVE_INLINE -DSINGLET
* after having compiled singlet_mc
*/

#include "common.h"
#include "singlet.h"

int main() {

clock_t start_t = clock();

printf("%s\n\n", "#####Testing Hamiltonians#####");

/** getting couplings */
couplings c;
get_couplings(&c, couplings_file);
/*get_counterterms(&c);*/
/** checking consistency of couplings */
if (!check_couplings(c)) {
	return 1;
}
else {
	printf("%s:\n",couplings_file);
	printf("########## Couplings\n");
	show_couplings(c, stdout);
	printf("########## Counterterms\n");
	show_counterterms(c, stdout);
	/*printf("########## Betas\n");
	show_betas(c, stdout);*/
	print_counterterms(c,cts_file);
	/*printf("\n");*/
}

/** getting paramters */
params p;
get_params(&p, params_file);

/** checking consistency of params */
if (!check_params(p)) {
	return 1;
}
else {
	printf("%s:\n",params_file);
	show_params(p, stdout);
	/*printf("\n");*/
}

/* some doubles to hold test parameters */
double wx = 5.0*2.0*M_PI/(double)p.Nx;
double wy = 5.0*2.0*M_PI/(double)p.Ny;
double wz = 5.0*2.0*M_PI/(double)p.Nz;
double f1 = 17.0/23.0;
double f2 = 5.0/11.0;
double f3 = 3.0/7.0;

/* allocating fields */
fields f, df;
alloc_fields(&f,p);
alloc_fields(&df,p);

/* assigning fields */
for(int x=0;x<(p.Nx);x++) {
  for(int y=0;y<(p.Ny);y++) {
  	for(int z=0;z<(p.Nz);z++) {
  			(f.phi)[x][y][z] = cos(wx*x+f1)*cos(wy*y+f2)*cos(wz*z+f3);
  			(df.phi)[x][y][z] = cos(wx*x+f1)*cos(wy*y+f2)*cos(wz*z+f3);
  	}
  }
}

/* calculating hamiltonians */
double h_n1 = hamiltonian(f,p,c);

/* known result */
double hknown = p.Nx*p.Ny*p.Nz/4096.0 * (256*c.b2 + 9*c.b4)
							+ 25*M_PI*M_PI*(p.Ny*p.Nz/(double)p.Nx + p.Nx*p.Ny/(double)p.Nz
																+ p.Nz*p.Nx/(double)p.Ny)/4.0;

double v1 = 1.0;
int xc1 = p.Nx/2;
int yc1 = 0;
int zc1 = 0;
(df.phi)[xc1][yc1][zc1] += v1;
double dh_n1 = delta_hamiltonian(f.phi,&p,&c,xc1,yc1,zc1,v1);
double h_n1_df = hamiltonian(df,p,c);

/* freeing fields */
free_fields(&f,p);
free_fields(&df,p);

/* printing test results */
printf("\nTest results:\n");
printf("h_n1              : %g\n", h_n1 );
printf("known result      : %g\n", hknown );
printf("error h_n1        : %g\n", (hknown-h_n1)/hknown );
printf("1/Nx              : %g\n", 1.0/(double)p.Nx );
printf("1/Nx^2            : %g\n\n", 1.0/(double)(p.Nx*p.Nx) );
printf("dh_n1             : %g\n", dh_n1 );
printf("dh_n1_exact       : %g\n", h_n1_df-h_n1 );
printf("error dh_n1       : %g\n", (dh_n1-h_n1_df+h_n1)/(h_n1_df-h_n1) );

double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("CPU time taken: %gs\n", total_secs );

return 0;
}
