/* test of heatbath update(s) stuff */

#include "common.h"

int main() {

clock_t start_t = clock();

printf("%s\n\n", "#####Testing copy fields#####");

/* variables */
double h, h1;

/* setting parameters for test */
params p;
get_params(&p, "params");
show_params(p, stdout);

/* allocating fields */
fields f, f1;
alloc_fields(&f,p);
alloc_fields(&f1,p);

/* assigning fields */
for(int x=0;x<(p.Nx);x++) {
  for(int y=0;y<(p.Ny);y++) {
  	for(int z=0;z<(p.Nz);z++) {
  			(f.phi1)[x][y][z] = x + y + z;
  			(f.phi2)[x][y][z] = x + y + z;
  	}
  }
}

h = hamiltonian(f,p);

printf("Copying fields:\n");
copy_fields(&f,&f1,p);
h1 = hamiltonian(f1,p);

/* printing test results */
printf("\nTest results:\n");
printf("%-16s%-16g\n","h",h);
printf("%-16s%-16g\n","h1",h1);
printf("%-16s%-16g\n","(h1-h)/h",(h1-h)/h);

/* freeing mallocs */
free_fields(&f,p);

double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("CPU time taken: %gs\n", total_secs );

return 0;
}
