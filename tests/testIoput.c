/* test of input/output stuff */

#include "common.h"
#include "mersenne.h"

int main() {

clock_t start_t = clock();

printf("%s\n\n", "#####Testing IOput#####");

/* setting parameters for test */
params p;
get_params(&p, "params0");
show_params(p, stdout);

/* testing for separatrix files */
int nSeparatrix = separatrix_file_count(p);
printf("Found %i separatrix files in %s\n",nSeparatrix,p.dir);

/* allocating fields */
fields f, g;
alloc_fields(&f,p);
alloc_fields(&g,p);

/* allocating random number generator, and setting seed */
unsigned long seedlu = dev_urandom();
seed_mersenne(seedlu);
printf("Allocated MT19937 (Nishimura) generator with seed %lu\n\n",seedlu);

/* measurements arrays */
int Ndata = p.Nmc/p.Tmeas;
double **data = make_2d_array(Ndata,NMEAS);
zero_2d_array(data,Ndata,NMEAS);
printf("Allocated data array\n");


/* multicanonical stuff */
if (!p.Umuca) {
	fprintf(stderr,"Need p.Umuca!=0\n");
	return -1;
}
multicanonical muca;
alloc_multicanonical(&muca,p);
set_multicanonical(&muca, p, 0.05,1);
printf("Allocated multicanonical\n");

/* assigning fields */
for(int x=0;x<(p.Nx);x++) {
  for(int y=0;y<(p.Ny);y++) {
  	for(int z=0;z<(p.Nz);z++) {
  			(f.phi1)[x][y][z] = mersenne();
  			(f.phi2)[x][y][z] = mersenne();
  	}
  }
}

/* writing fields */
char *file = "tests/fields.dat";
write_fields(f,p,file);

/* reading fields */
read_fields(&g,p,file);
show_params(p,stdout);

/* comparing fields */
double df = 0;
/* assigning fields */
for(int x=0;x<(p.Nx);x++) {
  for(int y=0;y<(p.Ny);y++) {
  	for(int z=0;z<(p.Nz);z++) {
  			df += fabs((f.phi1)[x][y][z] - (g.phi1)[x][y][z]);
  			df += fabs((f.phi2)[x][y][z] - (g.phi2)[x][y][z]);
  			
  			if (fabs((f.phi2)[x][y][z]-(g.phi2)[x][y][z])>DBL_EPSILON) {
  				printf("at (x,y,z)=(%i,%i,%i)\n",x,y,z);
  				printf("f.phi1 = %g\n",(f.phi1)[x][y][z]);
  				printf("g.phi1 = %g\n",(g.phi1)[x][y][z]);
  				printf("f.phi2 = %g\n",(f.phi2)[x][y][z]);
  				printf("g.phi2 = %g\n",(g.phi2)[x][y][z]);
  			}
  	}
  }
}

/* doing  monte-carlo sweeps */
printf("Monte-carlo sweeps:\n");
double op = order_parameter(f,p);
int tmeas;
for (int t=0; t<p.Nmc; t++) {
	
		metropolis(f.phi1, f.phi2, p, &muca, &op, 0.5);
		metropolis(f.phi2, f.phi1, p, &muca, &op, 0.5);	
		metropolis_global(f, p, &muca, &op, 1, 0.001);
		overrelaxation(f.phi1, f.phi2, p, &muca, &op);
		overrelaxation(f.phi2, f.phi1, p, &muca, &op);
	
		if (t%p.Tmeas == 0 && t) {			
			tmeas = t/p.Tmeas - 1;
		
			/* doing measurements */
			get_measurements(data[tmeas],f,muca,p);
			
			/* checkpoints */
			if ((tmeas+1)%p.Tcheck==0 && t) {	
						
				if (p.Umuca && t) {
					/* updating multicanonical weights */
					/*update_multicanonical(&muca,&(data[tmeas-(p.Tcheck-1)]),0,p);*/
					update_multicanonical(&muca,0,p);
				}	

			}
		}
		
		
}
printf("\n");

/* saving data */
char measfile[201];
sprintf(measfile,"tests/meas.txt");
char measfile3[201];
sprintf(measfile3,"tests/meas3.txt");
save_data(data, Ndata, measfile, 0);
printf("Saved %i measurements to %s\n",Ndata,measfile);
write_data(data, Ndata, measfile3, 0);
printf("Wrote %i measurements to %s\n",Ndata,measfile3);

/* loading data */
int Ndata2;
double **data2 = load_data(&Ndata2, measfile);
double dd = 0.0;
printf("Loaded %i measurements from %s\n",Ndata2,measfile);
for (int i=0; i<Ndata; i++) {
	for (int j=0; j<NMEAS; j++) {
		dd += fabs(data[i][j] - data2[i][j]);
	}
}
int Ndata3;
double **data3 = read_data(&Ndata3, measfile3);
double dd3 = 0.0;
printf("Read %i measurements from %s\n",Ndata3,measfile3);
for (int i=0; i<Ndata; i++) {
	for (int j=0; j<NMEAS; j++) {
		dd += fabs(data[i][j] - data3[i][j]);
	}
}

/* saving multicanonical */
char mucafile[201];
char mucafile2[201];
sprintf(mucafile,"tests/muca.dat");
sprintf(mucafile2,"tests/muca.txt");
write_multicanonical(muca, p, mucafile);
printf("Wrote multicanonical weights to %s\n",mucafile);
save_multicanonical(muca, p, mucafile2);
printf("Saved multicanonical weights to %s\n",mucafile2);

/* loading multicaonical */
multicanonical muca2, muca3;
alloc_multicanonical(&muca2,p);
alloc_multicanonical(&muca3,p);
read_multicanonical(&muca2, p, mucafile, 1);
load_multicanonical(&muca3, p, mucafile2, 1);
double dweights2 = 0.0;
double dweights3 = 0.0;
double dlims2 = 0.0;
double dlims3 = 0.0;
int dG2 = 0;
int dG3 = 0;
printf("Loaded multicanonical weights from %s\n",measfile);

for (int i=0; i<p.OPbins; i++) {
	dweights2 += fabs(muca.weights[i] - muca2.weights[i]);
	dweights3 += fabs(muca.weights[i] - muca3.weights[i]);
	dlims2 += fabs(muca.lims[i] - muca2.lims[i]);
	dlims3 += fabs(muca.lims[i] - muca3.lims[i]);
	dG2 += fabs(muca.Gi[i] - muca2.Gi[i]);
	dG3 += fabs(muca.Gi[i] - muca3.Gi[i]);
}

/* printing test results */
printf("\nTest results:\n");
printf("difference between saved and loaded fields = %g\n", df );
printf("difference between saved and loaded data = %g\n", dd );
printf("difference between writted and read data = %g\n", dd3 );
printf("difference between writted and read lims = %g\n", dlims2 );
printf("difference between writted and loaded lims = %g\n", dlims3 );
printf("difference between writted and read weights = %g\n", dweights2 );
printf("difference between saved and loaded weights = %g\n", dweights3 );
printf("difference between writted and read Gi = %i\n", dG2 );
printf("difference between saved and loaded Gi = %i\n", dG3 );

/* freeing mallocs */
free_fields(&f,p);
free_fields(&g,p);
free_2d_array(data);
free_2d_array(data2);
free_2d_array(data3);
free_multicanonical(&muca,p);
free_multicanonical(&muca2,p);

double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("CPU time taken: %gs\n", total_secs );

return 0;
}
