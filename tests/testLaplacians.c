/* test of laplacians against known results */

#include "common.h"

int main() {

clock_t start_t = clock();

printf("%s\n\n", "#####Testing Laplacians#####");
printf("%s \n", "Test phi: cos(wx*x+f1)*cos(wx*x+f1)*sin(wy*y-0.5*wz*z+f2)");
printf("%s \n", "           + cos(wx*x)");

/* setting parameters for test */
params p;
p.Nx = 64; p.Ny = 64; p.Nz = 64;
p.msq = 1.0;
p.l1 = 1.0;
p.l2 = 1.0;
p.dmsq = 0.0;
p.dl1 = 0.0;
p.dl2 = 0.0;
p.Zphi = 1.0;
p.Zm = 1.0;

/* fields to take laplacians of */
double ***ftrig = make_field(p);

/* doubles to hold results of tests */
double ftrigtestn1 = 0.0;
double ftrigtestn2 = 0.0;

/* some doubles to hold test parameters */
double wx = 3.0*2.0*M_PI/p.Nx;
double wy = 3.0*2.0*M_PI/p.Ny;
double wz = 4.0*2.0*M_PI/p.Nz;
double f1 = 2.0*M_PI*17.0/23.0;
double f2 = 2.0*M_PI*3.0/7.0;

/* showing parameters */
printf("(wx,wy,wz,f1,f2) = (%g,%g,%g,%g,%g)\n", wx,wy,wz,f1,f2);

/* assigning fields */
for(int x=0;x<(p.Nx);x++) {
  for(int y=0;y<(p.Ny);y++) {
  	for(int z=0;z<(p.Nz);z++) {
  			ftrig[x][y][z] = cos(wx*x+f1)*cos(wx*x+f1)
  											*sin(wy*y-0.5*wz*z+f2)
  												+ cos(wx*x);
  	}
  }
}

/* testing laplacians */
double laplacian_exact, err1, err2;
double laplacian_min = 1.0e16;
double max_err1 = 0.0;
double max_err2 = 0.0;
int xmax = 0, ymax = 0, zmax = 0;
for(int x=0;x<(p.Nx);x++) {
  for(int y=0;y<(p.Ny);y++) {
  	for(int z=0;z<(p.Nz);z++) {
  		laplacian_exact = -0.125*sin(wy*y-0.5*wz*z+f2)
  									*(wz*wz + 4.0*wy*wy
  									+ (16.0*wx*wx + 4.0*wy*wy + wz*wz)*cos(2.0*wx*x+2.0*f1))
  									- wx*wx*cos(wx*x);
			err1 = fabs(laplacian_n1(ftrig,p,x,y,z)-laplacian_exact)
											/fabs(laplacian_exact);
			err2 = fabs(laplacian_n2(ftrig,p,x,y,z)-laplacian_exact)
										/fabs(laplacian_exact);
			ftrigtestn1 += err1;
			ftrigtestn2 += err2;
			
			if (fabs(err2)>max_err2) {
				xmax = x;
				ymax = y;
				zmax = z;
				max_err2 = fabs(err2);
			}
			max_err1 = fmax(fabs(err1),max_err1);
			max_err2 = fmax(fabs(err2),max_err2);
			laplacian_min = fmin(fabs(laplacian_exact),laplacian_min);
			if (x==0 && y==0 && z==0) {
				printf("corner point:\n");
				printf("ftrig = %g\n",ftrig[x][y][z]);
				printf("laplacian_n1 = %g\n",laplacian_n1(ftrig,p,x,y,z));
				printf("laplacian_n2 = %g\n",laplacian_n2(ftrig,p,x,y,z));
				printf("laplacian_exact = %g\n",laplacian_exact);
			}
			if (x==(p.Nx-1)/2 && y==(p.Ny-1)/2 && z==(p.Nz-1)/2) {
				printf("middle point:\n");
				printf("ftrig = %g\n",ftrig[x][y][z]);
				printf("laplacian_n1 = %g\n",laplacian_n1(ftrig,p,x,y,z));
				printf("laplacian_n2 = %g\n",laplacian_n2(ftrig,p,x,y,z));
				printf("laplacian_exact = %g\n",laplacian_exact);
			}
  	}
  }
}
/* showing min laplacian, in case zero */
printf("laplacian_min = %g\n", laplacian_min);

/* printing test results */
printf("\nTests for laplacian_n1:\n");
printf(" trig max: %g\n", max_err2 );
printf(" trig avg: %g\n", ftrigtestn1/(p.Nx*p.Ny*p.Nz) );
printf(" 1/Nx: %g\n", 1.0/p.Nx);
printf("Tests for laplacian_n2:\n");
printf(" trig max: %g, at (x,y,z)=(%i,%i,%i)\n", max_err2 ,xmax, ymax, zmax);	
printf("               where laplacian_n2 = %g\n",
																				laplacian_n2(ftrig,p,xmax,ymax,zmax));
printf("               where laplacian_n1 = %g\n",
																				laplacian_n1(ftrig,p,xmax,ymax,zmax));
printf("               where laplacian_exact = %g\n",
										-0.125*sin(wy*ymax-0.5*wz*zmax+f2)
  									*(wz*wz + 4.0*wy*wy
  									+ (16.0*wx*wx + 4.0*wy*wy + wz*wz)*cos(2.0*wx*xmax+2.0*f1))
  									- wx*wx*cos(wx*xmax));			
printf(" trig avg: %g\n", ftrigtestn2/(p.Nx*p.Ny*p.Nz) );
printf(" 1/Nx^2: %g\n", 1.0/p.Nx/p.Nx);

/* freeing fields */
free_field(ftrig,p);

double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("CPU time taken: %gs\n", total_secs );

return 0;
}
