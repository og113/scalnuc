/* test of heatbath update(s) stuff */

#include "common.h"
#include "mersenne.h"

int main() {

clock_t start_t = clock();

printf("%s\n\n", "#####Testing Heatbath#####");

/* variables */
double h, h1, h2;
double op;
int Nmc = 1000;
int acc = 0;
int t;
double t1, t2;
int accepts_metropolis = 0;
int accepts_heatbath = 0;
int Ndofs;

/* setting parameters for test */
params p;
get_params(&p, "params");
show_params(p, stdout);
Ndofs = 2*Nmc*p.Nx*p.Ny*p.Nz;

/* allocating fields */
fields f;
alloc_fields(&f,p);

/* allocating mersenne_inline rng */
unsigned long seedlu = dev_urandom();
seed_mersenne(seedlu);
printf("Allocated MT19937 (Nishimura) generator with seed %lu\n\n",seedlu);

/* multicanonical stuff */
multicanonical muca;
alloc_multicanonical(&muca,p);

/* assigning fields */
for(int x=0;x<(p.Nx);x++) {
  for(int y=0;y<(p.Ny);y++) {
  	for(int z=0;z<(p.Nz);z++) {
  			(f.phi1)[x][y][z] = mersenne();
  			(f.phi2)[x][y][z] = mersenne();
  	}
  }
}

op = order_parameter(f, p);
set_multicanonical(&muca,p, op, acc);

printf("Metropolis evolution:\n");
printf("%32s%32s\n","t","op");
h = hamiltonian(f,p);
t1 = cpu_time();
for (t=0; t<Nmc; t++) {
	if (t%100==0) {
		printf("%32i%32g\n",t,op);
	}
	accepts_metropolis += metropolis(f.phi1, f.phi2, p, &muca, &op,1.0);
	accepts_metropolis += metropolis(f.phi2, f.phi1, p, &muca, &op,1.0);
}
printf("%32i%32g\n",t,op);
t1 = cpu_time() - t1;
h1 = hamiltonian(f,p);

printf("Heatbath evolution:\n");
printf("%32s%32s\n","t","op");
t2 = cpu_time();
for (t=0; t<Nmc; t++) {
	if (t%100==0) {
		printf("%32i%32g\n",t,op);
	}
	accepts_heatbath += heatbath(f.phi1, f.phi2, p, &muca, &op);
	accepts_heatbath += heatbath(f.phi2, f.phi1, p, &muca, &op);
}
printf("%32i%32g\n",t,op);
t2 = cpu_time() - t2;
h2 = hamiltonian(f,p);

/* printing test results */
printf("\nTest results:\n");
printf("%-16s%-16g\n","h",h);
printf("%-16s%-16g\n","h1",h1);
printf("%-16s%-16g\n","h2",h2);
printf("%-16s%-16g\n","(h1-h)/h",(h1-h)/h);
printf("%-16s%-16g\n","(h2-h)/h",(h2-h)/h);
printf("%-16s%-16g\n","t1",t1);
printf("%-16s%-16g\n","t2",t2);
printf("%-32s%-16g\n","acceptance metropolis"
											,accepts_metropolis/(double)(Ndofs));
printf("%-32s%-16g\n","acceptance heatbath"
											,accepts_heatbath/(double)(Ndofs));

/* freeing mallocs */
free_fields(&f,p);
free_multicanonical(&muca,p);

double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("CPU time taken: %gs\n", total_secs );

return 0;
}
