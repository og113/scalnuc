/* test of hamiltonians against known results */

#include "common.h"
#include "cubic.h"

int main() {

clock_t start_t = clock();

printf("%s\n\n", "#####Testing Hamiltonians#####");

/* setting parameters for test */
params p;
p.Nx = 64; p.Ny = 64; p.Nz = 64;
p.msq = 0.1;
p.l1 = 0.25;
p.l2 = 0.25;
p.dmsq = 0.0;
p.dl1 = 0.0;
p.dl2 = 0.0;
p.Zphi = 1.0;
p.Zm = 1.0;
show_params(p,stdout);

/* some doubles to hold test parameters */
double wx = 5.0*2.0*M_PI/(double)p.Nx;
double wy = 5.0*2.0*M_PI/(double)p.Ny;
double wz = 5.0*2.0*M_PI/(double)p.Nz;
double f1 = 17.0/23.0;
double f2 = 5.0/11.0;
double f3 = 3.0/7.0;

/* allocating fields */
fields f, df;
alloc_fields(&f,p);
alloc_fields(&df,p);

/* assigning fields */
for(int x=0;x<(p.Nx);x++) {
  for(int y=0;y<(p.Ny);y++) {
  	for(int z=0;z<(p.Nz);z++) {
  			(f.phi1)[x][y][z] = cos(wx*x+f1)*cos(wy*y+f2)*cos(wz*z+f3);
  			(f.phi2)[x][y][z] = cos(2.0*wx*x)*cos(2.0*wy*y)*cos(2.0*wz*z);
  			(df.phi1)[x][y][z] = cos(wx*x+f1)*cos(wy*y+f2)*cos(wz*z+f3);
  			(df.phi2)[x][y][z] = cos(2.0*wx*x)*cos(2.0*wy*y)*cos(2.0*wz*z);
  	}
  }
}

/* calculating hamiltonians */
double h_n1 = hamiltonian_n1(f,p);
double h_n2 = hamiltonian_n2(f,p);

/* known result */
double hknown = 16.0*(2048.0*p.msq + 375.0*M_PI*M_PI + 72.0*p.l1 + 64.0*p.l2);

double v1 = 1.0;
double v2 = 3.0;
int xc1 = p.Nx/2;
int yc1 = 0;
int zc1 = 0;
int xc2 = 0;
int yc2 = p.Ny/2;
int zc2 = p.Nz-1;
(df.phi1)[xc1][yc1][zc1] += v1;
(df.phi2)[xc2][yc2][zc2] += v2;
double dh_n1 = delta_hamiltonian_n1_phi1(f,p,xc1,yc1,zc1,v1);
dh_n1 += delta_hamiltonian_n1_phi2(f,p,xc2,yc2,zc2,v2);
double dh_n2 = delta_hamiltonian_n2_phi1(f,p,xc1,yc1,zc1,v1);
dh_n2 += delta_hamiltonian_n2_phi2(f,p,xc2,yc2,zc2,v2);
double h_n1_df = hamiltonian_n1(df,p);
double h_n2_df = hamiltonian_n2(df,p);

/* freeing fields */
free_fields(&f,p);
free_fields(&df,p);

/* printing test results */
printf("\nTest results:\n");
printf("h_n1              : %g\n", h_n1 );
printf("h_n2              : %g\n", h_n2 );
printf("known result          : %g\n", hknown );
printf("error h_n1        : %g\n", (hknown-h_n1)/hknown );
printf("error h_n2        : %g\n", (hknown-h_n2)/hknown );
printf("1/Nx                  : %g\n", 1.0/(double)p.Nx );
printf("1/Nx^2                : %g\n\n", 1.0/(double)(p.Nx*p.Nx) );
printf("dh_n1             : %g\n", dh_n1 );
printf("dh_n1_exact       : %g\n", h_n1_df-h_n1 );
printf("error dh_n1       : %g\n", (dh_n1-h_n1_df+h_n1)/(h_n1_df-h_n1) );
printf("dh_n2             : %g\n", dh_n2 );
printf("dh_n2_exact       : %g\n", h_n2_df-h_n2 );
printf("error dh_n2       : %g\n", (dh_n2-h_n2_df+h_n2)/(h_n2_df-h_n2) );

double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("CPU time taken: %gs\n", total_secs );

return 0;
}
