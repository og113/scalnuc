/* test of speed of gsl_polynomial stuff */

#include "common.h"
#include "mersenne.h"
#include <gsl/gsl_poly.h>

int main() {

/* timings */
clock_t start_t = clock();

printf("%s\n\n", "#####Testing GSL Polynomial speed#####");

/* getting seed for random number generator */
long seed = (long)dev_urandom(); /* or (long)time(NULL) */

/* allocating random number generator */
seed_mersenne(seed);
printf("Allocated %s generator with seed %li\n","MT19937 (Nishimura)",seed);

/* some variables */
double cputime1, cputime2;
int Nloops = 1e5, i;
double a3[] = {1.0,2.0,-1.0};
double x3[3];
double a9[] = {1.0, 0.0, 0.7,-0.866667,0.11,-0.12,0.285711,-0.00333333,-0.0111111,-0.00444444};
double z9[18];

gsl_poly_complex_workspace * w10 = gsl_poly_complex_workspace_alloc (10);

printf("Looping %i times\n",Nloops);

cputime1 = cpu_time();
for (i=0; i<Nloops; i++) {
	
	a3[0] += 0.01*mersenne()/(double)Nloops;
	if (gsl_poly_solve_cubic(a3[0],a3[1],a3[2],&x3[0],&x3[1],&x3[2]) != 1) {
		fprintf(stderr,"error: 3 roots found to polynomial");
	}
	
}
cputime1 = cpu_time() - cputime1;
printf("%g %g %g\n",x3[0],x3[1],x3[2]);

cputime2 = cpu_time();
for (i=0; i<Nloops; i++) {
	
	a9[0] += 0.01*mersenne()/(double)Nloops;
	gsl_poly_complex_solve(a9,10,w10,z9);
	
}
cputime2 = cpu_time() - cputime2;
for (i = 0; i < 9; i++) {
    printf ("x9%d = %+.18f %+.18f\n",
            i, z9[2*i], z9[2*i+1]);
}

/* printing test results */
printf("\nTest results:\n");
printf("cubic cputime: %g\n",cputime1);
printf("9th order cputime: %g\n\n",cputime2);
printf("ratio: %g\n\n",cputime2/cputime1);

gsl_poly_complex_workspace_free (w10);


double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("\nCPU time taken: %gs\n", total_secs );

return 0;
}
