/* test of overrelaxation update(s) stuff */

#include "common.h"

int main() {

clock_t start_t = clock();

printf("%s\n\n", "#####Testing Overrelaxation#####");

/* variables */
double h, h1, h2;
double op;
unsigned long seed;
int Nmc = 1000;
int Nevo = 5;
double t1, t2;

/* setting parameters for test */
params p;
get_params(&p, "params");
show_params(p, stdout);

/* allocating fields */
fields f;
alloc_fields(&f,p);

/* allocating random number generator, and setting seed */
seed = dev_urandom(); /* seed for rng */
gsl_rng *r = gsl_rng_alloc( gsl_rng_mt19937 );
if ( r==NULL ) {
	fprintf(stderr,"failed to allocate gsl random number generator\n");
	return 1;
}
gsl_rng_set( r, seed);
printf("Allocated %s generator with seed %li\n",gsl_rng_name(r),seed);

/* multicanonical stuff */
multicanonical muca;
alloc_multicanonical(&muca,p);
zero_multicanonical(&muca,p);

/* assigning fields */
for(int x=0;x<(p.Nx);x++) {
  for(int y=0;y<(p.Ny);y++) {
  	for(int z=0;z<(p.Nz);z++) {
  			(f.phi1)[x][y][z] = gsl_rng_uniform(r);
  			(f.phi2)[x][y][z] = gsl_rng_uniform(r);
  	}
  }
}

printf("Testing energy conservation 1:\n");
h = hamiltonian(f,p);
t1 = cpu_time();
for (int t=0; t<Nmc; t++) {
	overrelaxation(f.phi1, f.phi2, p, r, muca, &op);
	overrelaxation(f.phi2, f.phi1, p, r, muca, &op);
}
t1 = cpu_time() - t1;
h1 = hamiltonian(f,p);

printf("Testing energy conservation 2:\n");
t2 = cpu_time();
for (int t=0; t<Nmc; t++) {
	overrelaxation2(f.phi1, f.phi2, p, r, muca, &op);
	overrelaxation2(f.phi2, f.phi1, p, r, muca, &op);
}
t2 = cpu_time() - t2;
h2 = hamiltonian(f,p);

printf("\nEvolution of order parameter without switching:\n");
printf("%-16i%-16g\n",0,order_parameter(f,p));
for (int t=0; t<Nevo; t++) {
	overrelaxation(f.phi1, f.phi2, p, r, muca, &op);
	printf("%-16i%-16g\n",2*t+1,order_parameter(f,p));
	overrelaxation(f.phi1, f.phi2, p, r, muca, &op);
	printf("%-16i%-16g\n",2*t+2,order_parameter(f,p));
}

printf("\nEvolution of order parameter with switching:\n");
printf("%-16i%-16g\n",0,order_parameter(f,p));
for (int t=0; t<Nevo; t++) {
	overrelaxation(f.phi1, f.phi2, p, r, muca, &op);
	printf("%-16i%-16g\n",2*t+1,order_parameter(f,p));
	overrelaxation(f.phi2, f.phi1, p, r, muca, &op);
	printf("%-16i%-16g\n",2*t+2,order_parameter(f,p));
}

/* printing test results */
printf("\nTest results:\n");
printf("%-16s%-16g\n","h",h);
printf("%-16s%-16g\n","h1",h1);
printf("%-16s%-16g\n","h2",h2);
printf("%-16s%-16g\n","(h1-h)/h",(h1-h)/h);
printf("%-16s%-16g\n","(h2-h)/h",(h2-h)/h);
printf("%-16s%-16g\n","t1",t1);
printf("%-16s%-16g\n","t2",t2);

/* freeing mallocs */
free_fields(&f,p);
free_multicanonical(&muca,p);

double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("CPU time taken: %gs\n", total_secs );

return 0;
}
