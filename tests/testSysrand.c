/** test of sysrand.c */

#include "common.h"

int main() {

/** starting clock */
clock_t start_t = clock();

printf("%s\n\n", "#####Testing Sysrand #####");

int N = 20;
unsigned long int s;

printf("Doing %i successive calls to /dev/urandom/:\n", N);

/** calling /dev/urandom/ */
for (int i=0; i<N; i++) {
	s = dev_urandom();
	printf("%lu\n",s);
}

/** finishing up */
double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("CPU time taken: %gs\n", total_secs );

return 0;
}
