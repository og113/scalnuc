/* test of an interesting iterator of kari's */

#include "common.h"

int main() {

clock_t start_t = clock();

printf("%s\n\n", "#####Testing some iterator stuff #####");

double mc_max = 10.0;
double mc_min = 0.0;
int Nbins = 5;
double dmc = (mc_max - mc_min)/(Nbins-1.0);
double mc_lim[Nbins];

printf("Nbins = %i,\nmc_min = %g,\nmc_max = %g.\n\n",Nbins,mc_min,mc_max);

int i;

for (i = 0; i<Nbins; i++) {
	mc_lim[i] = mc_min + i*dmc;
}

double mc_new = mc_min + 1.0*(mc_max - mc_min);
int i_mc;

for (i_mc = 0; i_mc<Nbins; i_mc ++) {
	for (i=i_mc; mc_new > mc_lim[i]; i++) {printf("i = %i, ",i);}
	printf("\n");
	for ( ; mc_new < mc_lim[i]; i--) {printf("i = %i, ",i);}
	printf("\n");

	printf("intial guess %i, final answer %i\n",i_mc,i);
	printf("mc_lim[i] = %g, mc_new = %g, mc_lim[i+1] = %g\n\n",
																									mc_lim[i],mc_new,mc_lim[i+1]);
}

double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("CPU time taken: %gs\n", total_secs );

return 0;
}
