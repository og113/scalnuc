/* test of some RNGs, for speed */

#include "common.h"
#include "mersenne.h"
#include <gsl/gsl_randist.h>

int main() {

clock_t start_t = clock();

printf("%s\n\n", "#####Testing some RNGs #####");

double c;
double cputime1, cputime2, cputime3;
double cputime4, cputime5;
unsigned long seedlu = dev_urandom();
int i, Ncalls;
Ncalls = 1e8;

/* allocating mersenne_inline rng */
seed_mersenne(seedlu);
printf("Allocated MT19937 (Nishimura) generator with seed %lu\n\n",seedlu);

/* allocating gsl random number generators */
gsl_rng *rmt = gsl_rng_alloc( gsl_rng_mt19937 );
gsl_rng *rmrg = gsl_rng_alloc( gsl_rng_mrg );
gsl_rng_set( rmt, seedlu);
gsl_rng_set( rmrg, seedlu);
printf("Allocated %s generator with seed %lu\n",gsl_rng_name(rmt),seedlu);
printf("Allocated %s generator with seed %lu\n",gsl_rng_name(rmrg),seedlu);

printf("Ncalls = %i\n\n",Ncalls);

cputime1 = cpu_time();
for (i = 0; i<Ncalls; i++) {
	c = mersenne();
}
printf ("c = %g\n",c);
cputime1 = cpu_time() - cputime1;

cputime2 = cpu_time();
for (i = 0; i<Ncalls; i++) {
	c = gsl_rng_uniform(rmt);
}
printf ("c = %g\n",c);
cputime2 = cpu_time() - cputime2;

cputime3 = cpu_time();
for (i = 0; i<Ncalls; i++) {
	c = gsl_rng_uniform(rmrg);
}
printf ("c = %g\n",c);
cputime3 = cpu_time() - cputime3;

cputime4 = cpu_time();
for (i = 0; i<Ncalls; i++) {
	c = gaussian_ran();
}
printf ("c = %g\n",c);
cputime4 = cpu_time() - cputime4;

cputime5 = cpu_time();
for (i = 0; i<Ncalls; i++) {
	c = gsl_ran_gaussian_ziggurat(rmt,1.0);
}
printf ("c = %g\n",c);
cputime5 = cpu_time() - cputime5;

printf("\nTest results:\n");
printf("cpu time for MT19937 (Nishimura): %g\n",cputime1);
printf("cpu time for %s: %g\n",gsl_rng_name(rmt),cputime2);
printf("cpu time for %s: %g\n",gsl_rng_name(rmrg),cputime3);
printf("cpu time for Gaussian MT19937 (Nishimura): %g\n",cputime4);
printf("cpu time for Gaussian %s: %g\n",gsl_rng_name(rmt),cputime5);

gsl_rng_free(rmt);
gsl_rng_free(rmrg);
printf("Freed random number generators\n");


double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("CPU time taken: %gs\n", total_secs );

return 0;
}
