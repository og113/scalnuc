/* test of params.c */

#include "common.h"

int main() {

clock_t start_t = clock();

printf("%s\n\n", "#####Testing Parameters#####");

printf("Parameters set from within code:\n");

/* setting parameters struct p1 */
params p;
p.Nx = 64; p.Ny = 64; p.Nz = 64;
p.msq = 0.1;
p.l1 = 0.25;
p.l2 = 0.25;
p.Tmc = 0;
p.psteps = 10;
p.pstep_id = msq_id;
p.logstep = 1;
p.p_incr = 0.9;
show_params(p,stdout);

/* saving parameters p */
char pfile[] = "tmp/params";
printf("Saving parameters to %s\n",pfile);
print_params(p,pfile);

/* getting parameters p */
params p_new;
printf("Loading parameters from %s\n",pfile);
get_params(&p_new,pfile);

/* showing resulting params */
show_params(p_new,stdout);

/* getting parameters struct p2 */
params p2;
char pfile2[] = "tmp/params2";
printf("Loading parameters from %s\n",pfile2);
get_params(&p2,pfile2);
show_params(p2,stdout);

/* finally saving parameters struct p2 */
char pfile1[] = "tmp/params1";
printf("Saving parameters to %s\n",pfile1);
print_params(p2,pfile1);

double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("CPU time taken: %gs\n", total_secs );

return 0;
}
