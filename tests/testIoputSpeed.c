/* test of speed of input/output stuff */

#include "common.h"

int main() {

/* timings */
clock_t start_t = clock();

printf("%s\n\n", "#####Testing IOput speed#####");

/* setting parameters for test */
params p;
get_params(&p, "params0");
show_params(p, stdout);

/* some variables */
double cputime1, cputime2;
int Nloops = 1e3, i;
int acc = 0;
double op = 0.4;
double wide_bins = 0.05;
char mucafile[201];
char mucafile2[201];
char mucafile3[201];

/* multicanonical stuff */
if (!p.Umuca) {
	fprintf(stderr,"Need p.Umuca!=0\n");
	return -1;
}
multicanonical muca, muca2, muca3;
alloc_multicanonical(&muca,p);
alloc_multicanonical(&muca2,p);
alloc_multicanonical(&muca3,p);
set_multicanonical(&muca, p, op, acc, wide_bins);
printf("\nAllocated multicanonical\n");

/* multicanonical files */
sprintf(mucafile,"tests/weights.txt");
sprintf(mucafile2,"tests/muca.dat");
sprintf(mucafile3,"tests/muca.txt");
printf("\nInput file: %s\n",mucafile);
printf("Binary file: %s\n",mucafile2);
printf("Ascii file: %s\n\n",mucafile3);

/*load_multicanonical(&muca, p, mucafile2,acc);*/

printf("Looping %i times\n",Nloops);

cputime1 = cpu_time();
for (i=0; i<Nloops; i++) {
	write_multicanonical(muca, p, mucafile2);
	read_multicanonical(&muca2, p, mucafile2,acc);
}
cputime1 = cpu_time() - cputime1;

cputime2 = cpu_time();
for (i=0; i<Nloops; i++) {
	save_multicanonical(muca, p, mucafile3);
	load_multicanonical(&muca3, p, mucafile3,acc);
}
cputime2 = cpu_time() - cputime2;

/* printing test results */
printf("\nTest results:\n");
printf("Read/write cputime: %g\n",cputime1);
printf("Save/load cputime: %g\n\n",cputime2);


/* freeing mallocs */
free_multicanonical(&muca,p);
free_multicanonical(&muca2,p);
free_multicanonical(&muca3,p);

double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("\nCPU time taken: %gs\n", total_secs );

return 0;
}
