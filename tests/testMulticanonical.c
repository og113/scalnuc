/* test of multicanonical stuff */

#include "common.h"

int main() {

clock_t start_t = clock();

printf("%s\n\n", "#####Testing Multicanonical#####");

/* setting parameters for test */
params p;
get_params(&p, "params");
show_params(p,stdout);

/* allocating fields */
fields f;
alloc_fields(&f,p);

/* allocating random number generator, and setting seed */
unsigned long seed = dev_urandom(); /* seed for rng */
gsl_rng *r = gsl_rng_alloc( gsl_rng_mt19937 );
if ( r==NULL ) {
	fprintf(stderr,"failed to allocate gsl random number generator\n");
	return 1;
}
gsl_rng_set( r, seed);
printf("Allocated %s generator with seed %li\n",gsl_rng_name(r),seed);

/* measurements arrays */
long Ndata = p.Nmc/p.Tmeas;
double **data = make_data_array(Ndata);
zero_data_array(data,Ndata);
printf("Allocated data array\n");

/* multicanonical stuff */
multicanonical muca;
alloc_multicanonical(&muca,p);
zero_multicanonical(&muca,p);
if (p.Tmuca>0)
	  printf("Allocated multicanonical\n");
	  
/* choosing initial multicanonical weights based on mfile */
char minfile[201];
if (strcmp(p.mfile,"auto")==0) {
	sprintf(minfile,
	"data/temp/muca_msq_%g_l1_%g_l2_%g_Nx_%i_Ny_%i_Nz_%i_OPbins_%i_OPmax_%g.dat"
												,p.msq,p.l1,p.l2,p.Nx,p.Ny,p.Nz,p.OPbins,p.OPmax);		
}
else {
	strncpy(minfile,p.mfile,201);
}

if (file_exists(minfile)) {
	read_multicanonical(&muca,p,minfile);
	printf("Loaded multicanonical from:\n%s\n\n",minfile);
}

/* assigning fields */
for(int x=0;x<(p.Nx);x++) {
  for(int y=0;y<(p.Ny);y++) {
  	for(int z=0;z<(p.Nz);z++) {
  			(f.phi1)[x][y][z] = gsl_rng_uniform(r);
  			(f.phi2)[x][y][z] = gsl_rng_uniform(r);
  	}
  }
}

/* doing  monte-carlo sweeps */
printf("Monte-carlo sweeps:\n");
double op = order_parameter(f,p);
int tmeas, tmuca;
for (int t=0; t<p.Nmc; t++) {
	
	metropolis(f, p, r, muca, &op, 1, 0.5);
	metropolis_global(f, p, r, muca, &op, 0.001);
	overrelaxation(f, p, r, muca, &op);
	
	/* doing measurements */
	if (t%p.Tmeas == 0) {
		tmeas = t/p.Tmeas;
		get_measurements(data[tmeas],f,p);
		/* updating multicanonical weights */
		if (p.Tmuca>0 && t>0 && t%(p.Tmeas*p.Tmuca) == 0) {
			tmuca = t/p.Tmeas/p.Tmuca - 1;
			update_multicanonical(&muca, data, tmuca, 0.0, p);
		}
	}	
}
printf("\n");

/* saving data */
char measfile[201];
sprintf(measfile,"tests/meas.txt");
save_data(data, Ndata, measfile, 0);
printf("Saved %li measurements to %s\n",Ndata,measfile);

/* saving multicanonical */
char mucafile[201];
sprintf(mucafile,
"data/temp/muca_msq_%g_l1_%g_l2_%g_Nx_%i_Ny_%i_Nz_%i_OPbins_%i_OPmax_%g.dat"
												,p.msq,p.l1,p.l2,p.Nx,p.Ny,p.Nz,p.OPbins,p.OPmax);	
write_multicanonical(muca, p, mucafile);
printf("Wrote multicanonical weights to %s\n\n",mucafile);

/* printing multicanonical */
for (int j=0; j<p.OPbins; j++) {
	printf("weight[%i] = %g\n",j,muca.weights[j]);
}

/* printing test results */
printf("\nTest results:\n");


/* freeing mallocs */
free_fields(&f,p);
free_2d_array(data);
free_multicanonical(&muca,p);


double total_secs = (double)(clock() - start_t) / CLOCKS_PER_SEC;
printf("CPU time taken: %gs\n", total_secs );

return 0;
}
