/** @file singlet.h
 * Functions specific to the Singlet Scalar Model
 */
#ifndef __SINGLET_H_INCLUDED__
#define __SINGLET_H_INCLUDED__

#include <gsl/gsl_poly.h> // gsl polynomial stuff, for solving polynomials
#include "common.h"

/** number of fields */
#define NFIELDS 1

/** total number of couplings (excluding derived params) */
#define NCOUPLINGS 5

/** number of different measurements */
#define NMEAS_FIELD_AVG 5
#define NMEAS_FIELD_GLOBAL 3
#define NMEAS_MOM_AVG 2
#define NMEAS_MOM_GLOBAL 1
#define NMEAS_TOT (NMEAS_FIELD_AVG + NMEAS_FIELD_GLOBAL + NMEAS_MOM_AVG + \
                   NMEAS_MOM_GLOBAL)

/** columns for each measurement
 *     assumes w, op and h at start */
#define i_1     0   /**< \f$\phi\f$ */
#define i_2     1   /**< \f$\phi^2\f$ */
#define i_3     2   /**< \f$\phi^3\f$ */
#define i_4     3   /**< \f$\phi^4\f$ */
#define i_l     4   /**< \f$\phi*laplacian(\phi)\f$) */

/** columns for standard measurements
 *     w, op and h at start */
#define i_h    5    /**< hamiltonian */
#define i_op   6    /**< order parameter (op) */
#define i_w    7    /**< weight */

#define i_m    8    /**< \f$\m\f$ */
#define i_m2   9    /**< \f$\m^2\f$ */
#define i_e    10   /**< energy */

/** numbers for montecarlo algorims */
#define NALG_MET    1   /**< number of local metropolis updates per sweep */
#define NALG_GLOBAL 0   /**< number of global metropolis updates per sweep */
#define NALG_OVER   4   /**< number of local overrelaxation updates per sweep */
#if defined(IMPROVED) || defined(IMPROVED_OA2)
// O(a^2) improved
#define NCOLOURS 4  /** < how many colours in the checkerboard stencil */
#else
// either unimproved or O(a) improved
#define NCOLOURS 2  /** < how many colours in the checkerboard stencil */
#endif

/** couplings struct for the singlet anisotropy model */
typedef struct {
    /* couplings of theory, all in units with b4=1 */
    double b1;     /**< MSbar renormalised linear coupling */
    double b2;     /**< MSbar renormalised quadratic coupling (mass) */
    double b3;     /**< MSbar renormalised cubic coupling */
    double a;      /**< lattice spacings */
    double mu3;    /**< renormalisation scale */

    /* multiplicative and additive renormalisation couplings (derived) */
    double db1;    /**< additive renormalisation for \f$b_1\f$ */
    double db2;    /**< additive renormalisation for \f$b_2\f$ */
    double db3;    /**< additive renormalisation for \f$b_3\f$ */
    double db4;    /**< additive renormalisation for \f$b_4\f$ */
    double Zm;     /**< multiplicative renormalisation for \f$b_2\f$ */
    double Zphi;   /**< multiplicative renormalisation for \f$\phi^2\f$ */
    double c42;    /**< \f$\phi^4\f$ + constant * \f$\phi^2\f$*/
    double c44;    /**< \f$\phi^4\f$ + constant * \f$\phi^1\f$*/
    double c4lap;  /**< \f$\phi^4\f$ + constant * \f$\phi^2\f$*/

    /* coefficients of terms in the action */
    double betalap;  /**< coefficient of laplacian in action */
    double beta1;    /**< coefficient of \f$\phi\f$ in action */
    double beta2;    /**< coefficient of \f$\phi^2\f$ in action */
    double beta3;    /**< coefficient of \f$\phi^3\f$ in action */
    double beta4;    /**< coefficient of \f$\phi^4\f$ in action */
    double betamom;  /**< coefficient of momentum^2 in energy */

} couplings;

/** fields struct for the singlet anisotropy model */
typedef struct {
    double ***phi;   /**< field component */
} fields;

/** rk_fields are used for the RK4 update, uses the fields struct */
typedef struct {
	fields k1;
	fields l1;
	fields k2;
	fields l2;
	fields k3;
	fields l3;
	fields k4;
	fields l4;
} rk_fields_array;

/*
 *    singlet_alloc.c
 */
void alloc_fields(fields *f, params p);
void free_fields(fields *f, params p);
void zero_fields(fields *f, params p);
void set_random_fields(fields *f, params p, gsl_rng* r, double hotness);
void copy_fields(fields *fin, fields *fout, params p);
void init_mom(fields mom, params p, couplings c, gsl_rng *r);
/*
 *    singlet_couplings.c
 */
void show_couplings(couplings c, FILE* stream);
void print_couplings(couplings c, char *file);
void write_couplings(couplings c, FILE * fs);
void read_couplings(couplings *c, FILE * fs);
void get_couplings(couplings *c, char *file);
int check_couplings(couplings c);
void show_counterterms(couplings c, FILE* stream);
void print_counterterms(couplings c, char *file);
void show_betas(couplings c, FILE* stream);
int compare_couplings(couplings c, couplings d);
void set_betas_to_zero(couplings *p);

/*
 *   singlet_ioput.c
 */
void write_fields(fields f, params p, char *file);
void read_fields(fields *f, params p, char *file);
void write_many_fields(fields f, params p, char *file);

/*
 *    singlet_measure.c
 */
void get_measurements (double* data, fields f, params p, couplings c);
void get_measurements_mom(double *data, fields m, fields f,
                          params p, couplings c);
void show_measurement_labels(FILE* stream);
void print_measurement_labels(char *file);


/*
 *    singlet_metropolis.c
 */
int metropolis(fields f, params p, couplings c, gsl_rng* r,
               multicanonical *muca, double *op, double sigma);
int metropolis_global_radial(fields f, params p, couplings c, gsl_rng* r,
                             multicanonical *muca, double *op,
                             int Nlocal, double epsilon);
int metropolis_global(fields f, params p, couplings c, gsl_rng* r,
                      multicanonical *muca, double *op,
                      int Nglobal, double epsilon);


/*
 *   singlet_overrelax.c
 */
int overrelaxation(fields f, params p, couplings c,
                   gsl_rng* r, multicanonical *muca, double *op);


/*
 *    singlet_order_parameter.c
 */
double order_parameter_local(fields f, int x, int y, int z,
                             params *p, couplings *c);
double delta_order_parameter (double*** phi, params *p, couplings *c,
                              int x, int y, int z, double dphi);


/*
 *    singlet_theory.c
 */
double local_kinetic (double*** phi, params *p, couplings *c,
                      int x, int y, int z);
double local_potential (double phi, couplings *c);
double local_linear_potential(double phi, couplings *c);
double local_quadratic_potential (double phi, couplings *c);
double local_cubic_potential(double phi, couplings *c);
double local_quartic_potential(double phi, couplings *c);
double hamiltonian (fields f, params p, couplings c);
double delta_hamiltonian(double*** phi, params *p, couplings *c,
                         int x, int y, int z, double dphi);
double d_hamiltonian_d_phi(double ***phi, params *p, couplings *c,
                           int x, int y, int z);
double partial_mom(double*** mom, double*** phi, params *p,
                   couplings *c, int x, int y, int z);
double momentum_energy(fields f, params p, couplings c);
double energy(fields f, fields m, params p, couplings c);


/*
 *    singlet_time.c
 */
/*void time_evolution_euler(fields m, fields f, params p, couplings c,
                            gsl_rng *r, fields tmp);*/
void leap_mom_field(fields f, fields m, params p, couplings c, gsl_rng *r);
void leap_field(fields f, fields m, params p, couplings c);
void time_evolution_leapfrog(fields f, fields m, params p, couplings c,
                             gsl_rng *r);
void time_evolution_leapfrog_reverse(fields f, fields m, params p, couplings c,
                                     gsl_rng *r);
void mom_half_step(fields f, fields m, params p,  couplings c, gsl_rng *r);
void mom_reverse(fields m, params p);
void mom_refresh(fields m, params p, couplings c, gsl_rng *r);
void kick_drift_kick(fields f, fields m, params p,  couplings c, gsl_rng *r);
void move_both_4(fields f, fields m, params p, couplings c, gsl_rng *r);
void alloc_runge_kutta(rk_fields_array *rk_fields, params p);
void free_runge_kutta(rk_fields_array *rk_fields, params p);
void runge_kutta_iter(fields f, fields m, fields k, fields l, params p, couplings c, gsl_rng *r);
void update_rk_fields(fields f, fields m, fields f_init, fields m_init, fields k, fields l, params p);
void runge_kutta(fields f, fields m, fields f_init, fields m_init, rk_fields_array rk_fields, params p, couplings c, gsl_rng *r);


/*
 *    montecarlo_algorithm.c
 */
void montecarlo_set_zeros();
void montecarlo_algorithm(fields f, params p, couplings c, gsl_rng* r,
                          multicanonical *muca, double *op,
                          double sigma, double epsilon);
void montecarlo_set_scales(int steps, int dofs, int set_sigma, double *sigma,
                           int set_eps, double *epsilon);
void montecarlo_show_times(int steps, int dofs);

/*
 *    order_parameter.c
 */
double order_parameter_meas(double *datum, params p, couplings c);
double order_parameter(fields f, params p, couplings c);
void scale_field_to_order_parameter(fields *f, params p, couplings c);
void save_local_order_parameter(fields f, params p, couplings c, char *file);

#endif //__SINGLET_H_INCLUDED__
