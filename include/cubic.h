/** @file cubic.h
 * Functions specific to the Cubic Anisotropy Model
 */
#ifndef __CUBIC_H_INCLUDED__
#define __CUBIC_H_INCLUDED__

/** number of fields */
#define NFIELDS 2

/** improved laplacian or not */
//#define IMPROVED

/** total number of couplings (excluding derived params) */
#define NCOUPLINGS 3

/** number of different measurements */
#define NMEAS_FIELD_AVG 9
#define NMEAS_FIELD_GLOBAL 3
#define NMEAS_MOM_AVG 4
#define NMEAS_MOM_GLOBAL 1
#define NMEAS_TOT (NMEAS_FIELD_AVG + NMEAS_FIELD_GLOBAL + NMEAS_MOM_AVG + \
                   NMEAS_MOM_GLOBAL)

/** columns for each measurement
 *     assumes w, op and h at start */
#define i_1         0        /**< \f$\phi_1\f$ */
#define i_2         1        /**< \f$\phi_2\f$ */
#define i_1sq       2        /**< \f$\phi_1^2\f$ */
#define i_2sq       3        /**< \f$\phi_2^2\f$ */
#define i_1qu       4        /**< \f$\phi_1^4\f$ */
#define i_2qu       5        /**< \f$\phi_2^4\f$ */
#define i_1sq2sq    6        /**< \f$\phi_1^2\phi_2^2\f$ */
#define i_l1        7        /**< \f$\phi_1laplacian(\phi_2)\f$) */
#define i_l2        8        /**< \f$\phi_2*laplacian(\phi_2)\f$ */

/** columns for standard measurements
 *     w, op and h at start */
#define i_h         9        /**< hamiltonian */
#define i_op        10       /**< order parameter (op) */
#define i_w         11       /**< weight */

#define i_m1        12       /**< \f$\m_1\f$ */
#define i_m2        13       /**< \f$\m_2\f$ */
#define i_m1sq      14       /**< \f$\m_1^2\f$ */
#define i_m2sq      15       /**< \f$\m_2^2\f$ */
#define i_e         16       /**< energy */

/** numbers for montecarlo algorims */
#define NALG_MET  1    /**< number of local metropolis updates per sweep */
#define NALG_GLOBAL 0  /**< number of global metropolis updates per sweep */
#define NALG_OVER 4    /**< number of local overrelaxation updates per sweep */

/** couplings struct for the cubic anisotropy model */
typedef struct {
    /* couplings of theory, all in lattice units */
    double msq;         /**< mass squared*/
    double l1;          /**< \f$\lambda_1\f$ */
    double l2;          /**< \f$\lambda_2\f$ */

    /* multiplicative and additive renormalisation couplings (derived) */
    double Zphi;        /**< multiplicative renormalisation for \f$\phi^2\f$ */
    double Zm;          /**< multiplicative renormalisation for \f$m^2\f$ */
    double dmsq;        /**< additive renormalisation for \f$m^2\f$ */
    double dl1;         /**< additive renormalisation for \f$\lambda_1\f$ */
    double dl2;         /**< additive renormalisation for \f$\lambda_2\f$ */
    double dphisq;      /**< additive renormalisation for measurements of
                         * \f$\phi^2\f$ */

} couplings;

/** fields struct for the cubic anisotropy model */
typedef struct {
    double ***phi1;     /**< field component 1 */
    double ***phi2;     /**< field component 2 */
} fields;

/** rk_fields are used for the RK4 update, uses the fields struct */
typedef struct {
    fields k1;
    fields l1;
    fields k2;
    fields l2;
    fields k3;
    fields l3;
    fields k4;
    fields l4;
} rk_fields_array;

/*
 *    cubic_alloc.c
 */
void alloc_fields(fields *f, params p);
void free_fields(fields *f, params p);
void zero_fields(fields *f, params p);
void set_random_fields(fields *f, params p, gsl_rng* r, double hotness);
void copy_fields(fields *fin, fields *fout, params p);
void init_mom(fields mom, params p, couplings c, gsl_rng *r);

/*
 *    cubic_couplings.c
 */
void show_couplings(couplings c, FILE* stream);
void print_couplings(couplings c, char *file);
void write_couplings(couplings c, FILE * fs);
void read_couplings(couplings *c, FILE * fs);
void get_couplings(couplings *c, char *file);
int check_couplings(couplings c);
void show_counterterms(couplings c, FILE* stream);
void print_counterterms(couplings c, char *file);
void show_betas(couplings c, FILE* stream);
int compare_couplings(couplings c, couplings d);


/*
 *   cubic_ioput.c
 */
void write_fields(fields f, params p, char *file);
void write_many_fields(fields f, params p, char *file);
void read_fields(fields *f, params p, char *file);


/*
 *    cubic_measure.c
 */
void get_measurements (double* data, fields f, params p, couplings c);
void get_measurements_mom(double *data, fields m, fields f, params p,
                          couplings c);
void show_measurement_labels(FILE* stream);
void print_measurement_labels(char *file);


/*
 *    cubic_metropolis.c
 */
int metropolis(fields f, params p, couplings c, gsl_rng* r,
               multicanonical *muca, double *op, double sigma);
int metropolis_global_radial(fields f, params p, couplings c, gsl_rng* r,
                             multicanonical *muca, double *op, int Nlocal,
                             double epsilon);
int metropolis_global(fields f, params p, couplings c, gsl_rng* r,
                      multicanonical *muca, double *op, int Nglobal,
                      double epsilon);


/*
 *    cubic_order_parameter.c
 */
double order_parameter_local(fields f, int x, int y, int z, params *p,
                             couplings *c);
double delta_order_parameter(double*** phi, params *p, couplings *c, int x,
                             int y, int z, double dphi);


/*
 *    cubic_overrelaxation.c
 */
int overrelaxation(fields f, params p, couplings c, gsl_rng* r,
                   multicanonical *muca, double *op);


/*
 *    cubic_theory.c
 */
double local_kinetic_1 (double*** phi, params *p, couplings *c, int x, int y,
                        int z);
double local_potential (double phi1, double phi2, couplings *c);
double local_quadratic_potential_1 (double phi, couplings *c);
double local_quartic_potential_1 (double phi, couplings *c);
double local_quartic_potential_2 (double phi1, double phi2, couplings *c);
double local_quadratic_potential (double phi1, double phi2, couplings *c);
double local_quartic_potential (double phi1, double phi2, couplings *c);
double hamiltonian (fields f, params p, couplings c);
double delta_hamiltonian(double*** phiA, double*** phiB, params *p,
                         couplings *c, int x, int y, int z, double dphiA);
double functional_hamiltonian(double*** phiA, double*** phiB, params *p,
                              couplings *c, int x, int y, int z);
double partial_mom(double*** momA, double*** phiA, double*** phiB, params *p,
                   couplings *c, int x, int y, int z);
double momentum_energy(fields f, params p, couplings c);
double energy(fields f, fields m, params p, couplings c);


/*
 *    cubic_time.c
 */
void time_evolution_euler(fields f, fields m, params p, couplings c, gsl_rng *r,
                          fields tmp);
void leap_mom_field(fields f, fields m, params p, couplings c, gsl_rng *r);
void leap_field(fields f, fields m, params p, couplings c);
void time_evolution_leapfrog(fields f, fields m, params p, couplings c,
                             gsl_rng *r);
void time_evolution_leapfrog_reverse(fields f, fields m, params p, couplings c,
                                     gsl_rng *r);
void mom_half_step(fields f, fields m, params p, couplings c, gsl_rng *r);
void field_half_step(fields f, fields m, params p,  couplings c);
void kick_drift_kick(fields f, fields m, params p,  couplings c, gsl_rng *r);
void move_both_4(fields f, fields m, params p, couplings c, gsl_rng *r);
void mom_refresh(fields m, params p, couplings c, gsl_rng *r);
void mom_reverse(fields m, params p);
void runge_kutta(fields f, fields m, fields f_init, fields m_init,
                 rk_fields_array rk_fields, params p, couplings c, gsl_rng *r);
void runge_kutta_iter(fields f, fields m, fields k, fields l, params p,
                      couplings c, gsl_rng *r);
void alloc_runge_kutta(rk_fields_array *rk_fields, params p);
void free_runge_kutta(rk_fields_array *rk_fields, params p);

/*
 *    montecarlo_algorithm.c
 */
void montecarlo_set_zeros();
void montecarlo_algorithm(fields f, params p, couplings c, gsl_rng* r,
                          multicanonical *muca, double *op, double sigma,
                          double epsilon);
void montecarlo_set_scales(int steps, int dofs, int set_sigma, double *sigma,
                           int set_eps, double *epsilon);
void montecarlo_show_times(int steps, int dofs);

/*
 *    order_parameter.c
 */
double order_parameter_meas(double *datum, params p, couplings c);
double order_parameter(fields f, params p, couplings c);
void scale_field_to_order_parameter(fields *f, params p, couplings c);
void save_local_order_parameter(fields f, params p, couplings c, char *file);


#endif //__CUBIC_H_INCLUDED__
