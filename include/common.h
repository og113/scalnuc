/** @file common.h
 *    Common header for scalar nucleation program, scalnuc
 */
#ifndef __COMMON_H_INCLUDED__
#define __COMMON_H_INCLUDED__

#include <float.h> // DBL_EPSILON, DBL_DIG ...
#include <glob.h> // for the file_exists function
#include <gsl/gsl_errno.h> // gsl error codes (and handling)
#include <gsl/gsl_rng.h> // gsl random number generators
#include <gsl/gsl_randist.h> // gsl random number distributions
/*#include <gsl/gsl_poly.h> // gsl polynomial stuff, for solving polynomials */
#include <math.h> // pow, log, exp, cos, sqrt, ...
/*#include <omp.h> // OpenMP */
/*#include <stdarg.h> // va_list, ... (variable arguments) */
#include <stdio.h> // printf, fprintf, sprintf, fwrite...
#include <stdlib.h> // atof, strtod, rand, malloc, free, exit, ...
#include <string.h> // memcpy, strcat, memcmp, strcmp, memset, strstr...
#include <sys/types.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/resource.h>
#include <time.h> // time_t, struct tm, difftime, time, mktime
#include <unistd.h> // access
/*#include <unistd.h> // system call wrapper functions */

/** file names */
#define couplings_file "couplings"
#define params_file "params"
#define status_file "status"
#define rng_file    "rng_state"
#define fields_file "fields"
#define data_file   "data"
#define vis_file "vis"
//#define data_file_b    "backwards_data"
//#define data_file_f    "forwards_data"
#define weight_file "weight"
#define cts_file    "counterterms"
#define labels_file "labels"
#define strix_file  "strix"
#define points_file "points"
#define op_file     "op_lattice"

/** spatial dimension */
#define DIM 3

/** total number of parameters (excluding derived params) */
#define NPARAMS 19

/** Number of extrema and midpoints */
#define NPOINTS 5

/** numbers of double and int parameters
 * for ioput (excluding also run params) */
#define NPARAMSD 0
#define NPARAMSI 3

/** length of char arrays (to avoid malloc malarkey), S for short */
#define NSTRSS  21
#define NSTRS   81
#define NSTR    256
#define NSTRL   1024

/** cube root of 2 and 3 */
#define CUBR2 1.25992104989487316476
#define CUBR3 1.44224957030740838232



/** parameters struct */
typedef struct {
    /* sizes of dimensions
     * for speed, consider defining these so that the compiler knows
     *  them, and making them a power of 2 */
    int Nx;             /**< lattice points in x direction */
    int Ny;             /**< lattice points in y direction */
    int Nz;             /**< lattice points in z direction */

    /* config parametrs of run */
    long unsigned seed; /**< rng seed, default 0 for random seed */
    int Nth;            /**< prethermalisation timesteps */
    int Nmc;            /**< monte carlo timesteps */
    int Tmeas;          /**< measurement period, in monte-carlo timesteps */
    int Tcheck;         /**< checkpoint period, in units where Tmeas=1 */

    /* multicanonical parameters */
    int Umuca;          /**< nonzero if updating multicanonical */
    int OPbins;         /**< number of bins for order parameter,
                         * set to zero to drop weights in monte-carlo updates */
    double OPmin;       /**< minimum value of order parameter considered */
    double OPmax;       /**< maximum value of order parameter considered */
    double Cacc;        /**< acceleration parameter, 0 if no acceleration */

    /* prefactor parameters */
    double OPC;         /**< midpoint of separatrix */
    double dOP;         /**< range of separatrix */
    double gamma;       /**< friction coefficient >= 0 */
    double dt;          /**< delta t */
    int Nt;             /**< Number of time steps */
    double OPsym;

} params;


/** points structs */
typedef struct {
    double OPS;
    double OPC;
    double OPB;

    double OPMS;
    double OPMB;

} points;


/** multicanonical struct, see arXiv:hep-lat/9804019 */
typedef struct {
    double *weights;  /**< weights, \f$w_i\f$ */
    double *lims;     /**< order parameter bin limits */
    int *Gi;          /**< \f$G_i = \sum_{runs} g_i\f$ */
    double *w0;       /**< zeroth order coefficient of order parameter
                       *    (\f$op^0\f$) in (piecewise linear) weight function
                       **/
    double *w1;       /**< coefficient of \f$op^1\f$ */
    int *ni;          /**< number of hits in bin i during run
                       *  (approx of multicanonical histogram) */
    double *hi;       /**< weighted \f$n_i\f$
                       *  (approx of canonical histogram) */
    double *acci;     /**< acceleration \f$-C*\log(G_i)\f$ */

    double op;        /**< current value of order parameter */
    int binl;         /**< bin to immediate left of op */
    double w;         /**< weight for this order parameter */
    double w_norm;    /**< norm factor for weight in exp terms, to prevet over/
                       *    underflow, updates at every checkpoint */

    double op_new;    /**< proposed value of order parameter */
    int binl_new;     /**< bin to immediate left of proposed op */
    double w_new;     /**< weight for proposed op */

} multicanonical;


/*
 *    alloc.c
 */
double ***make_field(params p);
void free_field(double ***field, params p);
void set_random_field(double ***field, params p, gsl_rng* r, double hotness);
void zero_field(double ***field, params p);
void copy_field(double ***fin, double ***fout, params p);
double **make_2d_array(int Lx, int Ly);
void zero_2d_array(double **array, int Lx, int Ly);
void free_2d_array(double **array);
void alloc_multicanonical(multicanonical *muca, params p);
void free_multicanonical(multicanonical *muca, params p);
void reverse_2d_array(double **array, int Lx, int Ly);
void swap_elements(double *a, double *b);

/*
 *    ioput.c
 */
int file_exists(char *file);
void write_data( double **array, int nmeas, int len, char *file, int append);
int count_bytes(FILE *fs);
double **read_data(int nmeas, int *len, char *file);
void save_data( double **array, int nmeas, int len, char *file, int append);
int count_lines(FILE *fs);
void sscanf_cols (char *s, double *d, int n);
double **load_data(int nmeas, int *len, char *file);
void write_multicanonical(multicanonical muca, params p, char *file);
void read_multicanonical(multicanonical *muca, params p, char *file, int acc);
void save_multicanonical(multicanonical muca, params p, char *file);
void load_multicanonical(multicanonical *muca, params p, char *file, int acc);
int file_pattern_count(char *pattern);
void write_rng_state(gsl_rng *r, char *file);
void read_rng_state(gsl_rng *r, char *file);
void write_field_stream(double ***f, params p, FILE *fs);
void read_field_stream(double ***f, params p, FILE *fs);

/*
 *    multicanonical.c
 */
int get_lower_bin(multicanonical *muca, double op, params *p);
void set_multicanonical_proposed_state(multicanonical *muca, double op,
                                       params *p);
void set_multicanonical_current_state(multicanonical *muca, params *p);
void set_multicanonical_state(multicanonical *muca, double op, params p);
void set_multicanonical_derived(multicanonical *muca, params p, int acc);
void set_multicanonical(multicanonical *muca, params p, double op, int acc);
void update_multicanonical_ancillary_arrays(multicanonical *muca, params *p);
double update_multicanonical(multicanonical *muca, int acc, params p);
double update_multicanonical_simple(multicanonical *muca, double delta, int acc,
                                    params p);
void show_multicanonical_current_state(multicanonical *muca, FILE* stream);
double get_multicanonical_weight(multicanonical *muca);
double get_multicanonical_order_parameter(multicanonical *muca);
double weight_function(multicanonical *muca, double op, params *p);

/*
 *    params.c
 */
int get_int(char *buffer, char *name, int *param);
int get_long_unsigned(char *buffer, char *name, long unsigned *param);
int get_double(char *buffer, char *name, double *param);
int get_str(char *buffer, char *name, char *param);
void show_params( params p, FILE* stream);
void show_params0( params p, FILE* stream);
void print_params( params p, char *file);
int check_params (params p);
int checkpoint_params (params p, params q);
void write_lattice_size( params p, FILE * fs);
void read_lattice_size( params *p, FILE * fs);
int compare_lattice_size( params p, params q);
void get_params( params *p, char *file);
void save_status(int th, int t, double sigma, double epsilon, char *file);
void get_status(int *th, int *t, double *sigma, double *epsilon, char *file);


/*
 *    systools.c
 */
unsigned long int dev_urandom();
double cpu_time();
double wall_time(clock_t start_t);


/*
 *    theory.c
 */
double laplacian (double*** f, params *p, int x, int y, int z);

/*
 * time.c
 */
double noise(gsl_rng *r, params *p, double a);
void get_points(points *p, char *file);


#endif //__COMMON_H_INCLUDED__
