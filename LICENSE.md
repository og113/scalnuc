scalnuc (c) 2024 Oliver Gould, Anna Kormu and David J. Weir

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
