/** @file theory.c
* Defining generic functions used in scalar field theory Lagrangians.
*/
#include "common.h"

/** Laplacian (nearest neighbour or next to nearest neighbour) */
double laplacian (double*** f, params *p, int x, int y, int z) {
#if defined(IMPROVED) || defined(IMPROVED_OA2)
    return -15.0/2.0*f[x][y][z]
           + 4.0/3.0*(f[(x+1)%p->Nx][y][z] + f[(x+p->Nx-1)%p->Nx][y][z]
                      + f[x][(y+1)%p->Ny][z] + f[x][(y+p->Ny-1)%p->Ny][z]
                      + f[x][y][(z+1)%p->Nz] + f[x][y][(z+p->Nz-1)%p->Nz])
           - 1.0/12.0*(f[(x+2)%p->Nx][y][z] + f[(x+p->Nx-2)%p->Nx][y][z]
                       + f[x][(y+2)%p->Ny][z] + f[x][(y+p->Ny-2)%p->Ny][z]
                       + f[x][y][(z+2)%p->Nz] + f[x][y][(z+p->Nz-2)%p->Nz]);
#else
    return -6.0*f[x][y][z]
           + f[(x+1)%p->Nx][y][z] + f[(x+p->Nx-1)%p->Nx][y][z]
           + f[x][(y+1)%p->Ny][z] + f[x][(y+p->Ny-1)%p->Ny][z]
           + f[x][y][(z+1)%p->Nz] + f[x][y][(z+p->Nz-1)%p->Nz];
#endif
}
