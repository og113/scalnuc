/** @file alloc.c
 * Functions for allocating and freeing memory for fields, and other structs.
 */
#include "common.h"


/**
 * Here I follow David Weir in "performing a sneaky trick" to construct a 3D
 * array with contiguous memory, accessed as field[x][y][z]
 */
double ***make_field(params p) {

    double *true_field = malloc((p.Nx) * (p.Ny) * (p.Nz) * sizeof(double));

    double ***field = (double ***)malloc((p.Nx) * sizeof(double **));
    int x, y;

    for(x = 0; x < (p.Nx); x++) {
        field[x] = (double **)malloc((p.Ny) * sizeof(double *));
        for(y = 0; y < (p.Ny); y++) {
            field[x][y] = &true_field[x * (p.Ny) * (p.Nz) + y * (p.Nz)];
        }
    }

    field[0][0] = true_field;

    return field;
}

/**
 * set field values to zero
 */
void zero_field(double ***field, params p) {

    int x, y, z;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                field[x][y][z] = 0.0;
            }
        }
    }
}

/**
 * set field values to random values
 */
void set_random_field(double ***field, params p, gsl_rng* r, double hotness) {

    int x, y, z;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                field[x][y][z] = 3.0 + hotness * (gsl_rng_uniform(r) - 0.5);
            }
        }
    }
}

/**
 * Again, from David Weir: Frees the memory associated with a field allocated
 * by make_field above: first frees the contiguous blob,
 * then the 'shortcut' arrays and finally the outermost layer.
 */
void free_field(double ***field, params p) {

    int x;

    free(field[0][0]);

    for(x = 0; x < (p.Nx); x++) {
        free(field[x]);
    }

    free(field);
}

/**
 * copy a field
 */
void copy_field(double ***fin, double ***fout, params p) {

    memcpy(fout[0][0], fin[0][0], p.Nx * p.Ny * p.Nz * sizeof(double));

}

/**
 * make 2d array
 */
double **make_2d_array(int Lx, int Ly) {

    double **array = (double **)malloc(Lx * sizeof(double *));

    array[0] = malloc(Lx * Ly * sizeof(double));

    int i;

    for(i = 1; i < Lx; i++) {
        array[i] = array[i - 1] + Ly;
    }

    return array;
}

/**
 * set 2d array elements to zero
 */
void zero_2d_array(double **array, int Lx, int Ly) {

    int i, j;

    for(i = 0; i < Lx; i++) {
        for(j = 0; j < Ly; j++) {
            array[i][j] = 0.0;
        }
    }
}

/**
 * swap elements in a 2d array
 */
void swap_elements(double *a, double *b) {
    double tmp = *a;
    *a = *b;
    *b = tmp;
}

/**
 * reverse 2d array
 */
void reverse_2d_array(double **array, int Lx, int Ly) {
    int i, j;

    for(j = 0; j < Ly; j++) {
        for(i = 0; i < Lx / 2; i++) {
            swap_elements(&array[i][j], &array[Lx - 1 - i][j]);
        }
        if(Lx % 2 != 0) {
            swap_elements(&array[i][j], &array[Lx - 1 - i][j]);
        }
    }
}

/**
 * free 2d array
 */
void free_2d_array(double **array) {

    free(array[0]);

    free(array);
}

/**
 *    allocate multicanonical struct
 */

void alloc_multicanonical(multicanonical *muca, params p) {
    if (p.OPbins) {
        muca->weights = (double *)malloc(p.OPbins * sizeof(double));
        muca->lims = (double *)malloc(p.OPbins * sizeof(double));
        muca->Gi = (int *)malloc(p.OPbins * sizeof(int));
        muca->w0 = (double *)malloc(p.OPbins * sizeof(double));
        muca->w1 = (double *)malloc(p.OPbins * sizeof(double));
    }

    if (p.Umuca) { /* if updating */
        muca->ni = (int *)malloc(p.OPbins * sizeof(int));
        muca->hi = (double *)malloc(p.OPbins * sizeof(double));
        muca->acci = (double *)malloc(p.OPbins * sizeof(double));
    }

}

/**
 *    free multicanonical struct
 */
void free_multicanonical(multicanonical *muca, params p) {

    if (p.OPbins) {
        free(muca->weights);
        free(muca->lims);
        free(muca->Gi);
        free(muca->w0);
        free(muca->w1);
        printf("Freed weights\n");
    }

    if (p.Umuca) { /* if updating */
        free(muca->ni);
        free(muca->hi);
        free(muca->acci);
        printf("Freed ancillary multicanonical arrays\n");
    }

}
