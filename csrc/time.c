/** @file time.c
 * \brief Time evolution stuff not specific to model
 */
#include "common.h"

/**
 * Random gaussian noise term
 */
double noise(gsl_rng *r, params *p, double a) {
    return gsl_ran_gaussian(r, sqrt(2.0 * p->gamma / (pow(a, 3) * p->dt)));
}

/**
 * loads points from file
 */
void get_points(points *p, char *file) {
    /* opening file */
    FILE * fs = fopen(file, "r");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        exit(2);
    }

    /* char arrays (on stack) to hold lines and parameter names */
    char buffer[NSTR]; /* assuming line <NSTR chars */
    /*char param_name[NSTRSS];*/

    int k = 0; /* goes from 0 to total number of parameters */
    int terms; /* to check that there are two things on each line */

    /* continue while not at the end of the file */
    while (!feof(fs)) {

        /* read into buffer and check line is not empty */
        if ( fgets (buffer, NSTR, fs) != NULL ) {

            /* ignore line and move on if starts with # or if blank */
            if ( buffer[0] == '\n' || buffer[0] == '#') {
                continue;
            }

            // read from buffer into couplings, assuming expected format of file
            /* checks parameter name is what we expect */
            if (k == 0) {
                terms = get_double(buffer, "OPS", &(p->OPS));
            }
            else if (k == 1)
                terms = get_double(buffer, "OPC", &(p->OPC));
            else if (k == 2)
                terms = get_double(buffer, "OPB", &(p->OPB));
            else if (k == 3)
                terms = get_double(buffer, "OPMS", &(p->OPMS));
            else if (k == 4)
                terms = get_double(buffer, "OPMB", &(p->OPMB));

            k++;

            /* checking expected number of things per line */
            if (terms != 2) {
                fprintf(stderr,
                        "Incorrect number of terms, %i, on line %i in %s\n",
                        terms, k, file);
                exit(2);
            }

        }
    }

    if (k != NPOINTS) {
        fprintf(stderr, "Only set %i parameters out of %i\n", k, NPOINTS);
        exit(2);
    }

    /* closing file */
    fclose(fs);
}
