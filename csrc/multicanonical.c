/** @file multicanonical.c
 * \brief Multicanonical stuff for monte carlo evaluations
 *
 * the sign choice is such that simulations are done with the distribution:
 *         exp(-s+w)
 * where s is the action and w is the weight function. This choice follows
 * laine and rummukainen's paper, hep-lat/9804019. An ideal w is such as to
 * cancel s exactly, so we are trying to find w ~ s.
 */
#include "common.h"

#define bin_min 10

/**
 * finds bin just to left of op, with out-of-bounds checks
 * which returns -1
 */
int get_lower_bin(multicanonical *muca, double op, params *p) {

    /* op must be within bounds */
    if ( op >= p->OPmax || op < p->OPmin)
        return -1;

    /*  following kari's code: this traps the new mc-index */
    int i, i0;
    i0 = (muca->binl >= 0 && muca->binl < p->OPbins ? muca->binl : 0);
    for (i = i0; op > muca->lims[i]; i++) {;}   /* the curly brackets are to */
    for ( ; op < muca->lims[i]; i--) {;}        /* shut the compiler up */

    return i;
}

/**
 * get weight from order parameter
 */
double weight_function(multicanonical *muca, double op, params *p) {

    int i;
    double w;

    i = get_lower_bin(muca, op, p);
    if (i >= 0 && i < p->OPbins)
        w = muca->w0[i] + op * muca->w1[i];
    else if ( (op < p->OPmin && op < muca->op) ||
              (op >= p->OPmax && op > muca->op) )
        w = -1000.0; /* always reject */
    else
        w = muca->w;

    return w;
}

/**
 * sets op_new, binl_new and w_new of multicanonical struct, based on op arg
 */
void set_multicanonical_proposed_state(multicanonical *muca,
                                       double op, params *p) {

    muca->op_new = op;
    muca->binl_new = get_lower_bin(muca, op, p);
    muca->w_new = weight_function(muca, op, p);

}

/**
 * sets op, binl and w of multicanonical struct from the corresponding '_new's
 */
void set_multicanonical_current_state(multicanonical *muca, params *p) {

    muca->op = muca->op_new;
    muca->binl = muca->binl_new;
    muca->w = muca->w_new;

}

/**
 * wrapper for set_multicanonical_proposed_state
 * and set_multicanonical_current_state
 */
void set_multicanonical_state(multicanonical *muca, double op, params p) {

    set_multicanonical_proposed_state(muca, op, &p);
    set_multicanonical_current_state(muca, &p);
    muca->w_norm = muca->w; /** Set the initial value of w_norm */

}

/**
 * setting derived arrays, acci, w0 and w1, from weights, lims and Gi,
 * i.e. w0 and w1 from weights and lims and acci from Gi (and acc)
 * these arrays are used because it is faster to store the results than
 * recalculate them, and get_weight gets called a lot of times
 */
void set_multicanonical_derived(multicanonical *muca, params p, int acc) {

    int i;
    double wi, wip;
    int acc_Umuca = (int)(p.Umuca && acc);

    if (acc_Umuca) {
        for (i = 0; i < p.OPbins; i++) {
            /* this loop defines the acceleration term */
            muca->acci[i] = -p.Cacc * log((muca->Gi[i] / muca->Gi[0]));
        }
    }

    i = p.OPbins - 1;
    muca->w0[i] = (acc_Umuca ? muca->weights[i] + muca->acci[i]
                   : muca->weights[i]);
    muca->w1[i] = 0.0;
    for (i = 0; i < (p.OPbins - 1); i++) {

        wi = (acc_Umuca ? muca->weights[i] + muca->acci[i] : muca->weights[i]);
        wip = (acc_Umuca ? muca->weights[i + 1] + muca->acci[i + 1]
               : muca->weights[i + 1]);

        muca->w1[i] = (wip - wi) / (muca->lims[i + 1] - muca->lims[i]);
        muca->w0[i] = wi - muca->w1[i] * muca->lims[i];

    }

    /* zeroing ancillary arrays */
    if (p.Umuca) {
        for (i = 0; i < p.OPbins; i++) {
            muca->ni[i] = 0;
            muca->hi[i] = 0.0;
        }
    }
}

/**
 * sets multicanonical arrays, if there is nothing to load
 */
void set_multicanonical(multicanonical *muca, params p, double op, int acc) {

    int i;
    double dop = (p.OPmax - p.OPmin) / (p.OPbins - 1.0);

    for (i = 0; i < p.OPbins; i++) {
        muca->weights[i] = 0.0;
        muca->Gi[i] = 2 * bin_min;
        muca->lims[i] = p.OPmin + i * dop;
    }

    muca->w = 0.0;

    set_multicanonical_derived(muca, p, acc);
    set_multicanonical_state(muca, op, p);
}

/**
 *    adds current state to the ni and hi ancilliary arrays
 * should be called at every measurement
 */
void update_multicanonical_ancillary_arrays(multicanonical *muca, params *p) {
    if (!p->Umuca) {
        fprintf(stderr, "update_multicanonical_ancillary_arrays called "
                "but p->Umuca==0\n");
        exit(-1);
    }

    int bin, binl;
    double op;
    op = muca->op;
    binl = muca->binl;

    /* op must be within bounds */
    if ( binl < 0 || binl >= p->OPbins )
        return;

    if (binl == p->OPbins - 1)
        bin = binl;
    else {
        /* following kari's code again */
        if (op - muca->lims[binl] < muca->lims[binl + 1] - op)
            bin = binl;
        else
            bin = binl + 1;
    }

    //fprintf(stderr, "w_norm: %g \t w: %g\n", muca->w_norm, muca->w);
    muca->ni[bin] += 1;
    muca->hi[bin] += exp(-muca->w + muca->w_norm);

}

/**
 * function to update multicanonical array based on measurements
 * should be called at the checkpoints
 */
double update_multicanonical(multicanonical *muca, int acc, params p) {
    if (!p.Umuca) {
        fprintf(stderr, "update_multicanonical called but p.Umuca==0\n");
        exit(-1);
    }

    int i, gi;
    double wi, wil, diff;
    diff = 0.0;
    muca->w_norm = muca->w; /* update the value of w_norm */
    /* normalising probability distribution by width of bin */
    muca->hi[0] *= 0.5 / (muca->lims[1] - muca->lims[0]);
    muca->hi[p.OPbins - 1] *= 0.5 /
                              (muca->lims[p.OPbins - 1] -
                               muca->lims[p.OPbins - 2]);
    for (i = 1; i < (p.OPbins - 1); i++) {
        muca->hi[i] *= 0.5 / (muca->lims[i + 1] - muca->lims[i - 1]);
    }

    /* note that weights[0] are fixed at 0.0 */
    wil = muca->weights[0];
    //muca->Gi[0] += muca->ni[0];

    for (i = 1; i < p.OPbins; i++) {

        wi = muca->weights[i];

        if (muca->ni[i] > bin_min && muca->ni[i - 1] > bin_min) {

            gi = muca->ni[i] + muca->ni[i - 1];

            muca->Gi[i] += gi;
            muca->weights[i] = muca->weights[i - 1]
                               + (muca->Gi[i] - gi) * (wi - wil) /
                               (double)(muca->Gi[i])
                               + (gi / (double)(muca->Gi)[i])
                               * log(muca->hi[i - 1] / muca->hi[i]);
        }
        else {
            muca->weights[i] += muca->weights[i - 1] - wil;
        }

        diff += fabs(muca->weights[i] - wi);

        wil = wi;

    }

    set_multicanonical_derived(muca, p, acc);

    return diff;
}

/**
 * alternative function to update multicanonical array based on measurements
 * formulation follows david weir's approach in musca
 * should be called at checkpoints
 */
double update_multicanonical_simple(multicanonical *muca,
                                    double delta, int acc, params p) {
    if (!p.Umuca) {
        fprintf(stderr, "update_multicanonical called but p.Umuca==0\n");
        exit(-1);
    }

    int i;
    double diff;
    diff = 0.0;

    /* using hi to store old weights, as hi not used in david weir's approach */
    for (i = 0; i < p.OPbins; i++) {
        muca->hi[i] = muca->weights[i];
    }

    /* updating weights and Gi */
    for (i = 0; i < p.OPbins; i++) {
        muca->Gi[i] += muca->ni[i];
        muca->weights[i] -= delta * muca->ni[i];
    }

    /* note that weights[0] are fixed at 0.0, and the rest should be relative */
    for (i = 1; i < p.OPbins; i++) {
        muca->weights[i] -= muca->weights[0];
        diff += fabs(muca->weights[i] - muca->hi[i]); // hi stores old weights
    }
    muca->weights[0] = 0.0;

    set_multicanonical_derived(muca, p, acc);

    return diff;
}

/**
 * show current multicanonical state
 */
void show_multicanonical_current_state(multicanonical *muca, FILE* stream) {
    fprintf(stream, "Multicanonical state:\n");
    fprintf(stream, "%-12s %-12s %-12s\n", "op", "binl", "w");
    fprintf(stream, "%-12g %-12i %-12g\n", muca->op, muca->binl, muca->w);
    fprintf(stream, "\n");
}

/**
 * get multicanonical weight
 */
double get_multicanonical_weight(multicanonical *muca) {
    return muca->w;
}

/**
 * get multicanonical order parameter
 */
double get_multicanonical_order_parameter(multicanonical *muca) {
    return muca->op;
}
