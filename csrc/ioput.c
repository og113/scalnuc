/** @file ioput.c
 * Input and output for scalnuc.c program
 *     in general write and read are in binary
 *        , whereas save and load are in ascii
 *        i use glob to find out if files exist
 *        , and otherwise just stdio.h
 */
#include "common.h"

/**
 * check if file exists, uses glob.h
 */
int file_exists(char *file) {
    glob_t globbuf;
    int err = glob(file, GLOB_NOSORT, NULL, &globbuf);
    /* GLOB_NOSORT if doing sort elsewhere, otherwise set to zero */
    if(err == 0) {
        if(globbuf.gl_pathc > 0) {
            globfree(&globbuf);
        }
        return 1;
    }
    else if (err == GLOB_NOSPACE) {
        fprintf(stderr, "file_exists error %i, running out of memory\n", err);
        return 0;
    }
    else if (err == GLOB_ABORTED) {
        fprintf(stderr, "file_exists error %i, GLOB_ABORTED\n", err);
        return 0;
    }
    else if (err == GLOB_NOMATCH) {
        return 0;
    }
    else {
        fprintf(stderr, "file_exists error %i\n", err);
        return 0;
    }
}

/**
 * write measurements to file in binary
 */
void write_data( double **array, int nmeas, int len, char *file, int append) {

    /* opening file */
    FILE * fs;
    if (append) fs = fopen(file, "ab"); /* appending not overwriting */
    else fs = fopen(file, "wb");

    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    /* writing data array */
    int i;

    for(i = 0; i < len; i++) {
        fwrite(array[i], sizeof(double), nmeas, fs);
    }

    fclose(fs);

}

/**
 *    get length of FILE*
 */
int count_bytes(FILE *fs) {

    int len;

    if( fs == NULL )  {
        fprintf(stderr, "len_file error: FILE* == NULL\n");
        return 0;
    }

    fseek(fs, 0, SEEK_END);

    len = ftell(fs);

    rewind(fs);
    return len;
}

/**
 * read measurements from file in binary
 *     N.B. should be used instead of make_data_array, NOT as well as
 */
double **read_data( int nmeas, int *len, char *file) {

    /* opening file */
    FILE * fs = fopen(file, "rb");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return 0;
    }

    /* getting number of data points */
    *len = count_bytes(fs) / sizeof(double) / nmeas;
    int count;
    double **array = make_2d_array(*len, nmeas);
    count = 0;

    if ((*len) == 0) {
        fprintf(stderr, "read_data error: 0 doubles found in %s\n", file);
        return 0;
    }

    /* reading data */
    int i;

    for(i = 0; i < (*len); i++) {
        count = fread(array[i], sizeof(double), nmeas, fs);
    }

    if (count != nmeas) {
        fprintf(stderr, "read_data error: read %i, not %i doubles in %s",
                count, nmeas, file);
    }

    fclose(fs);

    return array;
}


/**
 * write measurements to file in ascii
 */
void save_data(double **array, int nmeas, int len, char *file, int append) {

    /* opening file */
    FILE * fs;
    if (append) fs = fopen(file, "a"); /* appending not overwriting */
    else fs = fopen(file, "w");

    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    if(access(file, R_OK) != 0 || access(file, W_OK) != 0 ) {
        fprintf(stderr, "Unable to access %s file\n", file);
        return;
    }

    /* writing data array */
    int i;
    int j;

    for(i = 0; i < len; i++) {
        for(j = 0; j < nmeas; j++) {
            /* btw, this weird DBL_DIG thing came to my attention in
             *  stackoverflow.com/questions/31861160 */
            fprintf(fs, "%-32.*f ", DBL_DIG - 1, array[i][j]);
        }
        fprintf(fs, "\n");
    }

    fflush(fs);
    fclose(fs);

}


/**
 *    function to read n columns of doubles from a string
 */
void sscanf_cols (char *s, double *d, int n) {

    int i, offset;

    for (i = 0; i < n; i++) {
        if (sscanf(s, " %lf%n", &(d[i]), &offset) == 1) {
            s += offset;
        }
        else {
            fprintf(stderr, "sscanf_cols error: nothing found at position %i\n",
                    i);
            return;
        }
    }

}

/**
 *    function to count lines in FILE*
 */
int count_lines(FILE *fs) {
    int ch;
    int lines = 0;

    if (fs == NULL)
        return 0;

    rewind(fs);
    while(!feof(fs)) {
        ch = fgetc(fs);
        if(ch == '\n') {
            lines++;
        }
    }
    rewind(fs);
    return lines;
}

/**
 * read measurements from file in ascii
 *     N.B. should be used instead of make_data_array, NOT as well as
 */
double **load_data(int nmeas, int *len, char *file) {

    /* opening file */
    FILE * fs = fopen(file, "r");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return 0;
    }

    *len = count_lines(fs);

    if ((*len) == 0) {
        fprintf(stderr, "load_data error: 0 lines found in %s\n", file);
        return 0;
    }

    double **array = make_2d_array(*len, nmeas);
    zero_2d_array(array, *len, nmeas);

    /* reading data */
    int i;

    /* char arrays (on stack) to hold lines and parameter names */
    char buffer[NSTRL]; /* assuming line <=NSTRL-1 chars */

    for(i = 0; i < (*len); i++) {
        /* read into buffer and check line is not empty */
        if ( fgets(buffer, NSTRL, fs) != NULL ) {

            /* read from buffer into array, assuming expected format of file */
            sscanf_cols(buffer, array[i], nmeas);

        }
    }

    fclose(fs);

    return array;
}

/**
 * write multicanonical weights to file in binary
 */
void write_multicanonical(multicanonical muca, params p, char *file) {

    /* opening file */
    FILE * fs = fopen(file, "wb");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    /* writing multicanonical parameters */
    fwrite( &p.OPbins, sizeof(int), 1, fs);
    fwrite( &p.OPmax, sizeof(double), 1, fs);

    /* writing weights */
    fwrite( muca.weights, sizeof(double), p.OPbins, fs);
    fwrite( muca.lims, sizeof(double), p.OPbins, fs);
    fwrite( muca.Gi, sizeof(int), p.OPbins, fs);

    fclose(fs);

}

/**
 * read weights from file in binary
 */
void read_multicanonical(multicanonical *muca, params p, char *file, int acc) {

    int opbins, count;
    double opmin, opmax;
    count = 0;

    /* opening file */
    FILE * fs = fopen(file, "rb");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        exit(2);
    }

    /* reading extra multicanonical parameters */
    count += fread( &opbins, sizeof(int), 1, fs);
    if (opbins != p.OPbins) {
        fprintf(stderr, "Different number of OPbins found in file %s\n", file);
        fprintf(stderr, "%i versus %i\n", p.OPbins, opbins);
        exit(2);
    }
    count += fread( &opmin, sizeof(double), 1, fs);
    if (fabs(opmin - p.OPmin) > DBL_EPSILON) {
        fprintf(stderr, "Different value for OPmin found in file %s\n", file);
        fprintf(stderr, "%lf versus %lf\n", p.OPmin, opmin);
        exit(2);
    }
    count += fread( &opmax, sizeof(double), 1, fs);
    if (fabs(opmax - p.OPmax) > DBL_EPSILON) {
        fprintf(stderr, "Different value for OPmax found in file %s\n", file);
        fprintf(stderr, "%lf versus %lf\n", p.OPmax, opmax);
        exit(2);
    }

    /* reading weights */
    count += fread( muca->weights, sizeof(double), p.OPbins, fs);
    count += fread( muca->lims, sizeof(double), p.OPbins, fs);
    count += fread( muca->Gi, sizeof(int), p.OPbins, fs);

    if (count != 3 + 3 * p.OPbins) {
        fprintf(stderr,
                "read_multicanonical error: read %i, not %i numbers in %s",
                count, 3 + 3 * p.OPbins, file);
    }

    fclose(fs);

    /* setting w0, w1 and acci */
    set_multicanonical_derived(muca, p, acc);

}

/**
 * write multicanonical weights to file in ascii
 */
void save_multicanonical( multicanonical muca, params p, char *file) {

    /* opening file */
    FILE * fs = fopen(file, "w");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    int j;

    /* writing weights */
    for (j = 0; j < p.OPbins; j++) {
        /* interestingly with my compiler, using %lf prints with lower precision
         *  that with %f */
        fprintf(fs, "%-32.*f", DBL_DIG - 1, muca.lims[j]);
        fprintf(fs, "%-32.*f", DBL_DIG - 1, muca.weights[j]);
        fprintf(fs, "%-32i\n", muca.Gi[j]);
    }

    fflush(fs);
    fclose(fs);

}

/**
 * load multicanonical weights from file in ascii
 */
void load_multicanonical(multicanonical *muca, params p, char *file, int acc) {

    /* opening file */
    FILE * fs = fopen(file, "r");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    int len, i;

    len = count_lines(fs);

    if (len != p.OPbins) {
        fprintf(stderr,
                "load_multicanonical error: %i lines found in %s, expected %i\n"
                , len, file, p.OPbins);
        return;
    }

    /* char arrays (on stack) to hold lines and parameter names */
    char buffer[NSTR]; /* assuming line <NSTR chars */

    for(i = 0; i < p.OPbins; i++) {
        /* read into buffer and check line is not empty */
        if ( fgets(buffer, NSTR, fs) != NULL ) {

            /* read from buffer into array, assuming expected format of file */
            if (sscanf(buffer, "%lg %lg %i", &(muca->lims[i])
                       , &(muca->weights[i])
                       , &(muca->Gi[i])) != 3) {
                fprintf(stderr,
                        "load_multicanonical error: on line %i in %s\n", i,
                        file);
                return;
            }

        }
    }

    fclose(fs);

    /* setting w0, w1 and acci */
    set_multicanonical_derived(muca, p, acc);

}


/**
 * check how files match a given pattern plus _#.dat, uses glob.h
 */
int file_pattern_count(char *pattern) {

    int i = 0;
    int exists = 1;
    char file[NSTR];

    glob_t globbuf;
    int err;

    while (exists) {
        sprintf(file, "%s_%i.dat", pattern, i);
        err = glob(file, GLOB_NOSORT, NULL, &globbuf);
        /* GLOB_NOSORT if doing sort elsewhere, otherwise set to zero */
        if(err == 0) {
            i++;
        }
        else {
            exists = 0;
        }
    }

    globfree(&globbuf);

    return i;
}

/**
 * write rng state
 */
void write_rng_state(gsl_rng * r, char *file) {
    FILE *fs;
    fs = fopen(file, "wb");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }
    int success = gsl_rng_fwrite(fs, r);
    fclose(fs);
    if (success != 0) {
        fprintf(stderr, "failed to write rng state to %s\n", file);
        return;
    }
}

/**
 * read rng state
 */
void read_rng_state(gsl_rng * r, char *file) {
    FILE *fs;
    fs = fopen(file, "rb");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        exit(2);
    }
    int success = gsl_rng_fread(fs, r);
    fclose(fs);
    if (success != 0) {
        fprintf(stderr, "failed to read rng state from %s\n", file);
        exit(2);
    }
}

/**
 * write binary field to stream
 */
void write_field_stream(double ***f, params p, FILE *fs) {
    int x, y;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            if (fwrite(&f[x][y][0], sizeof(double), p.Nz, fs) != p.Nz) {
                fprintf(stderr, "Error in write_field_stream\n");
                exit(1);
            }
        }
    }
}

/**
 * read binary field tfrom stream
 */
void read_field_stream(double ***f, params p, FILE *fs) {
    int x, y;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            if (fread(&f[x][y][0], sizeof(double), p.Nz, fs) != p.Nz) {
                fprintf(stderr, "Error in read_field_stream\n");
                exit(1);
            }
        }
    }

}
