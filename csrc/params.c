/** @file params.c
 *    parameters stuff for scalnuc.c program
 */
#include "common.h"


/**
 * print parameters (or rather, a subset thereof) to stream
 *    - means left align
 *    number (e.g. 8) gives chars to print
 *    g is for doubles
 *    i is for ints
 */
void show_params( params p, FILE* stream) {
    fprintf(stream, "########## Lattice parameters\n");
    fprintf(stream, "%-8s%-8s%-8s\n", "Nx", "Ny", "Nz");
    fprintf(stream, "%-8i%-8i%-8i\n", p.Nx, p.Ny, p.Nz);
    fprintf(stream, "########## Monte-Carlo parameters\n");
    fprintf(stream, "%-8s%-8s%-8s%-8s%-8s\n", "seed", "Nth", "Nmc", "Tmeas",
            "Tcheck");
    fprintf(stream, "%-8lu%-8i%-8i%-8i%-8i\n", p.seed, p.Nth, p.Nmc, p.Tmeas,
            p.Tcheck);
    fprintf(stream, "########## Multicanonical parameters\n");
    fprintf(stream, "%-8s%-8s%-8s%-8s%-8s\n"
            , "Umuca", "OPbins", "OPmin", "OPmax", "Cacc");
    fprintf(stream, "%-8i%-8i%-8g%-8g%-8g\n"
            , p.Umuca, p.OPbins, p.OPmin, p.OPmax, p.Cacc);
    fprintf(stream, "########## Prefactor parameters\n");
    fprintf(stream, "%-8s%-8s%-8s%-8s%-8s\n"
            , "OPC", "dOP", "dt", "Nt", "gamma");
    fprintf(stream, "%-8g%-8g%-8g%-8i%-8g%-8g\n"
            , p.OPC, p.dOP, p.dt, p.Nt, p.gamma, p.OPsym);
    fprintf(stream, "\n");
}

/**
 * print params to file
 */
void print_params( params p, char *file) {
    /* opening file */
    FILE * fs = fopen(file, "w");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    /* printing parameters in format:
     *             param name, param value         */
    fprintf(fs, "########## Lattice parameters\n");
    fprintf(fs, "%-12s %-12i\n", "Nx", p.Nx);
    fprintf(fs, "%-12s %-12i\n", "Ny", p.Ny);
    fprintf(fs, "%-12s %-12i\n", "Nz", p.Nz);
    fprintf(fs, "########## Monte-Carlo parameters\n");
    fprintf(fs, "%-12s %-12lu\n", "seed", p.seed);
    fprintf(fs, "%-12s %-12i\n", "Nth", p.Nth);
    fprintf(fs, "%-12s %-12i\n", "Nmc", p.Nmc);
    fprintf(fs, "%-12s %-12i\n", "Tmeas", p.Tmeas);
    fprintf(fs, "%-12s %-12i\n", "Tcheck", p.Tcheck);
    fprintf(fs, "########## Multicanonical parameters\n");
    fprintf(fs, "%-12s %-12i\n", "Umuca", p.Umuca);
    fprintf(fs, "%-12s %-12i\n", "OPbins", p.OPbins);
    fprintf(fs, "%-12s %-12g\n", "OPmin", p.OPmin);
    fprintf(fs, "%-12s %-12g\n", "OPmax", p.OPmax);
    fprintf(fs, "%-12s %-12g\n", "Cacc", p.Cacc);
    fprintf(fs, "########## Prefactor parameters\n");
    fprintf(fs, "%-12s %-12g\n", "OPC", p.OPC);
    fprintf(fs, "%-12s %-12g\n", "dOP", p.dOP);
    fprintf(fs, "%-12s %-12g\n", "dt", p.dt);
    fprintf(fs, "%-12s %-12i\n", "Nt", p.Nt);
    fprintf(fs, "%-12s %-12g\n", "gamma", p.gamma);
    fprintf(fs, "%-12s %-12g\n", "OPsym", p.OPsym);

    /* closing file */
    fflush(fs);
    fclose(fs);
}



/**
 * write params to file in binary
 */
void write_lattice_size( params p, FILE * fs) {

    /* checking file */
    if(fs == NULL) {
        fprintf(stderr, "ERROR: FILE* is null\n");
        return;
    }

    int nparams;

    /* writing double params */
    nparams = NPARAMSD;
    fwrite(&nparams, sizeof(int), 1, fs);

    /* writing int params */
    nparams = NPARAMSI;
    fwrite(&nparams, sizeof(int), 1, fs);
    fwrite(&(p.Nx), sizeof(int), 1, fs);
    fwrite(&(p.Ny), sizeof(int), 1, fs);
    fwrite(&(p.Nz), sizeof(int), 1, fs);

}

/**
 * read params from file in binary
 */
void read_lattice_size( params *p, FILE * fs) {

    /* checking file */
    if(fs == NULL) {
        fprintf(stderr, "ERROR: FILE* is null\n");
        return;
    }

    int nparams, count;
    count = 0;

    /* reading double params */
    count += fread(&nparams, sizeof(int), 1, fs);
    if (nparams != NPARAMSD) {
        fprintf(stderr, "ERROR: unexpected number of double parameters\n");
        fprintf(stderr, "       %i != %i\n", nparams, NPARAMSD);
        return;
    }

    /* reading int params */
    count += fread(&nparams, sizeof(int), 1, fs);
    if (nparams != NPARAMSI) {
        fprintf(stderr, "ERROR: unexpected number of int parameters\n");
        fprintf(stderr, "       %i != %i\n", nparams, NPARAMSI);
        return;
    }
    count += fread(&(p->Nx), sizeof(int), 1, fs);
    count += fread(&(p->Ny), sizeof(int), 1, fs);
    count += fread(&(p->Nz), sizeof(int), 1, fs);

    if (count != 2 + NPARAMSD + NPARAMSI) {
        fprintf(stderr, "read_params0 error: read %i, not %i numbers",
                count, 2 + NPARAMSD + NPARAMSI);
    }

}

/**
 *    check consistency of parameters
 * returns 0 if inconsistent (so can be used like a bool)
 */
int check_params (params p) {

    int good;

    if (p.Nmc < p.Tmeas || p.Nmc < p.Tmeas * p.Tcheck) {
        fprintf(stderr, "Need:\n!(p.Nmc<p.Tmeas)\n!(p.Nmc<p.Tmeas*p.Tcheck)\n");
        good = 0;
    }
    else if (p.Umuca && p.OPbins <= 1) {
        fprintf(stderr, "Need:\n!(p.Umuca && p.OPbins<=1)\n");
        good = 0;
    }
    else {
        good = 1;
    }
    if (!good) {
        fprintf(stderr, "\n########## Parameters inconsistent! ##########\n\n");
        show_params(p, stderr);
    }
    return good;
}

/**
 * compare key params
 */
int compare_lattice_size( params p, params q) {

    int check = 1;

    /* comparing double params */

    /* comparing int params */
    if (p.Nx != q.Nx) check = 0;
    if (p.Ny != q.Ny) check = 0;
    if (p.Nz != q.Nz) check = 0;

    return check;
}

/**
 *    check if there has been a change in the params
 */
int checkpoint_params (params p, params q) {

    if (p.Nmc == q.Nmc)
        return 0;
    else
        return 1;
}

/**
 * get a single integer parameter, to be used in get_params
 */
int get_int(char *buffer, char *name, int *param) {
    int terms;
    char param_name[NSTRSS];

    /* reading line (from buffer) */
    terms = sscanf(buffer, "%s %i", param_name, param);

    /* checking name is what was asked */
    if (strcmp(param_name, name) != 0) {
        fprintf(stderr, "Parameter name %s found where %s should be\n"
                , param_name, name);
        exit(2);
    }

    return terms;
}


/**
 * get a single long unsigned parameter, to be used in get_params
 */
int get_long_unsigned(char *buffer, char *name, long unsigned *param) {
    int terms;
    char param_name[NSTRSS];

    /* reading line (from buffer) */
    terms = sscanf(buffer, "%s %lu", param_name, param);

    /* checking name is what was asked */
    if (strcmp(param_name, name) != 0) {
        fprintf(stderr, "Parameter name %s found where %s should be\n"
                , param_name, name);
        exit(2);
    }

    return terms;
}

/**
 * get a single double parameter, to be used in get_params
 */
int get_double(char *buffer, char *name, double *param) {
    int terms;
    char param_name[NSTRSS];

    /* reading line (from buffer) */
    terms = sscanf(buffer, "%s %lf", param_name, param);

    /* checking name is what was asked */
    if (strcmp(param_name, name) != 0) {
        fprintf(stderr, "Parameter name %s found where %s should be\n"
                , param_name, name);
        exit(2);
    }

    return terms;
}

/**
 * get a single string parameter, to be used in get_params
 */
int get_str(char *buffer, char *name, char *param) {
    int terms;
    char param_name[NSTRSS];

    /* reading line (from buffer) */
    terms = sscanf(buffer, "%s %s", param_name, param);

    /* checking name is what was asked */
    if (strcmp(param_name, name) != 0) {
        fprintf(stderr, "Parameter name %s found where %s should be\n"
                , param_name, name);
        exit(2);
    }
    return terms;
}


/**
 * get parameters from file, with checking that the file conforms
 */
void get_params( params *p, char *file) {
    /* opening file */
    FILE * fs = fopen(file, "r");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        exit(2);
    }

    /* char arrays (on stack) to hold lines and parameter names */
    char buffer[NSTR]; /* assuming line <NSTR chars */
    /*char param_name[NSTRSS];*/

    int k = 0; /* goes from 0 to total number of parameters */
    int terms; /* to check that there are four things on each line */

    /* continue while not at the end of the file */
    while (!feof(fs)) {

        /* read into buffer and check line is not empty */
        if ( fgets (buffer, NSTR, fs) != NULL ) {

            /* ignore line and move on if starts with # or if blank */
            if ( buffer[0] == '\n' || buffer[0] == '#') {
                continue;
            }

            /* read from buffer into params, assuming expected format of file */
            /* checks parameter name is what we expect */
            if (k == 0) {
                terms = get_int(buffer, "Nx", &(p->Nx));
            }
            else if (k == 1)
                terms = get_int(buffer, "Ny", &(p->Ny));
            else if (k == 2)
                terms = get_int(buffer, "Nz", &(p->Nz));
            else if (k == 3)
                terms = get_long_unsigned(buffer, "seed", &(p->seed));
            else if (k == 4)
                terms = get_int(buffer, "Nth", &(p->Nth));
            else if (k == 5)
                terms = get_int(buffer, "Nmc", &(p->Nmc));
            else if (k == 6)
                terms = get_int(buffer, "Tmeas", &(p->Tmeas));
            else if (k == 7)
                terms = get_int(buffer, "Tcheck", &(p->Tcheck));
            else if (k == 8)
                terms = get_int(buffer, "Umuca", &(p->Umuca));
            else if (k == 9)
                terms = get_int(buffer, "OPbins", &(p->OPbins));
            else if (k == 10)
                terms = get_double(buffer, "OPmin", &(p->OPmin));
            else if (k == 11)
                terms = get_double(buffer, "OPmax", &(p->OPmax));
            else if (k == 12)
                terms = get_double(buffer, "Cacc", &(p->Cacc));
            else if (k == 13)
                terms = get_double(buffer, "OPC", &(p->OPC));
            else if (k == 14)
                terms = get_double(buffer, "dOP", &(p->dOP));
            else if (k == 15)
                terms = get_double(buffer, "dt", &(p->dt));
            else if (k == 16)
                terms = get_int(buffer, "Nt", &(p->Nt));
            else if (k == 17)
                terms = get_double(buffer, "gamma", &(p->gamma));
            else if (k == 18)
                terms = get_double(buffer, "OPsym", &(p->OPsym));

            k++;

            /* checking expected number of things per line */
            if (terms != 2) {
                fprintf(stderr, "Incorrect number of terms, %i, on line %i in %s\n"
                        , terms, k,
                        file);
                exit(2);
            }

        }
    }

    if (k != NPARAMS) {
        fprintf(stderr, "Only set %i parameters out of %i\n"
                , k, NPARAMS);
        exit(2);
    }

    /* closing file */
    fclose(fs);

}



/**
 * save status to file
 */
void save_status(int th, int t, double sigma, double epsilon, char *file) {
    /* opening file */
    FILE * fs = fopen(file, "w");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        exit(2);
    }

    /* very simple output */
    fprintf(fs, "%-12s %-12i\n", "th", th);
    fprintf(fs, "%-12s %-12i\n", "t", t);
    fprintf(fs, "%-12s %-12lg\n", "sigma", sigma);
    fprintf(fs, "%-12s %-12lg\n", "epsilon", epsilon);

    /* closing file */
    fflush(fs);
    fclose(fs);

}

/**
 * get status from file
 */
void get_status(int *th, int *t, double *sigma, double *epsilon, char *file) {
    /* opening file */
    FILE * fs = fopen(file, "r");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        exit(2);
    }

    /* very simple input */
    char buffer[81];
    int n_read = 0;
    n_read += fscanf(fs, "%s %i", buffer, th);
    n_read += fscanf(fs, "%s %i", buffer, t);
    n_read += fscanf(fs, "%s %lg", buffer, sigma);
    n_read += fscanf(fs, "%s %lg", buffer, epsilon);

    if (n_read != 8) {
        fprintf(stderr, "Can't read file, %s\n", file);
        exit(2);
    }

    /* closing file */
    fclose(fs);

}
