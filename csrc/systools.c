/** @file systools.c
 * Tools using system specific stuff
 */
#include "common.h"

/**
 * gets a random 32 bit unsigned integer from the environmental noise,
 * using /dev/urandom, can be used as random seed for other generators
 * from jones2010good
 */
unsigned long dev_urandom() {
    unsigned long s;
    FILE *fp;
    fp = fopen("/dev/urandom", "rb");
    if (fp == NULL) {
        fprintf (stderr, "File error reading /dev/urandom/\n");
        exit (1);
    }
    if ( fread(&s, sizeof(s), 1, fp) != 1 ) {
        fprintf (stderr, "File error reading /dev/urandom/\n");
        exit (2);
    }
    fclose(fp);
    return s;
}

/**
 * gets cpu time from system, from kari's cubic anisotropy code
 */
double cpu_time() {
    struct rusage resource;

    getrusage(RUSAGE_SELF, &resource);
    return (resource.ru_utime.tv_sec + 1e-6 * resource.ru_utime.tv_usec +
            resource.ru_stime.tv_sec + 1e-6 * resource.ru_stime.tv_usec);
}

/**
 * gets time on wall clock since start_t
 */
double wall_time(clock_t start_t) {
    return (double)(clock() - start_t) / CLOCKS_PER_SEC;
}
