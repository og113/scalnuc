# scalnuc
Authors: Oliver Gould, Anna Kormu and David Weir

This code, scalnuc, was written to study first-order phase transitions in some
high-temperature effective field theories involving scalar fields. In
particular, the goal has been to reliably compute thermodynamics quantities and
the bubble nucleation rate, for comparison to perturbative approaches.

scalnuc has primarily been used to study a model of a single
real scalar field with the following Euclidean Lagrangian,

![L_singlet = 1/2*(dphi/dx^i)^2 + sigma*phi + 1/2*m^2*phi^2 + 1/3!*g*phi^3 + 1/4!*lambda*phi^4](figs/L_singlet.svg)

The equilibrium thermodynamics of first-order phase
transitions in this model were studied in
[JHEP 04 (2021) 057](https://doi.org/10.1007/JHEP04(2021)057).
The code has since been further developed and used to study bubble
nucleation in this theory, for which see [arXiv:2404.01876 [hep-th]](https://arxiv.org/abs/2404.01876).

The code has also reproduced some of the results of
[JHEP 04 (2001) 017](https://doi.org/10.1088/1126-6708/2001/04/017)
for the cubic anisotropy model, a model of two interacting scalar fields
with Euclidean Lagrangian,

![L_cubic = 1/2*(dphi_a/dx^i)^2 + 1/2*m^2*phi_a^2 + 1/4!*lambda_1*phi_a^4 + 1/4*lambda_2*phi_1^2*phi_2^2](figs/L_cubic.svg)

where sums are implied over the spatial indices i=1,2,3 and internal indices a=1,2.

## Requirements
The simulation code is written in C, and hence requires a C compiler, such as gcc.
It also uses the standard C math library and the GNU Scientific Library.

The analysis code is written in Python 3, and the library requirements are given
as in `analysis/requirements.txt`.

## Configuration
The Makefile must be linked to a file called `config.mk` in the root directory.
Examples can be found in the configs directory. To use the default (set up on
Ubuntu Linux) make a symbolic link to `configs/default.mk` called `config.mk`,

    ln -s configs/default.mk config.mk


## Compiling
For each model there are two different simulation codes: a Monte-Carlo code and
a time evolution code. The naming convention is `<model>_mc` for the Monte-Carlo
code, and `<model>_te` for the time evolution code. The two models are `singlet`
and `cubic`.

Make is used for compilation. The first time you pull the repository, you should
also make the directory structure. This is done with

	make dirs

Once this is done, to compile the codes, call `make` followed by the name of the
model, e.g. for the singlet model,

	make singlet

This compiles both the `singlet_mc` Monte-Carlo code, and the `singlet_te`
time evolution code.

To make one of the test programs, e.g. testIoput.c, which tests some of the
input/ouput stuff in ioput.c, type

	make tests/testIoput

Generally though we have not kept the tests up to date, so they may need rejigging
to get them to compile/run.

## Making the documentation
To make html files from the comments within the code, use doxygen on the
Doxyfile, i.e.

	doxygen Doxyfile

## Running
To run the Monte-Carlo codes, there must be `params` and `couplings` files
present in the directory from which the code is run. Examples of such files can
be found at `examples/params` and `examples/couplings_<model>`.
With these two files present, the Monte-Carlo codes are run, for example, as

	./singlet_mc

The file `params` contains simulation parameters which are present for all
models, and the file `couplings` contains Lagrangian couplings which are
specific to a given model.

To run the time evolution codes an additional file `points` is required, giving
the locations of various important points on the histogram of the order
parameter. An example such file is located at `examples/points`.
The code is run as

	./singlet_te <forwards_data_file> <backwards_data_file> <strix_file>

where `<forwards_data_file>` is the filename where to store the data for
forwards time evolution, `<backwards_data_file>` is that for backwards
time evolution, and `<strix_file>` is the filename of the starting field
configuration.

## Analysis
The results of Monte-Carlo measurements taken during a simulation are stored
in the file `data`, written in ASCII. The analysis of these results is then done
separately. The analysis codes are written in Python 3 and can be found
in the directory `analysis/`. Data from time evolution runs is stored in the
`<forwards_data_file>` and `<backwards_data_file>`. This is also analysed
using the Python codes in the directory `analysis/`. For more information, see
the codes themselves.

## Directory structure
The directory structure is:

- `analysis/`: mostly Python codes for analysing data
- `cobjs/`: object files, compiled from csrc
- `configs/`: configuration files for make
- `csrc/`: common source code
- `dox/`: doxygen output directory
- `examples/`: example parameters and couplings files
- `figs/`: figures
- `include/`: header files
- `objs/`: object files compiled from src
- `src/`: source code (excluding common stuff)
- `tests/`: test files (typically not up to date)

## Contact
scalnuc is the work of:

- Oliver Gould (ORCID: 0000-0002-7815-3379, INSPIRE: O.Gould.3)
- Anna Kormu (ORCID: 0000-0002-0309-3471, INSPIRE: A.Kormu.1)
- David J. Weir (ORCID: 0000-0001-6986-0517, INSPIRE: D.J.Weir.1)

If you have any questions or comments regarding the code, please feel free to contact us via email.