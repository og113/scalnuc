# makefile for scalar droplet nucleation program, scalnuc
#----------------------------------------------------------------------------

# config file

include config.mk

#----------------------------------------------------------------------------
# variables

# general variables
HEADERS		= $(wildcard $(HDIR)/*.h)
COMMONSRC	= $(wildcard $(CSDIR)/*.c)
COMMONOBJS	= $(patsubst $(CSDIR)/%.c,$(CODIR)/%.o,$(COMMONSRC))
SRC			= $(wildcard $(SDIR)/*.c)
EXE			= $(patsubst $(SDIR)/%.c,%,$(SRC))

# montecarlo variables
MCSRC		= $(SDIR)/montecarlo_algorithm.c
MCOBJS		= $(patsubst $(SDIR)/%.c,$(ODIR)/%.o,$(MCSRC))

# order parameter variables
OSRC		= $(SDIR)/order_parameter.c
OOBJS		= $(patsubst $(SDIR)/%.c,$(ODIR)/%.o,$(OSRC))

# one scalar variables
ONESRC		= $(wildcard $(SDIR)/onescalar_*.c)
ONEOBJS 	= $(patsubst $(SDIR)/%.c,$(ODIR)/%.o,$(ONESRC))

# two scalar variables
TWOSRC		= $(wildcard $(SDIR)/twoscalar_*.c)
TWOOBJS 	= $(patsubst $(SDIR)/%.c,$(ODIR)/%.o,$(TWOSRC))

# cubic model variables
CUBICSRC	= $(wildcard $(SDIR)/cubic_*.c)
CUBICOBJS 	= $(patsubst $(SDIR)/%.c,$(ODIR)/%.o,$(CUBICSRC))
CUBICDEFS   = -DCUBIC

# singlet model variables
SINGSRC		= $(wildcard $(SDIR)/singlet_*.c)
SINGOBJS 	= $(patsubst $(SDIR)/%.c,$(ODIR)/%.o,$(SINGSRC))
SINGDEFS	= -DSINGLET

# rectangular model variables
RECTSRC		= $(wildcard $(SDIR)/rectangular_*.c)
RECTOBJS 	= $(patsubst $(SDIR)/%.c,$(ODIR)/%.o,$(RECTSRC))
RECTDEFS	= -DRECTANGULAR

#-----------------------------------------------------------------------------
# some useful PHONYs

.PHONY : variables
variables :
	@echo HEADERS: $(HEADERS)
	@echo COMMONSRC: $(COMMONSRC)
	@echo COMMONOBJS: $(COMMONOBJS)
	@echo SRC: $(SRC)
	@echo EXE: $(EXE)

.PHONY: cubic
cubic: cubic_mc cubic_te

.PHONY: singlet
singlet: singlet_mc singlet_te

#-----------------------------------------------------------------------------
# targets, dependencies and rules for executables

cubic_mc: DEFINES += $(CUBICDEFS)
cubic_mc: $(ODIR)/scalnuc.o $(CUBICOBJS) $(TWOOBJS) $(MCOBJS) $(OOBJS) \
			$(COMMONOBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(INCLUDES) $(LIBS) $(DEFINES)
	@echo cubic_mc has been compiled

cubic_te: DEFINES += $(CUBICDEFS)
cubic_te: $(ODIR)/timelution.o $(CUBICOBJS) $(TWOOBJS) $(OOBJS) $(COMMONOBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(INCLUDES) $(LIBS) $(DEFINES)
	@echo cubic_te has been compiled

singlet_mc: DEFINES += $(SINGDEFS)
singlet_mc: $(ODIR)/scalnuc.o $(SINGOBJS) $(ONEOBJS) $(MCOBJS) $(OOBJS) \
				$(COMMONOBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(INCLUDES) $(LIBS) $(DEFINES)
	@echo singlet_mc has been compiled

singlet_te: DEFINES += $(SINGDEFS)
singlet_te: $(ODIR)/timelution.o $(SINGOBJS) $(ONEOBJS) $(OOBJS) $(COMMONOBJS)
	$(CC) -o $@ $^ $(CFLAGS) $(INCLUDES) $(LIBS) $(DEFINES)
	@echo singlet_te has been compiled

#-----------------------------------------------------------------------------
# generic rules

$(CODIR)/%.o: $(CSDIR)/%.c
	$(CC) -c -o $@ $< $(CFLAGS) $(INCLUDES) $(LIBS) $(DEFINES)

$(ODIR)/%.o: $(SDIR)/%.c
	$(CC) -c -o $@ $< $(CFLAGS) $(INCLUDES) $(LIBS) $(DEFINES)

#-----------------------------------------------------------------------------
# clean

.PHONY: clean
clean:
	rm -f cubic_mc cubic_te singlet_mc singlet_te
	rm -f $(ODIR)/*.o
	rm -f $(CODIR)/*.o
	rm -f $(TSDIR)/*.o

.PHONY: veryclean
veryclean: clean
	rm -f */*~
	rm -f *~

#-----------------------------------------------------------------------------
# directory structure

.PHONY: dirs
dirs:
	mkdir -p $(ODIR)/*
	mkdir -p $(CODIR)/*
