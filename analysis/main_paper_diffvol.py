import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import json
from scipy.optimize import curve_fit

# Read in params
if len(sys.argv)!=2:
    print("pass the options file as an argument")
    sys.exit()
else:
    options_file = np.array(sys.argv)[1]
    print("---------------------Singlet model--------------------")
    print("\n-------------------", "Analysis code for nucleation rate calculations", "--------------------")
    print("For help with the functions try 'python3 -m pydoc <module>\n")


# reading options file
with open(options_file, 'r') as stream:
    json_string = stream.read()

    # parse file
    opts_list = json.loads(json_string)['options']

N_runs = len(opts_list)

rate_runs = np.zeros(N_runs)
error_runs = np.zeros(N_runs)
phys_vol_runs = np.zeros(N_runs)

# Setting up seaborn
sns.set_context("paper")
sns.set_palette("magma", 5)
colors = sns.color_palette('magma', 5)


plt.rc('font', **{"family": "Times New Roman"})
plt.rc("axes", labelsize=18)
plt.rc("axes", titlesize=18)
plt.rc("xtick", labelsize=18)
plt.rc("ytick", labelsize=18)
plt.rc("legend", fontsize=18)
plt.rc("text", usetex=True)
text_size = 18.0  # for the annotations inside


for i in range(N_runs):


    import singlet_newop_analysis as singlet_newop
    theory = singlet_newop.Singlet_NewOP()

    print("\n---------------------------", "Load data", "---------------------------")

    labels = opts_list[i]["directory"]+opts_list[i]["label"]
    data = theory.load_data_mc(opts_list[i]["directory"]+opts_list[i]["hists"], labels)
    weight = theory.load_weight(opts_list[i]["directory"]+"weight")
    counterterms = theory.load_params(opts_list[i]["directory"]+opts_list[i]["counterterms"])
    params = theory.load_params(opts_list[i]["directory"]+opts_list[i]["params"])
    couplings = theory.load_params(opts_list[i]["directory"]+opts_list[i]["couplings"])

    nucleation = theory.load_crossings(opts_list[i]["directory"]+opts_list[i]["nucleation"])
    dynamic, dynamic_mean, dynamic_error = theory.calc_dynamical_factor_error(nucleation["weight"], nucleation["cross"])
    print("-------------------------------------------------------------------\n")

    print("\n---------------------------", "Parameters", "---------------------------")

    sigma = couplings["b1"]
    msq = couplings["b2"]
    g = couplings["b3"]
    a = couplings["a"]
    '''
    if  i==0:
        benchmark = opts_list[i]["benchmark"]
        Tmin = 92.5
        Tmax = 93.5
        import singlet_analysis as singlet_old
        old_theory = singlet_old.Singlet()
        sigma_new, msq_new, T, S_3d, tree_level, one_loop, m_no_linear = old_theory.temp_range(benchmark, Tmin, Tmax, 10)


        i_pert = np.argwhere(sigma >= sigma_new)[0][0]
        T = T[i_pert]
        print(T)
        print(sigma_new[i_pert])
        print(msq_new[i_pert])
        print(f"m in sigma=0 basis {m_no_linear[i_pert]}")
    '''

    Zm=counterterms["Zm"]
    Zphi=counterterms["Zphi"]

    Nx = params["Nx"]
    L = Nx*a
    vol = L**3 # volume
    epsilon = params["dOP"]
    OPsym = params["OPsym"]

    # Time evolution params
    dt = params["dt"]
    gamma = params["gamma"]

    bins = opts_list[i]["bins"]
    crit = opts_list[i]["loc_bubble"]

    # Benchamrk and temp range info, for reweighting
    benchmark = opts_list[i]["benchmark"]
    Tmin=opts_list[i]["temp_range"][0]
    Tmax=opts_list[i]["temp_range"][1]
    dT=opts_list[i]["temp_range"][2]


    # Jackknife resampling
    points_to_remove = 10000 # How many points to remove from the original data


    print(f"Nx {Nx} |  a {a}  | L  {L} |   msq {msq}  |   sigma {sigma}  |   g {g}  |  epsilon  {epsilon} |   dt {dt}  |   gamma {gamma}   |   bins {bins}")
    print("-------------------------------------------------------------------\n")


    x, y = theory.get_xy_data(data['order'], data['weight'], bins)
    index_array_orig  = theory.find_extrema_index(x, y, crit, 'half')
    extrema_points_orig, midpoints_orig= theory.find_points(x, y, index_array_orig)

    # Jackknife data for error analysis
    points_to_remove = 10000 # How many points to remove from the original data
    order_jackknifed = theory.jackknife(data['order'], points_to_remove)
    weight_jackknifed = theory.jackknife(data['weight'], points_to_remove)

    N_jackknife = len(order_jackknifed) # How many jackknife resamples we have
    x_jack = np.zeros([N_jackknife, bins])
    y_jack = np.zeros([N_jackknife, bins])
    index_array = np.zeros([N_jackknife, 3], dtype='int')
    extrema_points = np.zeros([N_jackknife, 6])
    midpoints = np.zeros([N_jackknife, 4])
    probs_jack = np.zeros(N_jackknife)
    flux_jack = np.zeros(N_jackknife)

    for sample in range(N_jackknife):
        # Create hists for the reweight params
        x_jack[sample], y_jack[sample] = theory.get_xy_data(order_jackknifed[sample], weight_jackknifed[sample], bins)

        # For each sample calculate the location of the phases (index, extrema & midpoints)
        index_array[sample]  = theory.find_extrema_index(x_jack[sample], y_jack[sample], crit, 'half')
        extrema_points[sample], midpoints[sample] = theory.find_points(x_jack[sample], y_jack[sample], index_array[sample])
        opC = extrema_points[sample][2]
        probs_jack[sample] = theory.probability(x_jack[sample], y_jack[sample], epsilon, opC, index_array[sample])
        flux_jack[sample] = theory.flux(opC, OPsym, vol)



    prob_mean = np.mean(probs_jack)
    prob_error = np.sqrt(np.sum((probs_jack-prob_mean)**2)*(N_jackknife-1)/N_jackknife)
    flux_mean = np.mean(flux_jack)
    flux_error = np.sqrt(np.sum((flux_jack-flux_mean)**2)*(N_jackknife-1)/N_jackknife)

    rate_jack = flux_jack*probs_jack
    rate_mean = np.mean(rate_jack)
    rate_error = np.sqrt(np.sum((rate_jack-rate_mean)**2)*(N_jackknife-1)/N_jackknife)

    # Calculate error of full rate
    rate_error = np.sqrt((rate_error/rate_mean)**2+(dynamic_error/dynamic_mean)**2) # note that this is now the error associated with log(rate)!!
    rate_mean = rate_mean*dynamic_mean*0.5

    # Calculate the error and mean of extrema_points
    extrema_points_mean = np.zeros(6)
    extrema_points_error = np.zeros(6)
    midpoints_mean = np.zeros(4)
    midpoints_error = np.zeros(4)

    for j in range(6):
        if j<4:
            midpoints_mean[j] = np.mean(midpoints[:,j])
            midpoints_error[j] = np.sqrt(np.sum((midpoints[:,j]-midpoints_mean[j])**2)*(N_jackknife-1)/N_jackknife)

        extrema_points_mean[j] = np.mean(extrema_points[:,j])
        extrema_points_error[j] = np.sqrt(np.sum((extrema_points[:,j]-extrema_points_mean[j])**2)*(N_jackknife-1)/N_jackknife)


    x_mean, x_error, y_mean, y_error = theory.bin_error_from_resamples(x_jack, y_jack, bins, N_jackknife)


    print(f"orig: {extrema_points_orig[2]}")
    print(f"OPS: {extrema_points_mean[0]}, OPC: {extrema_points_mean[2]}, OPMS: {midpoints_mean[0]}")
    print(f"OPS error: {extrema_points_error[0]}, OPC error: {extrema_points_error[2]}, OPMS error: {midpoints_error[0]}")

    rate_runs[i]  = rate_mean/vol
    error_runs[i]  = rate_error
    phys_vol_runs[i] = L

    plt.figure(1)
    plt.errorbar(1/L, np.log(rate_mean/vol), yerr=rate_error, marker='o', capsize=10, linestyle='none', color=colors[2], label=opts_list[i]["fig_label"])



xfit = np.linspace(0.0001, 1/min(phys_vol_runs), 1000)
fit_points= np.zeros([N_runs,3])

popt_exponential, pcov_exponential, perr_exponential, rsq_exponential = theory.fit_func(theory.exponential, 1/phys_vol_runs, np.log(rate_runs), error_runs, *(-74,10e5,0.3))

plt.figure(1)
plt.plot(xfit, theory.exponential(xfit, *popt_exponential), linestyle='--', color=colors[0])
plt.errorbar(0, popt_exponential[0], yerr=perr_exponential[0], fmt='s', color=colors[0], capsize=10, label=r"$\log(\Gamma/\lambda_3^4)={:.2f}({:.0f})$".format(popt_exponential[0], perr_exponential[0]*100))      

print(f"Goodness of fit (all data): {rsq_exponential}")
print(f"Error on a = {popt_exponential[0]} exponential fit {perr_exponential[0]}")
print(f"Error on b = {popt_exponential[1]} in exponential fit {perr_exponential[1]}")
print(f"Error on c = {popt_exponential[2]} in exponential fit {perr_exponential[-1]}")

for i in range(N_runs):
    new_physvol = np.delete(phys_vol_runs, i)
    new_rates = np.delete(rate_runs, i)
    new_errors = np.delete(error_runs, i)

    popt_exponential, pcov_exponential, perr_exponential, rsq_exponential = theory.fit_func(theory.exponential, 1/new_physvol, np.log(new_rates), new_errors, *(-74,10e5,0.3))

    fit_points[i] = popt_exponential
    
    '''
    print(f"Goodness of fit: {rsq_exponential}")
    print(f"Error on a = {popt_exponential[0]} exponential fit {perr_exponential[0]}")
    print(f"Error on b = {popt_exponential[1]} in exponential fit {perr_exponential[1]}")
    print(f"Error on c = {popt_exponential[2]} in exponential fit {perr_exponential[-1]}")
    '''


N_fit = len(fit_points)
mean_fit = np.mean(fit_points, axis=0)
error_fit = np.sqrt(np.sum((fit_points-mean_fit)**2, axis=0)*(N_fit-1)/N_fit)

print(f"Jackknife approach: a={mean_fit[0]}, error {error_fit[0]}")
print(f"Jackknife approach: b={mean_fit[1]}, error {error_fit[1]}")
print(f"Jackknife approach: c={mean_fit[2]}, error {error_fit[2]}")

plt.figure(1)
#plt.errorbar(0, mean_fit[0], yerr=error_fit[0], fmt='s', color=colors[0], capsize=10, label=r"$\log(\Gamma/\lambda_3^4)={:.2f}({:.0f})$".format(mean_fit[0], error_fit[0]*100))
#plt.plot(xfit, theory.exponential(xfit, *mean_fit), color=colors[0], linestyle="--")
plt.xlabel(r"$1/(L\lambda_3)$", fontsize=text_size)
plt.ylabel(r"$\log(\Gamma/\lambda_3^4)$",fontsize=text_size)
plt.legend(loc='upper left')
theory.set_fig_specs("vol_limit_paper")


file_save = "vol_limit_data"
names = "T\tNx\ta\tlogGamma\tlogGammaError"

with open(file_save, 'w') as f:
    f.write(names+"\n")
    for i in range(N_runs):
        f.write("93.121\t" + str(phys_vol_runs[i]/1.5) + "\t1.5" + "\t" + str(np.log(rate_runs[i])) +
                "\t" + str(error_runs[i])+'\n')


plt.show()
