# libraries
import numpy as np


def V0(x, sig, msq, g, lam, f3):
    """
    The potential for the 3d singlet scalar theory.
    """
    return (
        f3
        + sig * x
        + 1 / 2 * msq * x**2
        + 1 / 6 * g * x**3
        + 1 / 24 * lam * x**4
    )


def singlet_dimensionless(sig, msq, g, lam, f3):
    """
    Changes to units where lam=1, for 3d singlet scalar model.
    """
    # initialising a dict to hold the results
    params_dimless = {}

    # change of units, to lam=1
    params_dimless["sig"] = sig / np.power(lam, 2.5)
    params_dimless["msq"] = msq / lam**2
    params_dimless["g"] = g / np.power(lam, 1.5)
    params_dimless["lam"] = 1
    params_dimless["f3"] = f3 / np.power(lam, 3)
    return params_dimless


def dr_xSM(T, b1, mSsq, b3, b4, a1, a2):
    """
    Dimensional reducion function starting from xSM model, and reducing to the
    3d singlet scalar model.
    Note:
        - The factors of 2 and 6 in g and lam are due to a difference in
        conventions.
        - This can be mapped to a simple Yukawa model with
        a2 = y**2, a1 = 2*y*m_psi.
    """
    # initialising a dict to hold the results
    params_3d = {}

    # number of degrees of freedom
    g_star = 107.75

    # direct leading-order DR
    params_3d["sig"] = (b1 + (b3 / 12 + a1 / 12) * T**2) / np.sqrt(T)
    params_3d["msq"] = mSsq + (b4 / 4 + a2 / 6) * T**2
    params_3d["g"] = 2 * np.sqrt(T) * b3
    params_3d["lam"] = 6 * T * b4
    params_3d["f3"] = -np.pi ** 2 / 90 * T ** 3 * g_star
    return params_3d


def singlet_no_cubic_basis(sig, msq, g, lam, f3):
    """
    Shifts basis to g=0, for 3d singlet scalar model.
    See arXiv:2103.14454 Eqs. (3.2) and (3.3).
    """
    # initialising a dict to hold the results
    params_new = {}

    # change of basis
    params_new["sig"] = sig + g**3 / (3 * lam**2) - g * msq / lam
    params_new["msq"] = msq - g**2 / (2 * lam)
    params_new["g"] = 0
    params_new["lam"] = lam
    params_new["f3"] = f3
    return params_new


def singlet_no_linear_basis(sig, msq, g, lam, f3):
    """
    Shifts basis to sig=0, for 3d singlet scalar model.
    """
    # to find basis with no linear term, need to solve for x=x0 such that
    # dV/dx = c0 + c1*x + c2*x**2 + c3*x**3
    c0 = sig
    c1 = msq
    c2 = g / 2
    c3 = lam / 6

    # soliving the cubic with np
    cubic = np.polynomial.polynomial.Polynomial([c0, c1, c2, c3])
    x0_list = cubic.roots()

    # removing complex solutions
    # there should be either 1 or 3 real solutions
    x0_real = x0_list[np.isreal(x0_list)]

    if len(x0_real) == 1:
        # only one solution
        x0 = x0_real[0].real
    elif len(x0_real) == 3:
        # take the left most solution
        x0 = np.sort(x0_real)[0].real
    else:
        print(len(x0_real), "real solutions in singlet_no_linear_basis")
        exit()

    # Initialising a dict to hold the results
    params_new = {}

    # change of basis
    params_new["sig"] = 0
    params_new["msq"] = msq + g * x0 + 1 / 2 * lam * x0**2
    params_new["g"] = g + lam * x0
    params_new["lam"] = lam
    params_new["f3"] = f3
    return params_new


def singlet_rg_3d(sig, msq, g, lam, f3, mu_ratio):
    """
    Changes 3d rg scale
    """
    # initialising a dict to hold the results
    params_new = {}

    # change of basis
    params_new["sig"] = sig + g * lam / 6 / (4 * np.pi) ** 2 * np.log(mu_ratio)
    params_new["msq"] = msq + lam**2 / 6 / (4 * np.pi) ** 2 * np.log(mu_ratio)
    params_new["g"] = g
    params_new["lam"] = lam
    params_new["f3"] = f3 + g ** 2 / 12 / (4 * np.pi) ** 2 * np.log(mu_ratio)
    # NB only running with the EFT is included here.
    return params_new
