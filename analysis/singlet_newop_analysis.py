######################## The Real Scalar Theory #########################
from base_analysis import Base
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

class Singlet_NewOP(Base):


    def load_data_mc(self, filename, labels):
        """
        Loads data from the MC runs in to a numpy array
        Takes the data file and the corresponding labels file as arguments. (Singlet model)
        """

        columns = np.genfromtxt(labels, usecols=1, dtype=str) # This returns a numpy array
        columns = np.char.replace(columns, "^", "") #Python doesn't like these when indexing arrays
        columns[4]="kinetic" #phi (laplacian) phi was getting shortened to just phi
        columns=columns[:8].tolist() #othrwise genfromtxt won't like it

        data = np.genfromtxt(filename, names=columns)

        print(columns)
        print("Loaded MC run data (Singlet model)")

        return data



    def get_2d_hist(self, data1, data2, weight, bin_no):
        """
        Same as get_xy_data but for 2D hists, note that this is not normed! (Singlet model)
        Takes the data for the input values data1 and data2, weight and the number of bins as arguments.
        Returns a 2d histogram and the new x-axes for both input data sets.
        """

        hist2d, xedges, yedges = np.histogram2d(data1, data2, bins =bin_no, weights=np.exp(-weight))
        x_data1 = 0.5*(xedges[1:]+xedges[:-1])
        x_data2 = 0.5*(yedges[1:]+yedges[:-1])


        return hist2d, x_data1, x_data2



    def return1D_hist(self, hist2D):
        """
        Takes in a 2Dhist and returns 1D hist
        """

        N = len(hist2D[0])
        hist1D = np.zeros(N)

        for i in range(N):
            for j in range(N):
                hist1D[i]+=hist2D[i,j]

        return hist1D



    def reweight2D(self, coeff_linear, coeff_square, dmsq, dsigma, x_op, x_lin, hist2d_orig, opsym):
        """
        Reweight the probability distribution.  y(x) = P(op). Uses logarithms for the calculations due to under/overflow.
        Input args: reweight coeffs coeff_linear and coeff_square, the values of msq and sigma we want to reweight to
        and the data related to phi and phi^2 as well as the 2d hist or the original data.
        Returns the new probability distribution array for multiple reweight values
        """
        #for quick ref the coeffs are
        #coeff_linear=vol 
        #coeff_square=0.5*vol*Zm*Zphi

        N = len(dmsq)
        M = len(hist2d_orig[0])
        P = np.zeros([N, len(hist2d_orig[0])])

        # Loop over the params we want to reweight to
        for i in range(N):
            hist2d_new=np.zeros([M, M])

            for j in range(M):
                for k in range(M):
                    # Because most of the hist2d data is a bunch of zeros this leads to runtime warnings when log(hist2d_orig[j,k])=-inf
                    # to work around this, we set these entries in the reweighting to zero (it works perfectly well in both cases
                    # but perhaps it's nicer to catch this exception here)
                    if np.longdouble(hist2d_orig[j,k]) == 0:
                        hist2d_new[j,k]=0
                    else:
                        # note that j and k have switched place here (hist2d_orig[op, phi] and not [phi_phi^2] so I _think_ this should be done)
                        #hist2d_new[j,k]=np.exp(-coeff_linear*(dsigma[i]+2.0*dmsq[i]*opsym)*x_lin[k]-coeff_square*dmsq[i]*x_op[j]+np.log(np.longdouble(hist2d_orig[j,k])))
                        hist2d_new[j,k]= np.exp(-coeff_linear*dsigma[i]*x_lin[k]- coeff_square*dmsq[i]*(x_op[j] + 2.0 * opsym * x_lin[k])+ np.log(np.longdouble(hist2d_orig[j,k]))
                    )


            hist_new = self.return1D_hist(hist2d_new)

            norm=self.logtrapz(np.log(hist_new), x_op)
            norm=np.exp(norm)
            norm = self.hist_norm(x_op, hist_new)
            P[i]=hist_new/norm


            #Check that there are no zero elements too close to the edge
            if min(P[i][:-10])==0:
                print("Probability distribution has too many zero entries.")


        # This is an array of the reweighted histograms
        return P



    def flux(self, *argv):
        """
        The flux parameter of the nucleation rate, can be evaluated analytically
        flux = sqrt(2(opC+OPsym)/pi*V), where vol is the size of the box (Singlet model)
        """
        opC, OPsym, vol = argv
        #print("op+opsym:",opC+OPsym)
        return np.sqrt(8*(opC+OPsym**2)/(np.pi*vol))



    def temp_range(self, benchmark, T_min, T_max, dT):
        """
        Takes in the benchmark data, and the tem range (T_min, T_max, dT)
        Returns the sigma and masq ranges as a function of the temperature for reweighting,
        the 3D action and the estimate at one loop.
        """

        from cosmoTransitions.tunneling1D import SingleFieldInstanton
        import pprint

        # importing local functions
        from dimensional_reduction import (
            dr_xSM,
            singlet_no_cubic_basis,
            singlet_no_linear_basis,
        )

        from nucleation_fits import S0_fit, SH1_fit, omega_fit, alpha_fn, beta_fn

        # new parameter point
        # Ms = 240, sTh = 0.1
        p_xSM = {'a1': 33.87410276678806,
         'a2': 1.5,
         'b1': -513396.88758474897,
         'b3': -111.87732181027066,
         'b4': 0.2581447353400604,
         'mSsq': 11712.788297109793,
         }
        Tc = 98.51253501118245

        print("\nxSM parameters:")
        pprint.pprint(p_xSM)
        ###############################################################################

        # using dr_xSM next

        # using dr_xSM next

        # setting up steps in T
        sigma_list = []
        msq_list = []
        Tre_list = []
        S_3d_list = []
        one_loop = []
        tree_level = []

        T_list = np.linspace(T_max, T_min, num=dT)

        # choosing basis here
        # choosing basis here
        basis_shift_fn = singlet_no_cubic_basis
        # basis_shift_fn = singlet_no_linear_basis

        # initialising (only needed for finding where crosses Tc and T0)
        p_no_cubic_high = dr_xSM(1.01 * Tc, **p_xSM)
        p_no_cubic_high = singlet_no_cubic_basis(**p_no_cubic_high)
        p_no_cubic_old = dr_xSM(T_list[0], **p_xSM)
        p_no_cubic_old = singlet_no_cubic_basis(**p_no_cubic_old)
        compute_bounce = T_max < Tc

        # looping over T
        print("\n3d effective parameters:")
        print(
            "%-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s"
            % ("T", "sigma", "msq", "g", "lam", "eps", "S_0", "S_1", "L_min")
        )
        for T in T_list[1:]:
            # doing DR - only called once per temperature
            p = dr_xSM(T, **p_xSM)
            # shifting to chosen basis
            p = basis_shift_fn(**p)

            # to find Tc and T0, easiest in the g=0 basis
            p_no_cubic = singlet_no_cubic_basis(**p)

            # to find minima (and hence compute bounce) easiest in sig=0 basis
            p_no_linear = singlet_no_linear_basis(**p)

            # expansion parameter
            m = np.sqrt(p_no_linear["msq"])
            eps_g = p_no_linear["g"] ** 2 / (4 * np.pi * m ** 3) / 3
            eps_lam = p_no_linear["lam"] / (4 * np.pi * m)
            eps = np.max([eps_g, eps_lam]) / np.sqrt(3)

            # printing if crossing T0
            if (
                p_no_cubic["sig"] * p_no_cubic_high["sig"] < 0
                and p_no_cubic["sig"] ** 2
                > -8 * p_no_cubic["msq"] ** 3 / 9 / p_no_cubic["lam"]
            ):
                print("----- crossing T0, unstable at tree-level  -----")
                print("-----    (tree-level thick wall limit)     -----")
                p_no_cubic_high = p_no_cubic
                compute_bounce = False

            if compute_bounce:
                # using cosmoTransitions to compute bubble profile and action
                # in d=3
                cosmoTransObj = SingleFieldInstanton(
                    phi_true(p_no_linear["msq"], p_no_linear["g"]),
                    phi_false(p_no_linear["msq"], p_no_linear["g"]),
                    lambda x: V(x, p_no_linear["msq"], p_no_linear["g"]),
                    lambda x: dV(x, p_no_linear["msq"], p_no_linear["g"]),
                    alpha=2,
                )
                profile = cosmoTransObj.findProfile()
                S_3d = cosmoTransObj.findAction(profile)
                # repeating in d=2
                cosmoTransObj = SingleFieldInstanton(
                    phi_true(p_no_linear["msq"], p_no_linear["g"]),
                    phi_false(p_no_linear["msq"], p_no_linear["g"]),
                    lambda x: V(x, p_no_linear["msq"], p_no_linear["g"]),
                    lambda x: dV(x, p_no_linear["msq"], p_no_linear["g"]),
                    alpha=1,
                )
                profile = cosmoTransObj.findProfile()
                S_2d = cosmoTransObj.findAction(profile)

                # nucleation stuff from Andreas's fits
                alpha = alpha_fn(p_no_linear["msq"], p_no_linear["g"], 1)
                beta = beta_fn(p_no_linear["msq"], p_no_linear["g"], 1)
                S0 = beta * S0_fit(alpha)
                S1 = (
                      SH1_fit(alpha)
                      - 3 / 2 * np.log(beta)
                      - 2 * np.log(p_no_linear["msq"])
                      - np.log(np.sqrt(omega_fit(alpha)) / (2 * np.pi))
                     )

                # printing results
                print(
                    "%-12.6f %-12.6f %-12.6f %-12.6f %-12.6f %-12.6f %-12.6f %-12.6f %-12.6f"
                    % (T, p["sig"], p["msq"], p["g"], p["lam"], eps, S_3d, S1, S_3d / S_2d)
                )
            else:
                print(
                    "%-12.6f %-12.6f %-12.6f %-12.6f %-12.6f %-12.6f"
                    % (T, p["sig"], p["msq"], p["g"], p["lam"], eps)
                )

            # printing if crossing Tc
            if p_no_cubic["sig"] * p_no_cubic_old["sig"] <= 0:
                print("----- crossing Tc, critical temperature  -----")
                print("-----         (thin wall limit)          -----")
                compute_bounce = True

            p_no_cubic_old = p_no_cubic
            sigma_list.append(p["sig"])
            msq_list.append(p["msq"])
            Tre_list.append(T)
            S_3d_list.append(S_3d)
            one_loop.append(S0+S1)
            tree_level.append(S0)

        return sigma_list, msq_list, np.asarray(Tre_list), S_3d_list, tree_level, one_loop


# definitions of potential and parameters
# in basis with no linear term, and where lam=1
# for the temp range function, not used outside of this class
def V(x, msq, g):
    return 1 / 2 * msq * x ** 2 + 1 / 6 * g * x ** 3 + 1 / 24 * x ** 4


def dV(x, msq, g):
    return msq * x + 1 / 2 * g * x ** 2 + 1 / 6 * x ** 3


def phi_false(msq, g):
    return 0.0


def phi_true(msq, g):
    return (-3 * g + np.sqrt(9 * g ** 2 - 24 * msq)) / 2


def msqc(g):
    return 1 / 3 * g ** 2
