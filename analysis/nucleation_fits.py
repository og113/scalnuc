# libraries
import numpy as np  # arrays and maths

# fits from arXiv:2104.11804
def f_fit(alpha, A, B, C, D, E, F):
    return (
        A
        + B * alpha
        + C * alpha ** 2
        + D / (1 - alpha)
        + E / (1 - alpha) ** 2
        + F / (1 - alpha) ** 3
    )


def S0_fit(alpha):
    return f_fit(alpha, 7.24, 5.68, 0, 10.4, 1.25, 0)


def SH1_fit(alpha):
    return f_fit(alpha, 0.213, 4.86, -9.56, 3.97, 0.505, 8.78e-4)


def SG1_fit(alpha):
    return f_fit(alpha, 0.895, 1.08, -1.84, 1.51, 0.469, 0.0329)


def omega_fit(alpha):
    return (1 - alpha) * (
        2.39
        - 0.854 * alpha
        + 2.42 * alpha ** 2
        - 9.67 * alpha ** 3
        + 5.85 * alpha ** 4
    )


def alpha_fn(msq, g, lam):
    etaEkstedt = - g / 3
    lamEksted = lam / 3
    return msq * lamEksted / etaEkstedt**2


def beta_fn(msq, g, lam):
    etaEkstedt = - g / 3
    return np.power(msq, 1.5) / etaEkstedt**2
