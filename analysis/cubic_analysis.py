#################### The Cubic Anisotropy Model #############################
from base_analysis import Base
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

class Cubic(Base):


    def load_data_mc(self, filename, labels):
        """
        Loads data from the MC runs in to a numpy array
        Takes the filename as an argument. (Cubic model)
        """

        #columns = ["phi1", "phi2", "phi1sq", "phi2sq", "phi1sq2","phi2sq2", "phi1sqphi2sq",
        #            "phi1_lap", "phi2_lap", "ham", "op", "w"]
        columns = np.genfromtxt(labels, usecols=1, dtype=str) # This returns a re numpy array
        np.char.replace(columns, "^", "")
        columns=columns[:12].tolist() #othrwise genfromtxt won't like it

        data = np.genfromtxt(filename, names=columns)

        print("Loaded MC run data (Cubic model)")

        return data



    def flux(self, *argv):
        """
        The flux parameter of the nucleation rate, can be evaluated analytically
        flux = sqrt(2*opC/pi*vol), where opC is the value of the critical bubble
        and vol is the size of the box
        """
        opC, vol = argv

        return np.sqrt(2*opC/(np.pi*vol))



    def find_critical_mass(self, x, y, V, msq, msq_low, msq_high, crit):
        """
        Finds the value where the peaks of the histogram are of equal height
        i.e. finds the value of the critical mass.
        Arguments: histogram data x and y, V is the size of the box, msq the original
        mass parameter, msq_low the lower edge of the mass range, msq_high the higher
        edge and crit is the approximate location of the critical bubble.
        Return the critical mass and the difference between the integral of the two halves.
        """

        #Create the mass points that where the peaks are evaluated
        msq_new=np.linspace(msq_low, msq_high, 10000)

        # Initialise arrays
        N=len(msq_new)
        P=np.zeros([N,len(y)])
        int_s=np.zeros(N)
        int_b=np.zeros(N)
        dmsq=msq_new[i]-msq
        # Loop through the reweighted histograms
        # When the peaks are of equal height the integrals should be 50-50
        for i in range(N):
            P[i]=reweight1D(V, dmsq, x, y)
            int_s[i], int_b[i]=self.integrate_half(x,P[i], crit)
            plt.figure(2)
            plt.plot(x, P[i])
            plt.yscale('log')

        diff = abs(int_s-int_b) # Calculate diffeence between the areas
        id = diff.argmin() # Find the minimum difference
        msq_c=msq_new[id]  # Critical mass

        if diff[id]>0.05:
            print("The mass range given is not correct.")

        print("The critical mass is: ", msq_c)
        print("The difference between the two integrals is ", diff[id])

        return msq_c, diff[id]



    def compare(self, x, y, paper_x, paper_y):
        """
        Compare the paper to results
        """

        cs_paper = CubicSpline(paper_x, paper_y)
        cs = CubicSpline(x[::-1], y[::-1])

        return cs, cs_paper
