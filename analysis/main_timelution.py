import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import json
from scipy.optimize import curve_fit


if len(sys.argv) != 2:
    print("Usage: python3 main_mc.py options_time")
    sys.exit()
else:
    options_file = np.array(sys.argv)[1]

# Makes sure one can run this script from anywhere
sys.path.insert(0,"/home/koranna/Projects/scalnuc/analysis")

import singlet_analysis as singlet
theory = singlet.Singlet()
print("---------------------Singlet model--------------------")
print("\n-------------------", "Analysis code for time evolution behaviour", "--------------------")
print("For help with the functions try 'python3 -m pydoc <module>\n")

# reading options file
with open(options_file, 'r') as stream:
    json_string = stream.read()

    # parse file
    opts_list = json.loads(json_string)['options']

N_runs = len(opts_list)

# Setting up seaborn
sns.set_context("paper")
sns.set_palette("magma", 5)
colors = sns.color_palette('magma', 5)


plt.rc('font', **{"family": "Times New Roman"})
plt.rc("axes", labelsize=18)
plt.rc("axes", titlesize=18)
plt.rc("xtick", labelsize=18)
plt.rc("ytick", labelsize=18)
plt.rc("legend", fontsize=18)
plt.rc("text", usetex=True)
text_size = 18.0  # for the annotations inside

gammas = np.zeros(N_runs)
dts = np.zeros(N_runs)

for i in range(N_runs):

    labels = opts_list[i]["directory"]+"labels"
    counterterms = theory.load_params(opts_list[i]["directory"]+ "counterterms")
    couplings = theory.load_params(opts_list[i]["directory"]+ "couplings")
    points = theory.load_params(opts_list[i]["directory"] + "points")
    params = theory.load_params(opts_list[i]["directory"]+"params")
    
    # Load strix evolution data
    nucleation = theory.load_crossings(opts_list[i]["directory"]+opts_list[i]["nucleation"])

    one_strix_data_forwards = theory.load_data_time(opts_list[i]["directory"]+opts_list[i]["forwards"], labels)
    one_strix_data_backwards = theory.load_data_time(opts_list[i]["directory"]+opts_list[i]["backwards"], labels)
    op_data = one_strix_data_forwards["order"][0]


    opc = points["OPC"]
    msq = couplings["b2"]
    sigma = couplings["b1"]
    g = couplings["b3"]
    Zm=counterterms["Zm"]
    Zphi=counterterms["Zphi"]

    L = params["Nx"]*couplings["a"]
    vol = L**3 # volume
    epsilon = params["dOP"]
    epsilon = 0.001
    dt = params["dt"]
    dts[i] = dt
    gamma = params["gamma"]
    gammas[i] = gamma

    
    # sigma and msq as a function of temperature
    x_forwards = np.linspace(0, len(one_strix_data_forwards), len(one_strix_data_forwards))
    x_backwards = np.linspace(0, -len(one_strix_data_backwards), len(one_strix_data_backwards))

    plt.figure(1)
    plt.xlabel("sim time")
    plt.ylabel("Energy")
    plt.plot(dt*x_forwards, one_strix_data_forwards["energy"], label = f'$\gamma=${gamma}, $dt=${dt}', color=colors[i])
    plt.plot(dt*x_backwards, one_strix_data_backwards["energy"], color=colors[i])
    plt.legend(loc='best')
    theory.set_fig_specs("energy_vs_simtime")

    plt.figure(2)
    plt.xlabel("Sim time")
    plt.ylabel("Order parameter")
    plt.plot(dt*x_forwards, one_strix_data_forwards["order"], label = f'$\gamma=${gamma}, $dt=${dt}', color=colors[i])
    plt.plot(dt*x_backwards, one_strix_data_backwards["order"], color=colors[i])
    plt.axhline(opc, color='red')
    plt.axhline(op_data, color='green')
    plt.legend(loc='best')
    theory.set_fig_specs("op_vs_simtime")

    plt.figure(3)
    plt.xlabel("Sim time")
    plt.ylabel(r"$\pi$")
    plt.plot(dt*x_forwards, one_strix_data_forwards["mom"],label = f'$\gamma=${gamma}, $dt=${dt}', color=colors[i])
    plt.plot(dt*x_backwards, one_strix_data_backwards["mom"], color=colors[i])
    plt.legend(loc='best')
    theory.set_fig_specs("mom_vs_simtime")

    plt.figure(4)
    plt.xlabel("Sim time")
    plt.ylabel(r"$\phi$")
    plt.plot(dt*x_forwards, one_strix_data_forwards["phi"],label = f'$\gamma=${gamma}, $dt=${dt}', color=colors[i])
    plt.plot(dt*x_backwards, one_strix_data_backwards["phi"], color=colors[i])
    plt.legend(loc='best')
    theory.set_fig_specs("phi_vs_simtime")
    

    dynamic, dynamic_mean, dynamic_error = theory.calc_dynamical_factor_error(nucleation["weight"], nucleation["cross"])
    noise = theory.noise_magnitude(gamma, dt)
    plt.figure(5)
    plt.title("Noise magnitude")
    plt.errorbar(noise, dynamic_mean, yerr= dynamic_error, label = str(gamma), marker='o')
    plt.legend(loc='best')
    plt.savefig("noise_magnitude.png")

    plt.figure(6)
    plt.errorbar(dt, dynamic_mean, yerr= dynamic_error, marker='o')
    plt.ylabel(r"$<d>$")
    plt.xlabel(r"$\Delta t$")


plt.show()
