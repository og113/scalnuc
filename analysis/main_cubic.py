import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
import json


# Read in params
if len(sys.argv)!=2:
    print("pass the options file as an argument")
    sys.exit()
else:
    options_file = np.array(sys.argv)[1]
    import cubic_analysis as cubic
    theory = cubic.Cubic()
    print("---------------------Cubic model--------------------")
    print("\n-------------------", "Analysis code for nucleation rate calculations", "--------------------")
    print("For help with the functions try 'python3 -m pydoc <module>\n")


# reading options file
with open(options_file, 'r') as stream:
    json_string = stream.read()

    # parse file
    opts_list = json.loads(json_string)['options']


for i in range(len(opts_list)):

    print("\n---------------------------", "Load data", "---------------------------")

    labels = opts_list[i]["directory"]+opts_list[i]["label"]
    data = theory.load_data_mc(opts_list[i]["directory"]+opts_list[i]["hists"], labels)
    weight = theory.load_weight(opts_list[i]["directory"]+"weight")
    counterterms = theory.load_params(opts_list[i]["directory"]+opts_list[i]["counterterms"])
    params = theory.load_params(opts_list[i]["directory"]+opts_list[i]["params"])
    couplings = theory.load_params(opts_list[i]["directory"]+opts_list[i]["couplings"])

    crossings = theory.load_crossings(opts_list[0]["directory"]+opts_list[0]["nucleation"])
    prob_x, prob_y = np.genfromtxt(opts_list[0]["directory"]+"paper/prob_paper", unpack=True, usecols=(0,1))
    full_x, full_y = np.genfromtxt(opts_list[0]["directory"]+"paper/full_paper", unpack=True, usecols=(0,1))

    print("-------------------------------------------------------------------\n")

    print("\n---------------------------", "Parameters", "---------------------------")

    L = params["Nx"]
    vol = L**3
    epsilon = params["dOP"]
    dt = params["dt"]
    gamma = params["gamma"]

    msq = couplings["msq"]
    l2 = couplings["l2"]

    Zm=counterterms["Zm"]
    Zphi=counterterms["Zphi"]

    bins = opts_list[i]["bins"]
    crit = opts_list[i]["loc_bubble"]

    coeff_reweight=0.5*vol*Zm*Zphi


    print(f"L  {L} |   msq {msq}  |   l2 {l2}   |  epsilon  {epsilon} |   dt {dt}  |   gamma {gamma}   |   bins {bins}")
    print("-------------------------------------------------------------------\n")


    msq_new=np.array(opts_list[i]["msq_range"])
    dmsq=msq_new-msq

    x, y=theory.get_xy_data(data['op'], data['w'], bins)
    #theory.gen_points_file(x, y, crit, opts_list[0]["hist_length"])

    # Critical mass
    if opts_list[i]["msq_c"]==0:
        print("Evaluating critical mass")
        mcs_c = theory.find_critical_mass(x, y, vol, msq, msq-0.01, msq+0.01, crit)
    else:
        msq_c=opts_list[i]["msq_c"]

    delta_m =  msq_c-msq_new

    #Reweight
    P=theory.reweight1D(coeff_reweight, dmsq, x, y)
    index_array, extrema_points = theory.find_extrema_points(x, P, crit, opts_list[i]["hist_length"])

    plt.figure(1)
    plt.plot(x,y)
    plt.yscale('log')

    N=len(msq_new)


    rate, prob = theory.nucleation_rate(x, P, vol, epsilon, extrema_points, index_array, crossings["weight"], crossings['op'].to_numpy(), crossings['cross'].to_numpy())

    # Thin wall approximation
    '''
    x_thin=np.linspace(0.042,0.052,50)
    m_c_thin=0.0527714
    m_new_thin=m_c_thin-x_thin
    y_thin=err.thin_wall(m_new_thin,l2/8,l2)
    rate_thin=0.05**4*np.exp(-y_thin)'''

    cs, cs_paper = theory.compare(delta_m/(l2**2), np.log(rate/(vol*l2**4)), full_x, full_y )
    cs_prob, cs_paperprob = theory.compare(delta_m/(l2**2), np.log(prob/(vol*l2**3)),prob_x, prob_y)
    colors = plt.cm.viridis(np.linspace(0,1,4))
    plt.figure(2)
    plt.style.use('bmh')
    plt.title("Nucleation rate, improved action cubic 80^3")
    plt.plot(delta_m/(l2**2), np.log(rate/(vol*l2**4)), color=colors[0], label="Full rate")
    plt.plot(delta_m/l2**2, np.log(prob/(vol*l2**3)), color=colors[1], label="Probability")
    plt.plot(prob_x, prob_y, color=colors[2], label="Probability, paper")
    plt.plot(full_x, full_y, color=colors[3], label="Full rate, paper")
    plt.plot(delta_m/l2**2, cs_paper(delta_m/l2**2), color='red')
    plt.plot(delta_m/l2**2, cs(delta_m/l2**2), color='b')
    plt.ylabel("$\log \Gamma/\lambda^4_2$")
    plt.xlabel("$\delta m^2/\lambda_2^2$")
    plt.legend(loc='lower right')
    #plt.plot(m_new_thin/(l2**2), np.log(rate_thin/l2**4), color="orange", label="thin-wall")
    plt.savefig("rate.jpeg")

    plt.figure(4)
    plt.plot(delta_m/l2**2, cs_paper(delta_m/l2**2)-cs(delta_m/l2**2))
    plt.plot(delta_m/l2**2, cs_prob(delta_m/l2**2)-cs(delta_m/l2**2))
    plt.plot(delta_m/(l2**2), cs_paperprob(delta_m/l2**2)-cs_paper(delta_m/l2**2))
    plt.show()
