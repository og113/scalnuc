import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import json
from scipy.optimize import curve_fit

import singlet_newop_analysis as singlet_newop
theory = singlet_newop.Singlet_NewOP()

# Read in params
if len(sys.argv)!=2:
    print("pass the options file as an argument")
    sys.exit()
else:
    options_file = np.array(sys.argv)[1]
    print("---------------------Singlet model--------------------")
    print("\n-------------------", "Analysis code for nucleation rate calculations", "--------------------")
    print("For help with the functions try 'python3 -m pydoc <module>\n")


# reading options file
with open(options_file, 'r') as stream:
    json_string = stream.read()

    # parse file
    opts_list = json.loads(json_string)['options']

N_runs = len(opts_list)

# Setting up seaborn
sns.set_context("paper")
sns.set_palette("magma", 5)
colors = sns.color_palette('magma', 5)


plt.rc('font', **{"family": "Times New Roman"})
plt.rc("axes", labelsize=18)
plt.rc("axes", titlesize=18)
plt.rc("xtick", labelsize=18)
plt.rc("ytick", labelsize=18)
plt.rc("legend", fontsize=18)
plt.rc("text", usetex=True)
text_size = 18.0  # for the annotations inside

dT=10
sigma_new, msq_new, T, S_3d, tree_level, one_loop = theory.temp_range(opts_list[0]["benchmark"], 92.5, 94, dT)

rate_runs = np.zeros([N_runs, dT-1])
error_runs = np.zeros([N_runs, dT-1])
lat_space_runs = np.zeros(N_runs)

for i in range(N_runs):

    print("\n---------------------------", "Load data", "---------------------------")

    labels = opts_list[i]["directory"]+opts_list[i]["label"]
    data = theory.load_data_mc(opts_list[i]["directory"]+opts_list[i]["hists"], labels)
    weight = theory.load_weight(opts_list[i]["directory"]+"weight")
    counterterms = theory.load_params(opts_list[i]["directory"]+opts_list[i]["counterterms"])
    params = theory.load_params(opts_list[i]["directory"]+opts_list[i]["params"])
    couplings = theory.load_params(opts_list[i]["directory"]+opts_list[i]["couplings"])

    nucleation = theory.load_crossings(opts_list[i]["directory"]+opts_list[i]["nucleation"])
    dynamic, dynamic_mean, dynamic_error = theory.calc_dynamical_factor_error(nucleation["weight"], nucleation["cross"])
    print("-------------------------------------------------------------------\n")

    print("\n---------------------------", "Parameters", "---------------------------")

    sigma = couplings["b1"]
    msq = couplings["b2"]
    g = couplings["b3"]
    a = couplings["a"]
    '''
    if  i==0:
        benchmark = opts_list[i]["benchmark"]
        Tmin = 92.5
        Tmax = 93.5
        import singlet_analysis as singlet_old
        old_theory = singlet_old.Singlet()
        sigma_new, msq_new, T, S_3d, tree_level, one_loop, m_no_linear = old_theory.temp_range(benchmark, Tmin, Tmax, 10)


        i_pert = np.argwhere(sigma >= sigma_new)[0][0]
        T = T[i_pert]
        print(T)
        print(sigma_new[i_pert])
        print(msq_new[i_pert])
        print(f"m in sigma=0 basis {m_no_linear[i_pert]}")
    '''

    Zm=counterterms["Zm"]
    Zphi=counterterms["Zphi"]

    Nx = params["Nx"]
    L = Nx*a
    vol = L**3 # volume
    epsilon = params["dOP"]
    OPsym = params["OPsym"]

    # Time evolution params
    dt = params["dt"]
    gamma = params["gamma"]

    bins = opts_list[i]["bins"]
    crit = opts_list[i]["loc_bubble"]

    # Benchamrk and temp range info, for reweighting
    benchmark = opts_list[i]["benchmark"]
    Tmin=opts_list[i]["temp_range"][0]
    Tmax=opts_list[i]["temp_range"][1]
    #dT=opts_list[i]["temp_range"][2]


    # Jackknife resampling
    points_to_remove = 10000 # How many points to remove from the original data

    coeff_linear=vol # sigma, phi
    coeff_square=0.5*vol*Zm*Zphi # msq, phi^2

    print(f"Nx {Nx} |  a {a}  | L  {L} |   msq {msq}  |   sigma {sigma}  |   g {g}  |  epsilon  {epsilon} |   dt {dt}  |   gamma {gamma}   |   bins {bins} | OPsym {OPsym}")
    print("-------------------------------------------------------------------\n")
    #sigma_new, msq_new, T, S_3d, tree_level, one_loop = theory.temp_range(benchmark, Tmin, Tmax, dT)

    # The difference between the 'old' variable and the one we are reweighting to
    dsigma= sigma_new-sigma
    dmsq=msq_new-msq
    x, y = theory.get_xy_data(data['order'], data['weight'], bins)


    # Create hists for the reweight params
    H, x_op, x_lin = theory.get_2d_hist(data["order"], data["phi"], data["weight"], bins)
    #reweight_array = np.zeros([dT, bins])

    
    # Reweight each 2D hist for the given range of dsgima and dmsq
    #for i in range(dT):
    reweight_array = theory.reweight2D(coeff_linear, coeff_square, dmsq, dsigma, x_op, x_lin, H, OPsym)

 
    plt.figure(2+i)
    plt.title(f"Reweighted histograms NX:{Nx}, a:{a}")
    plt.plot(x,y, label="orig")
    plt.plot(x_op, reweight_array[0], label=f'T={T[0]}')
    plt.plot(x_op, reweight_array[-1], label=f'T={T[-1]}')
    plt.yscale('log')
    plt.legend(loc='best')
    plt.xlabel('op')
    plt.ylabel('P')
    theory.set_fig_specs(f"reweight_newop_Nx{Nx}_a{a}")


    points_to_remove = 10000
    
    # Jackknife data for error analysis
    order_jackknifed = theory.jackknife(data['order'], points_to_remove)
    phi_jackknifed = theory.jackknife(data['phi'], points_to_remove)
    weight_jackknifed = theory.jackknife(data['weight'], points_to_remove)

    N_jackknife = len(phi_jackknifed) # How many jackknife resamples we have
    N_reweight = len(dsigma) # How many different values are we reweighting to

    ############################################### SET UP NEEDED ARRAYS  #######################################################

    # Init arrays for all jackknife samples and reweights within each sample
    reweight_array = np.zeros([N_jackknife, N_reweight, bins])
    x_op = np.zeros([N_jackknife, bins]) # The x-axis stays the same for reweights

    # Init arrays for points
    index_array = np.zeros([N_jackknife, N_reweight,3], dtype=int)
    extrema_points = np.zeros([N_jackknife, N_reweight, 6])
    midpoints = np.zeros([N_jackknife, N_reweight, 4])

    # Arrays for jackknifed samples
    flux = np.zeros([N_jackknife, N_reweight])
    prob = np.zeros([N_jackknife, N_reweight])

    # Calculate the bin error for each reweighted histogram
    final_reweights = np.zeros([N_reweight, bins])
    error_reweights = np.zeros([N_reweight, bins])

    # Init arrays for the location and error of the phases
    extrema_points_mean = np.zeros([N_reweight, 6])
    extrema_points_error = np.zeros([N_reweight, 6])

    midpoints_mean = np.zeros([N_reweight, 4])
    midpoints_error = np.zeros([N_reweight, 4])

    ############################################### CALCULATE DYNAMICAL FACTOR #######################################################

    # Only calculate the dynamical factor error if the hist is a "half" lenght one
    if opts_list[i]["hist_length"]=="half":
        dynamic, dynamic_mean, dynamic_error = theory.calc_dynamical_factor_error(nucleation["weight"], nucleation["cross"])


    ############################################### JACKKNIFE DATA AND DO ANALYSIS #######################################################


    # Loop through all the jackknife samples to calculate the error
    for sample in range(N_jackknife):
        # Create hists for the reweight params
        H, x_op[sample], x_lin = theory.get_2d_hist(order_jackknifed[sample],
                                        phi_jackknifed[sample], weight_jackknifed[sample], bins)

        # Reweight each 2D hist for the given range of dsgima and dmsq
        reweight_array[sample] = theory.reweight2D(coeff_linear, coeff_square, dmsq,
                                    dsigma, x_op[sample], x_lin, H, OPsym)

        # For each reweight param (in one sample), calculate the location of the phases (index, extrema & midpoints)
        for reweight_param in range(N_reweight):
            index_array[sample][reweight_param]  = theory.find_extrema_index(x_op[sample],
                                                    reweight_array[sample][reweight_param], crit,
                                                    opts_list[i]["hist_length"])

            extrema_points[sample][reweight_param],\
            midpoints[sample][reweight_param] = theory.find_points(x_op[sample],
                                                reweight_array[sample][reweight_param],
                                                index_array[sample][reweight_param])



        # Calculate the bubble nucleation rate
        flux[sample], prob[sample] = theory.nucleation_rate(x_op[sample],
                                        reweight_array[sample], nucleation["weight"],
                                        nucleation["op"], nucleation["cross"], vol, epsilon,
                                        extrema_points[sample], index_array[sample], OPsym)

    ############################################### BIN ERROR FOR ALL REWEIGHTED HISTS #######################################################

    for reweight_param in range(N_reweight):
        hist_for_reweight = np.zeros(bins)
        for bin_no in range(bins):
            final_reweights[reweight_param][bin_no] = np.mean(reweight_array[:,reweight_param,:][:,bin_no])
            error_reweights[reweight_param][bin_no] = np.sqrt(np.sum((np.longdouble(reweight_array[:,reweight_param,:][:,bin_no]
                                                    -final_reweights[reweight_param][bin_no]))**2)*(N_jackknife-1)/N_jackknife)



    ############################################### JACKKNIFE ERRORS FOR EVERYTHING ELSE #######################################################

    # Loop over each point, xs, ys, xc, yc, xb and yb
    for j in range(6):
        if j < 4:
            midpoints_mean[:,j], midpoints_error[:,j] = theory.jackknife_error(midpoints[:,:,j], N_reweight, N_jackknife)

        extrema_points_mean[:,j], extrema_points_error[:,j] = theory.jackknife_error(extrema_points[:,:,j], N_reweight, N_jackknife)



    # Calculate the error related to each jackknifed quantity
    rate = 0.5*prob*flux
    rate_mean, rate_error = theory.jackknife_error(rate, N_reweight, N_jackknife)
    prob_mean, prob_errors = theory.jackknife_error(prob, N_reweight, N_jackknife)
    flux_mean, flux_errors = theory.jackknife_error(prob, N_reweight, N_jackknife)

    # Calculate the error first, so in only contains the prob*flux part
    rate_error = np.sqrt((rate_error/rate_mean)**2+(dynamic_error/dynamic_mean)**2)
    rate_mean = rate_mean*dynamic_mean # And finally we have the full rate


    plt.figure()
    plt.plot(T, np.log(rate_mean/vol), label=f'Nx:{Nx}')
    plt.legend(loc='best')
    plt.fill_between(T, np.log(rate_mean/vol)-rate_error,
                    np.log(rate_mean/vol)+rate_error, alpha=0.5)
    plt.ylabel("$\ln(\Gamma/V)$")
    plt.xlabel("T")

    print(T)
    print(np.log(rate_mean/vol))
    
    rate_runs[i]  = rate_mean/vol
    error_runs[i]  = rate_error
    lat_space_runs[i] = a

    plt.figure(0)
    if i==0:
        plt.errorbar(a, np.log(rate_mean[-1]/vol), yerr=rate_error[-1], marker='o', capsize=10, color=colors[2], label=f"T={T[-1]}")
    else:
        plt.errorbar(a, np.log(rate_mean[-1]/vol), yerr=rate_error[-1], marker='o', capsize=10, color=colors[2])
   

xfit = np.linspace(0, 3.5, 1000)
print(dT-1)

for t in range(dT-1):
    rate_dT = rate_runs[:,t]
    error_dT = error_runs[:,t]

    #  Lattice spacing
    popt_cubic, pcov_cubic, perr_cubic, rsq_cubic = theory.fit_func(theory.cubic, lat_space_runs, np.log(rate_dT), error_dT, *(1,1))

    plt.figure(0)
    plt.plot(xfit, theory.cubic(xfit, *popt_cubic), linestyle='--', color=colors[0])
    plt.errorbar(0, popt_cubic[0], yerr=perr_cubic[0], fmt='s', color=colors[0], capsize=10, label=r"$\log(\Gamma/\lambda_3^4)={:.2f}({:.0f})$".format(popt_cubic[0], perr_cubic[0]*100))
    
    
    print(f"Fitting all data points (Cubic fit) at T={T[t]}")
    print(f"Goodness of fit: {rsq_cubic}")
    print(f"Error on a = {popt_cubic[0]} cubic fit {perr_cubic[0]}")
    print(f"Error on b = {popt_cubic[1]} in cubic fit {perr_cubic[1]}")


plt.figure(0)
plt.xlabel(r"$a\lambda_3$", fontsize=text_size)
plt.ylabel(r"$\log(\Gamma/ \lambda_3^4)$", fontsize=text_size)
plt.legend(loc='best')
#theory.set_fig_specs("a_limit_paper_reweight_do not ignore12")

plt.show()
    