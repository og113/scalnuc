import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

import singlet_analysis as singlet
theory= singlet.Singlet()


plt.rc('font', **{"family": "Times New Roman"})
plt.rc("axes", labelsize=18)
plt.rc("axes", titlesize=18)
plt.rc("xtick", labelsize=18)
plt.rc("ytick", labelsize=18)
plt.rc("legend", fontsize=18)
plt.rc("text", usetex=True)
text_size = 18.0  # for the annotations inside

sns.set_context("paper")
sns.set_palette("magma", 5)
colours = sns.color_palette('magma', 5)
# colours, taken from the seaborn colorblind palette
'''
colours = [
    "#029e73", #green
    "#de8f05", #orange
    "#0173b2",  #blue
    "#56b4e9", #lighter blue
    "#ece133", #yellow
    "#949494", #grey
    "#fbafe4", #pink
    "#ca9161", #light orange
    "#cc78bc", # darker pink
    "#d55e00", #darker orange
    ]
'''
directory = "/home/koranna/Projects/scalnuc/run/puhti/final_runs/improved/full_range/benchmark2/wider_range/"

labels =  directory + "labels"
data = theory.load_data_mc( directory+"data", labels)
params = theory.load_params(directory + "params")
couplings = theory.load_params(directory + "couplings")

bins = 100
x, y  = theory.get_xy_data(data['order'], data['weight'], bins) 

index_array  = theory.find_extrema_index(x, y, 0.0, 'full')
extrema_points, midpoints = theory.find_points(x, y, index_array)
ops_x = extrema_points[0]
ops_y = extrema_points[1]
opc_x = extrema_points[2]   
opc_y = extrema_points[3]
opb_x = extrema_points[4]
opb_y = extrema_points[5]

names = ['OPSX', 'OPSY', 'OPCX', 'OPCY', 'OPBX', 'OPBY']

with open(directory+'points_annotated', 'w') as f:
    for i in range(len(names)):
        f.write(names[i] + "\t" + str(extrema_points[i]) + '\n')


plt.figure(0)
plt.plot(x, y, color=colours[0])
plt.yscale('log')
plt.xlabel(r"$\theta$", fontsize=text_size)
plt.ylabel(r"$P(\theta)$", fontsize=text_size)
plt.tick_params(axis='both', which='both', bottom=False, top=False, left=False, labelbottom=False, labelleft=False)
plt.scatter(ops_x, ops_y, marker='o', color=colours[2])
plt.scatter(opc_x, opc_y, marker='o', color=colours[3])
plt.scatter(opb_x, opb_y, marker='o', color=colours[4])

offset = 0.05
ax = plt.gca()
ax.annotate("Symmetric phase", (ops_x-0.1, 100*ops_y), fontsize=text_size)
ax.annotate("Critical phase", (opc_x-0.1, 10e-10*opc_y),fontsize=text_size)
ax.annotate("Broken phase", xy=(opb_x-0.1, 100*opb_y), fontsize=text_size)
ax.annotate(r"$\}$",fontsize=text_size+4, xy=(opc_x-offset, 10e4*opc_y), rotation=90)
ax.annotate(r"$\epsilon$", (opc_x-0.01, 10e6*opc_y),fontsize=text_size)

ax.set_ylim([10e-125, 10e20])
theory.set_fig_specs("full_hist_annotated")


plt.show()
