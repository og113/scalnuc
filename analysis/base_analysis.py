"""
Tools module. Contains the nucleation rate calculation, finding of the critical
mass, reweighting, generation of the points array and the points file
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks, peak_prominences, peak_widths
from scipy.integrate import trapz
from scipy.special import logsumexp
from scipy.interpolate import CubicSpline
from scipy.optimize import curve_fit
from scipy.stats import linregress
from scipy.signal import savgol_filter
import scipy.interpolate
import sys

################### The Base Class ###########################################

class Base():

    def load_data_time(self, filename, labels):
        """
        Loads data from a real time evolution run into a numpy array
        Takes the filename and the labels the arguments.
        """

        columns = np.genfromtxt(labels, usecols=1, dtype=str)
        columns = np.char.replace(columns, "^", "")
        columns = columns.tolist()

        data = np.genfromtxt(filename, names=columns)

        print("Loaded real time evolution data")

        return data



    def load_params(self, filename):
        """
        Load run params, counterterms, couplings
        """

        names = np.genfromtxt(filename, usecols=0, dtype=str)
        values = np.genfromtxt(filename, usecols=1, dtype=float)
        data = dict(zip(names, values))

        print('Loaded params')

        return data




    def load_weight(self, filename):
        """
        Loads the weight file generated in phase 1
        """

        columns = ["op", "w"]
        data =  np.genfromtxt(filename, usecols=(0,1), names=columns)

        print("Loaded weight file")

        return data



    def load_points(self, filename):
        """
        Load points file, takes name of the filename as an argument
        """

        points = np.genfromtxt(filename, usecols=(1))

        print("OPS           OPC        OPB         OPMS       OPMB")
        print(points, "\n")

        points = np.transpose(points[np.newaxis]) # Flip the rows into columns

        print("Loaded points file")

        return points



    def load_crossings(self, filename):
        """
        Load the crossings file generated in the real time evolution runs
        The columns are yn (yes/no tunnelling?), count (how many times has crossed the strix),
        phase_b (phase at the beginning), phase_e (phase in the end) and
        op (order parameter for the specific strix config). Function returns a
        DataFrame object.
        """

        columns = ["yn", "cross", "phase_b", "phase_e", "op", "weight"]
        data =  np.genfromtxt(filename, usecols=(0,1, 2, 3, 4, 5),
                dtype=None, encoding=None, names=columns) # Encoding needed for strings

        print("Loaded crossings file")

        return data



    def get_xy_data(self, op, w, bin_no):
        """
        Get the data from a histogram, creates an x-axis where the middle value
        of the bin is taken
        Takes op (order parameter), w (weight) and bin_no (number of bins)
        as arguments
        """

        # get data from the histogram, binE are the bin edge values
        y, binE = np.histogram(op, bins=bin_no, weights=np.exp(-w), density=True)

        # Generate the x-axis values
        x = 0.5*(binE[1:]+binE[:-1]) # Middle of the bins

        #print("Created a weighted histogram of the order parameter\n")

        return x, y



    def convert_to_int(self, data):
        """
        For doing statistic on the tunnel: yes/no parameter. Convert to int.
        """

        N=len(data)
        data_array = np.zeros(N)

        for i in range(N):
            if data[i]=='YES':
                data_array[i]=1

        return data_array



    def hist_norm(self, x, y):
        """
        Function for normalising histograms, returns the norm
        """

        interp = CubicSpline(x, y)
        norm = interp.integrate(x[0], x[-1])
        return norm



    def noise_magnitude(self, gamma, dt):
        """
        Noise magnitude function from cubic paper fig. 11
        gamma is the damping term and dt the timestep
        """

        return (1-np.exp(-2*gamma*dt))**(1/2)



    def integrate_half(self, x, y, crit):  #Use cubic spline instead, only needed here
        """
        Integrate from symmetric phase (int_sc) to critical and from critical to
        broken phase (int_bc). Takes data x, y  as an argument, approximate location
        for the critical phase is needed (crit)
        Returns int_sc and int_bc.
        """

        norm = np.trapz(y,x)
        i_s, i_c, i_b = self.find_extrema_index(x, y, crit) # Find the location of the phases

        yb_half = y[i_c:]
        ys_half = y[:i_c]

        xb_half = x[i_c:]
        xs_half = x[:i_c]

        int_bc = np.trapz(yb_half, xb_half)/norm
        int_sc = np.trapz(ys_half, xs_half)/norm

        return int_sc, int_bc



    def logtrapz(self, lnf, x):
        """
        Logarithmic integral, lnf is the logarithm of the function.
        Thank you Matt Pitkin on stackexchange
        https://math.stackexchange.com/questions/451771/how-to-numerically-integrate-expression-using-its-log-transform
        """

        deltas = np.log(np.diff(x))
        return -np.log(2.) + logsumexp([logsumexp(lnf[:-1]+deltas), logsumexp(lnf[1:]+deltas)])



    def reweight1D(self, coeff, dvariable, x, y):
        """
        Reweight the probability distribution.  y(x) = P(op). Uses logarithms for
        the calculations due to under/overflow.
        Returns the new probability distribution
        """
        N = len(dvariable)
        P = np.zeros([N, len(y)])

        #Loop over the msq_new array
        for i in range(N):
            hist_new=np.exp(-coeff*dvariable[i]*x+np.log(np.longdouble(y)))

            norm=self.logtrapz(np.log(hist_new), x)
            norm=np.exp(norm)
            P[i]=hist_new/norm


            #Check that there are no zero elements too close to the edge
            if min(P[i][:-10])==0:
                print("Probability distribution has too many zero entries.")


        return P



    def r_squared(self, y, y_hat):
        """
        The goodness of the fit
        """
        y_avg = np.mean(y)
        # residual sum of squares
        ss_res = np.sum((y-y_hat)**2)

        # total sum of squares
        ss_tot = np.sum((y-y_avg)**2)

        # r-squared
        rsq = 1-(ss_res/ss_tot)

        return rsq

    def chi_squared(self, data, fit, popt, error):

        chi = np.sum((data-fit)**2/error**2)

        dof = len(data)-len(popt)

        return chi/dof

    def find_extrema_index(self, x, y, crit, value):
        """
        Finding the extrema points from given data x,y
        crit is the approximate location for the critical bubble,
        usually set to 0.6
        Returns the index for symmetric, critical and broken phases
        """

        peaks, _ = find_peaks(y)
        #prominences = peak_prominences(y, peaks)[0]
        results_half = peak_widths(y, peaks, rel_height=1)

        peak_width = results_half[0]
        i_max = peak_width.argmax()
        peak_width[i_max]=0    # "Remove" the max value
        i_smax = peak_width.argmax()   # Find the second highest index
        i_max = peaks[i_max]    # Match them to the peak indices
        i_smax = peaks[i_smax]


        if value == 'half':
            i_s = i_max
            i_b = len(y)-1 #artifically set it to be the last point in the array

            y_ccut = y[i_s:]
            i_c = y_ccut.argmin()+i_s

        else:

            # Check which one is symmetric and which broken
            if i_max < i_smax:
                i_s = i_max
                i_b = i_smax
            else:
                i_s = i_smax
                i_b = i_max

            if (x[i_s]>crit):

                part=len(y[x<crit])
                y_scut = y[:part]
                i_s=y_scut.argmax()

                y_bcut = y[part:]
                i_b = y_bcut.argmax()+len(y_scut)

                y_ccut=y[i_s:i_b]
                i_c = y_ccut.argmin()+len(y[:i_s])

            else:
                y_cut = y[i_s:i_b]  # Find the minimun value between symmetric and broken
                k = len(y[0:i_s]) # Find how many indices were before symmetric phase
                i_c = y_cut.argmin()+k  # Sum them together to find the correct point

        return i_s, i_c, i_b


    def fit_quadratic(self, x, y, index):

        i_low = int(index-20)
        i_high = int(index+20)

        xdata= x[i_low:i_high]
        ydata= y[i_low:i_high]
        popt, pcov = curve_fit(quadratic, xdata, ydata)

        xfit = np.linspace(x[i_low], x[i_high], 100)

        if popt[0]>0:
            i = np.argmin(quadratic(xfit, *popt))
        else:
            i = np.argmax(quadratic(xfit, *popt))

        yfit = quadratic(xfit, *popt)

        plt.figure(2)
        plt.plot(xfit, yfit)
        print(popt[0])

        return xfit[i], yfit[i]


    def find_points(self, x, y, index_array):
        """
        Find the midpoints between symmetric-critical (sc) and critical-broken (bc)
        Takes in data x, y  as an argumnet as well as an index array that Contains
        the locations of the phases.
        Returns the x and y values for those midpoints
        """

        i_s, i_c, i_b = index_array
        # Find the min and max points

        # The x values for symmetric, critical and broken phase
        xs = x[i_s]
        xc = x[i_c]
        xb = x[i_b]

        # The y values for symmetric, critical and broken phase
        ys = y[i_s]
        yc = y[i_c]
        yb = y[i_b]
        try:
            # The midpoint value for symmetric-critical phase
            tmp = y[i_s:i_c] # Cut of the list at the broken point
            k = len(y[0:i_s])
            ysc = np.sqrt(ys*yc) # Geometric mean
            i = np.where(tmp==min(tmp, key=lambda a:abs(a-ysc)))[0]+k
            xsc = x[i[0]]
            # Map the y value into the actual data
            ysc = y[i[0]]

            # The midpoint value for critical-broken phase
            tmp = y[i_c:i_b] # Cut of the list at the broken point
            k = len(y[0:i_c])
            ybc = np.sqrt(yc*yb) # Geometric mean
            i = np.where(tmp==min(tmp, key=lambda a:abs(a-ybc)))[0]+k
            xbc=x[i[0]]
            ybc = y[i[0]]

            midpoints = np.array([xsc, ysc, xbc, ybc])

        except ValueError:
            print("Evaluating middle points fails, mistake in critical bubble location?")
            print("Or are you perhaps only plotting the symmetric phase?")

            midpoints = np.array([0, 0, 0, 0])

        extrema_points = np.array([xs, ys, xc, yc, xb, yb])

        return extrema_points, midpoints



    def find_extrema_points(self, x, y_reweight, crit, value, error):
        """
        Finds the x and y values of the extrema points only
        Returns the values for symmetric, critical and broken phases
        """

        N = len(y_reweight)
        extrema_points = np.zeros([N, 6])
        midpoints = np.zeros([N, 4])
        index_array = np.zeros([N, 3], dtype=int)

        for i in range(N):
            index_array[i] = self.find_extrema_index(x, y_reweight[i], crit, value)
            extrema_points[i], midpoints[i] = self.find_points(x, y_reweight[i], index_array[i])



        return index_array, extrema_points, midpoints



    def gen_points_file(self, path, extrema_points, midpoints):
        """
        Generates a file containing the extrema and midpoints.
        The points file is needed for the real time evolution run.
        Generates a figure called probability_test.pdf.
        """

        xsc, ysc, xbc, ybc = midpoints
        xs, ys, xc, yc, xb, yb = extrema_points
        x_ex = np.array([xs, xc, xb, xsc, xbc])

        # Write the location of points to a file, to be used in timelution
        names = ['OPS', 'OPC', 'OPB', 'OPMS', 'OPMB']

        with open(path+'points', 'w') as f:
            for i in range(len(names)):
                f.write(names[i] + "\t" + str(x_ex[i]) + '\n')


        print("Generated a points file\n")




    def trajectories(self, weight, cross):
        """
        Calculate the trajectories, see eq. 5.9 in Anna's thesis
        This is the dynamical prefactor <d>=<delta_tunnel/N_crossings>
        Takes in the weight, order parameter op and how many crossings
        the system made as the arguments.
        Returns the dynamical factor <d>.
        """

        #w = find_weights(weight, op) #old method before w for each strix was saved
        div = np.sum(np.exp(-weight)) # Total amount of trajectories

        N_crossings = np.copy(cross) # Make a copy of the original
        N_crossings[N_crossings==0]=1 # Avoid division by zero

        cross[cross%2==0]=0 # Even entries do not tunnel
        cross[cross%2!=0]=1 # Odd entries tunnel

        num = np.sum(cross*np.exp(-weight)/N_crossings)
        res=num/div

        return res



    def probability(self, x, y, epsilon, opC, index):
        """
        Calculates the probability part of the nucleation rate.
        That is the integrated ratio of P_op/(P_s*epsilon).
        """
        i_s, i_c, i_b = index
        i_low=i_c-5
        i_high=i_c+5

        #Probability of the symmetric phase (this is the easy well-behaving part)
        P_s=np.exp(self.logtrapz(np.log(y[:i_c]), x[:i_c]))

        # To avoid unnecessary runs, check if using the correct params file
        if epsilon==0:
            print("You are using a params file that has epsilon set to zero you dingus")
            sys.exit()

        # Makes sure our interpolation range is larger than epsilon
        while epsilon > np.abs(x[i_low]-x[i_high]):
            i_low= i_low-1
            i_high=i_high+1

        # Generate more data points to make the integration smoother
        new_length = 100
        new_x = np.linspace(x[i_low], x[i_high], new_length)
        new_y = scipy.interpolate.interp1d(x[i_low-1:i_high+1], np.longdouble(y[i_low-1:i_high+1]), kind='nearest')(new_x)


        '''
        plt.figure()
        plt.plot(x,y)
        plt.plot(new_x,new_y, color='orange')
        plt.yscale('log')
        plt.show()
        plt.close()
        '''
        # Set the indices to actually match the epsilon range
        i_low = np.argwhere(new_x<=x[i_c]-epsilon/2)[-1][0]
        i_high = np.argwhere(new_x>=x[i_c]+epsilon/2)[0][0]
        # Probability of our bubble being in the critical phase (problem child)
        P_op=np.exp(self.logtrapz(np.log(new_y[i_low:i_high]), new_x[i_low:i_high]))

        P=P_op/(P_s*epsilon)

        return P



    def nucleation_rate(self, x, y_reweight, weight, op, cross, vol, epsilon, extrema_points, index_array, OPsym):
        """
        Calculate the nucleation rate at different values of msq. Arguments are:
        the reweighted histogram data x, y, the location of the critical bubble opC,
        size of the box V and epsilon is the area around the critical bubble.
        Returns the flux and probability parts.
        """

        #Initialize arrays
        N=len(y_reweight)
        prob_array=np.zeros(N)
        flux_array=np.zeros(N)

        opC_array=extrema_points[:,2]

        # Loop through the mass values
        for i in range(N):
            flux_array[i] = self.flux(opC_array[i], OPsym, vol)
            prob_array[i] = self.probability(x, y_reweight[i], epsilon, opC_array[i], index_array[i])

        #d = self.trajectories(weight, cross)
        #rate = prob_array*flux_array*d
        #rate = prob_array*flux_array

        return flux_array, prob_array



    def free_energy(self, extrema_points):
        """
        Free energy, the probability at opC divided by probability at opS
        The definition can be found in The Paper on page 26
        Takes the extrema points as an argument
        Return -ln(P_c/P_s)
        """

        P_s = extrema_points[:,1] # Height of the symmetric peak
        P_c = extrema_points[:,3] # Height of the critical 'peak'


        return -np.log(P_c/P_s)



    def find_weights(self, weight, op_strix):
        """
        (THIS IS AN OLD RETIRED FUNCTION)
        Find the weights for the critical order parameters
        This depends on the initial field configuration (strix files)
        Needed for dynamical factor calculation
        """

        # Load the weight file
        xx, yy =  weight

        # Initialise arrays
        N=len(op_strix)
        find_y = np.zeros(N)
        find_x = np.zeros(N)

        # Find the closest weight value for a specific value of opC
        for i in range(N):
            tmp= min(xx, key=lambda x:abs(x-op_strix[i]))
            find_x[i]=tmp
            find_y[i]=yy[np.where(xx==tmp)[0][0]]

        # Plot the different weight values as function of the critical op values
        # This is mostly for checking that everything works like it should
        plt.figure()
        plt.plot(find_x,np.exp(-find_y))
        plt.yscale('log')
        plt.savefig("weight_strix.pdf")
        plt.close()

        return find_y



    def error_global(x):
        """
        Total accumulated error (global truncation error)
        """

        return np.abs(x[-1]-x[0])/x[0]



    def error_local(x, i):
        """
        Error caused by one iteration (local truncation error)
        """

        return np.abs(x[i+1]-x[i]/x[i])



    def error_bootstrap(rho, rho_m, M):
        """
        Error for bootstrap
        """

        return np.sqrt((M-1)/M*np.sum((rho_m-rho)**2))



    def jackknife(self, data, n):
        """
        The resampling part of jackknife, n is the amount of data points you want to
        remove. Returns resamples.
        """

        N = len(data)
        m = N//n    # How many resample arrays to create
        int_array = np.arange(n) # Generate int array from 0 to N-1
        resamples = np.zeros([m, N-n])  # Table of the cut values

        for i in range(m):
            resamples[i] = np.delete(data, int_array) # Remove certain indices from the data
            int_array = int_array+n  # Up the int_array to the next indices

        return resamples



    def jackknife_error(self, jackknifed_quantity_array, N_reweight, N_jackknife):
        """
        Calculates the error of a jackknifed quantity
        Note the dimensionality of the array
        """

        quantity_mean = np.zeros(N_reweight)
        quantity_errors = np.zeros(N_reweight)

        for i in range(N_reweight):
            quantity_mean[i] = np.mean(jackknifed_quantity_array[:,i])
            quantity_errors[i] =  np.sqrt(np.sum((np.longdouble(jackknifed_quantity_array[:,i]-quantity_mean[i]))**2)*(N_jackknife-1)/N_jackknife)


        return quantity_mean, quantity_errors



    def bootstrap(data, M):
        """
        Bootstrap method to estimate the error, M is the amount of blocks
        to divide the data into. Return the mean and variance.
        """

        N = len(data)

        data_split = np.array_split(data, M) # Split data into M blocks
        rn = np.random.randint(M, size=M) # array of RNs, allows double sampling

        new_data = np.zeros(M)
        for i in range(len(rn)):    # Generate a new data set from the blocks
            new_data[i] = data_split[rn[i]]

        new_data = np.reshape(new_data, N) # 1D array

        x_mean = np.mean(new_data)
        x_var = np.var(new_data)


        return x_mean, x_var



    def bootstrap_iter(data, M, N):
        """
        N is how many times to do the bootstrap analysis
        rho is some variable that the bootstrap returns and we estimate
        the error of this variable
        """

        res_rho = np.zeros(N)

        for  i in range(N):
            mean, var = bootstrap(data,M)
            res_rho[i]=mean

        tmp = np.sum(res_rho/N)
        err = np.sqrt(1/(N-1)*np.sum(res_rho-tmp)**2)

        return err



    def bin_error_from_resamples(self, resample_x, resample_y, bins, N_jackknife):
        """
        Get jackknifed error from histrogram data
        """

        x_mean = np.zeros(bins)
        x_error = np.zeros(bins)

        y_mean = np.zeros(bins)
        y_error = np.zeros(bins)

        for i in range(bins):
            x_mean[i] = np.mean(resample_x[:,i])
            x_error[i] = np.sqrt(np.sum(np.longdouble(resample_x[:,i]-x_mean[i])**2)*(N_jackknife-1)/N_jackknife)

            y_mean[i] = np.mean(resample_y[:,i])
            y_error[i] = np.sqrt(np.sum(np.longdouble(resample_y[:,i]-y_mean[i])**2)*(N_jackknife-1)/N_jackknife)


        return x_mean, x_error, y_mean, y_error



    def calc_dynamical_factor_error(self, weight, cross):
        """
        Calculate the error related to the trajectories.
        Takes the weights and the amount of crossings as an argument
        """

        # Jackknife the data
        cross_jackknifed = self.jackknife(cross, 50)
        weight_jackknifed = self.jackknife(weight, 50)

        # Initialise arrays
        N_jackknife = len(weight_jackknifed)
        dynamical_factor_array = np.zeros(N_jackknife)

        # Loop over the new data sets
        for i in range(N_jackknife):
            dynamical_factor_array[i] = self.trajectories(weight_jackknifed[i], cross_jackknifed[i])

        # Calculate the mean and the error
        dynamic_mean = np.mean(dynamical_factor_array)
        dynamic_error = np.sqrt(np.sum((dynamical_factor_array-dynamic_mean)**2)*(N_jackknife-1)/N_jackknife)
        print("dyn:", dynamic_mean)
        print("dyn error:", dynamic_error)

        return dynamical_factor_array, dynamic_mean, dynamic_error



    def autocorrelation(data, k):
        """
        Autocorrelation function
        """
        N = len(data)
        xi = data[:int(N-k)]
        xik = data[int(k)::]

        x = np.sum(data)/N
        xsq = np.sum(data**2)/N
        xx = np.sum(xi*xik)/(N-k)

        return (xx-x**2)/(xsq-x**2)



    def autocorrelation_iter(data, k):
        """
        Loopity loop or autocorrelation
        """

        N= len(k)
        C=np.zeros(N)

        for i in range(N):
            C[i]= autocorrelation(data,k[i])

        return C



    def thin_wall(msq, l1, l2):
        """
        The action for thin-wall approximation. Arguments are the mass parameter squared
        (msq) and the lambda1 and lambda2 terms.
        """

        m_c=0.0527714 #Pre-calculated value from mathematica
        sig=sigma(l1,l2)
        x= msq*l2**3/(16*np.pi*l1**2) # Also called epsilon, the diff between b and s phases

        return 16*np.pi*sig**3/(3*(x)**2)



    def sigma(l1, l2):
        """
        Wall tension for thin-wall approximation. Takes in lambda1 and lambda2 as
        arguments.
        """

        sigma =l2**(9/2)/(192*np.sqrt(6)*np.pi**3*l1**(5/2))

        return sigma



    def set_fig_specs(self, figname):
        """
        What it says on the tin, set the font size etc and save figure both as
        pdf (paper) and png (presentations). Takes in the figure name as an argument.
        """

        plt.tight_layout()

        plt.savefig(f"figures/png/{figname}.png", dpi=300) # For presentations
        plt.savefig(f"figures/pdf/{figname}.pdf", dpi=300)



    def tunnel_stats(self, data, plot_label):
        """
        Plots the statistics of a time evolution run. Used to see how the different
        real time evolution algorithms behave.
        Arguments: DataFrame (df) of the crossings data, plot_label to indicae the
        run/algorithm.
        """

        import pandas
        from collections import Counter

        yn_counts = Counter(data["yn"])
        df = pandas.DataFrame.from_dict(yn_counts, orient='index')
        plt.style.use('seaborn-deep')

        d = {'col1': data["phase_b"], 'col2': data["phase_e"]}
        df_phases = pandas.DataFrame(data=d)

        plt.figure()
        plt.hist(data["op"])
        self.set_fig_specs(f"hist_{plot_label}")
        plt.close()

        plt.figure()
        df.plot(kind='bar')
        plt.title("Tunnel? Yes/No")
        self.set_fig_specs(f"yn_hist_{plot_label}")
        plt.close()

        phase=df_phases["col1"]+df_phases["col2"]
        plt.figure()
        phase.value_counts().plot(kind='bar')
        plt.title("S=symmetric, B=Broken")
        self.set_fig_specs(f"phases_hist_{plot_label}")
        plt.close()



    def does_bubble_fit(self, op_sym, op_critical, op_broken, T):
        """
        Does the bubble fit inside the box?
        """
        N = len(op_sym)
        X = np.zeros(N)
        T_tm = []

        for i in range(N):
            X[i] =  (op_critical[i]-op_sym[i])/(op_broken[i]-op_sym[i])

            if X[i] > 4*np.pi/81:
                print("Bubble does not fit inside the box, x=", X[i])
                print("T=", T[i])
                T_tm.append(T[i])

        return X, T_tm



    def straight_line(self, x, a, b):
        """
        What it says on the tin, a straight line for fitting
        """

        return a+ b*x



    def straight_line_one_param(self, x, a):
        """
        just a straight line
        """
        return a


    def quad(self, x, a, b):
        """
        Fit quadratic with less params
        """
        return a + b*x**2



    def cubic(self, x, a, b):
        """
        cubic
        """

        return b*x**3 + a


    def exponential(self, x, a, b, c):
        """
        Fit something exponential
        """

        return a + b*np.exp(-c/x)



    def fit_func(self, func, xdata, ydata, yerror, *args):
        """
        Takes the function we are fitting to as an argument
        Returns the fit, error and r² value for the fit
        """
        popt, pcov = curve_fit(func, xdata, ydata, sigma=yerror, p0=args, absolute_sigma=True)
        perr = np.sqrt(np.diag(pcov))
        #rsq = self.r_squared(ydata, func(xdata, *popt))
        chi = self.chi_squared(ydata, func(xdata, *popt), popt, yerror)

        return popt, pcov, perr, chi



def quadratic(x, a, b, c): # Not needed outside this class
    """
    Quadratic polynomial for fitting
    ax^2+bx+c
    """

    return a*x**2+b*x+c



def fill_square(x, a, x0, c): # Not needed outside this class
    """
    Quadratic polynomial for fitting
    a(x-x0)^2+c
    """

    return a*(x-x0)**2+c
