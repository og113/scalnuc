import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import sys
import json

# Import the signlet model
import singlet_analysis as singlet
theory = singlet.Singlet()


def smooth_field(arr_orig, Nx, Ny, Nz, smooth_iters):
    """
    Smooth out the UV from the fields (nearest neighbour averaging)
    """

    a = 0.5
    b = 1/(4*3)

    for smooth in range(smooth_iters):
        arr_3dtmp = np.zeros([Nx,Ny,Nz]) #Zero tmp field between iters
        for i_x in range(Nx):
            for i_y in range(Ny):
                for i_z in range(Nz):
                    # Boundary conditions are peridodic
                    arr_3dtmp[i_x,i_y,i_z] = a*arr_orig[i_x,i_y,i_z]+b*(arr_orig[(i_x-1)%Nx,i_y,i_z]+arr_orig[(i_x+1)%Nx,i_y,i_z]
                                        +arr_orig[i_x,(i_y-1)%Ny,i_z]+arr_orig[i_x,(i_y+1)%Ny,i_z]
                                        +arr_orig[i_x,i_y,(i_z-1)%Nz]+arr_orig[i_x,i_y,(i_z+1)%Nz])

        arr_orig = np.copy(arr_3dtmp) # Copy the array, use np.copy to avoid same memory location

    return arr_orig



def how_much_to_shift(arr_orig, Nx, Ny, Nz):
    """
    Figure out how much you need to shift the bubble in order to have
    it sit nicely in the middle of the box
    """

    # Forking 3d arrays for max indices rather difficult, this is the best method?
    max_value = np.max(arr_orig)
    max_x, max_y, max_z = np.argwhere(arr_orig==max_value)[0]

    # Values can be negative, we need the "direction" of how far away we are from the middle of the box
    x0 = int(Nx*0.5-max_x)
    y0 = int(Ny*0.5-max_y)
    z0 = int(Nz*0.5-max_z)

    return x0, y0, z0




def shift_bubble(arr_orig, Nx, Ny, Nz, shift):
    """
    Shift the bubble in the middle of the box
    """

    x0, y0, z0 = shift
    arr_3dtmp = np.zeros_like(arr_orig)

    for i_x in range(Nx):
        for i_y in range(Ny):
            for i_z in range(Nz):
                # Map the original values to the shifted indices
                arr_3dtmp[(i_x+x0)%Nx,(i_y+y0)%Ny,(i_z+z0)%Nz]=arr_orig[i_x,i_y,i_z]

    return arr_3dtmp


def calculate_op_value(arr_orig, Nx, Ny, Nz):

    op = 0.0
    for i_x in range(Nx):
        for i_y in range(Ny):
            for i_z in range(Nz):
                op += arr_orig[i_x][i_y][i_z]


    return op/(Nx*Ny*Nz)



def read_binary(file_vis):
    """
    Read the field data (binary)
    """

    with open(file_vis, 'rb') as f:
        b = f.read()

    # Preamble bit that tells you how many double params, int params and the size of the lattice
    dt = np.dtype([
        ('nparamsd','int32'),
        ('nparamsi','int32'),
        ('Nx','int32'),
        ('Ny','int32'),
        ('Nz','int32')])

    # Get rid of said preamble, so you read just the lattice data
    preamble = np.frombuffer(b, dt, count=1)
    main_data = np.frombuffer(b, 'double', count=-1, offset=dt.itemsize) # Rest of the stuff, can have many fields saved

    Nx = int(preamble['Nx'])
    Ny = int(preamble['Ny'])
    Nz = int(preamble['Nz'])
    return main_data, Nx, Ny, Nz



def set_button():
    """
    Button setup for the animation
    """

    button = {
    "type": "buttons",
    "buttons": [
    {
    "label": "Play",
    "method": "animate",
    "args": [None, {"frame": {"duration": 200},  "fromcurrent": True}]
    },
    {
    "label": "Pause",
    "method": "animate",
    "args": [None, {"frame": {"duration": 0, "redraw": False},"mode": "immediate", "transition": {"duration": 0}}]
    }
    ],
    }

    return button


def main():

    if len(sys.argv) != 2:
        print("Did you forget to pass the relevan options file?")
        sys.exit()
    
    options_file = np.array(sys.argv)[1]

    # reading options file
    with open(options_file, 'r') as stream:
        json_string = stream.read()

        # parse file
        opts_list = json.loads(json_string)['options']
        # Start main script


    path_sim = opts_list[0]["directory"]
    path_bubble= opts_list[0]["visualisation"] # only the  time/damping_0.???/ part!
    file_id = opts_list[0]["file_id"]
    mode = opts_list[0]["mode"]
    save_loc = opts_list[0]["save_loc"]


    # Load data
    if mode=='bubble':
        # Determine the location of the time evolution of the visualisation run
        path_bubble_full = path_sim + path_bubble

        file_vis = path_bubble_full + "vis"
        fig_name = "timelution"

        # Load params
        params = theory.load_params(path_bubble_full+"params")
        labels = path_bubble_full+"labels"
        dt = params["dt"]
        print(dt)
        # Load time evolution data
        one_strix_data_forwards = theory.load_data_time(path_bubble_full+ "time_data/forwards_data_"+file_id, labels)
        one_strix_data_backwards = theory.load_data_time(path_bubble_full + "time_data/backwards_data_"+file_id, labels)

        # Create x axis, MCtime x dt
        x_forwards = np.linspace(0, len(one_strix_data_forwards["order"])*dt, len(one_strix_data_forwards["order"]))
        x_backwards = np.linspace(0, -len(one_strix_data_backwards["order"])*dt, len(one_strix_data_backwards["order"]))
    

        #Create the scatter plots for the order parameter value evolution
        scatter_time = go.Scatter(x=x_forwards, y=one_strix_data_forwards["order"], mode="lines",line=dict(width=2, color="blue"), showlegend=False)
        scatter_point_in_time = go.Scatter(x=[x_forwards[0]], y=[one_strix_data_forwards["order"][0]], mode="markers", showlegend=False)

        specs_blurb=[[{}, {"is_3d": True, "rowspan": 2}],[{}, None]]
        titles = ['Histogram of the order parameter', '3D slice of the bubble', 'Time evolution of the order parameter']

    else:
        file_vis = path_sim + "muca0/strix_"+file_id+".dat"
        fig_name = "strix"
        params = theory.load_params(path_sim+"muca0/params")

        specs_blurb=[[{"rowspan": 2}, {"is_3d": True, "rowspan": 2}],[None, None]]
        titles =['Histogram of the order parameter', '3D slice of the bubble']


    fig = make_subplots(rows=2, cols=2,
    specs=specs_blurb,
    subplot_titles=titles
    )

    # For saving singular frames
    fig_timeseries = go.Figure()

    couplings = theory.load_params(path_sim+"muca0/couplings")
    labels = path_sim+"muca0/labels"
    file_mcdata = path_sim +"muca0/data"
    a=couplings["a"]
    # Set up the histogram data and the critical bubble location
    data = theory.load_data_mc(file_mcdata, labels)
    x_hist, y_hist = theory.get_xy_data(data["order"], data["weight"], 100)


    #x_mean, x_error, y_mean, y_error = theory.bin_error_from_resamples(x_jack, y_jack, bins, N_jackknife)


    index_array = theory.find_extrema_index(x_hist, y_hist, -1.0,opts_list[0]["hist_length"])
    i_c=index_array[1]

    # Graph object scatter plots for the order parameter histogram
    scatter_ophist = go.Scatter(x=x_hist, y=np.log(y_hist), mode="lines",line=dict(width=2, color="blue"), showlegend=False)
    scatter_critical_point = go.Scatter(x=[x_hist[i_c]], y=[np.log(y_hist)[i_c]], mode="markers", showlegend=False) # This updates, so needs a separate set up


    main_data, Nx, Ny, Nz = read_binary(file_vis)
    vol = Nx*Ny*Nz
    N = len(main_data)//vol # How many data frames do we have in one file

    fig_name_blurb = f"Nx{Nx}_a{a}_{mode}"


    # Set up the isosruface for the field config
    iso = go.Isosurface(x=np.arange(vol), y=np.arange(vol), z=np.arange(vol), value=np.zeros(vol),
                        isomin=-0.2, isomax=1.0,
                        surface_count=10, opacity=0.6,
                        caps=dict(x_show=False, y_show=False),
                        colorscale='Viridis',
                        showlegend=False,
                        )

    #frame = go.Frame(data=[iso, scatter], layout=go.Layout(title_text=f"frames {j}"))

    #Set up traces, this is why you need the initial set up of the graph_objects
    fig.add_trace(scatter_ophist, 1, 1)
    fig.add_trace(scatter_critical_point, 1, 1)
    button = set_button()

    camera = dict(eye=dict(x=1.25, y=1.75, z=1.75))
    fig_timeseries.update_layout(scene_camera=camera,
                                paper_bgcolor='rgba(0,0,0,0)',
                                plot_bgcolor='rgba(0,0,0,0)'
                                )

    if mode=="bubble":
        # Add traces to the html video
        fig.add_trace(iso, 1, 2)
        fig.add_trace(scatter_time, 2, 1)
        fig.add_trace(scatter_point_in_time, 2, 1)
    else:
        fig.add_trace(iso, 1, 2)


    # Set up a list of frames
    frames = []
    l=49
    sign= -1

    # Loop over frame(s)
    for j in range(N):
        # Don't let the array size fool you, each value needs the location information
        x = np.zeros(vol)
        y = np.zeros(vol)
        z = np.zeros(vol)
        values = np.zeros(vol)

        i = 0+j*vol # if multiple frames, offset i to start from the right index
        k = 0

        # Read coordinates and values to 1D arrays so you can use the Isosurface from plotly
        for i_x in range(Nx):
            for i_y in range(Ny):
                for i_z in range(Nz):
                    x[k] = i_x
                    y[k] = i_y
                    z[k] = i_z
                    values[k] = (float(main_data[i]))
                    i += 1
                    k += 1

        # Reshape the array so that figuring out nearest neighbours is easier
        arr_orig = np.reshape(values, (Nx, Ny, Nz))
        print("Init bubble config op: ", calculate_op_value(arr_orig, Nx, Ny, Nz))

        # Smooth the respahed array
        smooth_iters = 8
        smooth = smooth_field(arr_orig, Nx, Ny, Nz, smooth_iters)

        # Shift the bubble to be in the center
        if j==0:
            x0, y0, z0 = how_much_to_shift(arr_orig, Nx, Ny, Nz)

        shifted_bubble = shift_bubble(smooth, Nx, Ny, Nz, (x0, y0, z0)).flatten()

        # Create an isosurface
        iso = go.Isosurface(x=x, y=y, z=z, value=shifted_bubble,
                        isomin=-0.2, isomax=1.0,
                        surface_count=10, opacity=0.6,
                        caps=dict(x_show=False, y_show=False),
                        colorscale='Viridis',
                        showscale=False,
                        )

        scatter_critical_point = go.Scatter(x=[x_hist[i_c+j//2]], y=[np.log(y_hist)[i_c+j//2]], mode="markers")

        if j==0:
            fig_timeseries.add_trace(iso)
            fig_timeseries.update_layout(dict(font_family="Times New Roman", font_size=16, 
                                            scene = dict(
                                            xaxis = dict(tickmode="array",
                                                    range=[0,48],
                                                    title="𝑥𝜆₃",
                                                    tickvals=[0,16,32,48],
                                                    ticktext=["0", "24", "48", "72"]),
                                            yaxis = dict(tickmode="array",
                                                    tickvals=[0,16,32,48],
                                                    title="𝑦𝜆₃",
                                                    ticktext=["0", "24", "48", "72"],
                                                    range=[0,48],),
                                            zaxis = dict(tickmode="array",
                                                    tickvals=[0,16,32,48],
                                                    title="𝑧𝜆₃",
                                                    ticktext=[" ", "24", "48", "72"],
                                                    range=[0,48])) # box gets weird edges without this
                                    ))

        if mode=="bubble":
            scatter_point_in_time = go.Scatter(x=[x_forwards[j]], y=[one_strix_data_forwards["order"][j]], mode="markers")
            frame = go.Frame(data=[scatter_ophist, scatter_critical_point, iso, scatter_time, scatter_point_in_time], traces=[0,1,2,3, 4], layout=go.Layout(title_text=f"frames {j}"))
            
            if j != 0:
                fig_timeseries.update_traces(iso,overwrite=True)
            tmp=sign*l
            fig_timeseries.update_layout(title=dict(text = f"$t={tmp}\Delta t$", font_size=26, x=0.5, y=0.1))
            #### fix this thing below when have time!!!
            l+=50
            if l> 499 and j==9:
                l=49
                sign=1

            fig_timeseries.write_image(save_loc + fig_name_blurb + f"_{j}.png", scale=6)
        else:
            frame = go.Frame(data=[scatter_ophist, scatter_critical_point, iso], traces=[0,1,2], layout=go.Layout(title_text=f"frames {j}, Nx={Nx}, a={a}, smoothed {smooth_iters} times"))
            fig_timeseries.update_layout(title=dict(text = f"$t=0\Delta t$", font_size=26, x=0.5, y=0.1))
            fig_timeseries.write_image(save_loc + fig_name_blurb + f"_strix.png", scale=6) 

        frames.append(frame)

    fig.update(frames=frames)
    fig.update_layout(updatemenus=[button]);

    fig.update_layout(
        #margin=dict(l=0, r=0, t=0, b=0),
        font=dict(
            family="Times New Roman",
            size=12,
        ),
        #title_font_size=40,
        title={
            'text': f"Nx={Nx}, a={a}, smoothed {smooth_iters} times",
            'y':0.97,
            'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top',
            'font': {'size': 27},
        },
        
    )


    #fig.write_html(save_loc + fig_name_blurb + ".html", auto_play=False)
    #fig.write_image(save_loc + fig_name_blurb + "_bubblet0.png", scale=6)
    #fig.show()

    return 0


if __name__=="__main__": 
    main() 