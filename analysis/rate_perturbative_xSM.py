# libraries
import numpy as np
from cosmoTransitions.tunneling1D import SingleFieldInstanton
from BubbleDet import BubbleConfig, ParticleConfig, BubbleDeterminant
from BubbleDet.helpers import derivative

# importing local functions
from model_parameters import ParametersxSM
from dimensional_reduction import (
    dr_xSM,
    singlet_dimensionless,
    singlet_no_linear_basis,
    singlet_rg_3d,
)


# --------------------- tree level ---------------------


def V(x, msq, g, lam):
    # definitions of potential and parameters
    return 1 / 2 * msq * x**2 + 1 / 6 * g * x**3 + 1 / 24 * lam * x**4


def dV(x, msq, g, lam):
    return msq * x + 1 / 2 * g * x**2 + 1 / 6 * lam * x**3


def ddV(x, msq, g, lam):
    return msq + g * x + 1 / 2 * lam * x**2


def phi_false(msq, g, lam):
    return 0.0


def phi_true(msq, g, lam):
    return (-3 * g + np.sqrt(9 * g**2 - 24 * lam * msq)) / 2 / lam


def msqc(g, lam):
    return 1 / 3 * g**2 / lam


def log_rate_on_lam34_tree_level(
    T: float,
    params: ParametersxSM,
    mu_ratio: float = 1,
):
    # dimensional reduction
    params_3d = dr_xSM(T, **params)

    # changing 3d renormalisation scale
    params_3d = singlet_rg_3d(**params_3d, mu_ratio=mu_ratio)

    # units
    lam_3 = params_3d["lam"]

    # making dimensionless (units of lam_3=1)
    params_3d = singlet_dimensionless(**params_3d)

    # proceeding in the no_linear_basis
    params_3d = singlet_no_linear_basis(**params_3d)

    # asserting that we are below the critical temperature
    assert params_3d["msq"] < msqc(params_3d["g"], params_3d["lam"]), "Above Tc"

    # setting up CosmoTransitions
    ct_obj = SingleFieldInstanton(
        phi_true(params_3d["msq"], params_3d["g"], params_3d["lam"]),
        phi_false(params_3d["msq"], params_3d["g"], params_3d["lam"]),
        lambda x: V(x, params_3d["msq"], params_3d["g"], params_3d["lam"]),
        lambda x: dV(x, params_3d["msq"], params_3d["g"], params_3d["lam"]),
        d2V=lambda x: ddV(x, params_3d["msq"], params_3d["g"], params_3d["lam"]),
        alpha=2,
    )

    # solving bounce equation and computing action
    profile = ct_obj.findProfile()
    S0 = ct_obj.findAction(profile)

    # returning log(rate/lambda_3^4)
    return -S0 + 4 * np.log(T / lam_3) + 1.5 * np.log(S0 / (2 * np.pi))


# --------------------- one-loop Veff(abs(msq)) bodge ---------------------


def Va(x, msq, g, lam):
    return (
        V(x, msq, g, lam) - 1 / (12 * np.pi) * abs(ddV(x, msq, g, lam)) ** 1.5
    )


def dVa(x, msq, g, lam):
    return derivative(
        lambda x: Va(x, msq, g, lam),
        x,
        dx=1e-3 * phi_true(msq, g, lam),
        n=1,
        order=4,
        scheme="center",
    )


def log_rate_on_lam34_LPA_abs(
    T: float,
    params: ParametersxSM,
    mu_ratio: float = 1,
) -> float:
    # dimensional reduction
    params_3d = dr_xSM(T, **params)

    # changing 3d renormalisation scale
    params_3d = singlet_rg_3d(**params_3d, mu_ratio=mu_ratio)

    # units
    lam_3 = params_3d["lam"]

    # making dimensionless (units of lam_3=1)
    params_3d = singlet_dimensionless(**params_3d)

    # proceeding in the no_linear_basis
    params_3d = singlet_no_linear_basis(**params_3d)

    # setting up CosmoTransitions
    ct_obj = SingleFieldInstanton(
        phi_true(params_3d["msq"], params_3d["g"], params_3d["lam"]),
        phi_false(params_3d["msq"], params_3d["g"], params_3d["lam"]),
        lambda x: Va(x, params_3d["msq"], params_3d["g"], params_3d["lam"]),
        lambda x: dVa(x, params_3d["msq"], params_3d["g"], params_3d["lam"]),
        alpha=2,
    )

    # solving bounce equation and computing action
    profile = ct_obj.findProfile()
    Sa = ct_obj.findAction(profile)

    # returning log(rate/lambda_3^4)
    return -Sa + 4 * np.log(T / lam_3) + 1.5 * np.log(Sa / (2 * np.pi))


# --------------------- one-loop re(Veff(msq)) bodge ---------------------


def Vb(x, msq, g, lam):
    return V(x, msq, g, lam) - 1 / (12 * np.pi) * np.real(
        np.power(ddV(x, msq, g, lam) + 1j * 0, 1.5)
    )


def dVb(x, msq, g, lam):
    return derivative(
        lambda x: Vb(x, msq, g, lam),
        x,
        dx=1e-3 * phi_true(msq, g, lam),
        n=1,
        order=4,
        scheme="center",
    )


def log_rate_on_lam34_LPA_re(T: float, params: ParametersxSM, mu_ratio: float = 1):
    # dimensional reduction
    params_3d = dr_xSM(T, **params)

    # changing 3d renormalisation scale
    params_3d = singlet_rg_3d(**params_3d, mu_ratio=mu_ratio)

    # units
    lam_3 = params_3d["lam"]

    # making dimensionless (units of lam_3=1)
    params_3d = singlet_dimensionless(**params_3d)

    # proceeding in the no_linear_basis
    params_3d = singlet_no_linear_basis(**params_3d)

    # setting up CosmoTransitions
    ct_obj = SingleFieldInstanton(
        phi_true(params_3d["msq"], params_3d["g"], params_3d["lam"]),
        phi_false(params_3d["msq"], params_3d["g"], params_3d["lam"]),
        lambda x: Vb(x, params_3d["msq"], params_3d["g"], params_3d["lam"]),
        lambda x: dVb(x, params_3d["msq"], params_3d["g"], params_3d["lam"]),
        alpha=2,
    )

    # solving bounce equation and computing action
    profile = ct_obj.findProfile()
    Sb = ct_obj.findAction(profile)

    # returning log(rate/lambda_3^4)
    return -Sb + 4 * np.log(T / lam_3) + 1.5 * np.log(Sb / (2 * np.pi))


# --------------------- full one-loop ---------------------


def log_rate_on_lam34_one_loop(T: float, params: ParametersxSM, mu_ratio: float = 1):
    # dimensional reduction
    params_3d = dr_xSM(T, **params)

    # changing 3d renormalisation scale
    params_3d = singlet_rg_3d(**params_3d, mu_ratio=mu_ratio)

    # making dimensionless (units of lam_3=1)
    params_3d = singlet_dimensionless(**params_3d)

    # proceeding in the no_linear_basis
    params_3d = singlet_no_linear_basis(**params_3d)

    # setting up CosmoTransitions
    ct_obj = SingleFieldInstanton(
        phi_true(params_3d["msq"], params_3d["g"], params_3d["lam"]),
        phi_false(params_3d["msq"], params_3d["g"], params_3d["lam"]),
        lambda x: V(x, params_3d["msq"], params_3d["g"], params_3d["lam"]),
        lambda x: dV(x, params_3d["msq"], params_3d["g"], params_3d["lam"]),
        d2V=lambda x: ddV(x, params_3d["msq"], params_3d["g"], params_3d["lam"]),
        alpha=2,
    )

    # solving bounce equation and computing action
    profile = ct_obj.findProfile()
    S0 = ct_obj.findAction(profile)

    # creating bubble config instance
    bub_config = BubbleConfig.fromCosmoTransitions(ct_obj, profile)

    # creating particle instance
    higgs = ParticleConfig(
        W_Phi=lambda x: ddV(x, params_3d["msq"], params_3d["g"], params_3d["lam"]),
        spin=0,
        dof_internal=1,
        zero_modes="Higgs",
    )

    # creating bubble determinant instance
    bub_det = BubbleDeterminant(bub_config, higgs)

    # fluctuation determinant
    S1, S1_err = bub_det.findDeterminant()

    # returning log(rate/lambda_3^4)
    return -S0 - S1
