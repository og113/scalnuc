from typing import TypedDict  # typed dicts, so that a linter can check things


class ParametersxSM(TypedDict):
    """ Holds parameter point for xSM

    Initialise as, for example:
    params = ParametersxSM(
        a1=33.87410276678806,
        a2=1.5,
        b1=-513396.88758474897,
        b3=-111.87732181027066,
        b4=0.2581447353400604,
        mSsq=11712.788297109793,
    )
    """
    a1: float
    a2: float
    b1: float
    b3: float
    b4: float
    mSsq: float
