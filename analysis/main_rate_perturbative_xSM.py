# libraries
import numpy as np
import pprint  # for prettier printing of dicts
from cosmoTransitions.tunneling1D import SingleFieldInstanton
from BubbleDet import BubbleConfig, ParticleConfig, BubbleDeterminant
from BubbleDet.helpers import derivative

# importing local functions
from model_parameters import ParametersxSM
from dimensional_reduction import (
    dr_xSM,
    singlet_dimensionless,
    singlet_no_cubic_basis,
    singlet_no_linear_basis,
    singlet_rg_3d,
)
# from nucleation_fits import S0_fit, SH1_fit, omega_fit, alpha_fn, beta_fn
from rate_perturbative_xSM import (
    log_rate_on_lam34_tree_level,
    log_rate_on_lam34_LPA_abs,
    log_rate_on_lam34_LPA_re,
    log_rate_on_lam34_one_loop,
)


###############################################################################
verbosity = 1
print_non_tunnelling = False

###############################################################################
# new parameter point
# Ms = 240, sTh = 0.1
p_xSM = ParametersxSM(
    a1=33.87410276678806,
    a2=1.5,
    b1=-513396.88758474897,
    b3=-111.87732181027066,
    b4=0.2581447353400604,
    mSsq=11712.788297109793,
)
Tc = 98.51253501118245
Tn = 93.12106210621062
mu_ratio = 0.5

if verbosity > 1:
    print("\nxSM parameters:")
    pprint.pprint(p_xSM)
###############################################################################

# using dr_xSM next

# setting up steps in T
T_min = 92
T_max = Tn
dT = (T_max - T_min) / 2
T_list = np.arange(T_max, T_min - dT / 2, -dT)

# choosing basis here
basis_shift_fn = singlet_no_cubic_basis
# basis_shift_fn = singlet_no_linear_basis

# initialising (only needed for finding where crosses Tc and T0)
p_no_cubic_high = dr_xSM(1.01 * Tc, **p_xSM)
p_no_cubic_high = singlet_no_cubic_basis(**p_no_cubic_high)
p_no_cubic_old = dr_xSM(T_list[0], **p_xSM)
p_no_cubic_old = singlet_no_cubic_basis(**p_no_cubic_old)
compute_bounce = T_max < Tc

# looping over T
if verbosity > 1:
    print("\n3d effective parameters:")
if verbosity >= 1:
    print(
        "%-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s %-12s"
        % (
            "T",
            "sigma",
            "msq",
            "g",
            "lam",
            "f3",
            "eps",
            "m_s",
            "logGamma_0",
            "logGamma_A",
            "logGamma_B",
            "logGamma_1",
        )
    )
for T in T_list:
    # doing DR - only called once per temperature
    p = dr_xSM(T, **p_xSM)
    # changing 3d rg scale
    p = singlet_rg_3d(**p, mu_ratio=mu_ratio)
    # shifting to chosen basis
    p = basis_shift_fn(**p)
    # making dimensionless
    #p = singlet_dimensionless(**p)

    # to find Tc and T0, easiest in the g=0 basis
    p_no_cubic = singlet_no_cubic_basis(**p)

    # to find minima (and hence compute bounce) easiest in sig=0 basis
    p_no_linear = singlet_no_linear_basis(**p)

    # expansion parameter
    m = np.sqrt(p_no_linear["msq"])
    g = p_no_linear["g"]
    lam = p_no_linear["lam"]
    eps_g = g ** 2 / (4 * np.pi * m**3) / 3
    eps_lam = lam / (4 * np.pi * m)
    eps = np.max([eps_g, eps_lam]) / np.sqrt(3)

    # one-loop mass
    msq_one_loop = p_no_linear["msq"] * (
        1 + eps_lam * (-0.5) + eps_g * 3 * (-0.25 * np.log(3) + 0.5)
    )
    c_one_loop = np.sqrt(msq_one_loop / lam ** 2)

    # printing if crossing T0
    if (
        p_no_cubic["sig"] * p_no_cubic_high["sig"] < 0
        and p_no_cubic["sig"] ** 2 > -8 * p_no_cubic["msq"] ** 3 / 9 / p_no_cubic["lam"]
    ):
        if verbosity > 1:
            print("----- crossing T0, unstable at tree-level  -----")
            print("-----    (tree-level thick wall limit)     -----")
        p_no_cubic_high = p_no_cubic
        compute_bounce = False

    if compute_bounce:
        # tree-level
        log_rate_tree_level = log_rate_on_lam34_tree_level(
            T, p_xSM, mu_ratio=mu_ratio,
        )

        # LPA abs
        log_rate_LPA_re = log_rate_on_lam34_LPA_abs(
            T, p_xSM, mu_ratio=mu_ratio,
        )

        # LPA re
        log_rate_LPA_abs = log_rate_on_lam34_LPA_re(
            T, p_xSM, mu_ratio=mu_ratio,
        )

        # one-loop
        log_rate_one_loop = log_rate_on_lam34_one_loop(
            T, p_xSM, mu_ratio=mu_ratio,
        )

        # printing results
        print(
            "%-12.8g %-12.8g %-12.8g %-12.8g %-12.8g %-12.6g %-12.8g %-12.8g %-12.8g %-12.8g %-12.8g %-12.8g"
            % (
                T,
                p["sig"],
                p["msq"],
                p["g"],
                p["lam"],
                p["f3"],
                eps,
                c_one_loop,
                log_rate_tree_level,
                log_rate_LPA_re,
                log_rate_LPA_abs,
                log_rate_one_loop,
            )
        )
    elif print_non_tunnelling:
        print(
            "%-12.8g %-12.8g %-12.8g %-12.8g %-12.8g %-12.8g"
            % (T, p["sig"], p["msq"], p["g"], p["lam"], eps)
        )

    # printing if crossing Tc
    if p_no_cubic["sig"] * p_no_cubic_old["sig"] <= 0:
        if verbosity > 1:
            print("----- crossing Tc, critical temperature  -----")
            print("-----         (thin wall limit)          -----")
        compute_bounce = True

    p_no_cubic_old = p_no_cubic
