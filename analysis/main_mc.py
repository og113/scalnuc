import numpy as np
import matplotlib.pyplot as plt
import sys
from scipy.signal import find_peaks, peak_prominences, peak_widths
from scipy.interpolate import CubicSpline
from scipy.optimize import curve_fit
import importlib.util

'''
This is a light weight analysis script used for the intermediate step analysis
in the monte carlo runs that are needed for the next phases
'''

if len(sys.argv) != 4:
    print("Usage: python3 main_mc.py path_to_file bins mode(muca1/muca0)")
    sys.exit()

path_to_data = sys.argv[1]
bins = int(sys.argv[2])
mode = sys.argv[3]

# Makes sure one can run this script from anywhere
sys.path.insert(0,"/home/koranna/Projects/scalnuc/analysis")

import singlet_analysis as singlet
theory = singlet.Singlet()

labels = path_to_data+"labels"
data = theory.load_data_mc(path_to_data+"data", labels)
weight = theory.load_weight(path_to_data+"weight")
counterterms = theory.load_params(path_to_data+"counterterms")
params = theory.load_params(path_to_data+"params")
couplings = theory.load_params(path_to_data+"couplings")

# Parameters
L = params["Nx"]
msq = couplings["b2"]
a = couplings["a"]

crit = -1.0
V = L**3


len_data_orig=len(data["order"])
mc_steps = np.linspace(0,len(data['order']), len(data['order']))

x, y = theory.get_xy_data(data['order'], data['weight'], bins)
index_array_orig  = theory.find_extrema_index(x, y, crit, 'half')
extrema_points_orig, midpoints_orig= theory.find_points(x, y, index_array_orig)

# Jackknife data for error analysis
points_to_remove = 10000 # How many points to remove from the original data
order_jackknifed = theory.jackknife(data['order'], points_to_remove)
weight_jackknifed = theory.jackknife(data['weight'], points_to_remove)

N_jackknife = len(order_jackknifed) # How many jackknife resamples we have
x_jack = np.zeros([N_jackknife, bins])
y_jack = np.zeros([N_jackknife, bins])
index_array = np.zeros([N_jackknife, 3], dtype='int')
extrema_points = np.zeros([N_jackknife, 6])
midpoints = np.zeros([N_jackknife, 4])

critx_jack = np.zeros([N_jackknife])
crity_jack = np.zeros([N_jackknife])

for sample in range(N_jackknife):
    # Create hists for the reweight params
    x_jack[sample], y_jack[sample] = theory.get_xy_data(order_jackknifed[sample], weight_jackknifed[sample], bins)

    # For each sample calculate the location of the phases (index, extrema & midpoints)
    index_array[sample]  = theory.find_extrema_index(x_jack[sample], y_jack[sample], crit, 'half')
    extrema_points[sample], midpoints[sample] = theory.find_points(x_jack[sample], y_jack[sample], index_array[sample])
    critx_jack[sample], crity_jack[sample] = theory.fit_quadratic(x_jack[sample], y_jack[sample], index_array[sample][1])

#critx, crity = theory.fit_quadratic(x, y, index_array_orig[1])

# Calculate the error and mean of extrema_points
extrema_points_mean = np.zeros(6)
extrema_points_error = np.zeros(6)
midpoints_mean = np.zeros(4)
midpoints_error = np.zeros(4)

critx = np.mean(critx_jack)
critx_error = np.sqrt(np.sum((critx_jack-critx)**2)*(N_jackknife-1)/N_jackknife)
crity = np.mean(crity_jack)
crity_error = np.sqrt(np.sum((crity_jack-crity)**2)*(N_jackknife-1)/N_jackknife)

for i in range(6):
    if i<4:
        midpoints_mean[i] = np.mean(midpoints[:,i])
        midpoints_error[i] = np.sqrt(np.sum((midpoints[:,i]-midpoints_mean[i])**2)*(N_jackknife-1)/N_jackknife)

    extrema_points_mean[i] = np.mean(extrema_points[:,i])
    extrema_points_error[i] = np.sqrt(np.sum((extrema_points[:,i]-extrema_points_mean[i])**2)*(N_jackknife-1)/N_jackknife)


x_mean, x_error, y_mean, y_error = theory.bin_error_from_resamples(x_jack, y_jack, bins, N_jackknife)


print(f"orig: {extrema_points_orig[2]}")
print(f"OPS: {extrema_points_mean[0]}, OPC: {extrema_points_mean[2]}, OPMS: {midpoints_mean[0]}")
print(f"OPS error: {extrema_points_error[0]}, OPC error: {extrema_points_error[2]}, OPMS error: {midpoints_error[0]}")

if (mode=='muca0'):
    #calculat error and mean of midpoints
    theory.gen_points_file(path_to_data, extrema_points_mean, midpoints_mean)



plt.style.use('bmh')
colors = plt.cm.viridis(np.linspace(0,1,4))
plt.figure(1)
plt.title(f"Nx: {L}, a: {a}, msq = {msq}")
plt.plot(weight["op"], weight["w"], color=colors[0])
plt.xlabel('Op')
plt.ylabel('w')
plt.savefig(path_to_data+"weight.png")

plt.figure(2)
plt.title(f"Nx: {L}, a: {a}, msq = {msq}")
plt.plot(x, y_mean, color=colors[0])
plt.errorbar(critx, crity, yerr=crity_error, xerr=critx_error, marker='v')
plt.fill_between(x, y_mean+y_error, y_mean-y_error, color=colors[0], alpha=0.25)
plt.errorbar(extrema_points_mean[0], extrema_points_mean[1], yerr=extrema_points_error[1], xerr=extrema_points_error[0], marker='o', label="OPS")
plt.errorbar(extrema_points_mean[2], extrema_points_mean[3], yerr=extrema_points_error[3], xerr=extrema_points_error[2], marker='o', label="OPC")
plt.errorbar(midpoints_mean[0], midpoints_mean[1], yerr=midpoints_error[1], xerr=midpoints_error[0], marker='o', label="OPMS")
plt.yscale('log')
plt.xlabel("Op")
plt.ylabel("P")
plt.legend(loc='best')
plt.savefig(path_to_data+"hist.png")


plt.figure(3)
plt.title(f"Nx: {L}, a: {a}, msq = {msq}")
plt.ylabel("Op")
plt.xlabel("MC steps")
plt.plot(mc_steps, data['order'], color=colors[0])
plt.savefig(path_to_data+"op.png")

print(f"The value from quadratic fitting: {critx} and the error: {critx_error}")

plt.show()
