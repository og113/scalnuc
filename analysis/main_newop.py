import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import json
from scipy.optimize import curve_fit

# Read in params
if len(sys.argv)!=2:
    print("pass the options file as an argument")
    sys.exit()
else:
    options_file = np.array(sys.argv)[1]
    print("---------------------Singlet model--------------------")
    print("\n-------------------", "Analysis code for nucleation rate calculations", "--------------------")
    print("For help with the functions try 'python3 -m pydoc <module>\n")


# reading options file
with open(options_file, 'r') as stream:
    json_string = stream.read()

    # parse file
    opts_list = json.loads(json_string)['options']

N_runs = len(opts_list)

rate_runs = np.zeros(N_runs)
error_runs = np.zeros(N_runs)
prob_runs = np.zeros(N_runs)
lat_space_runs = np.zeros(N_runs)
phys_vol_runs = np.zeros(N_runs)

# Setting up seaborn
sns.set_context("paper")
sns.set_palette("magma", N_runs)
colors = sns.color_palette('magma', N_runs)


plt.rc('font', **{"family": "Times New Roman"})
plt.rc("axes", labelsize=18)
plt.rc("axes", titlesize=18)
plt.rc("xtick", labelsize=18)
plt.rc("ytick", labelsize=18)
plt.rc("legend", fontsize=18)
plt.rc("text", usetex=True)
text_size = 18.0  # for the annotations inside


for i in range(N_runs):

    if i ==0:
        import singlet_analysis as singlet
        theory= singlet.Singlet()
    else:
        import singlet_newop_analysis as singlet_newop
        theory = singlet_newop.Singlet_NewOP()

    print("\n---------------------------", "Load data", "---------------------------")

    labels = opts_list[i]["directory"]+opts_list[i]["label"]
    data = theory.load_data_mc(opts_list[i]["directory"]+opts_list[i]["hists"], labels)
    weight = theory.load_weight(opts_list[i]["directory"]+"weight")
    counterterms = theory.load_params(opts_list[i]["directory"]+opts_list[i]["counterterms"])
    params = theory.load_params(opts_list[i]["directory"]+opts_list[i]["params"])
    couplings = theory.load_params(opts_list[i]["directory"]+opts_list[i]["couplings"])

    nucleation = theory.load_crossings(opts_list[i]["directory"]+opts_list[i]["nucleation"])
    dynamic, dynamic_mean, dynamic_error = theory.calc_dynamical_factor_error(nucleation["weight"], nucleation["cross"])
    print("-------------------------------------------------------------------\n")

    print("\n---------------------------", "Parameters", "---------------------------")

    sigma = couplings["b1"]
    msq = couplings["b2"]
    g = couplings["b3"]
    a = couplings["a"]

    if  i==0:
        benchmark = opts_list[i]["benchmark"]
        Tmin = 92.5
        Tmax = 93.5
        sigma_new, msq_new, T, S_3d, tree_level, one_loop, m_no_linear = theory.temp_range(benchmark, Tmin, Tmax, 10)


        i_pert = np.argwhere(sigma >= sigma_new)[0][0]
        T = T[i_pert]
        print(T)
        print(sigma_new[i_pert])
        print(msq_new[i_pert])
        print(f"m in sigma=0 basis {m_no_linear[i_pert]}")


    Zm=counterterms["Zm"]
    Zphi=counterterms["Zphi"]

    Nx = params["Nx"]
    L = Nx*a
    vol = L**3 # volume
    epsilon = params["dOP"]

    # Time evolution params
    dt = params["dt"]
    gamma = params["gamma"]

    bins = opts_list[i]["bins"]
    crit = opts_list[i]["loc_bubble"]

    # Benchamrk and temp range info, for reweighting
    benchmark = opts_list[i]["benchmark"]
    Tmin=opts_list[i]["temp_range"][0]
    Tmax=opts_list[i]["temp_range"][1]
    dT=opts_list[i]["temp_range"][2]

    if  i!=0:
        OPsym = params["OPsym"]


    # Jackknife resampling
    points_to_remove = 10000 # How many points to remove from the original data


    print(f"Nx {Nx} |  a {a}  | L  {L} |   msq {msq}  |   sigma {sigma}  |   g {g}  |  epsilon  {epsilon} |   dt {dt}  |   gamma {gamma}   |   bins {bins}")
    print("-------------------------------------------------------------------\n")

    #T = np.array([93])

    x, y = theory.get_xy_data(data['order'], data['weight'], bins)
    index_array_orig  = theory.find_extrema_index(x, y, crit, 'half')
    extrema_points_orig, midpoints_orig= theory.find_points(x, y, index_array_orig)

    # Jackknife data for error analysis
    points_to_remove = 10000 # How many points to remove from the original data
    order_jackknifed = theory.jackknife(data['order'], points_to_remove)
    weight_jackknifed = theory.jackknife(data['weight'], points_to_remove)

    N_jackknife = len(order_jackknifed) # How many jackknife resamples we have
    x_jack = np.zeros([N_jackknife, bins])
    y_jack = np.zeros([N_jackknife, bins])
    index_array = np.zeros([N_jackknife, 3], dtype='int')
    extrema_points = np.zeros([N_jackknife, 6])
    midpoints = np.zeros([N_jackknife, 4])
    probs_jack = np.zeros(N_jackknife)
    flux_jack = np.zeros(N_jackknife)

    for sample in range(N_jackknife):
        # Create hists for the reweight params
        x_jack[sample], y_jack[sample] = theory.get_xy_data(order_jackknifed[sample], weight_jackknifed[sample], bins)

        # For each sample calculate the location of the phases (index, extrema & midpoints)
        index_array[sample]  = theory.find_extrema_index(x_jack[sample], y_jack[sample], crit, 'half')
        extrema_points[sample], midpoints[sample] = theory.find_points(x_jack[sample], y_jack[sample], index_array[sample])
        opC = extrema_points[sample][2]
        probs_jack[sample] = theory.probability(x_jack[sample], y_jack[sample], epsilon, opC, index_array[sample])
        if i ==0:
            flux_jack[sample] = theory.flux(opC, vol)
        else:
            flux_jack[sample] = theory.flux(opC, OPsym, vol)



    prob_mean = np.mean(probs_jack)
    prob_error = np.sqrt(np.sum((probs_jack-prob_mean)**2)*(N_jackknife-1)/N_jackknife)
    flux_mean = np.mean(flux_jack)
    flux_error = np.sqrt(np.sum((flux_jack-flux_mean)**2)*(N_jackknife-1)/N_jackknife)

    rate_jack = flux_jack*probs_jack
    rate_mean = np.mean(rate_jack)
    rate_error = np.sqrt(np.sum((rate_jack-rate_mean)**2)*(N_jackknife-1)/N_jackknife)

    # Calculate error of full rate
    rate_error = np.sqrt((rate_error/rate_mean)**2+(dynamic_error/dynamic_mean)**2) # note that this is now the error associated with log(rate)!!
    rate_mean = rate_mean*dynamic_mean*0.5

    # Calculate the error and mean of extrema_points
    extrema_points_mean = np.zeros(6)
    extrema_points_error = np.zeros(6)
    midpoints_mean = np.zeros(4)
    midpoints_error = np.zeros(4)

    for j in range(6):
        if j<4:
            midpoints_mean[j] = np.mean(midpoints[:,j])
            midpoints_error[j] = np.sqrt(np.sum((midpoints[:,j]-midpoints_mean[j])**2)*(N_jackknife-1)/N_jackknife)

        extrema_points_mean[j] = np.mean(extrema_points[:,j])
        extrema_points_error[j] = np.sqrt(np.sum((extrema_points[:,j]-extrema_points_mean[j])**2)*(N_jackknife-1)/N_jackknife)


    x_mean, x_error, y_mean, y_error = theory.bin_error_from_resamples(x_jack, y_jack, bins, N_jackknife)


    print(f"orig: {extrema_points_orig[2]}")
    print(f"OPS: {extrema_points_mean[0]}, OPC: {extrema_points_mean[2]}, OPMS: {midpoints_mean[0]}")
    print(f"OPS error: {extrema_points_error[0]}, OPC error: {extrema_points_error[2]}, OPMS error: {midpoints_error[0]}")

    if i==4:
        plt.figure(0)
        plt.plot(x, y_mean)
        plt.fill_between(x, y_mean+y_error, y_mean-y_error, alpha=0.25)
        ops_x = extrema_points_mean[0]
        ops_y = extrema_points_mean[1]
        opc_x = extrema_points_mean[2]
        opc_y = extrema_points_mean[3]
        plt.errorbar(ops_x, ops_y, yerr=extrema_points_error[1], xerr=extrema_points_error[0], marker='o', label="Symmetric phase")
        plt.errorbar(opc_x, opc_y, yerr=extrema_points_error[3], xerr=extrema_points_error[2], marker='o', label="OPC")
        #plt.errorbar(midpoints_mean[0], midpoints_mean[1], yerr=midpoints_error[1], xerr=midpoints_error[0], marker='o', label="OPMS")
        #plt.legend(loc='lower right')
        plt.xlabel(r"$\theta$")
        plt.ylabel(r"$P(\theta)$")
        plt.yscale('log')
        plt.tick_params(axis='both', which='both', bottom=False, top=False, left=False, labelbottom=False, labelleft=False)

        ax = plt.gca()
        ax.set_ylim(bottom=10e-34)
        ax.annotate("Symmetric phase", (ops_x/1.01, ops_y), fontsize=text_size)
        ax.annotate("Critical phase", (1.05*opc_x, 10e-33),fontsize=text_size)
        ax.annotate("", xy=(-0.18, 10e-8), xytext=(-0.2, 10e-15), arrowprops=dict(arrowstyle='->', color="black", lw = 1.5))
        ax.annotate("Broken phase", xy=(-0.207, 10e-13), rotation=50, fontsize=text_size)
        ax.annotate(r"$\}$",fontsize=text_size+4, xy=(-0.261, 10*opc_y), rotation=90)
        ax.annotate(r"$\epsilon$", (opc_x, 100*opc_y),fontsize=text_size)

        theory.set_fig_specs("hist_"+opts_list[i]["fig_label"])


    plt.figure(1)
    plt.title(f"Nx: {Nx}, a: {a}, $\sigma$ = {sigma}, $m^2$ = {msq}")
    plt.plot(x, y_mean)
    plt.fill_between(x, y_mean+y_error, y_mean-y_error, alpha=0.25)
    plt.yscale('log')
    plt.xlabel(r"$\theta_{\mathrm{op}}$")
    plt.ylabel(r"$P(\theta_{\mathrm{op}})$")
    plt.legend(loc='best')
    plt.close()

    plt.figure(2)
    #plt.title(f'Probability, Nx:{params["Nx"]}, a:{couplings["a"]}')
    plt.errorbar(T, np.log(prob_mean/vol), yerr=prob_error/prob_mean, label=opts_list[i]["fig_label"], marker='o', capsize=10)
    plt.legend(loc='best')
    plt.ylabel("$\ln(\Gamma/V)$")
    plt.xlabel("$T$")
    theory.set_fig_specs("prob")

    prob_runs[i] = prob_mean/vol
    rate_runs[i]  = rate_mean/vol
    error_runs[i]  = rate_error
    lat_space_runs[i] = a
    phys_vol_runs[i] = L

    plt.figure(3)
    #plt.title(f'Full rate, Nx:{params["Nx"]}, a:{couplings["a"]}')
    #plt.scatter(T, np.log(rate_mean/vol))
    plt.errorbar(T, np.log(rate_mean/vol), yerr=rate_error, label=opts_list[i]["fig_label"], marker='o', capsize=10)
    plt.ylabel("$\ln(\Gamma/V)$")
    plt.xlabel("$T$")
    plt.legend(loc='best')
    theory.set_fig_specs("probxflux")

    plt.figure(4)
    plt.errorbar(a, np.log(rate_mean/vol), yerr=rate_error, label=opts_list[i]["fig_label"], marker='o', capsize=10)
    #if i==0:
    #    plt.scatter(0, -np.asarray(one_loop[i_pert]), label="Bounce + fluc det", color='green')
        #plt.scatter(0, -np.asarray(tree_level[i_pert]), label="Bounce action", color='blue')
    plt.legend(loc='best')
    plt.ylabel("$\ln(\Gamma/V)$")
    plt.xlabel("$a$")
    theory.set_fig_specs("newop_rate_latspace_sims")


    plt.figure(5)
    plt.errorbar(1/L, np.log(rate_mean/vol), yerr=rate_error, label=opts_list[i]["fig_label"], marker='o', capsize=10)
    #if i==0:
    #    plt.scatter(0, -np.asarray(one_loop[i_pert]), label="Bounce + fluc det", color='green')
        #plt.scatter(0, -np.asarray(tree_level[i_pert]), label="Bounce action", color='blue')
    plt.legend(loc='best')
    plt.ylabel("$\ln(\Gamma/V)$")
    plt.xlabel("$1/L$")
    theory.set_fig_specs("newop_rate_latspace_sims")



    plt.figure(7)
    plt.errorbar(1/L, np.log(rate_mean/vol), yerr=rate_error, label=opts_list[i]["fig_label"], marker='o', capsize=10)
    #if i==0:
    #    plt.scatter(0, -np.asarray(one_loop[i_pert]), label="Bounce + fluc det", color='green')
        #plt.scatter(0, -np.asarray(tree_level[i_pert]), label="Bounce action", color='blue')
    plt.legend(loc='best')
    plt.ylabel("$\ln(\Gamma/V)$")
    plt.xlabel("$1/L$")


    plt.figure(8)
    plt.errorbar(Nx, dynamic_mean, yerr=dynamic_error, label=opts_list[i]["fig_label"], marker='o', capsize=10)
    plt.legend(loc='best')
    plt.ylabel("$<d>$")
    plt.xlabel("$Nx$")
    theory.set_fig_specs("dynamic")



plt.figure(6)
plt.title("Difference in prob x flux & prob")
plt.scatter(93, np.log(prob_runs[0]/prob_runs[1]), label="prob")
print("Diff between rates:", np.log(rate_runs[0]/rate_runs[1]))
plt.scatter(93, np.log(rate_runs[0]/rate_runs[1]), label="prob x flux")
plt.legend(loc='best')
plt.ylabel("$\ln(\Gamma/V)$")
plt.xlabel("$T$")


# Lattice spacing
xfit = np.linspace(0, 3.5, 1000)
popt, pcov, perr, rsq = theory.fit_func(theory.straight_line, 1/lat_space_runs[1:], np.log(rate_runs[1:]), error_runs[1:], *(1,1))
popt_one, pcov_one, perr_one, rsq_one = theory.fit_func(theory.straight_line_one_param, 1/lat_space_runs[1:], np.log(rate_runs[1:]), error_runs[1:], 1)
popt_cubic, pcov_cubic, perr_cubic, rsq_cubic = theory.fit_func(theory.cubic, lat_space_runs[1:], np.log(rate_runs[1:]), error_runs[1:], *(1,1))


plt.figure(4)
#plt.plot(xfit, theory.straight_line(xfit, *popt), 'g--',label='fit: a=%5.3f, b=%5.3f' % tuple(popt))
plt.plot(xfit, theory.cubic(xfit, *popt_cubic), 'g--')
#plt.axhline(popt_one, label='fit: b=%5.3f' % tuple(popt_one), linestyle='--')
#plt.errorbar(0, popt[1], yerr=perr[1], fmt='v')
#plt.errorbar(0, popt_one[0], yerr=perr_one[0], fmt='v')
plt.legend(loc='best')
theory.set_fig_specs("newop_rate_latspace")

xfit = np.linspace(0.0001, 1/min(phys_vol_runs), 1000)

#popt_one, pcov_one, perr_one, rsq_one = theory.fit_func(theory.straight_line_one_param, 1/phys_vol_runs[1:], np.log(rate_runs[1:]), error_runs[1:], 1)
#popt, pcov, perr, rsq = theory.fit_func(theory.straight_line, 1/phys_vol_runs[1:], np.log(rate_runs[1:]), error_runs[1:], *(1,1))
#popt_quad, pcov_quad, perr_quad, rsq_quad = theory.fit_func(theory.quad, 1/phys_vol_runs[1:], np.log(rate_runs[1:]), error_runs[1:], *(1,1))
#popt_cubic, pcov_cubic, perr_cubic, rsq_cubic = theory.fit_func(theory.cubic, 1/phys_vol_runs[1:], np.log(rate_runs[1:]), error_runs[1:], *(1,1))
popt_exponential, pcov_exponential, perr_exponential, rsq_exponential = theory.fit_func(theory.exponential, 1/phys_vol_runs[1:], np.log(rate_runs[1:]), error_runs[1:], *(-74,10e5,0.3))
print(popt_exponential)

#sys.exit()
plt.figure(5)
#plt.axhline(popt_one, label='Constant: b=%5.3f' % tuple(popt_one), linestyle='--', color='paleturquoise')
#plt.plot(xfit, theory.straight_line(xfit, *popt),  color='turquoise', linestyle='--',label='Linear: a=%5.3f, b=%5.3f' % tuple(popt))
#plt.plot(xfit, theory.quad(xfit, *popt_quad), color='steelblue', linestyle='--', label='Quadratic: a=%5.3f, b=%5.3f' % tuple(popt_quad))
#plt.plot(xfit, theory.cubic(xfit, *popt_cubic), color = 'cadetblue', linestyle = '--', label='Cubic: a=%5.3f, b=%5.3f' % tuple(popt_cubic))
plt.plot(xfit, theory.exponential(xfit, *popt_exponential), color = 'b', linestyle='--',label='Exponential: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt_exponential))

#plt.errorbar(0, popt_one[0], yerr=perr_one[0], fmt='v', color='paleturquoise')
#plt.errorbar(0, popt[0], yerr=perr[0], fmt='o', color ='turquoise')
#plt.errorbar(0, popt_quad[0], yerr=perr_quad[0], fmt='*', color ='steelblue')
#plt.errorbar(0, popt_cubic[0], yerr=perr_cubic[0], fmt='s', color ='cadetblue')
plt.errorbar(0, popt_exponential[0], yerr=perr_exponential[0], fmt='^', color ='b')
#plt.legend(loc='best')
theory.set_fig_specs("newop_rate_physvol")

print("Fitting all data points")
#print(f"Consant: {rsq_one}, linear: {rsq}, cuadratic: {rsq_quad}, cubic: {rsq_cubic}, exponential: {rsq_exponential}")
print(f"exponential: {rsq_exponential}")
print(f"Error on c in exponential fit {perr_exponential[-1]}")
print(f"Error on a exponential fit {perr_exponential[0]}")

########################################
'''
print("Constant:\n")
popt_one, pcov_one, perr_one, rsq_one = theory.fit_func(theory.straight_line_one_param, 1/phys_vol_runs[2:], np.log(rate_runs[2:]), error_runs[2:], 1)
print("Linear:\n")
popt, pcov, perr, rsq = theory.fit_func(theory.straight_line, 1/phys_vol_runs[2:], np.log(rate_runs[2:]), error_runs[2:], *(1,1))
print("Quadratic:\n")
popt_quad, pcov_quad, perr_quad, rsq_quad = theory.fit_func(theory.quad, 1/phys_vol_runs[2:], np.log(rate_runs[2:]), error_runs[2:], *(1,1))
print("Cubic:\n")
popt_cubic, pcov_cubic, perr_cubic, rsq_cubic = theory.fit_func(theory.cubic, 1/phys_vol_runs[2:], np.log(rate_runs[2:]), error_runs[2:], *(1,1))
print("Exponential:\n")
#popt_exponential, pcov_exponential, perr_exponential, rsq_exponential = theory.fit_func(theory.exponential, 1/phys_vol_runs[2:], np.log(rate_runs[2:]), error_runs[2:], *popt_exponential, maxfev=2000)


plt.figure(7)
plt.axhline(popt_one, label='Constant: b=%5.3f' % tuple(popt_one), linestyle='--', color='paleturquoise')
plt.plot(xfit, theory.straight_line(xfit, *popt),  color='turquoise', linestyle='--',label='Linear: a=%5.3f, b=%5.3f' % tuple(popt))
plt.plot(xfit, theory.quad(xfit, *popt_quad), color='steelblue', linestyle='--', label='Quadratic: a=%5.3f, b=%5.3f' % tuple(popt_quad))
plt.plot(xfit, theory.cubic(xfit, *popt_cubic), color = 'cadetblue', linestyle = '--', label='Cubic: a=%5.3f, b=%5.3f' % tuple(popt_cubic))
#plt.plot(xfit, theory.exponential(xfit, *popt_exponential), color = 'b', linestyle='--',label='Exponential: a=%5.3f, b=%5.3f, c=%5.3f' % tuple(popt_exponential))

plt.errorbar(0, popt_one[0], yerr=perr_one[0], fmt='v', color='paleturquoise')
plt.errorbar(0, popt[0], yerr=perr[0], fmt='o', color ='turquoise')
plt.errorbar(0, popt_quad[0], yerr=perr_quad[0], fmt='*', color ='steelblue')
plt.errorbar(0, popt_cubic[0], yerr=perr_cubic[0], fmt='s', color ='cadetblue')
#plt.errorbar(0, popt_exponential[0], yerr=perr_exponential[0], fmt='^', color ='b')
plt.legend(loc='best')
theory.set_fig_specs("newop_rate_physvol_ignore24")
'''
print(np.log(rate_runs), error_runs)
#print("Fitting three data points")
#print(f"Consant: {rsq_one}, linear: {rsq}, cuadratic: {rsq_quad}, cubic: {rsq_cubic}")


plt.show()
