import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import json
from scipy.optimize import curve_fit

# Read in params
if len(sys.argv)!=2:
    print("pass the options file as an argument")
    sys.exit()
else:
    options_file = np.array(sys.argv)[1]
    import singlet_analysis as singlet
    theory = singlet.Singlet()
    print("---------------------Singlet model--------------------")
    print("\n-------------------", "Analysis code for nucleation rate calculations", "--------------------")
    print("For help with the functions try 'python3 -m pydoc <module>\n")


# reading options file
with open(options_file, 'r') as stream:
    json_string = stream.read()

    # parse file
    opts_list = json.loads(json_string)['options']

N_runs = len(opts_list)

# Code needs to know where the location of the broken phase is
broken = np.zeros(opts_list[0]["temp_range"][2]-1)
broken_errors = np.zeros(opts_list[0]["temp_range"][2]-1)

# Code needs to save the rates for a certain temp and the error relted to it
rates_one_temp = np.zeros([N_runs-1, 2])
lattice_size = np.zeros(N_runs-1)
lattice_space = np.zeros(N_runs-1)

# Setting up seaborn
sns.set_theme(style='darkgrid')
sns.set(rc={"xtick.bottom" : True, "ytick.left" : True})
sns.set_context("paper")
sns.set_palette("magma", 8)
colors = sns.color_palette('magma', 8)


for i in range(N_runs):


    print("\n---------------------------", "Load data", "---------------------------")

    labels = opts_list[i]["directory"]+opts_list[i]["label"]
    data = theory.load_data_mc(opts_list[i]["directory"]+opts_list[i]["hists"], labels)
    weight = theory.load_weight(opts_list[i]["directory"]+"weight")
    counterterms = theory.load_params(opts_list[i]["directory"]+opts_list[i]["counterterms"])
    params = theory.load_params(opts_list[i]["directory"]+opts_list[i]["params"])
    couplings = theory.load_params(opts_list[i]["directory"]+opts_list[i]["couplings"])

    if i !=0:
        nucleation = theory.load_crossings(opts_list[i]["directory"]+opts_list[i]["nucleation"])

    print("-------------------------------------------------------------------\n")

    print("\n---------------------------", "Parameters", "---------------------------")

    sigma = couplings["b1"]
    msq = couplings["b2"]
    g = couplings["b3"]
    a = couplings["a"]

    Zm=counterterms["Zm"]
    Zphi=counterterms["Zphi"]

    Nx = params["Nx"]
    L = Nx*a
    vol = L**3 # volume
    epsilon = params["dOP"]

    # Time evolution params
    dt = params["dt"]
    gamma = params["gamma"]

    bins = opts_list[i]["bins"]
    crit = opts_list[i]["loc_bubble"]

    # Benchamrk and temp range info, for reweighting
    benchmark = opts_list[i]["benchmark"]
    Tmin=opts_list[i]["temp_range"][0]
    Tmax=opts_list[i]["temp_range"][1]
    dT=opts_list[i]["temp_range"][2]

    #Reweight coeffs
    coeff_linear=vol # sigma, phi
    coeff_square=0.5*vol*Zm*Zphi # msq, phi^2
    coeff_cubic=1/6*vol*Zphi**(1.5) # g, phi^3

    # Jackknife resampling
    points_to_remove = 10000 # How many points to remove from the original data


    print(f"Nx {Nx} |  a {a}  | L  {L} |   msq {msq}  |   sigma {sigma}  |   g {g}  |  epsilon  {epsilon} |   dt {dt}  |   gamma {gamma}   |   bins {bins}")
    print("-------------------------------------------------------------------\n")

    # sigma and msq as a function of temperature
    sigma_new, msq_new, T, S_3d, tree_level, one_loop = theory.temp_range(benchmark, Tmin, Tmax, dT)

    # The difference between the 'old' variable and the one we are reweighting to
    dsigma= sigma_new-sigma
    dmsq=msq_new-msq

    # Original data
    x, y = theory.get_xy_data(data['phi'], data['weight'], bins)

    # Jackknife data for error analysis
    phi_jackknifed = theory.jackknife(data['phi'], points_to_remove)
    phi2_jackknifed = theory.jackknife(data['phi2'], points_to_remove)
    weight_jackknifed = theory.jackknife(data['weight'], points_to_remove)

    N_jackknife = len(phi_jackknifed) # How many jackknife resamples we have
    N_reweight = len(dsigma) # How many different values are we reweighting to

    ############################################### SET UP NEEDED ARRAYS  #######################################################

    # Init arrays for all jackknife samples and reweights within each sample
    reweight_array = np.zeros([N_jackknife, N_reweight, bins])
    x_linear = np.zeros([N_jackknife, bins]) # The x-axis stays the same for reweights

    # Init arrays for points
    index_array = np.zeros([N_jackknife, N_reweight,3], dtype=int)
    extrema_points = np.zeros([N_jackknife, N_reweight, 6])
    midpoints = np.zeros([N_jackknife, N_reweight, 4])

    # Arrays for jackknifed samples
    flux = np.zeros([N_jackknife, N_reweight])
    prob = np.zeros([N_jackknife, N_reweight])
    free_energy = np.zeros([N_jackknife, N_reweight])

    # Calculate the bin error for each reweighted histogram
    final_reweights = np.zeros([N_reweight, bins])
    error_reweights = np.zeros([N_reweight, bins])

    # Init arrays for the location and error of the phases
    extrema_points_mean = np.zeros([N_reweight, 6])
    extrema_points_error = np.zeros([N_reweight, 6])

    midpoints_mean = np.zeros([N_reweight, 4])
    midpoints_error = np.zeros([N_reweight, 4])

    ############################################### CALCULATE DYNAMICAL FACTOR #######################################################

    # Only calculate the dynamical factor error if the hist is a "half" lenght one
    if opts_list[i]["hist_length"]=="half":
        dynamic, dynamic_mean, dynamic_error = theory.calc_dynamical_factor_error(nucleation["weight"], nucleation["cross"])


    ############################################### JACKKNIFE DATA AND DO ANALYSIS #######################################################


    # Loop through all the jackknife samples to calculate the error
    for sample in range(N_jackknife):
        # Create hists for the reweight params
        H, x_linear[sample], x_square = theory.get_2d_hist(phi_jackknifed[sample],
                                        phi2_jackknifed[sample], weight_jackknifed[sample], bins)

        # Reweight each 2D hist for the given range of dsgima and dmsq
        reweight_array[sample] = theory.reweight2D(coeff_linear, coeff_square, dmsq,
                                    dsigma, x_linear[sample], x_square, H)

        # For each reweight param (in one sample), calculate the location of the phases (index, extrema & midpoints)
        for reweight_param in range(N_reweight):
            index_array[sample][reweight_param]  = theory.find_extrema_index(x_linear[sample],
                                                    reweight_array[sample][reweight_param], crit,
                                                    opts_list[i]["hist_length"])

            extrema_points[sample][reweight_param],\
            midpoints[sample][reweight_param] = theory.find_points(x_linear[sample],
                                                reweight_array[sample][reweight_param],
                                                index_array[sample][reweight_param])


        if opts_list[i]["hist_length"]=="half":

            # Calculate the bubble nucleation rate
            free_energy[sample] = theory.free_energy(extrema_points[sample])
            flux[sample], prob[sample] = theory.nucleation_rate(x_linear[sample],
                                        reweight_array[sample], nucleation["weight"],
                                        nucleation["op"], nucleation["cross"], vol, epsilon,
                                        extrema_points[sample], index_array[sample])


    ############################################### BIN ERROR FOR ALL REWEIGHTED HISTS #######################################################

    for reweight_param in range(N_reweight):
        hist_for_reweight = np.zeros(bins)
        for bin_no in range(bins):
            final_reweights[reweight_param][bin_no] = np.mean(reweight_array[:,reweight_param,:][:,bin_no])
            error_reweights[reweight_param][bin_no] = np.sqrt(np.sum((np.longdouble(reweight_array[:,reweight_param,:][:,bin_no]
                                                    -final_reweights[reweight_param][bin_no]))**2)*(N_jackknife-1)/N_jackknife)



    ############################################### JACKKNIFE ERRORS FOR EVERYTHING ELSE #######################################################

    # Loop over each point, xs, ys, xc, yc, xb and yb
    for j in range(6):
        if j < 4:
            midpoints_mean[:,j], midpoints_error[:,j] = theory.jackknife_error(midpoints[:,:,j], N_reweight, N_jackknife)

        extrema_points_mean[:,j], extrema_points_error[:,j] = theory.jackknife_error(extrema_points[:,:,j], N_reweight, N_jackknife)



    if opts_list[i]["hist_length"]=="full":
        # Get the location of the broken phase from the full range runs
        broken = extrema_points_mean[:,4]

    else:
        # Save the size of the lattice, needed later on
        lattice_size[i-1] = Nx
        lattice_space[i-1] = a

        # Calculate the error related to each jackknifed quantity
        rate = 0.5*prob*flux
        rate_mean, rate_error = theory.jackknife_error(rate, N_reweight, N_jackknife)
        prob_mean, prob_errors = theory.jackknife_error(prob, N_reweight, N_jackknife)
        flux_mean, flux_errors = theory.jackknife_error(prob, N_reweight, N_jackknife)
        free_energy_mean, free_energy_errors = theory.jackknife_error(free_energy, N_reweight, N_jackknife)

        # Calculate the error first, so in only contains the prob*flux part
        rate_error = np.sqrt((rate_error/rate_mean)**2+(dynamic_error/dynamic_mean)**2)
        rate_mean = rate_mean*dynamic_mean # And finally we have the full rate

        # Save the nucleation rate at a specific temp for each run
        rates_one_temp[i-1][0]=rate_mean[-1]/vol
        rates_one_temp[i-1][1]=rate_error[-1]

        # Does the bubble fit inside the box or do we have a critical cylinder?
        # Note that this is done using averages! (should it not be?)
        X, T_tm = theory.does_bubble_fit(extrema_points_mean[:,0], extrema_points_mean[:,2], broken, T)


        ############################################### REWEIGHT HISTS #######################################################

        plt.figure(1)
        plt.title("Reweighted histograms")
        plt.plot(x_linear[0], final_reweights[0], label=f'Nx:{params["Nx"]}, $\sigma=${sigma_new[0]}, $m^2=${msq_new[0]}')
        plt.plot(x_linear[-1], final_reweights[-1], label=f'Nx:{params["Nx"]}, $\sigma=${sigma_new[-1]}, $m^2=${msq_new[0]}')
        plt.fill_between(x_linear[0], final_reweights[0]-error_reweights[0], final_reweights[0]+error_reweights[0], alpha=0.5)
        plt.fill_between(x_linear[-1], final_reweights[-1]-error_reweights[-1], final_reweights[-1]+error_reweights[-1], alpha=0.5)
        plt.yscale('log')
        plt.legend(loc='best')
        plt.xlabel('op')
        plt.ylabel('P')
        theory.set_fig_specs("reweighted_hists")

        ############################################### NUCLEATION VOL AVG #######################################################

        plt.figure(2)
        plt.title("Nucleation rate (volume averaged), $\sigma$:{:2.2}, $m^2$:{:2.2}".format(sigma_new[0],msq_new[0]))
        if T_tm: #if list is not empty (ie. plot only if fig hits the cylinder config)
            index = len(T_tm)
            plt.plot(T[index-1:], np.log(rate_mean[index-1:]/vol), label=f'Nx:{params["Nx"]}, a:{couplings["a"]}')
            plt.fill_between(T[index-1:],
                            np.log(rate_mean[index-1:]/vol)-rate_error[index-1:],
                            np.log(rate_mean[index-1:]/vol)+rate_error[index-1:], alpha=0.5)
            plt.plot(T_tm, np.log(rate_mean[:index]/vol), color = colors[i+1], linestyle='--')

        else:
            plt.plot(T, np.log(rate_mean/vol), label=f'Nx:{params["Nx"]}, a:{couplings["a"]}')
            plt.fill_between(T, np.log(rate_mean/vol)-rate_error,
                            np.log(rate_mean/vol)+rate_error, alpha=0.5)
        plt.ylabel("$\ln(\Gamma/V)$")
        plt.xlabel("T")
        #theory.set_fig_specs("nucleation")

        if i==N_runs-1:    #Only plot the following once
            plt.plot(T, -np.asarray(one_loop), label="Bounce + fluc det", linestyle='-.')
            plt.legend(loc='best')
            theory.set_fig_specs("nucleation_oneloop")

            plt.plot(T, -np.asarray(tree_level), label="Bounce action", linestyle='-.')
            plt.legend(loc='best')
            theory.set_fig_specs("nucleation")

        ############################################### PROBABILITY VS FULL RATE VOL AVG #######################################################

        plt.figure(3)
        plt.title("Full rate vs. the probability part only (volume averaged)")

        if T_tm:
            index = len(T_tm)
            plt.plot(T[index-1:], np.log(rate_mean[index-1:]/vol),label=f'Nx:{params["Nx"]}, a:{couplings["a"]}')
            plt.fill_between(T[index-1:], np.log(rate_mean[index-1:]/vol)-rate_error[index-1:],
                            np.log(rate_mean[index-1:]/vol)+rate_error[index-1:], alpha=0.5, color=colors[i-1])

            plt.plot(T[index-1:], np.log(prob_mean[index-1:]/vol),label=f'P, Nx:{params["Nx"]}, a:{couplings["a"]}', linestyle='-.')
            plt.fill_between(T[index-1:], np.log(prob_mean[index-1:]/vol)-prob_errors[index-1:]/prob_mean[index-1:],
                            np.log(prob_mean[index-1:]/vol)+prob_errors[index-1:]/prob_mean[index-1:],
                            alpha=0.5, color=colors[i])

            plt.plot(T_tm, np.log(rate_mean[:index]/vol), color=colors[i-1], linestyle='--')
            plt.plot(T_tm, np.log(prob_mean[:index]/vol), color=colors[i], linestyle='--')

        else:
            plt.plot(T, np.log(rate_mean/vol),label=f'Nx:{params["Nx"]}, a:{couplings["a"]}')
            plt.fill_between(T, np.log(rate_mean/vol)-rate_error,
                            np.log(rate_mean/vol)+rate_error, alpha=0.5, color=colors[i-1])

            plt.plot(T, np.log(prob_mean/vol),label=f'P, Nx:{params["Nx"]}, a:{couplings["a"]}', linestyle='-.')
            plt.fill_between(T, np.log(prob_mean/vol)-prob_errors/prob_mean,
                            np.log(prob_mean/vol)+prob_errors/prob_mean, alpha=0.5, color=colors[i])
        plt.legend(loc='best')
        plt.ylabel("$\ln(\Gamma/V)$")
        plt.xlabel("T")
        theory.set_fig_specs("probability")

        ############################################### FREE ENERGY #######################################################

        plt.figure(4)
        plt.title("Free energy (not volume averaged)")
        if T_tm:
            index = len(T_tm)
            plt.plot(T[index-1:], free_energy_mean[index-1:], label = f'Nx:{params["Nx"]}, a:{couplings["a"]}')
            plt.fill_between(T[index-1:], free_energy_mean[index-1:]-free_energy_errors[index-1:],
            free_energy_mean[index-1:]+free_energy_errors[index-1:], alpha=0.5)

            plt.plot(T_tm, free_energy_mean[:index], color=colors[i], linestyle='--')
        else:
            plt.plot(T, free_energy_mean, label = f'Nx:{params["Nx"]}, a:{couplings["a"]}')
            plt.fill_between(T, free_energy_mean-free_energy_errors, free_energy_mean+free_energy_errors, alpha=0.5)

        plt.ylabel("$-\log(P_c/P_s)$")
        plt.xlabel("T")
        plt.legend(loc='best')
        theory.set_fig_specs("free_energy")

        ############################################### NUCLEATION RATE VS VOLUME #######################################################

        plt.figure(5)
        plt.title(f"Nucleation rate vs. volume, $T=${T[-1]}")
        plt.errorbar(params["Nx"], np.log(rate_mean[-1]), yerr=rate_error[-1],
                    label=f'Nx:{params["Nx"]}, a:{couplings["a"]}', marker='o')
        plt.xlabel("$N_x$")
        plt.ylabel("$\ln (\Gamma)$")
        plt.legend(loc='best')
        theory.set_fig_specs("nucleation_volume")

        ############################################### INF VOLUME LIMIT #######################################################

        plt.figure(6)
        plt.title(f"Inf volume limit, $T=${T[-1]}")
        plt.errorbar(1/params["Nx"], np.log(rate_mean[-1]/vol), yerr=rate_error[-1],
                    label=f'Nx:{params["Nx"]}, a:{couplings["a"]}', marker='o')

        ############################################### DYNAMIC FACTOR #######################################################

        plt.figure(7)
        plt.title("Dynamic factor only")
        plt.errorbar(params["Nx"], dynamic_mean, yerr=dynamic_error, label=f'Nx:{params["Nx"]}, a:{couplings["a"]}', marker='o')
        plt.legend(loc='best')
        plt.xlabel("Nx")
        plt.ylabel("d")
        theory.set_fig_specs("dynamic_factor")

        ############################################### DOES BUBBLE FIT IN THE BOX #######################################################

        plt.figure(8)
        plt.plot(T, X, label = f'Nx:{params["Nx"]}, a:{couplings["a"]}')
        plt.axhline(0.155)
        plt.legend(loc='best')
        theory.set_fig_specs("bubble_size")

        ############################################### NUCLEATION RATE VOL AVG VS LATTICE SPACING #######################################################

        plt.figure(20)
        plt.title(f"Nucleation rate vs. lattice spacing, $T=${T[-1]}")
        plt.errorbar(couplings["a"], np.log(rate_mean[-1]/vol), yerr=rate_error[-1],
                    label=f'Nx:{params["Nx"]}, a:{couplings["a"]}', marker='o')

        ############################################### TUNNELING STATISTICS #######################################################

        theory.tunnel_stats(nucleation, str(params["Nx"]))




############################################### INF VOLUME LIMIT #######################################################

# Uses data from all runs to do a fit, needs to be outside for loop
xfit = np.linspace(0, 1/min(lattice_size), 100)
popt, pcov, perr, rsq = theory.fit_func(theory.straight_line, 1/lattice_size, np.log(rates_one_temp[:,0]), rates_one_temp[:,1], *(1,1))
popt_one, pcov_one, perr_one, rsq_one = theory.fit_func(theory.straight_line_one_param, 1/lattice_size, np.log(rates_one_temp[:,0]), rates_one_temp[:,1], 1)

print(rsq, rsq_one)

plt.figure(6)
plt.plot(xfit, theory.straight_line(xfit, *popt), 'g--',label='fit: a=%5.3f, b=%5.3f' % tuple(popt))
plt.axhline(popt_one, label='fit: b=%5.3f' % tuple(popt_one), linestyle='--')
plt.errorbar(0, popt[1], yerr=perr[1], fmt='v')
plt.errorbar(0, popt_one[0], yerr=perr_one[0], fmt='v')
plt.xlabel("$1/N_x$")
plt.ylabel("$\ln (\Gamma/vol)$")
plt.legend(loc ='best')
theory.set_fig_specs("inf_volume_limit")

############################################### NUCLEATION RATE VOL AVG VS LATTICE SPACING #######################################################

xfit = np.linspace(0, max(lattice_space), 100)
popt, pcov, perr, rsq = theory.fit_func(theory.straight_line, lattice_space, np.log(rates_one_temp[:,0]), rates_one_temp[:,1], *(1,1))
popt_one, pcov_one, perr_one, rsq_one = theory.fit_func(theory.straight_line_one_param, lattice_space , np.log(rates_one_temp[:,0]), rates_one_temp[:,1], 1)

plt.figure(20)
plt.plot(xfit, theory.straight_line(xfit, *popt), 'g--',label='fit: a=%5.3f, b=%5.3f' % tuple(popt))
plt.axhline(popt_one, label='fit: b=%5.3f' % tuple(popt_one), linestyle='--')
plt.errorbar(0, popt[1], yerr=perr[1], fmt='v')
plt.errorbar(0, popt_one[0], yerr=perr_one[0], fmt='v')
plt.xlabel("$a$")
plt.ylabel("$\ln (\Gamma/V)$")
plt.legend(loc='best')
theory.set_fig_specs("nucleation_latspace")

plt.show()
