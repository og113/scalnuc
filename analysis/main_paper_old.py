import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import json
from scipy.optimize import curve_fit

# Read in params
if len(sys.argv)!=2:
    print("pass the options file as an argument")
    sys.exit()
else:
    options_file = np.array(sys.argv)[1]
    import singlet_analysis as singlet
    theory = singlet.Singlet()
    print("---------------------Singlet model--------------------")
    print("\n-------------------", "Analysis code for nucleation rate calculations", "--------------------")
    print("For help with the functions try 'python3 -m pydoc <module>\n")


# reading options file
with open(options_file, 'r') as stream:
    json_string = stream.read()

    # parse file
    opts_list = json.loads(json_string)['options']

N_runs = len(opts_list)

# Code needs to know where the location of the broken phase is
broken = np.zeros(opts_list[0]["temp_range"][2]-1)
broken_errors = np.zeros(opts_list[0]["temp_range"][2]-1)

# Code needs to save the rates for a certain temp and the error relted to it
rates_one_temp = np.zeros([N_runs-1, 2])
lattice_size = np.zeros(N_runs-1)
lattice_space = np.zeros(N_runs-1)

# Setting up seaborn
sns.set_context("paper")
sns.set_palette("magma", N_runs)
colors = sns.color_palette('magma', N_runs)


plt.rc('font', **{"family": "Times New Roman"})
plt.rc("axes", labelsize=18)
plt.rc("axes", titlesize=18)
plt.rc("xtick", labelsize=18)
plt.rc("ytick", labelsize=18)
plt.rc("legend", fontsize=18)
plt.rc("text", usetex=True)
text_size = 18.0  # for the annotations inside


for i in range(N_runs):


    print("\n---------------------------", "Load data", "---------------------------")

    labels = opts_list[i]["directory"]+opts_list[i]["label"]
    data = theory.load_data_mc(opts_list[i]["directory"]+opts_list[i]["hists"], labels)
    weight = theory.load_weight(opts_list[i]["directory"]+"weight")
    counterterms = theory.load_params(opts_list[i]["directory"]+opts_list[i]["counterterms"])
    params = theory.load_params(opts_list[i]["directory"]+opts_list[i]["params"])
    couplings = theory.load_params(opts_list[i]["directory"]+opts_list[i]["couplings"])

    if opts_list[i]["hist_length"]=="half":
        nucleation = theory.load_crossings(opts_list[i]["directory"]+opts_list[i]["nucleation"])

    print("-------------------------------------------------------------------\n")

    print("\n---------------------------", "Parameters", "---------------------------")

    sigma = couplings["b1"]
    msq = couplings["b2"]
    g = couplings["b3"]
    a = couplings["a"]

    Zm=counterterms["Zm"]
    Zphi=counterterms["Zphi"]

    Nx = params["Nx"]
    L = Nx*a
    vol = L**3 # volume
    epsilon = params["dOP"]

    # Time evolution params
    dt = params["dt"]
    gamma = params["gamma"]
    Nmc = params["Nmc"]

    bins = opts_list[i]["bins"]
    crit = opts_list[i]["loc_bubble"]

    # Benchamrk and temp range info, for reweighting
    benchmark = opts_list[i]["benchmark"]
    Tmin=opts_list[i]["temp_range"][0]
    Tmax=opts_list[i]["temp_range"][1]
    dT=opts_list[i]["temp_range"][2]

    #Reweight coeffs
    coeff_linear=vol # sigma, phi
    coeff_square=0.5*vol*Zm*Zphi # msq, phi^2
    coeff_cubic=1/6*vol*Zphi**(1.5) # g, phi^3

    # Jackknife resampling
    points_to_remove = 10000 # How many points to remove from the original data


    print(f"Nx {Nx} |  a {a}  | L  {L} |   msq {msq}  |   sigma {sigma}  |   g {g}  |  epsilon  {epsilon} |   dt {dt}  |   gamma {gamma}   |   bins {bins}   | Nmc {Nmc}")
    print("-------------------------------------------------------------------\n")

    # sigma and msq as a function of temperature
    sigma_new, msq_new, T, S_3d, tree_level, one_loop, m_no_linear = theory.temp_range(benchmark, Tmin, Tmax, dT)

    # The difference between the 'old' variable and the one we are reweighting to
    dsigma= sigma_new-sigma
    dmsq=msq_new-msq

    # Original data
    x, y = theory.get_xy_data(data['phi'], data['weight'], bins)

    # Jackknife data for error analysis
    phi_jackknifed = theory.jackknife(data['phi'], points_to_remove)
    phi2_jackknifed = theory.jackknife(data['phi2'], points_to_remove)
    weight_jackknifed = theory.jackknife(data['weight'], points_to_remove)

    N_jackknife = len(phi_jackknifed) # How many jackknife resamples we have
    N_reweight = len(dsigma) # How many different values are we reweighting to

    ############################################### SET UP NEEDED ARRAYS  #######################################################

    # Init arrays for all jackknife samples and reweights within each sample
    reweight_array = np.zeros([N_jackknife, N_reweight, bins])
    x_linear = np.zeros([N_jackknife, bins]) # The x-axis stays the same for reweights

    # Init arrays for points
    index_array = np.zeros([N_jackknife, N_reweight,3], dtype=int)
    extrema_points = np.zeros([N_jackknife, N_reweight, 6])
    midpoints = np.zeros([N_jackknife, N_reweight, 4])

    # Arrays for jackknifed samples
    flux = np.zeros([N_jackknife, N_reweight])
    prob = np.zeros([N_jackknife, N_reweight])

    # Calculate the bin error for each reweighted histogram
    final_reweights = np.zeros([N_reweight, bins])
    error_reweights = np.zeros([N_reweight, bins])

    # Init arrays for the location and error of the phases
    extrema_points_mean = np.zeros([N_reweight, 6])
    extrema_points_error = np.zeros([N_reweight, 6])

    midpoints_mean = np.zeros([N_reweight, 4])
    midpoints_error = np.zeros([N_reweight, 4])

    ############################################### CALCULATE DYNAMICAL FACTOR #######################################################

    # Only calculate the dynamical factor error if the hist is a "half" lenght one
    if opts_list[i]["hist_length"]=="half":
        dynamic, dynamic_mean, dynamic_error = theory.calc_dynamical_factor_error(nucleation["weight"], nucleation["cross"])


    ############################################### JACKKNIFE DATA AND DO ANALYSIS #######################################################


    # Loop through all the jackknife samples to calculate the error
    for sample in range(N_jackknife):
        # Create hists for the reweight params
        H, x_linear[sample], x_square = theory.get_2d_hist(phi_jackknifed[sample],
                                        phi2_jackknifed[sample], weight_jackknifed[sample], bins)

        # Reweight each 2D hist for the given range of dsgima and dmsq
        reweight_array[sample] = theory.reweight2D(coeff_linear, coeff_square, dmsq,
                                    dsigma, x_linear[sample], x_square, H)

        # For each reweight param (in one sample), calculate the location of the phases (index, extrema & midpoints)
        for reweight_param in range(N_reweight):
            index_array[sample][reweight_param]  = theory.find_extrema_index(x_linear[sample],
                                                    reweight_array[sample][reweight_param], crit,
                                                    opts_list[i]["hist_length"])

            extrema_points[sample][reweight_param],\
            midpoints[sample][reweight_param] = theory.find_points(x_linear[sample],
                                                reweight_array[sample][reweight_param],
                                                index_array[sample][reweight_param])


        if opts_list[i]["hist_length"]=="half":

            # Calculate the bubble nucleation rate
            flux[sample], prob[sample] = theory.nucleation_rate(x_linear[sample],
                                        reweight_array[sample], nucleation["weight"],
                                        nucleation["op"], nucleation["cross"], vol, epsilon,
                                        extrema_points[sample], index_array[sample])


    ############################################### BIN ERROR FOR ALL REWEIGHTED HISTS #######################################################

    for reweight_param in range(N_reweight):
        hist_for_reweight = np.zeros(bins)
        for bin_no in range(bins):
            final_reweights[reweight_param][bin_no] = np.mean(reweight_array[:,reweight_param,:][:,bin_no])
            error_reweights[reweight_param][bin_no] = np.sqrt(np.sum((np.longdouble(reweight_array[:,reweight_param,:][:,bin_no]
                                                    -final_reweights[reweight_param][bin_no]))**2)*(N_jackknife-1)/N_jackknife)



    ############################################### JACKKNIFE ERRORS FOR EVERYTHING ELSE #######################################################

    # Loop over each point, xs, ys, xc, yc, xb and yb
    for j in range(6):
        if j < 4:
            midpoints_mean[:,j], midpoints_error[:,j] = theory.jackknife_error(midpoints[:,:,j], N_reweight, N_jackknife)

        extrema_points_mean[:,j], extrema_points_error[:,j] = theory.jackknife_error(extrema_points[:,:,j], N_reweight, N_jackknife)



    plt.figure(1)
    plt.scatter(extrema_points_mean[0,2], extrema_points_mean[0,3])
    plt.scatter(extrema_points_mean[-1,2], extrema_points_mean[-1,3])
    plt.plot(x_linear[0], final_reweights[0], label=f'Nx:{params["Nx"]}, {T[0]}')
    plt.plot(x_linear[-1], final_reweights[-1], label=f'Nx:{params["Nx"]}, {T[-1]}')
    plt.fill_between(x_linear[0], final_reweights[0]-error_reweights[0], final_reweights[0]+error_reweights[0], alpha=0.5)
    plt.fill_between(x_linear[-1], final_reweights[-1]-error_reweights[-1], final_reweights[-1]+error_reweights[-1], alpha=0.5)
    plt.yscale('log')
    plt.legend(loc='best')
    plt.xlabel('op')
    plt.ylabel('P')
    
    if opts_list[i]["hist_length"]=="full":
        # Get the location of the broken phase from the full range runs
        broken = extrema_points_mean[:,4]

    else:
        # Calculate the error related to each jackknifed quantity
        rate = 0.5*prob*flux
        rate_mean, rate_error = theory.jackknife_error(rate, N_reweight, N_jackknife)
        flux_mean, flux_errors = theory.jackknife_error(prob, N_reweight, N_jackknife)

        # Calculate the error first, so in only contains the prob*flux part
        rate_error = np.sqrt((rate_error/rate_mean)**2+(dynamic_error/dynamic_mean)**2)
        rate_mean = rate_mean*dynamic_mean # And finally we have the full rate


        # Does the bubble fit inside the box or do we have a critical cylinder?
        # Note that this is done using averages! (should it not be?)
        X, T_tm = theory.does_bubble_fit(extrema_points_mean[:,0], extrema_points_mean[:,2], broken, T)

        ############################################### NUCLEATION VOL AVG #######################################################

        plt.figure(2)
        if T_tm: #if list is not empty (ie. plot only if fig hits the cylinder config)
            index = len(T_tm)
            plt.plot(T[index-1:], np.log(rate_mean[index-1:]/vol), label=f'Nx:{params["Nx"]}, a:{couplings["a"]}')
            plt.fill_between(T[index-1:],
                            np.log(rate_mean[index-1:]/vol)-rate_error[index-1:],
                            np.log(rate_mean[index-1:]/vol)+rate_error[index-1:], alpha=0.5)
            plt.plot(T_tm, np.log(rate_mean[:index]/vol), color = colors[i+1], linestyle='--')

        else:
            plt.plot(T, np.log(rate_mean/vol), label=f'Nx:{params["Nx"]}, a:{couplings["a"]}')
            plt.fill_between(T, np.log(rate_mean/vol)-rate_error,
                            np.log(rate_mean/vol)+rate_error, alpha=0.5)




        ############################################### DOES BUBBLE FIT IN THE BOX #######################################################

        plt.figure(3)
        plt.plot(T, X, label = f'Nx:{params["Nx"]}, a:{couplings["a"]}')
        plt.axhline(0.155)
        plt.legend(loc='best')

            
plt.figure(2)
plt.ylabel(r"$\log(\Gamma/V)$")
plt.xlabel(r"$T$")
plt.legend(loc='upper right')
theory.set_fig_specs("paper_old_nucleation")


plt.show()
