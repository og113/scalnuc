# Changelog

## 2.0.0 (2024-04-28)

Version of code used for study of bubble nucleation.

## 1.0.0 (2020-12-09)

Version of code used for study of equilibrium thermodynamics, published as arXiv:2101.05528.

## 0.0.1 (2018-07-28)

Project initiated.
