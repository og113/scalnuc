# configuration file for program to calcualte bubble nucleation rate

# N.B. the makefile will not work if there are spaces or tabs after variables,
# in their defintion

# folders
SDIR			= src
HDIR			= include
ODIR			= objs
CSDIR			= csrc
CODIR			= cobjs
TSDIR			= tests

# flags etc
CC				= cc
INCLUDES		= -I$(HDIR)
LIBS			= -lm -lgsl -lgslcblas
DEFINES			= -DHAVE_INLINE -DSAVE_OP_LATTICE
LIBST			= $(LIBS)
CFLAGS			= -Wall -O3 -g


#CFLAGS EXPLAINED:
#-g 				: for using gdb to debug
#-Wall				: turns on most compiler warnings
#-O0 				: does no optimizations (compiling quicker)
#-O 				: does some optimizations for running (compiling takes longer)
#-Os 				: optimises as long as code size isn't increased
#-O2 				: does some more optimizations that -O
#-O3 				: does all the optimizations for running
#-pg 				: also known as gprof, the gcc profiling tool
#-std=###			: choose a standard for the compiler, e.g. c90, c99, c11
#-lgsl -lgslcblas	: for gsl
#-DHAVE_INLINE		: for gsl, allows the inclusion of (faster) inline functions
