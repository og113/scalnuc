/** @file singlet_measure.c
 * Measurements for singlet scalar model
 */
#include "common.h"
#include "singlet.h"

/**
 *        measurements
 */

void get_measurements (double* data, fields f, params p, couplings c) {
    int x, y, z, i;
    double fi;
    double norm = 1.0 / (double)(p.Nx * p.Ny * p.Nz);
    int istart, iend;

    istart = 0;
    iend = NMEAS_FIELD_AVG + NMEAS_FIELD_GLOBAL;

    for (i = istart; i < iend; i++) {
        data[i] = 0.0;
    }

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                fi = f.phi[x][y][z];
                data[i_1] += fi;
                data[i_2] += fi * fi;
                data[i_3] += fi * fi * fi;
                data[i_4] += fi * fi * fi * fi;
                data[i_l] += fi * laplacian(f.phi, &p, x, y, z);
            }
        }
    }

    /* assumes h, op and w at end */
    for (i = istart; i < istart + NMEAS_FIELD_AVG; i++) {
        data[i] *= norm;
    }

    /* adding nonconstant counterterms to condensates */
    data[i_4] *= c.c44;
    data[i_4] += c.c42 * data[i_2] + c.c4lap * data[i_l];

    data[i_op] = order_parameter(f, p, c);
    data[i_h] = hamiltonian(f, p, c);
}


void get_measurements_mom(double *data, fields m, fields f, params p,
                          couplings c) {
    int x, y, z, i;
    double mval;
    double norm = 1.0 / (double)(p.Nx * p.Ny * p.Nz);
    int istart, iend;

    istart = NMEAS_FIELD_AVG + NMEAS_FIELD_GLOBAL;
    iend = NMEAS_TOT;

    for (i = istart; i < iend; i++) {
        data[i] = 0.0;
    }

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                mval = m.phi[x][y][z];
                data[i_m] += mval;
                data[i_m2] += mval * mval;
            }
        }
    }

    data[i_e] = energy(f, m, p, c);

    for (i = istart; i < istart + NMEAS_MOM_AVG; i++) {
        data[i] *= norm;
    }

}

/**
 * show measurement names
 */
void show_measurement_labels (FILE* stream) {
    fprintf(stream, "%-12i%-12s\n", i_1, "phi");
    fprintf(stream, "%-12i%-12s\n", i_2, "phi^2");
    fprintf(stream, "%-12i%-12s\n", i_3, "phi^3");
    fprintf(stream, "%-12i%-12s\n", i_4, "phi^4");
    fprintf(stream, "%-12i%-12s\n", i_l, "phi laplacian(phi)");
    fprintf(stream, "%-12i%-12s\n", i_h, "hamiltonian");
    fprintf(stream, "%-12i%-12s\n", i_op, "order parameter");
    fprintf(stream, "%-12i%-12s\n", i_w, "weight");
    fprintf(stream, "%-12i%-12s\n", i_m, "mom");
    fprintf(stream, "%-12i%-12s\n", i_m2, "mom^2");
    fprintf(stream, "%-12i%-12s\n", i_e, "energy");

    fprintf(stream, "\n");
}

/**
 * print measurement names
 */
void print_measurement_labels (char *file) {
    /* opening file */
    FILE * fs = fopen(file, "w");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }
    show_measurement_labels(fs);

    /* closing file */
    fflush(fs);
    fclose(fs);
}
