/** @file cubic_couplings.c
 *    couplings stuff for cubic anisotropy model
 */
#include "common.h"
#include "cubic.h"

/**
 * show cubic couplings
 */
void show_couplings( couplings c, FILE* stream) {
    fprintf(stream, "%-16s%-16s%-16s\n", "msq", "l1", "l2");
    fprintf(stream, "%-16g%-16g%-16g\n", c.msq, c.l1, c.l2);
    fprintf(stream, "\n");
}

/**
 * check cubic couplings
 */
int check_couplings( couplings c) {
    int good;

    if (c.l1 <= 0 || c.l1 <= 0) {
        fprintf(stderr, "Need:\n!(c.l1<=0)\n!(c.l2<=0)\n");
        good = 0;
    }
    else {
        good = 1;
    }
    if (!good) {
        fprintf(stderr, "Couplings inconsistent\n");
        show_couplings(c, stderr);
    }
    return good;
}


/**
 * print cubic couplings
 */
void print_couplings( couplings c, char *file) {
    /* opening file */
    FILE * fs = fopen(file, "w");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    fprintf(fs, "########## Cubic couplings\n");
    fprintf(fs, "%-12s %-12g\n", "msq", c.msq);
    fprintf(fs, "%-12s %-12g\n", "l1", c.l1);
    fprintf(fs, "%-12s %-12g\n", "l2", c.l2);

    /* closing file */
    fflush(fs);
    fclose(fs);
}

/**
 * function to get bare parameters from renormalised ones
 * assuming using improved laplacian
 */
static void get_counterterms(couplings *p) {

    double sigma, xi, C3;
    double l1, l2;
    double mu;

    /* shorthand */
    l1 = p->l1;
    l2 = p->l2;
    mu = l1;

    /* one loop */
#ifdef IMPROVED
    sigma = 2.75238391130752;
    xi   = -0.083647053040968;
    C3 = -0.86147916;
#else
    sigma = 3.17591153562522;
    xi   = 0.152859324966101;
    C3 = 0.08848010;
#endif

    p->dmsq = -(l1 / 2.0 + l2 / 2.0) * sigma / (4.0 * M_PI);
    p->Zm = 1.0 + (l1 / 2.0 + l2 / 2.0) * xi / (4.0 * M_PI);
    p->Zphi = 1.0;

    p->dl1 = (3.0 * l1 * l1 / 2.0 + 3.0 * l2 * l2 / 2.0) * xi / (4.0 * M_PI);
    p->dl2 = (l1 * l2 + 2.0 * l2 * l2) * xi / (4.0 * M_PI);

    p->dphisq = 2.0 * sigma / (4.0 * M_PI) - 2.0 * p->msq * xi / (4.0 * M_PI);

#ifdef IMPROVED
    /* two loop */
    double C1, C2;
    C1 = 0.0550612;
    C2 = 0.0334416;

    p->dl1 += -C1 / (16.0 * M_PI * M_PI) *
              (3.0 * l1 * l1 * l1 + 3.0 * l1 * l2 * l2
               + 6.0 * l2 * l2 * l2)
              - C2 / (16.0 * M_PI * M_PI) * (l1 * l1 * l1 / 3.0 + l1 * l2 * l2)
              + (xi * xi / (16.0 * M_PI * M_PI)) * (3.0 * l1 * l1 * l1 / 4.0
                                                    + 9.0 * l1 * l2 * l2 / 4.0);

    p->dl2 += -C1 / (16.0 * M_PI * M_PI) * (l1 * l1 * l2 + 6.0 * l1 * l2 * l2
                                            + 5.0 * l2 * l2 * l2)
              - C2 / (16.0 * M_PI * M_PI) * (l1 * l1 * l2 / 3.0 + l2 * l2 * l2)
              + (xi * xi / (16.0 * M_PI * M_PI))
              * (3.0 * l1 * l1 * l2 / 4.0 + 9.0 * l2 * l2 * l2 / 4.0);

    /* CHECK THE LOG(6/mu) here! */
    p->dmsq += (l1 * l1 / 6.0 + l2 * l2 / 2.0)
               / (16.0 * M_PI * M_PI) * (log(6.0 / mu) + C3)
               - (p->dl1 / 2.0 + p->dl2 / 2.0) * sigma / (4.0 * M_PI)
               + (1.0 - p->Zm) * (p->dmsq);

    /* modification of Zm after dm */
    p->Zm += (l1 + l2) * (l1 + l2) / 4.0 * (xi * xi / (16.0 * M_PI * M_PI))
             - (l1 * l1 / 2.0 + 3.0 * l2 * l2 / 2.0)
             * (C1 / (16.0 * M_PI * M_PI) + C2 / (3.0 * 16.0 * M_PI * M_PI));

    p->Zphi = C2 / (16.0 * M_PI * M_PI) * (l1 * l1 / 6.0 + l2 * l2 / 2.0) + 1.0;

    p->dphisq  += (l1 + l2) * sigma * xi / (16.0 * M_PI * M_PI);
#else
    /* CHECK THE LOG(6/mu) here! */
    p->dmsq += (l1 * l1 / 6.0 + l2 * l2 / 2.0)
               / (16.0 * M_PI * M_PI) * (log(6.0 / mu) + C3);
    p->dphisq  += (l1 + l2) * sigma * xi / (16.0 * M_PI * M_PI);

#endif
}


/**
 * get couplings from file, with checking that the file conforms
 */
void get_couplings( couplings *p, char *file) {
    /* opening file */
    FILE * fs = fopen(file, "r");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        exit(2);
    }

    /* char arrays (on stack) to hold lines and parameter names */
    char buffer[NSTR]; /* assuming line <NSTR chars */
    /*char param_name[NSTRSS];*/

    int k = 0; /* goes from 0 to total number of parameters */
    int terms; /* to check that there are two things on each line */

    /* continue while not at the end of the file */
    while (!feof(fs)) {

        /* read into buffer and check line is not empty */
        if ( fgets (buffer, NSTR, fs) != NULL ) {

            /* ignore line and move on if starts with # or if blank */
            if ( buffer[0] == '\n' || buffer[0] == '#') {
                continue;
            }

            /* read from buffer into couplings, assuming expected format of file
               */
            /* checks parameter name is what we expect */
            if (k == 0) {
                terms = get_double(buffer, "msq", &(p->msq));
            }
            else if (k == 1)
                terms = get_double(buffer, "l1", &(p->l1));
            else if (k == 2)
                terms = get_double(buffer, "l2", &(p->l2));

            k++;

            /* checking expected number of things per line */
            if (terms != 2) {
                fprintf(stderr, "Incorrect number of terms, %i, on line %i in %s\n"
                        , terms, k,
                        file);
                exit(2);
            }

        }
    }

    if (k != NCOUPLINGS) {
        fprintf(stderr, "Only set %i parameters out of %i\n"
                , k, NCOUPLINGS);
        exit(2);
    }

    /* closing file */
    fclose(fs);

    /* setting counterterms */
    get_counterterms(p);
}


/**
 * print counterterms
 */
void show_counterterms( couplings p, FILE* stream) {

    fprintf(stream, "%-12s%-12s%-12s%-12s%-12s%-12s\n",
            "dl1", "dl2", "dmsq", "Zm", "Zphi", "dphisq");
    fprintf(stream, "%-12.6g%-12.6g%-12.6g%-12.6g%-12.6g%-12.6g\n",
            p.dl1, p.dl2, p.dmsq, p.Zm, p.Zphi, p.dphisq);
    fprintf(stream, "\n");
}

/**
 * print counterterms to file
 */
void print_counterterms( couplings p, char *file) {
    /* opening file */
    FILE * fs = fopen(file, "w");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    /* printing parameters in format:
     *             param name, param value         */
    fprintf(fs, "%-12s %-32.*f\n", "dl1", DBL_DIG - 1, p.dl1);
    fprintf(fs, "%-12s %-32.*f\n", "dl2", DBL_DIG - 1, p.dl2);
    fprintf(fs, "%-12s %-32.*f\n", "dmsq", DBL_DIG - 1, p.dmsq);
    fprintf(fs, "%-12s %-32.*f\n", "Zm", DBL_DIG - 1, p.Zm);
    fprintf(fs, "%-12s %-32.*f\n", "Zphi", DBL_DIG - 1, p.Zphi);
    fprintf(fs, "%-12s %-32.*f\n", "dphisq", DBL_DIG - 1, p.dphisq);

    /* closing file */
    fflush(fs);
    fclose(fs);
}

/**
 * print counterterms
 */
void show_betas( couplings p, FILE* stream) {

    double betah, betah2, beta2, beta4, beta12;

    #ifdef IMPROVED
    betah = -4.0 / 3.0;
    betah2 = 1.0 / 12.0;
    beta2 = (p.msq + p.dmsq) * p.Zm / 2.0 + 15.0 / 4.0;
    #else
    betah = -1.0;
    betah2 = 0.0;
    beta2 = (p.msq + p.dmsq) * p.Zm / 2.0 + 3.0;
    #endif
    beta4 = (p.l1 + p.dl1) / 24.0;
    beta12 = (p.l2 + p.dl2) / 4.0;

    betah *= p.Zphi;
    betah2 *= p.Zphi;
    beta2 *= p.Zphi;
    beta4 *= p.Zphi * p.Zphi;
    beta12 *= p.Zphi * p.Zphi;

    fprintf(stream, "%-16s%-16s%-16s%-16s%-16s\n",
            "betah", "betah2", "beta2", "beta4", "beta12");
    fprintf(stream, "%-16.8g%-16.8g%-16.8g%-16.8g%-16.8g\n",
            betah, betah2, beta2, beta4, beta12);
    fprintf(stream, "\n");

}
