/** @file cubic_overrelaxation.c
 * Overrelaxation algorithm for monte carlo evaluations of cubic anisotropy
 *model
 */
#include "common.h"
#include "cubic.h"

/**
 *    performs overrelaxation algorithm, essentially following kajantie et al.
 * , the electroweak phase transition (1996)
 * it appears to be about 15% faster to solve by hand than with gsl
 */
static int overrelaxation_local(double*** phiA, double*** phiB, int x, int y,
                                int z, params *p, couplings *c, gsl_rng* r,
                                multicanonical *muca, double *op) {

    int accept;                                // accept/reject?
    double alpha, beta, gamma;                // polynomial coefficients
    double a, b, d;                            // intermediates
    double phi;                                // minimum
    /* double x2, x3;*/
    double f1, f2, f3, f4, f5, f6;
    double measure, prob, dop, dweight;

    /*
     *    solving
     *        alpha*phi**4 + beta*phi**2 + gamma*phi
     *                                            = alpha*phi0**4 + beta*phi0**2
     *+ gamma*phi0
     */
    alpha = c->Zphi * c->Zphi * (c->l1 + c->dl1) / 24.0;
    #ifdef IMPROVED
    beta = -0.5 * c->Zphi * (-(15.0 / 2.0))
           + 0.5 * c->Zphi * c->Zm * (c->msq + c->dmsq)
           + c->Zphi * c->Zphi * ((c->l2 + c->dl2) / 4.0)
           * phiB[x][y][z] * phiB[x][y][z];
    gamma = -c->Zphi *
            (laplacian(phiA, p, x, y, z) + (15.0 / 2.0) * phiA[x][y][z]);
    #else
    beta = -0.5 * c->Zphi * (-6.0)
           + 0.5 * c->Zphi * c->Zm * (c->msq + c->dmsq)
           + c->Zphi * c->Zphi * ((c->l2 + c->dl2) / 4.0)
           * phiB[x][y][z] * phiB[x][y][z];
    gamma = -c->Zphi * (laplacian(phiA, p, x, y, z) + 6.0 * phiA[x][y][z]);
    #endif

    /*
     *    this is equivalent to solving
     *        alpha*(phi-phi0)*(phi**3 + a*phi**2 + b*phi + d) = 0
     *    where
     *        alpha = phi0
     *        beta = b/a + phi0**2
     *        gamma = d/a + alpha*beta
     */
    a = phiA[x][y][z];
    b = beta / alpha + a * a;
    d = gamma / alpha + a * b;

    /* Now find the zeros of the 3-deg polynomial (taken from Kari's code)
     *  (s^3 + as^2 + bs + d)
     */

    f1 = -a * a + 3.0 * b;
    f2 = -2.0 * a * a * a + 9.0 * a * b;
    f6 = f2 - 27.0 * d;
    f4 = 4.0 * f1 * f1 * f1 + f6 * f6;

    /* only one real solution exists now, this is all what is accepted */
    f5 = sqrt(f4) + f6;
    f3 = cbrt(f5) * (1.0 / CUBR2);
    phi = (-a - f1 / f3 + f3) * (1.0 / 3.0);

    /* getting solution, phi (using gsl) */
    /*if (gsl_poly_solve_cubic(a,b,d,&phi,&x2,&x3) == 3) {
        fprintf(stderr,"overrelaxation error: 3 real roots found to cubic");
        return accepts;
       }    */

    /* correction to order parameter */
    dop = delta_order_parameter(phiA, p, c, x, y, z, phi - a);

    /* accept/reject? on measure */
    /* using that a = phiA[x][y][z] */
    measure = a * (4.0 * alpha * a * a + 2.0 * beta) + gamma;
    measure /= phi * (4.0 * alpha * phi * phi + 2.0 * beta) + gamma;
    measure = fabs(measure);
    accept = 0;
    if (p->OPbins) { /* multicanonical, following kari's code */
        set_multicanonical_proposed_state(muca, *op + dop, p);
        dweight = muca->w_new    - muca->w;
        measure *= exp(dweight); /* goes straight into accept/reject */
    }
    if (measure >= 1.0) {
        accept = 1;
    }
    else {
        prob = gsl_rng_uniform(r);
        if (measure >= prob) {
            accept = 1;
        }
    }

    /* accepting/rejecting change */
    if (accept) {
        phiA[x][y][z] = phi;
        *op += dop;
        if (p->OPbins) {
            set_multicanonical_current_state(muca, p);
        }
    }

    return accept;
}

/**
 *    sweep of the local update for one field
 */
static int overrelaxation_static(double*** phiA, double*** phiB, params p,
                                 couplings c, gsl_rng* r, multicanonical *muca,
                                 double *op) {
    int x, y, z;        // iterators over lattice
    int evenodd, i;     // even or odd
    int accepts;        // accept/reject?

    /* looping over lattice */
    accepts = 0;
    evenodd = (int)(gsl_rng_uniform(r) + 0.5);
    for (i = 0; i < 2; i++) {
        for (x = 0; x < p.Nx; x++) {
            for (y = 0; y < p.Ny; y++) {
                for (z = 0; z < p.Nz; z++) {
                    if ( (x + y + z) % 2 == evenodd )
                        accepts += overrelaxation_local(phiA, phiB, x, y, z,
                                                        &p, &c, r, muca, op);
                }
            }
        }
        if (p.Umuca) {
            /* updating ni and hi in based on current value of op */
            update_multicanonical_ancillary_arrays(muca, &p);
        }
        evenodd = (evenodd == 1 ? 0 : 1);
    }

    return accepts;
}

/**
 *    wrapper for the above
 */
int overrelaxation(fields f, params p, couplings c, gsl_rng* r
                   , multicanonical *muca, double *op) {
    int ret;
    ret = overrelaxation_static(f.phi1, f.phi2, p, c, r, muca, op);
    ret += overrelaxation_static(f.phi2, f.phi1, p, c, r, muca, op);
    return ret;
}
