/** @file onescalar_alloc.c
 * Functions for allocating and freeing memory for fields, and other structs.
 * For theories with two scalar fields, such as the cubic anisotropy model.
 */
#include "common.h"
#include "singlet.h"


/**
 *    allocate and free fields struct
 */

void alloc_fields(fields *f, params p) {

    f->phi = make_field(p);
    printf("Allocated fields\n");
}

/**
 * set fields values to zero
 */
void zero_fields(fields *f, params p) {

    zero_field(f->phi, p);
}

/**
 * set fields values random
 */
void set_random_fields(fields *f, params p, gsl_rng* r, double hotness) {

    set_random_field(f->phi, p, r, hotness);
}

/**
 * free fields
 */
void free_fields(fields *f, params p) {

    free_field(f->phi, p);
    printf("Freed fields\n");
}

/**
 * copy fields
 */
void copy_fields(fields *fin, fields *fout, params p) {

    copy_field(fin->phi, fout->phi, p);

}


void init_mom(fields mom, params p, couplings c, gsl_rng *r) {
    int x, y, z;

    for (x = 0; x < (p.Nx); x++) {
        for (y = 0; y < (p.Ny); y++) {
            for (z = 0; z < (p.Nz); z++) {
                (mom.phi)[x][y][z] = gsl_ran_gaussian(r, pow(c.a, -1.5));
            }
        }
    }

}
