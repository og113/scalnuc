/** @file cubic_ioput.c
 * Input and output for scalnuc.c program for cubic anisotropy model
 *    in general write and read are in binary
 *        , whereas save and load are in ascii
 *        i use glob to find out if files exist
 *        , and otherwise just stdio.h
 */
#include "common.h"
#include "cubic.h"

/**
 * write fields to file in binary
 */
void write_fields(fields f, params p, char *file) {

    /* opening file */
    FILE * fs = fopen(file, "wb");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    /* writing lattice size */
    write_lattice_size(p, fs);

    /* writing fields */
    write_field_stream(f.phi1, p, fs);
    write_field_stream(f.phi2, p, fs);

    fclose(fs);

}


void write_many_fields(fields f, params p, char *file) {

    FILE *fs = fopen(file, "ab");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    /* writing lattice size */


    /* writing fields */
    write_field_stream(f.phi1, p, fs);
    write_field_stream(f.phi2, p, fs);
    fprintf(stderr, "Wrote a field to visualisation");

    fclose(fs);

}

/**
 * read fields from file in binary
 */
void read_fields(fields *f, params p, char *file) {

    /* opening file */
    FILE * fs = fopen(file, "rb");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        exit(2);
    }

    /* reading parameters */
    params pf;
    read_lattice_size(&pf, fs);
    if (!compare_lattice_size(p, pf)) {
        fprintf(stderr,
                "\n!!! Lattice size does not agree with those in %s !!!\n\n",
                file);
        show_params(p, stderr);
        show_params(pf, stderr);
        exit(2);
    }

    /* reading fields */
    read_field_stream(f->phi1, p, fs);
    read_field_stream(f->phi2, p, fs);

    fclose(fs);

}
