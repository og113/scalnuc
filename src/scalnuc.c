/** @file scalnuc.c
 * \brief Main source for Monte-Carlo simulation in scalar nucleation program
 * This file does the Monte-Carlo sweeps and iteratively improves an
 * estimate for the constrained free energy as a function of an order parameter.
 * compiles with
 *        make <model>_mc
 * run with
 *        ./<model>_mc
 * There must be params and couplings files present in the directory.
 * See README.md for more details.
 */

#include "common.h"
#if defined(CUBIC)
#include "cubic.h"
#elif defined(RECTANGULAR)
#include "rectangular.h"
#elif defined(SINGLET)
#include "singlet.h"
#endif

/**************************************************************************//**
*                                main begins
******************************************************************************/
int main(int argc, char** argv) {

    printf("\n###########################  %s  ###########################\n"
           , "Scalar bubble nucleation");
#if defined(CUBIC)
    printf("###########################  %s  ###########################\n"
           , "Cubic anisotropy model");
#elif defined(RECTANGULAR)
    printf("###########################  %s  ###########################\n"
           , "Rectangular anisotropy model");
#elif defined(SINGLET)
    printf("###########################  %s  ###########################\n"
           , "Singlet scalar model");
#endif
printf("###########################  %s  #############################\n\n"
        ,"Monte Carlo");

/** starting clocks */
    clock_t start_t = clock();
    double cputime_total = cpu_time();

/**************************************************************************//**
*                                parameters
******************************************************************************/

/* showing whether we are using the improved action */
#if defined(IMPROVED) || defined (IMPROVED_OA2)
    printf("Using O(a^2) improved action.\n");
#elif defined (IMPROVED_OA1)
    printf("Using O(a) improved action.\n");
#else
    printf("Using unimproved action.\n");
#endif

/** getting couplings */
    couplings c;
    get_couplings(&c, couplings_file);
/** checking consistency of couplings */
    if (!check_couplings(c)) {
        return 1;
    }
    else {
        printf("%s:\n", couplings_file);
        printf("########## Couplings\n");
        show_couplings(c, stdout);
        printf("########## Counterterms\n");
        show_counterterms(c, stdout);
        printf("########## Betas\n");
        show_betas(c, stdout);
        print_counterterms(c, cts_file);
        /*printf("\n");*/
    }

/** getting paramters */
    params p, q;
    get_params(&p, params_file);

/** checking consistency of params */
    if (!check_params(p)) {
        return 1;
    }
    else {
        printf("%s:\n", params_file);
        show_params(p, stdout);
        /*printf("\n");*/
    }

/** Check that the stencil and lattice size make sense, otherwise the periodic
 * boundaries do not work.
 */
#if defined(SINGLET)
    if (p.Nx % NCOLOURS != 0 && p.Ny % NCOLOURS != 0 && p.Nz % NCOLOURS != 0)
    {
        fprintf(stderr, "Need:\n! Nx, Ny, Nz %% NCOLOURS == 0\n");
        return 1;
    }
#endif

    /** compile-time parameters/fudge factors and derived parameters */
    double sigma = 0.5;     // size of local metropolis mc changes
    double epsilon = 0.001; // logarithm of size of global mc changes
    double hotness = 1.0;   // used if hot initial conditions
    int set_sigma = 1;      // set sigma during pre-thermalisation?
    int set_eps = 1;        // set epsilon during pre-thermalisation?
    int verbosity = 2;      // 0 for final runs, >0 to print extra stuff
    int nmeas = NMEAS_FIELD_AVG + NMEAS_FIELD_GLOBAL;
    double acc = 1.0;       // fraction of run to accelerate multicanonical
    int strix_spacer = (p.Nth > 1000 ? p.Nth : 1000); // spacer for strix files
    long dofs = NFIELDS * p.Nx * p.Ny * p.Nz; /* total number of degrees of
                                                 freedom */

/** getting status */
    int start_status = 0;  // 0 if starting mc from beginning, else nonzero
    int t = 0;             // monte-carlo time (sweep) iterator
    int tth = 0;           // monte-carlo time iterator for pre-thermalisation
    int append = 0;        // append or overwrite data file
    if (file_exists(status_file)) {
        get_status(&tth, &t, &sigma, &epsilon, status_file);
        printf("Status: t %i and tth %i\n\n", t, tth);
        start_status = t;
        append = (t ? 1 : 0);
    }

/** printing measurement labels */
    print_measurement_labels(labels_file);

/**************************************************************************//**
*                                other variables
******************************************************************************/

/** various variables assigned during computations */
/** physical variables*/
    double op;                      // holds current value of order parameter
/** iterators */
    int tmeas;                      // monte-carlo time, for measurements data
    int istrix, istrix0, istrix_spacer;  // to iterate over separatrix files
    int umuca;                      // stores value of p.Umuca
/** convergence parameters */
    double dweights = 0.0;
/** cpu times */
    double cputime_montecarlo;
    double cputime_thermal;
    double cputime_checkpoints;
    double cputime_measurements;
    double cputime_ends;
    double cputime;

/**************************************************************************//**
*                                allocating memory
******************************************************************************/

    printf("Allocating memory:\n");
/** measurements arrays */
    if (p.Tcheck < 0) {
        fprintf(stderr, "Out of bounds error, %i < 0\n", p.Tcheck);
        return 1;
    }
    double **data = make_2d_array(p.Tcheck, nmeas);
    zero_2d_array(data, p.Tcheck, nmeas);
    printf("Allocated data array\n");

/* allocating and initialising random number generator */
/* options include: gsl_rng_taus2, gsl_rng_mt19937, gsl_rng_ranlxd2 */
    gsl_rng *r;
    r = gsl_rng_alloc( gsl_rng_taus2 );
    if ( r == NULL ) {
        fprintf(stderr, "failed to allocate gsl random number generator\n");
        exit(1);
    }
    printf("Allocated %s random number generator\n", gsl_rng_name(r));

/* allocating fields */
    fields f;
    alloc_fields(&f, p);
    zero_fields(&f, p);

/* allocating multicanonical weights array */
    multicanonical muca;
    alloc_multicanonical(&muca, p);
    printf("Allocated multicanonical\n\n");

    fflush(stdout);
    fflush(stderr);

/* cputime of ends */
    cputime_ends = cpu_time() - cputime_total;

/**************************************************************************//**
*                                initial conditions
******************************************************************************/

/* initial state of rng */
    long unsigned seed;
    if (file_exists(rng_file)) {
        read_rng_state(r, rng_file);
        printf("Loaded random number generator state from:\n%s\n", rng_file);
    }
    else if (p.seed != 0) {
        seed = p.seed;
        gsl_rng_set(r, seed);
        printf("Initialised random number generator with input seed:\n%lu\n",
               seed);
    }
    else {
        seed = (long unsigned)dev_urandom();
        /* (long unsigned)time(NULL) */
        gsl_rng_set(r, seed);
        printf("Initialised random number generator with random seed:\n%lu\n",
               seed);
    }

/* initial fields */
    if (file_exists(fields_file)) {
        read_fields(&f, p, fields_file);
        printf("Loaded fields from:\n%s\n", fields_file);
    }
    else {
        printf("%s can't be found\n", fields_file);
        set_random_fields(&f, p, r, hotness);
        if (p.OPbins) {
            scale_field_to_order_parameter(&f, p, c);
            printf("Hot initial conditions (scaled)\n");
        }
        else
            printf("Hot initial conditions\n");
    }

/* calculating order parameter */
    op = order_parameter(f, p, c);

/* initial multicanonical weights */
    if (p.OPbins) {
        if (file_exists(weight_file)) {
            load_multicanonical(&muca, p, weight_file, acc);
            set_multicanonical_state(&muca, op, p);
            printf("Loaded multicanonical from:\n%s\n", weight_file);
        }
        else {
            set_multicanonical(&muca, p, op, acc);
            printf("Set multicanonical to zero\n");
        }
        printf("\n");
        show_multicanonical_current_state(&muca, stdout);
    }
    else {
        printf("Non-multicanonical run\n\n");
    }

/**************************************************************************//**
*                                files to save to
******************************************************************************/

/* separatrix files */
    istrix = file_pattern_count(strix_file);
    istrix0 = istrix;
    istrix_spacer = 0;
    char strixfile[NSTR];
    sprintf(strixfile, "%s_%i.dat", strix_file, istrix);

/**************************************************************************//**
*                                pre-thermalisation
******************************************************************************/

/* cputimes */
    cputime_thermal = 0.0;
    cputime = cpu_time();

/* doing pre-thermalisation */
    printf("Pre-thermalisation:\n");
    umuca = p.Umuca;
    p.Umuca = 0;

    while (tth <= p.Nth) {
        if (tth == p.Nth / 2) {
            montecarlo_set_zeros();
        }

        /* doing monte-carlo sweep */
        montecarlo_algorithm(f, p, c, r, &muca, &op, sigma, epsilon);

        /* setting sigma and epsilon only in second half of prethermailsation */
        if ((tth - p.Nth / 2) > 2) {
            montecarlo_set_scales(tth - p.Nth / 2 + 1, dofs, set_sigma, &sigma,
                                  set_eps, &epsilon);
        }
        tth++;
    }
    tth--;
    p.Umuca = umuca;
/* printing times and set parameters */
    cputime_thermal = cpu_time() - cputime;
    printf("%g secs\n", cputime_thermal);
    printf("sigma = %g\nepsilon = %g\n\n", sigma, epsilon);

/* writing fields and updating status file and rng state */
    write_fields(f, p, fields_file);
    save_status(tth, t, sigma, epsilon, status_file);
    write_rng_state(r, rng_file);

    fflush(stdout);
    fflush(stderr);

/**************************************************************************//**
*                                actual monte-carlo
******************************************************************************/

/* cpu times */
    montecarlo_set_zeros();
    cputime_montecarlo = 0.0;
    cputime_measurements = 0.0;
    cputime_checkpoints = 0.0;
/* iterators */
    tmeas = 0;

/* doing  monte-carlo sweeps */
    printf("Monte-carlo sweeps:\n");
    if (verbosity) {
        printf("%-8s %-12s %-12s %-12s %-12s %-12s\n", "t", "wall_clock", "w",
               "op", "H", "dw");
    }

    while (t <= p.Nmc) {

        /* doing monte-carlo sweep */
        cputime = cpu_time();
        montecarlo_algorithm(f, p, c, r, &muca, &op, sigma, epsilon);
        cputime_montecarlo += cpu_time() - cputime;

        if (t % p.Tmeas == 0 && t > 0) {
            tmeas = (t / p.Tmeas - 1) % p.Tcheck;

            /* doing measurements */
            cputime = cpu_time();
            get_measurements(data[tmeas], f, p, c);
            data[tmeas][i_w] = get_multicanonical_weight(&muca);
            op = data[tmeas][i_op]; /* not strictly necessary */
            cputime_measurements += cpu_time() - cputime;

            /* printing fields if op is within separatrix and spaced */
            if (fabs(p.OPC - op) < p.dOP / 2 && istrix_spacer <= 0) {
                write_fields(f, p, strixfile);
                printf("########## %s: ", strixfile);
                printf("op = %-g, ", op);
                printf("w = %-g\n", data[tmeas][i_w]);
                istrix++;
                sprintf(strixfile, "%s_%i.dat", strix_file, istrix);
                istrix_spacer = strix_spacer;
            }
        }
        istrix_spacer--;

        /* checkpoints */
        if (t % p.Tmeas == 0 && t > 0 && tmeas == (p.Tcheck - 1)) {
            cputime = cpu_time();

            if (!start_status) {
                /* writing data */
                save_data(data, NMEAS_FIELD_AVG + NMEAS_FIELD_GLOBAL, p.Tcheck,
                          data_file, append || t > p.Tcheck * p.Tmeas);

                /* writing fields */
                write_fields(f, p, fields_file);

                /* writing rng state */
                write_rng_state(r, rng_file);

                if (p.Umuca) {
                    /* updating multicanonical weights */
                    dweights =
                        update_multicanonical(&muca, acc > t / (p.Nmc - 1.0),
                                              p);

                    /* writing multicanonical */
                    save_multicanonical(muca, p, weight_file);
                }

                /* checking if params have been changed */
                get_params(&q, params_file);
                if ( checkpoint_params(p, q) ) {
                    p.Nmc = q.Nmc;
                    printf("########## params file updated\n");
                    printf("%-8s %-8i\n", "Nmc", p.Nmc);
                    printf("##########\n");
                }

                /* update status file */
                save_status(tth, t, sigma, epsilon, status_file);
            }

            if (verbosity) {
                printf("%-8i %-12g %-12g %-12g %-12g %-12g\n", t,
                       wall_time(start_t), data[tmeas][i_w], data[tmeas][i_op],
                       data[tmeas][i_h], dweights);
            }

            fflush(stdout);
            fflush(stderr);

            start_status = 0;
            cputime_checkpoints += cpu_time() - cputime;
        }

        t++;
    }
    t--;
    printf("\n");
    /* update status file */
    save_status(tth, t, sigma, epsilon, status_file);

#ifdef SAVE_OP_LATTICE
    save_local_order_parameter(f, p, c, op_file);
    printf("Wrote order parameter lattice to:\n%s\n", op_file);
#endif

/**************************************************************************//**
*                                showing acceptances/cputimes
******************************************************************************/

/* saving multicanonical weights */
    if (p.Umuca) {
        printf("Wrote multicanonical weights to:\n%s\n", weight_file);
    }
    printf("Wrote %i measurements to:\n%s\n", p.Nmc / p.Tmeas, data_file);
    printf("Wrote fields to:\n%s\n", fields_file);
    printf("Wrote %i separatrix fields into:\n%s_#\n\n"
           , istrix - istrix0, strix_file);

    cputime_total = cpu_time() - cputime_total;

/* printing accepts ratio and cpu times spent on each algorithm */
    printf("%-16s%-16s%-16s%-16s%-16s\n", "algorithm", "accepts ratio",
           "microsec/site", "millisec/sweep", "sec");
    if (p.Nth)
        printf("%-16s%-16s%-16s%-16g%-16g\n", "prethermal"
               , " "
               , " "
               , 1.0e3 * cputime_thermal / (p.Nth + 1.0)
               , cputime_thermal);
/* printing times for monte-carlo algorithms */
    montecarlo_show_times( p.Nmc, dofs);
/* printing times for rest */
    printf("%-16s%-16s%-16s%-16g%-16g\n", "measurements"
           , " "
           , " "
           , 1.0e3 * cputime_measurements / (p.Nmc + 1.0)
           , cputime_measurements);
    printf("%-16s%-16s%-16s%-16g%-16g\n", "checkpoints"
           , " "
           , " " /* note this is per checkpoint, not per sweep */
           , 1.0e3 * cputime_checkpoints /
           ((p.Nmc + 1.0) / (double)p.Tmeas / p.Tcheck)
           , cputime_checkpoints);
    printf("%-16s%-16s%-16s%-16s%-16g\n", "rest"
           , " "
           , " "
           , " "
           , cputime_total - cputime_thermal - cputime_montecarlo
           - cputime_measurements - cputime_checkpoints);

    fflush(stdout);
    fflush(stderr);

/**************************************************************************//**
*                                finishing up
******************************************************************************/
/* printing separator */
    printf("\n%s%s\n\n", "----------------------------------------"
           , "----------------------------------------");

/* cputime of ends */
    cputime = cpu_time();

/* freeing memory */
    gsl_rng_free(r);
    free_fields(&f, p);
    free_2d_array(data);
    printf("Freed data array\n\n");
    if (p.OPbins) {
        free_multicanonical(&muca, p);
    }

/* showing total times taken */
    cputime_total += cpu_time() - cputime_total;
    cputime_ends += cpu_time() - cputime;
    printf("Ends time taken: %gs\n", cputime_ends);
    printf("Wall time taken: %gs (%g hours)\n", wall_time(start_t),
           wall_time(start_t) / 3600.0);
    printf("CPU time taken: %gs (%g hours)\n", cputime_total,
           cputime_total / 3600.0);
    printf("CPU/wall: %g\n", cputime_total / wall_time(start_t) );

/* printing final line break */
    printf("\n%s%s\n\n", "########################################"
           , "########################################");

    fflush(stdout);
    fflush(stderr);

    return 0;
}
