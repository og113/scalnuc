/** @file cubic_order_parameter.c
 * order parameter stuff for cubic anisotropy model
 */
#include "common.h"
#include "cubic.h"

/** local order parameter */
double order_parameter_local(fields f, int x, int y, int z,
                             params *p, couplings *c) {
    return f.phi1[x][y][z] * f.phi1[x][y][z] + f.phi2[x][y][z] *
           f.phi2[x][y][z];
}

/** local change to order parameter due to change in phi */
double delta_order_parameter(double*** phi, params *p, couplings *c,
                             int x, int y, int z, double dphi) {
    return ((phi[x][y][z] + dphi) * (phi[x][y][z] + dphi)
            - phi[x][y][z] * phi[x][y][z]) / (double)(p->Nx * p->Ny * p->Nz);
}

/** scales field to ensure order paramter at some fixed value */
void scale_field_to_order_parameter(fields *f, params p, couplings c) {
    int x, y, z;
    double op, op_new, scale;

    /* calculating order parameter */
    op = order_parameter(*f, p, c);

    /* determining how much to scale the field */
    op_new = 0.5 * (p.OPmin + p.OPmax);
    scale = sqrt(op_new / op);

    /* scaling the field */
    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                f->phi1[x][y][z] *= scale;
                f->phi2[x][y][z] *= scale;
            }
        }
    }
}
