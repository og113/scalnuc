/** @file cubic_alloc.c
 * Functions for allocating and freeing memory for fields, and other structs.
 * For theories with two scalar fields, such as the cubic anisotropy model.
 */
#include "common.h"
#include "cubic.h"


/**
 *    allocate and free fields struct
 */
void alloc_fields(fields *f, params p) {

    f->phi1 = make_field(p);
    f->phi2 = make_field(p);
    printf("Allocated fields\n");
}

/**
 * set fields values to zero
 */
void zero_fields(fields *f, params p) {

    zero_field(f->phi1, p);
    zero_field(f->phi2, p);
}

/**
 * set fields values random
 */
void set_random_fields(fields *f, params p, gsl_rng* r, double hotness) {

    set_random_field(f->phi1, p, r, hotness);
    set_random_field(f->phi2, p, r, hotness);
}

/**
 * free fields
 */
void free_fields(fields *f, params p) {

    free_field(f->phi1, p);
    free_field(f->phi2, p);
    printf("Freed fields\n");
}

/**
 * copy fields
 */
void copy_fields(fields *fin, fields *fout, params p) {

    copy_field(fin->phi1, fout->phi1, p);
    copy_field(fin->phi2, fout->phi2, p);
}

void init_mom(fields mom, params p, couplings c, gsl_rng *r) {
    int x, y, z;

    for (x = 0; x < (p.Nx); x++) {
        for (y = 0; y < (p.Ny); y++) {
            for (z = 0; z < (p.Nz); z++) {
                (mom.phi1)[x][y][z] = gsl_ran_gaussian(r, 1);
                (mom.phi2)[x][y][z] = gsl_ran_gaussian(r, 1);
            }
        }
    }
}
