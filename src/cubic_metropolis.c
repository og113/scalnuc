/** @file cubic_metropolis.c
 * Metropolis algorithms for monte carlo evaluations of cubic anisotropy model.
 */
#include "common.h"
#include "cubic.h"


/**
 *    performs simple metropolis sweep for cubic anisotropy model
 *     Nlocal and sigma can be optimised to accelerate the algorithm
 *        they give the number of local updates per sweep, and the standard
 *deviation
 *        of the changes to phi1 and phi2 which are proposed.
 */
static int metropolis_local(double*** phiA, double*** phiB, int x, int y, int z,
                            params *p, couplings *c, gsl_rng* r,
                            multicanonical *muca,
                            double *op, double sigma) {

    int accept;
    double dh, dphi, prob, dweight, dop;
    dweight = 0.0;

    /* accept/reject? */
    dphi = sigma * (gsl_rng_uniform(r) - 0.5);
    dh = delta_hamiltonian(phiA, phiB, p, c, x, y, z, dphi);
    dop = delta_order_parameter(phiA, p, c, x, y, z, dphi);
    if (p->OPbins) { /* multicanonical */
        set_multicanonical_proposed_state(muca, *op + dop, p);
        dweight = muca->w_new - muca->w;
    }
    accept = 0;
    if (dh - dweight <= 0.0) accept = 1;
    else {
        prob = gsl_rng_uniform(r);
        if (prob <= exp(-dh + dweight)) accept = 1;
    }

    /* accepting/rejecting change */
    if (accept) {
        phiA[x][y][z] += dphi;
        *op += dop;
        if (p->OPbins) {
            set_multicanonical_current_state(muca, p);
        }
    }

    return accept;
}

/**
 *    sweep of the local update for one field
 */
static int metropolis_static(double*** phiA, double*** phiB, params p,
                             couplings c, gsl_rng* r, multicanonical *muca,
                             double *op, double sigma) {
     int x, y, z;    // iterators over lattice
     int i, evenodd; // even or odd
     int accepts;    // accept/reject?

    /* looping over lattice */
    accepts = 0;
    evenodd = (int)(gsl_rng_uniform(r) + 0.5);
    for (i = 0; i < 2; i++) {
        for (x = 0; x < p.Nx; x++) {
            for (y = 0; y < p.Ny; y++) {
                for (z = 0; z < p.Nz; z++) {
                    if ( (x + y + z) % 2 == evenodd )
                        accepts +=
                            metropolis_local(phiA, phiB, x, y, z,
                                             &p, &c, r, muca, op, sigma);
                }
            }
        }
        if (p.Umuca) {
            /* updating ni and hi in based on current value of op */
            update_multicanonical_ancillary_arrays(muca, &p);
        }
        evenodd = (evenodd == 1 ? 0 : 1);
    }

    return accepts;
}

/**
 *    wrapper for the above
 */
int metropolis(fields f, params p, couplings c, gsl_rng* r
               , multicanonical *muca, double *op, double sigma) {
    int ret;
    ret = metropolis_static(f.phi1, f.phi2, p, c, r, muca, op, sigma);
    ret += metropolis_static(f.phi2, f.phi1, p, c, r, muca, op, sigma);
    return ret;
}

/**
 *    performs global radial metropolis sweep for cubic anisotropy model
 *   follows kajantie et al., the electroweak phase transition (1996)
 *     epsilon can be optimised to accelerate the algorithm
 *        it gives the range of the logarithm of the
 *        scaling factor of the changes to phi1 and phi2 which are proposed.
 */
int metropolis_global_radial(fields f, params p, couplings c, gsl_rng* r,
                             multicanonical *muca, double *op, int Nglobal,
                             double epsilon) {
    int l, x, y, z, accept, accepts;
    double h2, h4, dh, prob, R, Rtot, xi, dweight;
    accepts = 0;
    dweight = 0.0;
    h2 = 0.0;
    h4 = 0.0;
    Rtot = 1.0;

    /* finding values of terms in action */
    for (x = 0; x < p.Nx; x++) {
        for (y = 0; y < p.Ny; y++) {
            for (z = 0; z < p.Nz; z++) {
                h2 += local_kinetic_1(f.phi1, &p, &c, x, y, z);
                h2 += local_kinetic_1(f.phi2, &p, &c, x, y, z);
                h2 += local_quadratic_potential(f.phi1[x][y][z],
                                                f.phi2[x][y][z], &c);
                h4 += local_quartic_potential(f.phi1[x][y][z], f.phi2[x][y][z],
                                              &c);

            }
        }
    }

    /* repeating update Nglobal times */
    for (l = 0; l < Nglobal; l++) {

        accept = 0;

        /* radius */
        xi = 2.0 * epsilon * (gsl_rng_uniform(r) - 0.5);
        R = exp(xi);

        /* change to action */
        dh = (R * R - 1.0) * h2 + (R * R * R * R - 1.0) * h4;

        if (p.OPbins) { /* multicanonical */
            /* get weight function for propsed change */
            set_multicanonical_proposed_state(muca, R * R * (*op), &p);
            dweight = muca->w_new    - muca->w;
        }

        /* accept/reject? */
        if (dh - dweight - NFIELDS * xi * p.Nx * p.Ny * p.Nz <= 0.0) {
            accept = 1;
        }
        else {
            prob = gsl_rng_uniform(r);
            if (prob < exp(-dh + dweight + NFIELDS * xi * p.Nx * p.Ny * p.Nz)) {
                accept = 1;
            }
        }

        /* accepting/rejecting change */
        if (accept) {
            accepts++;
            set_multicanonical_current_state(muca, &p);
            *op = get_multicanonical_order_parameter(muca);
            h2 *= R * R;
            h4 *= R * R * R * R;
            Rtot *= R;
        }
        if (p.Umuca) {
            /* updating ni and hi in based on current value of op */
            update_multicanonical_ancillary_arrays(muca, &p);
        }
    }

    for (x = 0; x < p.Nx; x++) {
        for (y = 0; y < p.Ny; y++) {
            for (z = 0; z < p.Nz; z++) {

                f.phi1[x][y][z] *= Rtot;
                f.phi2[x][y][z] *= Rtot;

            }
        }
    }

    return accepts;
}

/**
 *    performs global radial metropolis sweep for cubic anisotropy model
 *   with two different radii for phi1 and phi2
 *     epsilon can be optimised to accelerate the algorithm
 *        it gives the range of the logarithm of the
 *        scaling factor of the changes to phi1 and phi2 which are proposed.
 */
int metropolis_global(fields f, params p, couplings c, gsl_rng* r
                      , multicanonical *muca, double *op, int Nglobal,
                      double epsilon) {

    int l, x, y, z, accept, accepts;
    double h2_1, h2_2, h4_1, h4_2, h4_12, dh;
    double op_1, op_2;
    double prob, R1, R2, R1tot, R2tot, xi1, xi2, dweight;
    accepts = 0;
    h2_1 = 0.0;
    h2_2 = 0.0;
    h4_1 = 0.0;
    h4_2 = 0.0;
    h4_12 = 0.0;
    op_1 = 0.0;
    op_2 = 0.0;
    dweight = 0.0;
    R1tot = 1.0;
    R2tot = 1.0;

    /* finding values of terms in action */
    for (x = 0; x < p.Nx; x++) {
        for (y = 0; y < p.Ny; y++) {
            for (z = 0; z < p.Nz; z++) {
                h2_1 += local_kinetic_1(f.phi1, &p, &c, x, y, z);
                h2_1 += local_quadratic_potential_1(f.phi1[x][y][z], &c);
                h2_2 += local_kinetic_1(f.phi2, &p, &c, x, y, z);
                h2_2 += local_quadratic_potential_1(f.phi2[x][y][z], &c);
                h4_1 += local_quartic_potential_1(f.phi1[x][y][z], &c);
                h4_2 += local_quartic_potential_1(f.phi2[x][y][z], &c);
                h4_12 += local_quartic_potential_2(f.phi1[x][y][z],
                                                   f.phi2[x][y][z], &c);
                op_1 += f.phi1[x][y][z] * f.phi1[x][y][z];
                op_2 += f.phi2[x][y][z] * f.phi2[x][y][z];
            }
        }
    }

    op_1 /= (double)(p.Nx * p.Ny * p.Nz);
    op_2 /= (double)(p.Nx * p.Ny * p.Nz);

    /* repeating update Nglobal times */
    for (l = 0; l < Nglobal; l++) {

        accept = 0;

        /* radii */
        xi1 = 2.0 * epsilon * (gsl_rng_uniform(r) - 0.5);
        R1 = exp(xi1);
        xi2 = 2.0 * epsilon * (gsl_rng_uniform(r) - 0.5);
        R2 = exp(xi2);

        /* change to action */
        dh = (R1 * R1 - 1.0) * h2_1 + (R2 * R2 - 1.0) * h2_2
             + (R1 * R1 * R1 * R1 - 1.0) * h4_1 + (R2 * R2 * R2 * R2 - 1.0) *
             h4_2
             + (R1 * R1 * R2 * R2 - 1.0) * h4_12;

        if (p.OPbins) { /* multicanonical */
            /* get weight function for propsed change */
            set_multicanonical_proposed_state(muca,
                                              R1 * R1 * op_1 + R2 * R2 * op_2,
                                              &p);
            dweight = muca->w_new    - muca->w;
        }

        /* accept/reject? */
        if (dh - dweight - (xi1 + xi2) * p.Nx * p.Ny * p.Nz <= 0.0) {
            accept = 1;
        }
        else {
            prob = gsl_rng_uniform(r);
            if (prob < exp((xi1 + xi2) * p.Nx * p.Ny * p.Nz - dh + dweight)) {
                accept = 1;
            }
        }

        /* accepting/rejecting change */
        if (accept) {
            accepts++;
            set_multicanonical_current_state(muca, &p);
            *op = get_multicanonical_order_parameter(muca);
            /* assumes quadratic order parameter */
            op_1 *= R1 * R1;
            op_2 *= R2 * R2;
            h2_1 *= R1 * R1;
            h2_2 *= R2 * R2;
            h4_1 *= R1 * R1 * R1 * R1;
            h4_2 *= R2 * R2 * R2 * R2;
            h4_12 *= R1 * R1 * R2 * R2;
            R1tot *= R1;
            R2tot *= R2;

        }
        if (p.Umuca) {
            /* updating ni and hi in based on current value of op */
            update_multicanonical_ancillary_arrays(muca, &p);
        }
    }

    for (x = 0; x < p.Nx; x++) {
        for (y = 0; y < p.Ny; y++) {
            for (z = 0; z < p.Nz; z++) {

                f.phi1[x][y][z] *= R1tot;
                f.phi2[x][y][z] *= R2tot;

            }
        }
    }

    return accepts;
}
