/** @file singlet_overrelax.c
 * Overrelax algorithm for monte carlo evaluations of singlet scalar model.
 */
#include "common.h"
#include "singlet.h"


/** Overrelax update
 *    Performs overrelax algorithm, following discussion following kajantie et
 *al.
 * , the electroweak phase transition (1996)
 */
static int overrelaxation_local(fields f, int x, int y, int z, params *p,
                                couplings *c,
                                gsl_rng* r, multicanonical *muca, double *op) {
    int accept;                           // accept/reject?
    double alpha, beta, gamma, delta;     // polynomial coefficients
    double a, b, d;                       // intermediates
    double phi_0, phi;                    // new phi
    double x1, x2, x3;                    // solutions cubic equation
    double measure, prob, dop, dweight;   // for accept/reject step
    dweight = 0.0;

    /*
     *    solving
     *        alpha*phi**4 + beta*phi**3 + gamma*phi**2 + delta*phi
     *            = alpha*phi0**4 + beta*phi0**3 + gamma*phi0**2 + delta*phi0
     */
    phi_0 = f.phi[x][y][z];
    alpha = c->beta4;
    beta = c->beta3;
#if defined(IMPROVED) || defined(IMPROVED_OA2)
    gamma = c->betalap * (-15.0 / 2.0) + c->beta2;
    delta = 2.0 * c->betalap *
            (laplacian(f.phi, p, x, y, z) + 15.0 / 2.0 * phi_0)
            + c->beta1;
#else
    gamma = c->betalap * (-6.0) + c->beta2;
    delta = 2.0 * c->betalap *
            (laplacian(f.phi, p, x, y, z) + 6.0 * phi_0) + c->beta1;
#endif

    /*
     *    this is equivalent to solving
     *        alpha*(phi-phi0)*(phi**3 + a*phi**2 + b*phi + d) = 0
     *    where
     *        alpha = phi0
     *        beta = b/a + phi0**2
     *        gamma = d/a + alpha*beta
     */
    a = phi_0 + beta / alpha;
    b = phi_0 * a + gamma / alpha;
    d = phi_0 * b + delta / alpha;

    /*
     *    solving
     *        phi**3 + a*phi**2 + b*phi + d = 0
     */
    if (gsl_poly_solve_cubic(a, b, d, &x1, &x2, &x3) == 3) {
        /*fprintf(stderr,"More than one solution in overrelax\n");*/
        prob = gsl_rng_uniform(r);
        if (prob > 2.0 / 3.0)
            x1 = x3;
        else if (prob > 1.0 / 3.0)
            x1 = x2;
    }

    /* new phi */
    phi = x1;

    /* correction to order parameter */
    dop = delta_order_parameter(f.phi, p, c, x, y, z, phi - phi_0);

    /* accept/reject? on measure */
    measure = phi_0 * (4.0 * alpha * phi_0 * phi_0 + 3.0 * beta * phi_0
                       + 2.0 * gamma) + delta;
    measure /= phi *
               (4.0 * alpha * phi * phi + 3.0 * beta * phi + 2.0 * gamma) +
               delta;
    measure = fabs(measure);
    accept = 0;
    if (p->OPbins) { /* multicanonical, following kari's code */
        set_multicanonical_proposed_state(muca, *op + dop, p);
        dweight = muca->w_new    - muca->w;
        /*measure *= exp(dweight);  goes straight into accept/reject */
        measure = log(measure) +  dweight; /* goes straight into accept/reject
                                              */
    }
    if (measure >= log(1.0)) {
        accept = 1;
    }
    else {
        prob = gsl_rng_uniform(r);
        if (measure >= log(prob)) {
            accept = 1;
        }
    }

    /* accepting/rejecting change */
    if (accept) {
        f.phi[x][y][z] = phi;
        *op += dop;
        if (p->OPbins) {
            set_multicanonical_current_state(muca, p);
        }
    }

    return accept;
}

/* sweep of the local update */
int overrelaxation(fields f, params p, couplings c,
                   gsl_rng* r, multicanonical *muca, double *op) {
    int x, y, z;     /**< iterators over lattice */
    int colour;      // Colour of the checkerboard
    int accepts;     /**< accept/reject? */

    /* looping over lattice */
    accepts = 0;
    for (colour = 0; colour < NCOLOURS; colour++) {
        for (x = 0; x < p.Nx; x++) {
            for (y = 0; y < p.Ny; y++) {
                for (z = 0; z < p.Nz; z++) {
                    if ( (x + y + z) % NCOLOURS == colour )
                        accepts += overrelaxation_local(f, x, y, z, &p, &c, r,
                                                        muca, op);
                }
            }
        }
        if (p.Umuca) {
            /* updating ni and hi in based on current value of op */
            update_multicanonical_ancillary_arrays(muca, &p);
        }
    }

    return accepts;
}
