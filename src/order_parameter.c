/** @file order_parameter.c
 * generic order parameter stuff
 */
#include "common.h"
#if defined(CUBIC)
#include "cubic.h"
#elif defined(RECTANGULAR)
#include "rectangular.h"
#elif defined(SINGLET)
#include "singlet.h"
#endif

/** function to calculate order parameter from measurements array */
double order_parameter_meas(double *datum, params p, couplings c) {
    return datum[i_op];
}

/** order parameter directly from fields */
double order_parameter(fields f, params p, couplings c) {
    double op = 0.0;
    int x, y, z;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                op += order_parameter_local(f, x, y, z, &p, &c);
            }
        }
    }

    return op / (double)(p.Nx * p.Ny * p.Nz);
}

/**
 * save local order parameter to file in ascii for plotting
 */
void save_local_order_parameter(fields f, params p, couplings c, char *file) {
    int x, y, z;
    double op;
    FILE *fs;

    /* opening file */
    fs = fopen(file, "w");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    /* saving order parameter */
    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                op = order_parameter_local(f, x, y, z, &p, &c);
                fprintf(fs, "%-12d %-12d %-12d ", x, y, z);
                fprintf(fs, "%-32.*f\n", DBL_DIG - 1, op);
            }
        }
    }

    fclose(fs);

}
