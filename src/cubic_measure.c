/** @file cubic_measure.c
 * Measurements for cubic anisotropy model.
 */
#include "common.h"
#include "cubic.h"

/**
 *		measurements
 */

void get_measurements (double* data, fields f, params p, couplings c) {
    int x, y, z, i;
    double f1, f2;
    double norm = 1.0 / (double)(p.Nx * p.Ny * p.Nz);
    int istart, iend;

    istart = 0;
    iend = NMEAS_FIELD_AVG + NMEAS_FIELD_GLOBAL;

    for (i = istart; i < iend; i++) {
        data[i] = 0.0;
    }

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                f1 = f.phi1[x][y][z];
                f2 = f.phi2[x][y][z];
                data[i_1] += f1;
                data[i_2] += f2;
                data[i_1sq] += f1 * f1;
                data[i_2sq] += f2 * f2;
                data[i_1qu] += f1 * f1 * f1 * f1;
                data[i_2qu] += f2 * f2 * f2 * f2;
                data[i_1sq2sq] += f1 * f1 * f2 * f2;
                data[i_l1] += f1 * laplacian(f.phi1, &p, x, y, z);
                data[i_l2] += f2 * laplacian(f.phi2, &p, x, y, z);
            }
        }
    }

    /* assumes h, op and w at end */
    for (i = istart; i < istart + NMEAS_FIELD_AVG; i++) {
        data[i] *= norm;
    }

    data[i_op] = order_parameter(f, p, c);
    data[i_h] = hamiltonian(f, p, c);
}


void get_measurements_mom(double *data, fields m, fields f, params p,
                          couplings c) {
    int x, y, z, i;
    double m1, m2;
    double norm = 1.0 / (double)(p.Nx * p.Ny * p.Nz);
    int istart, iend;

    istart = NMEAS_FIELD_AVG + NMEAS_FIELD_GLOBAL;
    iend = NMEAS_TOT;

    for (i = istart; i < iend; i++) {
        data[i] = 0.0;
    }

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                m1 = m.phi1[x][y][z];
                m2 = m.phi2[x][y][z];
                data[i_m1] += m1;
                data[i_m2] += m2;
                data[i_m1sq] += m1 * m1;
                data[i_m2sq] += m2 * m2;
            }
        }
    }

    data[i_e] = energy(f, m, p, c);

    for (i = istart; i < istart + NMEAS_MOM_AVG; i++) {
        data[i] *= norm;
    }

}

/**
 * show measurement names
 */
void show_measurement_labels (FILE* stream) {
    fprintf(stream, "%-12i%-12s\n", i_1, "phi_1");
    fprintf(stream, "%-12i%-12s\n", i_2, "phi_2");
    fprintf(stream, "%-12i%-12s\n", i_1sq, "phi_1^2");
    fprintf(stream, "%-12i%-12s\n", i_2sq, "phi_2^2");
    fprintf(stream, "%-12i%-12s\n", i_1qu, "phi_1^4");
    fprintf(stream, "%-12i%-12s\n", i_2qu, "phi_2^4");
    fprintf(stream, "%-12i%-12s\n", i_1sq2sq, "phi_1^2 phi_2^2");
    fprintf(stream, "%-12i%-12s\n", i_l1, "phi_1 laplacian(phi_1)");
    fprintf(stream, "%-12i%-12s\n", i_l2, "phi_2 laplacian(phi_2)");
    fprintf(stream, "%-12i%-12s\n", i_h, "hamiltonian");
    fprintf(stream, "%-12i%-12s\n", i_op, "order parameter");
    fprintf(stream, "%-12i%-12s\n", i_w, "weight");
    fprintf(stream, "%-12i%-12s\n", i_m1, "mom_1");
    fprintf(stream, "%-12i%-12s\n", i_m2, "mom_2");
    fprintf(stream, "%-12i%-12s\n", i_m1sq, "mom_1^2");
    fprintf(stream, "%-12i%-12s\n", i_m2sq, "mom_2^2");
    fprintf(stream, "%-12i%-12s\n", i_e, "energy");

    fprintf(stream, "\n");
}

/**
 * print measurement names
 */
void print_measurement_labels (char *file) {
    /* opening file */
    FILE * fs = fopen(file, "w");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }
    show_measurement_labels(fs);

    /* closing file */
    fflush(fs);
    fclose(fs);
}
