/** @file singlet_theory.c
 * Defining singlet scalar model in terms of its lagrangian, etc.
 */
#include "common.h"
#include "singlet.h"

/*
 *        kinetic term
 */

/** local kinetic term for one phi in singlet scalar model */
double local_kinetic (double*** phi, params *p, couplings *c, int x, int y,
                      int z) {
    return c->betalap * phi[x][y][z] * laplacian(phi, p, x, y, z);
}

/*
 *        potentials
 */

/** local potential in singlet scalar model  */
double local_potential (double phi, couplings *c) {
    double phisq = phi * phi;
    return c->beta1 * phi
           + c->beta2 * phisq
           +    c->beta3 * phisq * phi
           + c->beta4 * phisq * phisq;
}

/** linear part of local potential in singlet scalar model */
double local_linear_potential (double phi, couplings *c) {
    return c->beta1 * phi;
}

/** quadratic part of local potential in singlet scalar model */
double local_quadratic_potential (double phi, couplings *c) {
    return c->beta2 * phi * phi;
}

/** cubic part of local potential in singlet scalar model */
double local_cubic_potential (double phi, couplings *c) {
    return c->beta3 * phi * phi * phi;
}

/** quartic part of local potential in singlet scalar model */
double local_quartic_potential (double phi, couplings *c) {
    double phisq = phi * phi;
    return c->beta4 * phisq * phisq;
}


/*
 *        Hamiltonians
 */

/** total hamiltonian in singlet scalar model using n2*/
double hamiltonian (fields f, params p, couplings c) {
    double h = 0.0;
    int x, y, z;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                h += c.betalap * f.phi[x][y][z] * laplacian(f.phi, &p, x, y, z)
                     + local_potential(f.phi[x][y][z], &c);
            }
        }
    }

    return h;
}


/** local change to hamiltonian due to change in phi
 *             in singlet scalar model                               */
double delta_hamiltonian (double*** phi, params *p,
                          couplings *c, int x, int y, int z, double dphi) {
#if defined(IMPROVED) || defined (IMPROVED_OA2)
    return c->betalap * dphi *
           (2.0 * laplacian(phi, p, x, y, z) - (15.0 / 2.0) * dphi)
           + local_potential(phi[x][y][z] + dphi, c)
           - local_potential(phi[x][y][z], c);
#else
    return c->betalap * dphi * (2.0 * laplacian(phi, p, x, y, z) - 6.0 * dphi)
           + local_potential(phi[x][y][z] + dphi, c)
           - local_potential(phi[x][y][z], c);
#endif
}

/** functional derivative of hamiltonian */
double d_hamiltonian_d_phi(double ***phi, params *p, couplings *c, int x,
                           int y, int z)
{
    double phisqr = phi[x][y][z] * phi[x][y][z];

    return 2.0 * c->betalap * laplacian(phi, p, x, y, z)
           + c->beta1
           + 2.0 * c->beta2 * phi[x][y][z]
           + 3.0 * c->beta3 * phisqr
           + 4.0 * c->beta4 * phisqr * phi[x][y][z];
}

/** Hamiltonian stochastic equation for momentum, gaussian noise term not
   included */
double partial_mom(double*** mom, double*** phi, params *p,
                   couplings *c, int x, int y, int z) {

    return -2.0 * c->betalap * laplacian(phi, p, x, y, z)
           - d_hamiltonian_d_phi(phi, p, c, x, y, z)
           + (p->gamma) * mom[x][y][z];
}

/** kinetic energy in momentum field */
double momentum_energy(fields m, params p, couplings c) {
    int x, y, z;
    double res = 0;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                res += m.phi[x][y][z] * m.phi[x][y][z];
            }
        }
    }

    return c.betamom * res;
}

/** sum of momentum and field energy */
double energy(fields f, fields m, params p, couplings c) {
    return momentum_energy(m, p, c) + hamiltonian(f, p, c);
}
