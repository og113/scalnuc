/** @file cubic_time.c
 * Time evolution for cubic anisotropy model.
 */
#include "common.h"
#include "cubic.h"

// Forest-Ruth constants
const double a1 = 1.3512071919596578;
const double a2 = -1.7024143839193153;

// Time evolution (Euler)
void time_evolution_euler(fields f, fields m, params p, couplings c, gsl_rng *r,
                          fields tmp) {

    int x, y, z;
    double mA, mB, pA, pB;

    copy_fields( &f, &tmp, p);

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                mA = m.phi1[x][y][z] +
                     (partial_mom(m.phi1, tmp.phi1, tmp.phi2, &p, &c, x, y,
                                  z) + noise(r, &p, 1.0)) * p.dt;
                mB = m.phi2[x][y][z] +
                     (partial_mom(m.phi2, tmp.phi2, tmp.phi1, &p, &c, x, y,
                                  z) + noise(r, &p, 1.0)) * p.dt;
                pA = f.phi1[x][y][z] + m.phi1[x][y][z] * p.dt;
                pB = f.phi2[x][y][z] + m.phi2[x][y][z] * p.dt;

                m.phi1[x][y][z] = mA;
                m.phi2[x][y][z] = mB;
                f.phi1[x][y][z] = pA;
                f.phi2[x][y][z] = pB;

            }
        }
    }

}


void leap_mom_field(fields f, fields m, params p, couplings c, gsl_rng *r) {

    int x, y, z;
    double mA, mB;

    double fac = 1.0 / (1.0 + p.gamma * p.dt * 0.5);


    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {

                // deterministic parts
                mA = m.phi1[x][y][z] - p.dt * functional_hamiltonian(f.phi1,
                                                                     f.phi2, &p,
                                                                     &c, x, y,
                                                                     z);
                mB = m.phi2[x][y][z] - p.dt * functional_hamiltonian(f.phi2,
                                                                     f.phi1, &p,
                                                                     &c, x, y,
                                                                     z);

#if !defined(REFRESH)
                // damping and stochastic parts
                mA *= fac;
                mB *= fac;
                mA += -0.5 * p.gamma * p.dt * fac * m.phi1[x][y][z] + p.dt *
                      fac * noise(r, &p, 1.0);
                mB += -0.5 * p.gamma * p.dt * fac * m.phi2[x][y][z] + p.dt *
                      fac * noise(r, &p, 1.0);
#endif

                m.phi1[x][y][z] = mA;
                m.phi2[x][y][z] = mB;
            }
        }
    }

}


void leap_field(fields f, fields m, params p, couplings c) {

    int x, y, z;
    double phiA, phiB;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                phiA = f.phi1[x][y][z] + m.phi1[x][y][z] * p.dt;
                phiB = f.phi2[x][y][z] + m.phi2[x][y][z] * p.dt;

                f.phi1[x][y][z] = phiA;
                f.phi2[x][y][z] = phiB;
            }
        }
    }

}

/* Forwards time evolution (Leapfrog) */
void time_evolution_leapfrog(fields f, fields m, params p, couplings c,
                             gsl_rng *r) {

    leap_mom_field(f, m, p, c, r);
    leap_field(f, m, p, c);
}


/* Reverse time evolution (Leapfrog) */
void time_evolution_leapfrog_reverse(fields f, fields m, params p, couplings c,
                                     gsl_rng *r) {

    leap_field(f, m, p, c);
    leap_mom_field(f, m, p, c, r);
}


void mom_half_step(fields f, fields m, params p,  couplings c, gsl_rng *r) {

    p.dt = p.dt * 0.5;
    leap_mom_field(f, m, p, c, r);
    p.dt = 2.0 * p.dt;

}

void field_half_step(fields f, fields m, params p, couplings c) {

    p.dt = p.dt * 0.5;
    leap_field(f, m, p, c);
    p.dt = 2.0 * p.dt;

}

void kick_drift_kick(fields f, fields m, params p,  couplings c, gsl_rng *r) {

    mom_half_step(f, m, p, c, r);
    leap_field(f, m, p, c);
    mom_half_step(f, m, p, c, r);

}

void move_both_4(fields f, fields m, params p, couplings c, gsl_rng *r) {
    /**David's Forest-Ruth algorithm*/
    double init_dt = p.dt;

    p.dt = a1 * init_dt;
    kick_drift_kick(f, m, p, c, r);
    p.dt = a2 * init_dt;
    kick_drift_kick(f, m, p, c, r);
    p.dt = a1 * init_dt;
    kick_drift_kick(f, m, p, c, r);
    p.dt = init_dt;

}

void alloc_runge_kutta(rk_fields_array *rk_fields, params p) {
    /** A stupid way of allocating the RK4 fields */
    alloc_fields(&(rk_fields->k1), p);
    zero_fields(&(rk_fields->k1), p);

    alloc_fields(&(rk_fields->l1), p);
    zero_fields(&(rk_fields->l1), p);

    alloc_fields(&(rk_fields->k2), p);
    zero_fields(&(rk_fields->k2), p);

    alloc_fields(&(rk_fields->l2), p);
    zero_fields(&(rk_fields->l2), p);

    alloc_fields(&(rk_fields->k3), p);
    zero_fields(&(rk_fields->k3), p);

    alloc_fields(&(rk_fields->l3), p);
    zero_fields(&(rk_fields->l3), p);

    alloc_fields(&(rk_fields->k4), p);
    zero_fields(&(rk_fields->k4), p);

    alloc_fields(&(rk_fields->l4), p);
    zero_fields(&(rk_fields->l4), p);

}


void free_runge_kutta(rk_fields_array *rk_fields, params p) {
    /** A stupid way of freeing the RK4 fields */
    free_fields(&(rk_fields->k1), p);
    free_fields(&(rk_fields->l1), p);
    free_fields(&(rk_fields->k2), p);
    free_fields(&(rk_fields->l2), p);
    free_fields(&(rk_fields->k3), p);
    free_fields(&(rk_fields->l3), p);
    free_fields(&(rk_fields->k4), p);
    free_fields(&(rk_fields->l4), p);

}


void runge_kutta_iter(fields f, fields m, fields k, fields l, params p,
                      couplings c, gsl_rng *r) {
    /** A function for calculating each k_n and l_n value, n=1..4 */
    int x, y, z;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                k.phi1[x][y][z] = m.phi1[x][y][z];
                k.phi2[x][y][z] = m.phi2[x][y][z];

                l.phi1[x][y][z] = -functional_hamiltonian(f.phi1, f.phi2, &p,
                                                          &c, x, y, z);
                l.phi2[x][y][z] = -functional_hamiltonian(f.phi2, f.phi1, &p,
                                                          &c, x, y, z);
            }
        }
    }

}



void update_rk_fields(fields f, fields m, fields f_init, fields m_init,
                      fields k, fields l, params p) {
    /** Update all fields */
    int x, y, z;
    double h;

    h = p.dt;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                f.phi1[x][y][z] = f_init.phi1[x][y][z] + 0.5 * h *
                                  k.phi1[x][y][z];
                f.phi2[x][y][z] = f_init.phi2[x][y][z] + 0.5 * h *
                                  k.phi2[x][y][z];

                m.phi1[x][y][z] = m_init.phi1[x][y][z] + 0.5 * h *
                                  l.phi1[x][y][z];
                m.phi2[x][y][z] = m_init.phi2[x][y][z] + 0.5 * h *
                                  l.phi2[x][y][z];
            }
        }
    }

}


void runge_kutta(fields f, fields m, fields f_init, fields m_init,
                 rk_fields_array rk_fields, params p, couplings c, gsl_rng *r) {
    /** RK4 algorithm, takes one step of size h*/
    int x, y, z;
    double h;

    copy_fields(&f, &f_init, p); // Keep copies of the initial field configs
    copy_fields(&m, &m_init, p);

    runge_kutta_iter(f, m, rk_fields.k1, rk_fields.l1, p, c, r);
    update_rk_fields(f, m, f_init, m_init, rk_fields.k1, rk_fields.l1, p);
    runge_kutta_iter(f, m, rk_fields.k2, rk_fields.l2, p, c, r);
    update_rk_fields(f, m, f_init, m_init, rk_fields.k2, rk_fields.l2, p);
    runge_kutta_iter(f, m, rk_fields.k3, rk_fields.l3, p, c, r);
    p.dt = 2.0 * p.dt;
    update_rk_fields(f, m, f_init, m_init, rk_fields.k3, rk_fields.l3, p);
    p.dt = 0.5 * p.dt;
    runge_kutta_iter(f, m, rk_fields.k4, rk_fields.l4, p, c, r);

    h = p.dt;
    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                f.phi1[x][y][z] = f_init.phi1[x][y][z] + h / 6.0 *
                                  (rk_fields.k1.phi1[x][y][z] + 2.0 *
                                   rk_fields.k2.phi1[x][y][z] +
                                   2.0 * rk_fields.k3.phi1[x][y][z] +
                                   rk_fields.k4.phi1[x][y][z]);
                f.phi2[x][y][z] = f_init.phi2[x][y][z] + h / 6.0 *
                                  (rk_fields.k1.phi2[x][y][z] + 2.0 *
                                   rk_fields.k2.phi2[x][y][z] +
                                   2.0 * rk_fields.k3.phi2[x][y][z] +
                                   rk_fields.k4.phi2[x][y][z]);

                m.phi1[x][y][z] = m_init.phi1[x][y][z] + h / 6.0 *
                                  (rk_fields.l1.phi1[x][y][z] + 2.0 *
                                   rk_fields.l2.phi1[x][y][z] +
                                   2.0 * rk_fields.l3.phi1[x][y][z] +
                                   rk_fields.l4.phi1[x][y][z]);
                m.phi2[x][y][z] = m_init.phi2[x][y][z] + h / 6.0 *
                                  (rk_fields.l1.phi2[x][y][z] + 2.0 *
                                   rk_fields.l2.phi2[x][y][z] +
                                   2.0 * rk_fields.l3.phi2[x][y][z] +
                                   rk_fields.l4.phi2[x][y][z]);
            }
        }
    }

}


void mom_refresh(fields m, params p, couplings c, gsl_rng *r) {
    /** Partial momentum refresh */

    int x, y, z;
    double mA, mB, eta1, eta2;
    double epsq = 1.0 - exp(-2.0 * p.gamma * p.dt); // epsilon squared

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                eta1 = gsl_ran_gaussian(r, 1.0);
                eta2 = gsl_ran_gaussian(r, 1.0);
                mA = sqrt(1.0 - epsq) * m.phi1[x][y][z] + sqrt(epsq) * eta1;
                mB = sqrt(1.0 - epsq) * m.phi2[x][y][z] + sqrt(epsq) * eta2;

                m.phi1[x][y][z] = mA;
                m.phi2[x][y][z] = mB;
            }
        }
    }

}

void mom_reverse(fields m, params p) {
    /** Reverses the momentum field */
    int x, y, z;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                m.phi1[x][y][z] = -m.phi1[x][y][z];
                m.phi2[x][y][z] = -m.phi2[x][y][z];
            }
        }
    }
}
