/** @file singlet_metropolis.c
 * Metropolis algorithms for monte carlo evaluations of singlet scalar model.
 */
#include "common.h"
#include "singlet.h"


/**
 *    performs simple metropolis sweep for singlet scalar model
 *      Nlocal and sigma can be optimised to accelerate the algorithm
 *        they give the number of local updates per sweep, and the
 * standard deviation
 *        of the changes to phi which are proposed.
 */
static int metropolis_local(fields f, int x, int y, int z, params *p,
                            couplings *c, gsl_rng* r, multicanonical *muca,
                            double *op, double sigma) {

    int accept;
    double dh, dphi, prob, dweight, dop;
    dweight = 0.0;

    /* accept/reject? */
    dphi = sigma * (gsl_rng_uniform(r) - 0.5);
    dh = delta_hamiltonian(f.phi, p, c, x, y, z, dphi);
    dop = delta_order_parameter(f.phi, p, c, x, y, z, dphi);
    if (p->OPbins) { /* multicanonical */
        set_multicanonical_proposed_state(muca, *op + dop, p);
        dweight = muca->w_new   - muca->w;
    }
    accept = 0;
    if (dh - dweight <= 0.0 ) accept = 1;
    else {
        prob = gsl_rng_uniform(r);
        if ( log(prob) <= (-dh + dweight) ) accept = 1;
    }

    /* accepting/rejecting change */
    if ( accept ) {
        f.phi[x][y][z] += dphi;
        *op += dop;
        if (p->OPbins) {
            set_multicanonical_current_state(muca, p);
        }
    }

    return accept;
}

/* sweep of the local update */
int metropolis(fields f, params p, couplings c,
               gsl_rng* r, multicanonical *muca, double *op, double sigma) {
    int x, y, z;    // iterators over lattice
    int colour; // Colour of the checkerboard 
    int accepts;    // accept/reject?

    /* looping over lattice */
    accepts = 0;

    for (colour = 0; colour < NCOLOURS; colour++) {
        for (x = 0; x < p.Nx; x++) {
            for (y = 0; y < p.Ny; y++) {
                for (z = 0; z < p.Nz; z++) {
                    if ( (x + y + z) % NCOLOURS == colour )
                        accepts +=
                            metropolis_local(f, x, y, z, &p, &c, r, muca, op,
                                             sigma);
                }
            }
        }
        if (p.Umuca) {
            /* updating ni and hi in based on current value of op */
            update_multicanonical_ancillary_arrays(muca, &p);
        }
    }

    return accepts;
}

/**
 *    performs global radial metropolis sweep for singlet scalar model
 *   follows kajantie et al., the electroweak phase transition (1996)
 *      epsilon can be optimised to accelerate the algorithm
 *        it gives the range of the logarithm of the
 *        scaling factor of the changes to phi which are proposed.
 */
int metropolis_global(fields f, params p, couplings c, gsl_rng* r,
                      multicanonical *muca, double *op, int Nglobal,
                      double epsilon) {

    if (Nglobal == 0)
        return 0;
    int l, x, y, z, accept, accepts;
    double h1, h2, h3, h4, dh, prob, R, Rtot, xi, dweight;
    accepts = 0;
    dweight = 0.0;
    h1 = 0.0;
    h2 = 0.0;
    h3 = 0.0;
    h4 = 0.0;
    Rtot = 1.0;

    /* finding values of terms in action */
    for (x = 0; x < p.Nx; x++) {
        for (y = 0; y < p.Ny; y++) {
            for (z = 0; z < p.Nz; z++) {
                h1 += local_linear_potential(f.phi[x][y][z], &c);
                h2 += local_kinetic(f.phi, &p, &c, x, y, z);
                h2 += local_quadratic_potential(f.phi[x][y][z], &c);
                h3 += local_cubic_potential(f.phi[x][y][z], &c);
                h4 += local_quartic_potential(f.phi[x][y][z], &c);

            }
        }
    }

    /* repeating update Nglobal times */
    for (l = 0; l < Nglobal; l++) {

        accept = 0;

        /* radius */
        xi = 2.0 * epsilon * (gsl_rng_uniform(r) - 0.5);
        R = exp(xi);

        /* change to action */
        dh = (R - 1.0) * h1 + (R * R - 1.0) * h2 + (R * R * R - 1.0) * h3 +
             (R * R * R * R - 1.0) * h4;

        if (p.OPbins) { /* multicanonical */
            /* get weight function for propsed change */
            /* assumes linear order parameter */
            set_multicanonical_proposed_state(muca, R * R * R * (*op), &p);
            dweight = muca->w_new   - muca->w;
        }

        /* accept/reject? */
        if (dh - dweight - NFIELDS * xi * p.Nx * p.Ny * p.Nz <= 0.0) {
            accept = 1;
        }
        else {
            prob = gsl_rng_uniform(r);
            if (log(prob) <
                (-dh + dweight + NFIELDS * xi * p.Nx * p.Ny * p.Nz)) {
                accept = 1;
            }
        }

        /* accepting/rejecting change */
        if (accept) {
            accepts++;
            set_multicanonical_current_state(muca, &p);
            *op = get_multicanonical_order_parameter(muca);
            h1 *= R;
            h2 *= R * R;
            h3 *= R * R * R;
            h4 *= R * R * R * R;
            Rtot *= R;

        }
        if (p.Umuca) {
            /* updating ni and hi in based on current value of op */
            update_multicanonical_ancillary_arrays(muca, &p);
        }
    }

    for (x = 0; x < p.Nx; x++) {
        for (y = 0; y < p.Ny; y++) {
            for (z = 0; z < p.Nz; z++) {

                f.phi[x][y][z] *= Rtot;

            }
        }
    }

    return accepts;
}
