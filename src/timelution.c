/** @file timelution.c
 * \brief Main source for time evolution in scalar nucleation program
 * This file does the real-time evolution of the stochastic Hamiltonian
 * equations. It compiles with
 *        make <model>_te
 * run with
 *        ./<model>_te <forwards_data_file> <backwards_data_file> <strix_file>
 * or
 *        ./<model>_te <forwards_data_file> <backwards_data_file>
 * where params and status are the parameters and run status files respectively.
 * See README.md for more details.
 */


#include "common.h"
#if defined(CUBIC)
#include "cubic.h"
#elif defined(RECTANGULAR)
#include "rectangular.h"
#elif defined(SINGLET)
#include "singlet.h"
#endif

/**************************************************************************//**
*                                main begins
******************************************************************************/
int main(int argc, char* argv[]) {

    printf("\n########################### %s ###########################\n"
           , "Scalar bubble nucleation");
#if defined(CUBIC)
    printf("###########################  %s  ###########################\n\n"
           , "Cubic anisotropy model");
#elif defined(RECTANGULAR)
    printf("###########################  %s  ###########################\n\n"
           , "Rectangular anisotropy model");
#elif defined(SINGLE)
    printf("###########################  %s  ###########################\n\n"
           , "Single scalar model");
#endif
    printf("###########################  %s  #############################\n\n"
           , "time evolution");



    /* Read in the strix files, and the output files for forwards and backwards
       evolution data */
    if(argc < 3 || argc > 4) {
        fprintf(stderr,
                "Run with inital field config: %s %s %s %s\n",
                argv[0], "<forwards_data_file>", "<backwards_data_file>",
                "<strix_file>");
        fprintf(stderr,
                "If no strix file is given defaults to cold initial config\n");
        exit(1);
    } else if(argc == 3) {
        fprintf(stderr,
                "Run uses cold initial config, all fields set to zero\n");
    }


/** starting clocks */
    clock_t start_t = clock();
    double cputime_total = cpu_time();

/**************************************************************************//**
*                                parameters
******************************************************************************/

/* showing whether we are using the improved action */
#if defined(IMPROVED) || defined(IMPROVED_OA2)
    printf("Using improved action\n\n");
#else
    printf("Using unimproved action\n\n");
#endif

/* Show which update algorithm is being used */
#ifdef RK4
    printf("Using RK4 algorithm for real time evolution\n");
#elif FR
    printf("Using Forest-Ruth algorithm for real time evolution\n");
#else
    printf("Using Leapfrog for real time evolution\n");
#endif

#ifdef REFRESH
    printf("Using partial momentum refresh\n");
#endif

/** getting couplings */
    couplings c;
    get_couplings(&c, couplings_file);
/** checking consistency of couplings */
    if (!check_couplings(c)) {
        return 1;
    }
    else {
        printf("%s:\n", couplings_file);
        printf("########## Couplings\n");
        show_couplings(c, stdout);
        printf("########## Counterterms\n");
        show_counterterms(c, stdout);
        //set_betas_to_zero(&c);
    #if defined(SINGLET)
        printf("########## Betas\n");
        show_betas(c, stdout);
    #endif
        print_counterterms(c, cts_file);
        printf("\n");
    }

/** getting paramters */
    params p;
    get_params(&p, params_file);

/** checking consistency of params */
    if (!check_params(p)) {
        return 1;
    }
    else {
        printf("%s:\n", params_file);
        show_params(p, stdout);
        printf("\n");
    }

    points o;
    get_points(&o, points_file);
/** add consistency checks */


    int start_status;
    int i; /**< Iterator */
    int append = 1;         /**< append or overwrite data file */
    int tmeas;
    double op;
    double op_init;

    int tunnel_status = 2; /** whether or not the system tunneled */
    int crossing_status;
    int prev_status;
    int count_d = 0;
    char phase_f = 'I';
    char phase_b = 'I';

/**************************************************************************//**
*                                other variables
******************************************************************************/

/** various variables assigned during computations */
/** iterators */
/** cpu times */
    double cputime_ends;
    double cputime;
/** number of different measurements */
    int nmeas = NMEAS_TOT;

/**************************************************************************//**
*                                allocating memory
******************************************************************************/

/** measurements arrays */
    if (p.Tcheck < 0) {
        fprintf(stderr, "Out of bounds error, %i < 0\n", p.Tcheck);
        return 1;
    }
    double **data = make_2d_array(p.Tcheck, nmeas);
    zero_2d_array(data, p.Tcheck, nmeas);
    printf("Allocated data array\n");

    double **data_back = make_2d_array(p.Tcheck, nmeas);
    zero_2d_array(data_back, p.Tcheck, nmeas);
    printf("Allocated data array for backwards evolution\n");

/* getting seed for random number generator */
    unsigned long seed;
    if (p.seed != 0) {
        seed = p.seed;
        printf("Initialised random number generator with input seed:\n%lu\n",
               seed);
    } else {
        seed = (unsigned long)dev_urandom();
        printf("Initialised random number generator with random seed:\n%lu\n",
               seed);
    }
/* or (unsigned long)time(NULL) */

/* allocating random number generator */
    gsl_rng *r;
    r = gsl_rng_alloc( gsl_rng_taus2 );
    if ( r == NULL ) {
        fprintf(stderr, "failed to allocate gsl random number generator\n");
        exit(1);
    }
    gsl_rng_set( r, seed);
    printf("Allocated %s generator with seed %lu\n", gsl_rng_name(r), seed);

/* allocating fields */
    fields f;
    alloc_fields(&f, p);
    zero_fields(&f, p);

/* The momentum field */
    fields mom;
    alloc_fields(&mom, p);
    zero_fields(&mom, p);

/* Copy of the initial momentum config */
    fields mom_init;
    alloc_fields(&mom_init, p);
    zero_fields(&mom_init, p);

/* For RK4 */
#ifdef RK4
    fields f_rk;
    alloc_fields(&f_rk, p);
    zero_fields(&f_rk, p);

    fields mom_rk;
    alloc_fields(&mom_rk, p);
    zero_fields(&mom_rk, p);

    rk_fields_array rk_fields;
    alloc_runge_kutta(&rk_fields, p);
#endif

    fflush(stdout);
    fflush(stderr);

/* cputime of ends */
    cputime_ends = cpu_time() - cputime_total;

/**************************************************************************//**
*                                initial conditions
******************************************************************************/
/* Initialize momentum field(s) */
    init_mom(mom, p, c, r);
    printf("Generated a new initial momentum field\n");

/* initial fields */
    if (argc == 4 && file_exists(argv[3])) {
        read_fields(&f, p, argv[3]);
        printf("Loaded fields from:\n%s\n", argv[3]);

    } else if (argc == 3) {
        zero_fields(&f, p);
        zero_fields(&mom, p);
        printf("Cold initial conditions\n");

    } else {
        printf("%s can't be found\n", argv[3]);
    }

/* Copy the initial momentum config for backwards evolution */
    copy_fields(&mom, &mom_init, p);

#ifdef VISUALISATION
    FILE *fvis = fopen(vis_file, "ab");
    if(fvis == NULL) {
        fprintf(stderr, "Can't open file, %s\n", vis_file);
        exit(0);
    }
    write_lattice_size(p, fvis);
    fclose(fvis);
#endif

    printf("\n");

/**************************************************************************//**
*                                Forwards time evolution
******************************************************************************/
    start_status = 0;

    printf("Energy initially: %d, %32.16g\n", 0, energy(f, mom, p, c));

    get_measurements(data[0], f, p, c);
    get_measurements_mom(data[0], mom, f, p, c);
    op = order_parameter_meas(data[0], p, c);
    op_init = op;
    printf("Order parameter: %g\n", op);

    if (op > o.OPC) {
        prev_status = 1;
    } else {
        prev_status = 0;
    }

// Initialize momntum to be half step ahead of fields
#if !defined(RK4) && !defined(FR)
    mom_half_step(mom, f, p, c, r);
#endif

// Forwards time evolution
    for(i = 0; i < (p.Nt); i++) {

        tmeas = i % p.Tcheck;

        //kick_drift_kick(f, mom, p, c, r);
    #ifdef RK4
        runge_kutta(f, mom, f_rk, mom_rk, rk_fields, p, c, r);
    #elif FR
        move_both_4(f, mom, p, c, r);
    #else
        time_evolution_leapfrog_reverse(f, mom, p, c, r);
    #endif

    #ifdef REFRESH
        mom_refresh(mom, p, c, r);
    #endif

        /* Measurements */
        get_measurements(data[tmeas], f, p, c);
        get_measurements_mom(data[tmeas], mom, f, p, c);
        op = order_parameter_meas(data[tmeas], p, c);

        if (i % p.Tcheck == (p.Tcheck - 1)) {

            if (i && !start_status) {
                printf("%12d %32.16g %32.16g\n", i, energy(f, mom, p, c), op);
                save_data(data, nmeas, p.Tcheck, argv[1],
                          append || i > p.Tcheck);
            #ifdef VISUALISATION
                write_many_fields(f, p, vis_file);
            #endif
            }

            fflush(stderr);
            fflush(stdout);
            start_status = 0;
        }


        if (op > o.OPC) {
            crossing_status = 1;
        } else {
            crossing_status = 0;
        }

        if (crossing_status != prev_status) {
            count_d++;
        }

        prev_status = crossing_status;

        /* Evolve until field is in either phase */
        if (op < o.OPMS) {
            printf("Symmetric phase\n");
            tunnel_status = 0;
            save_data(data, nmeas, tmeas + 1, argv[1], append);
            phase_f = 'S';
            break;
        } else if (op > o.OPMB) {
            printf("Broken phase\n");
            tunnel_status = 1; /** Because broken = true vacuum */
            save_data(data, nmeas, tmeas + 1, argv[1], append);
            phase_f = 'B';
            break;
        }

    }


    printf("Energy finally: %d, %32.16g\n", i, energy(f, mom, p, c));


/**************************************************************************//**
*                                Backwards time evolution
******************************************************************************/

/* Evolve backwards from the critical point*/
/* Reverse the momentum and load the initial config again*/
    if (argc == 4) {
        read_fields(&f, p, argv[3]);
    } else {
        zero_fields(&f, p);
    }

    printf("Energy initially: %d, %32.16g\n", 0, energy(f, mom_init, p, c));
    mom_reverse(mom_init, p); /** Uses the initial momentum config */

    get_measurements(data_back[0], f, p, c);
    get_measurements_mom(data_back[0], mom_init, f, p, c);
    op = order_parameter_meas(data_back[0], p, c);
    printf("Order parameter: %g\n", op);

    if (op > o.OPC) {
        prev_status = 1;
    } else {
        prev_status = 0;
    }

/** Evolve half a step */
#if !defined(RK4) && !defined(FR)
    mom_half_step(mom_init, f, p, c, r);
#endif

    for(i = 0; i < (p.Nt); i++) {

        tmeas = i % p.Tcheck;

        //kick_drift_kick(f, mom, p, c, r);
    #ifdef RK4
        runge_kutta(f, mom_init, f_rk, mom_rk, rk_fields, p, c, r);
    #elif FR
        move_both_4(f, mom_init, p, c, r);
    #else
        time_evolution_leapfrog_reverse(f, mom_init, p, c, r);
    #endif

    #ifdef REFRESH
        mom_refresh(mom_init, p, c, r);
    #endif

        /* Measurements */
        get_measurements(data_back[tmeas], f, p, c);
        get_measurements_mom(data_back[tmeas], mom_init, f, p, c);
        op = order_parameter_meas(data_back[tmeas], p, c);

        if (i % p.Tcheck == (p.Tcheck - 1)) {

            if (i && !start_status) {
                printf("%12d %32.16g %32.16g\n", i, energy(f, mom_init, p,
                                                           c), op);
                save_data(data_back, nmeas, p.Tcheck, argv[2],
                          append || i > p.Tcheck);
            #ifdef VISUALISATION
                write_many_fields(f, p, vis_file);
            #endif
            }

            fflush(stderr);
            fflush(stdout);
            start_status = 0;
        }


        if (op > o.OPC) {
            crossing_status = 1;
        } else {
            crossing_status = 0;
        }

        if (crossing_status != prev_status) {
            count_d++;
        }

        prev_status = crossing_status;

        /* Evolve until field is in either phase */
        if (op < o.OPMS) {
            printf("Symmetric phase\n");
            if (tunnel_status == 1) {
                printf("Tunneled!\n");
            }
            save_data(data_back, nmeas, tmeas + 1, argv[2], append);
            phase_b = 'S';
            break;

        } else if (op > o.OPMB) {
            printf("Broken phase\n");
            if (tunnel_status == 0) {
                printf("Tunneled!\n");
            }
            save_data(data_back, nmeas, tmeas + 1, argv[2], append);
            phase_b = 'B';
            break;

        }
    }

    printf("D: %i\n", count_d);
    printf("Energy finally: %i, %32.16g\n", i, energy(f, mom_init, p, c));
    print_measurement_labels(labels_file);


/**************************************************************************//**
*                                saving fields
******************************************************************************/

//write_fields(f, p, fields_file);

/**************************************************************************//**
*                                finishing up
******************************************************************************/
    if(count_d % 2 != 0 && phase_f != 'I' && phase_b != 'I') {
        fprintf(stderr, "TUNELLING: YES %i %c %c %.16g\n", count_d, phase_f,
                phase_b, op_init);
    } else {
        fprintf(stderr, "TUNELLING: NO %i %c %c %.16g\n", count_d, phase_f,
                phase_b, op_init);
    }
    printf("\nWrote forwards real time evolution data to:\n%s\n", argv[1]);
    printf("\nWrote backwards real time evolution data to:\n%s\n", argv[2]);

    fflush(stdout);
    fflush(stderr);

/* printing separator */
    printf("\n%s%s\n\n", "----------------------------------------"
           , "----------------------------------------");

/* cputime of ends */
    cputime = cpu_time();

/* freeing memory */
    free_fields(&f, p);
    free_fields(&mom, p);
    free_fields(&mom_init, p);
    free_2d_array(data);
    free_2d_array(data_back);
    gsl_rng_free(r);

#ifdef RK4
    free_fields(&f_rk, p);
    free_fields(&mom_rk, p);
    free_runge_kutta(&rk_fields, p);
#endif

    printf("Freed data arrays\n\n");

/* showing total times taken */
    cputime_total = cpu_time() - cputime_total;
    cputime_ends += cpu_time() - cputime;
    printf("Ends time taken: %gs\n", cputime_ends);
    printf("CPU time taken: %gs (%g hours)\n", cputime_total,
           cputime_total / 3600.0);
    printf("Wall time taken: %gs (%g hours)\n", wall_time(start_t),
           wall_time(start_t) / 3600.0);
    printf("CPU/wall: %gs\n", cputime_total / wall_time(start_t) );

/* printing final line break */
    printf("\n%s%s\n\n", "########################################"
           , "########################################");

    fflush(stdout);
    fflush(stderr);

    return 0;
}
