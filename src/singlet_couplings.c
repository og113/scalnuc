/** @file single_couplings.c
 *    couplings stuff for singlet scalar model
 */
#include "common.h"
#include "singlet.h"

/**
 * show cubic couplings
 */
void show_couplings( couplings c, FILE* stream) {
    fprintf(stream, "%-16s%-16s%-16s%-16s%-16s\n", "b1", "b2", "b3", "a",
            "mu3");
    fprintf(stream, "%-16g%-16g%-16g%-16g%-16g\n", c.b1, c.b2, c.b3, c.a,
            c.mu3);
    fprintf(stream, "\n");
}

/**
 * check cubic couplings
 */
int check_couplings( couplings c) {
    int good;

    if (c.a <= 0 || c.mu3 <= 0) {
        fprintf(stderr, "Need:\n!(a<=0)\n");
        good = 0;
    }
    else {
        good = 1;
    }
    if (!good) {
        fprintf(stderr, "Couplings inconsistent\n");
        show_couplings(c, stderr);
    }
    return good;
}


/**
 * print cubic couplings
 */
void print_couplings( couplings c, char *file) {
    /* opening file */
    FILE * fs = fopen(file, "w");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    fprintf(fs, "########## Singlet couplings\n");
    fprintf(fs, "%-12s %-12g\n", "b1 ", c.b1);
    fprintf(fs, "%-12s %-12g\n", "b2 ", c.b2);
    fprintf(fs, "%-12s %-12g\n", "b3 ", c.b3);
    fprintf(fs, "%-12s %-12g\n", "a  ", c.a);
    fprintf(fs, "%-12s %-12g\n", "mu3", c.mu3);

    /* closing file */
    fflush(fs);
    fclose(fs);
}

/**
 * shifts basis of couplings to one where b3=0
 * note that b4=1
 */
static void shift_basis(couplings *p) {
    double b3 = p->b3;
    p->b1 = p->b1 + b3 * b3 * b3 / 3.0 - b3 * p->b2;
    p->b2 = p->b2 - b3 * b3 / 2.0;
    p->b3 = 0.0;
}

/**
 * function to get bare parameters from renormalised ones
 * assumes in basis where b3 = 0.
 */
static void get_counterterms(couplings *p) {
    double sigma, xi, C1, C2, C3;
    double loop, Oa1, Oa2;
    double a, mu3, b4;

    /* checking in correct basis */
    if (abs(p->b3) > DBL_MIN) {
        fprintf(stderr, "ERROR: Not in basis with b3=0.\n");
        exit(3);
    }

#if defined(IMPROVED) || defined(IMPROVED_OA2)
    // O(a^2) improved
    sigma = 2.75238391130752;
    xi = -0.083647053040968;
    C1 = 0.0550612;
    C2 = 0.0334416;
    C3 = -0.86147916;
    Oa1 = 1.0;
    Oa2 = 1.0;
#elif defined(IMPROVED_OA1)
    // O(a) improved
    sigma = 3.17591153562522;
    xi = 0.152859324966101;
    C1 = 0.0;
    C2 = 0.0;
    C3 = 0.08848010;
    Oa1 = 1.0;
    Oa2 = 0.0;
#else
    // unimproved
    sigma = 3.17591153562522;
    xi = 0.0;
    C1 = 0.0;
    C2 = 0.0;
    C3 = 0.08848010;
    Oa1 = 0.0;
    Oa2 = 0.0;
#endif

    /* shorthand */
    b4 = 1.0;
    mu3 = p->mu3;
    a = p->a;
    loop = 1.0 / (4.0 * M_PI);

    /* absorbing factors of loop and a, where they turn up naturally */
    sigma *= loop / a;
    xi *= loop * a;
    C1 *= loop * loop * a * a;
    C2 *= loop * loop * a * a;

    /* tree + one-loop counterterms */
    p->db1 = 0.0;
    p->db3 = 0.0;
    p->db2 = -0.5 * sigma * b4;
    p->db4 = Oa1 * (1.5 * xi * b4 * b4);
    p->Zphi = 1.0;
    p->Zm = 1.0 + Oa1 * (0.5 * xi * b4);

    /* two-loop corrections */
    p->Zphi += Oa2 * (C2 / 6.0 * b4 * b4);
    p->db4 += Oa2 * ((0.75 * xi * xi - 3.0 * C1) * b4 * b4 * b4);
    p->db4 += - 2.0 * b4 * (p->Zphi - 1.0);
    p->db2 += loop * loop * b4 * b4 / 6.0 * (log(6.0 / mu3 / a) + C3) \
              - 0.5 * sigma * p->db4 \
              + (1.0 - p->Zm) * p->db2;
    /* modification of Zm after db2, means O(a) terms not added to db2 */
    p->Zm += Oa2 * ((0.25 * xi * xi - 0.5 * C1) * b4 * b4);
    p->Zm += - p->Zphi + 1.0;

    /* extra counterterms for composite operators */
    p->c4lap = Oa2 * (-4.0 * C2 * b4);
    p->c44 = 1.0 + Oa1 * (3.0 * xi * b4) \
             + Oa2 * (3.0 * (-3.0 * C1 + 0.75 * xi * xi) * b4 * b4);
    p->c42 = 12.0 * (p->b2 + p->db2) * \
             (Oa1 * 0.5 * xi + Oa2 * (0.5 * xi * xi - C1) * b4) \
             + 12.0 * p->Zm * (-0.5 * sigma \
                             + loop * loop / 3.0 * b4 *
                             (log(6.0 / mu3 / a) + C3)
                             - sigma * xi * b4);
}

/**
 * calculate betas
 */
static void get_betas( couplings *p) {
    double b4 = 1.0;

    /* only Zphi in Laplacian term following hep-lat/0103036 convention */
    p->betalap = -0.5 * p->Zphi;
    p->beta1 = (p->b1 + p->db1);
    //p->beta2 = p->Zm * (p->b2 + p->db2) / 2.0;
    p->beta2 = p->Zphi * p->Zm * (p->b2 + p->db2) / 2.0;
    p->beta3 = (p->b3 + p->db3) / 6.0;
    //p->beta4 = (b4 + p->db4) / 24.0;
    p->beta4 = p->Zphi * p->Zphi * (b4 + p->db4) / 24.0;
    p->betamom = 0.5;

    p->betalap *= p->a;
    p->beta1 *= pow(p->a, 3);
    p->beta2 *= pow(p->a, 3);
    p->beta3 *= pow(p->a, 3);
    p->beta4 *= pow(p->a, 3);
    p->betamom *= pow(p->a, 3);
}


/**
 * get couplings from file, with checking that the file conforms
 */
void get_couplings( couplings *p, char *file) {
    /* opening file */
    FILE * fs = fopen(file, "r");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        exit(2);
    }

    /* char arrays (on stack) to hold lines and parameter names */
    char buffer[NSTR]; /* assuming line <NSTR chars */
    /*char param_name[NSTRSS];*/

    int k = 0; /* goes from 0 to total number of parameters */
    int terms; /* to check that there are two things on each line */

    /* continue while not at the end of the file */
    while (!feof(fs)) {

        /* read into buffer and check line is not empty */
        if ( fgets (buffer, NSTR, fs) != NULL ) {

            /* ignore line and move on if starts with # or if blank */
            if ( buffer[0] == '\n' || buffer[0] == '#') {
                continue;
            }

            /* read from buffer into couplings, assuming expected file format
               checks parameter name is what we expect */
            if (k == 0) {
                terms = get_double(buffer, "b1", &(p->b1));
            }
            else if (k == 1)
                terms = get_double(buffer, "b2", &(p->b2));
            else if (k == 2)
                terms = get_double(buffer, "b3", &(p->b3));
            else if (k == 3)
                terms = get_double(buffer, "a", &(p->a));
            else if (k == 4)
                terms = get_double(buffer, "mu3", &(p->mu3));

            k++;

            /* checking expected number of things per line */
            if (terms != 2) {
                fprintf(stderr,
                        "Incorrect number of terms, %i, on line %i in %s\n",
                        terms, k, file);
                exit(2);
            }

        }
    }

    if (k != NCOUPLINGS) {
        fprintf(stderr, "Only set %i parameters out of %i\n", k, NCOUPLINGS);
        exit(2);
    }

    /* closing file */
    fclose(fs);

    /* shifting basis */
    shift_basis(p);
    printf("Shifting basis so that the cubic term is zero.\n\n");

    /* setting counterterms and betas from input */
    get_counterterms(p);
    get_betas(p);

}

/**
 * print counterterms
 */
void show_counterterms( couplings p, FILE* stream) {

    fprintf(stream, "%-12s%-12s%-12s%-12s%-12s%-12s\n",
            "db1", "db2", "db3", "db4", "Zm", "Zphi");
    fprintf(stream, "%-12.6g%-12.6g%-12.6g%-12.6g%-12.6g%-12.6g\n",
            p.db1, p.db2, p.db3, p.db4, p.Zm, p.Zphi);
    fprintf(stream, "%-12s%-12s%-12s\n",
            "c42", "c44", "c4lap");
    fprintf(stream, "%-12.6g%-12.6g%-12.6g\n",
            p.c42, p.c44, p.c4lap);
    fprintf(stream, "\n");
}

/**
 * print counterterms to file
 */
void print_counterterms( couplings p, char *file) {
    /* opening file */
    FILE * fs = fopen(file, "w");
    if(fs == NULL) {
        fprintf(stderr, "Can't open file, %s\n", file);
        return;
    }

    /* printing parameters in format:
     *             param name, param value         */
    fprintf(fs, "%-12s %-32.*f\n", "db1", DBL_DIG - 1, p.db1);
    fprintf(fs, "%-12s %-32.*f\n", "db2", DBL_DIG - 1, p.db2);
    fprintf(fs, "%-12s %-32.*f\n", "db3", DBL_DIG - 1, p.db3);
    fprintf(fs, "%-12s %-32.*f\n", "db4", DBL_DIG - 1, p.db4);
    fprintf(fs, "%-12s %-32.*f\n", "Zm", DBL_DIG - 1, p.Zm);
    fprintf(fs, "%-12s %-32.*f\n", "Zphi", DBL_DIG - 1, p.Zphi);
    fprintf(fs, "%-12s %-32.*f\n", "c42", DBL_DIG - 1, p.c42);
    fprintf(fs, "%-12s %-32.*f\n", "c44", DBL_DIG - 1, p.c44);
    fprintf(fs, "%-12s %-32.*f\n", "c4lap", DBL_DIG - 1, p.c4lap);

    /* closing file */
    fflush(fs);
    fclose(fs);
}


/**
 * show betas
 */
void show_betas( couplings p, FILE* stream) {

    fprintf(stream, "%-16s%-16s%-16s%-16s%-16s%-16s\n",
            "betalap", "beta1", "beta2", "beta3", "beta4", "betamom");
    fprintf(stream, "%-16.8g%-16.8g%-16.8g%-16.8g%-16.8g%-16.8g\n",
            p.betalap, p.beta1, p.beta2, p.beta3, p.beta4, p.betamom);
    fprintf(stream, "\n");
}

void set_betas_to_zero(couplings *p) {
    p->beta1 = 0.0;
    p->beta2 = 0.0;
    p->beta3 = 0.0;
    p->beta4 = 0.0;
}
