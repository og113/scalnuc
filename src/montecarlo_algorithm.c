/** @file cubic_montecarlo_algorithm.c
 *
 */
#include "common.h"
#if defined(CUBIC)
#include "cubic.h"
#elif defined(RECTANGULAR)
#include "rectangular.h"
#elif defined(SINGLET)
#include "singlet.h"
#endif

/** monte-carlo acceptances */
static long metropolis_accepts; /**< number of metropolis acceptances */
static long global_accepts;     /**< number of global metropolis acceptances */
static long overrelax_accepts;  /**< number of overrelaxation acceptances */
/** cpu times */
static double cputime_metropolis; /**< cpu time for metropolis */
static double cputime_global;     /**< cpu time for global metropolis */
static double cputime_overrelax;  /**< cpu time for overrelaxation */

/** Monte Carlo Zero Acceptances and CPU Times
 *    setting acceptances to zero
 */
void montecarlo_set_zeros() {
    metropolis_accepts = 0;
    global_accepts = 0;
    overrelax_accepts = 0;

    cputime_metropolis = 0.0;
    cputime_global = 0.0;
    cputime_overrelax = 0.0;
}

/** Monte Carlo Algorithm
 *    Wrapper for other algorithms
 */
void montecarlo_algorithm(fields f, params p, couplings c, gsl_rng* r,
                          multicanonical *muca, double *op, double sigma,
                          double epsilon) {
    int ilocal;
    double cputime;

    cputime = cpu_time();
    for (ilocal = 0; ilocal < NALG_MET; ilocal++)
        metropolis_accepts += metropolis(f, p, c, r, muca, op, sigma);
    cputime_metropolis += cpu_time() - cputime;

    cputime = cpu_time();
    global_accepts += metropolis_global(f, p, c, r, muca, op, NALG_GLOBAL,
                                        epsilon);
    cputime_global += cpu_time() - cputime;

    cputime = cpu_time();
    for (ilocal = 0; ilocal < NALG_OVER; ilocal++)
        overrelax_accepts += overrelaxation(f, p, c, r, muca, op);
    cputime_overrelax += cpu_time() - cputime;

}

/** Monte Carlo Set Scales
 *    setting scales for metropolis type algorithms
 */
void montecarlo_set_scales(int steps, int dofs, int set_sigma, double *sigma,
                           int set_eps, double *epsilon) {
    double metropolis_acceptance;
    double global_acceptance;

    /* from kari's cubic anisotropy code */
    if (set_sigma && NALG_MET) {
        metropolis_acceptance =
            metropolis_accepts / (double)(steps * NALG_MET * dofs);
        if (metropolis_acceptance < 0.65) *sigma *= 0.95;
        if (metropolis_acceptance > 0.7) *sigma *= 1.05;
    }
    if (set_eps && NALG_GLOBAL) {
        global_acceptance = global_accepts / (double)(steps * NALG_GLOBAL);
        if (global_acceptance < 0.65) *epsilon *= 0.95;
        if (global_acceptance > 0.7) *epsilon *= 1.05;
    }
}

/** Print CPU times
 * shows cpu times for monte-carlo algorthims in format used in scalnuc
 */
void montecarlo_show_times(int steps, int dofs) {
    if (NALG_MET)
        printf("%-16s%-16g%-16g%-16g%-16g\n", "metropolis"
               , 1.0 * metropolis_accepts / ((steps + 1.0) * NALG_MET * dofs)
               , 1.0e6 * cputime_metropolis / ((steps + 1.0) * NALG_MET * dofs)
               , 1.0e3 * cputime_metropolis / (steps + 1.0)
               , cputime_metropolis);
    if (NALG_GLOBAL)
        printf("%-16s%-16g%-16g%-16g%-16g\n", "global"
               , 1.0 * global_accepts / ((steps + 1.0) * NALG_GLOBAL)
               , 1.0e6 * cputime_global /
               ((steps + 1.0) * NALG_GLOBAL * dofs / NFIELDS)
               , 1.0e3 * cputime_global / (steps + 1.0)
               , cputime_global);
    if (NALG_OVER)
        printf("%-16s%-16g%-16g%-16g%-16g\n", "overrelax"
               , 1.0 * overrelax_accepts / ((steps + 1.0) * NALG_OVER * dofs)
               , 1.0e6 * cputime_overrelax / ((steps + 1.0) * NALG_OVER * dofs)
               , 1.0e3 * cputime_overrelax / (steps + 1.0)
               , cputime_overrelax);
}
