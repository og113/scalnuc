/** @file cubic_theory.c
 * Defining cubic anisotropy model in terms of its lagrangian, etc.
 */
#include "common.h"
#include "cubic.h"

/*
 *		kinetic term
 */

/** local kinetic term for one phi in cubic anisotropy model */
double local_kinetic_1 (double*** phi, params *p, couplings *c, int x, int y,
                        int z) {
    return -0.5 * c->Zphi * phi[x][y][z] * laplacian(phi, p, x, y, z);
}

/*
 *		potentials
 */

/** local potential in cubic anisotropy model  */
double local_potential (double phi1, double phi2, couplings *c) {
    double phi1sqr = c->Zphi * phi1 * phi1;
    double phi2sqr = c->Zphi * phi2 * phi2;
    return c->Zm * ((c->msq + c->dmsq) / 2.0) * (phi1sqr + phi2sqr)
           +   ((c->l1 + c->dl1) / 24.0) *
           (phi1sqr * phi1sqr + phi2sqr * phi2sqr)
           + ((c->l2 + c->dl2) / 4.0) * phi1sqr * phi2sqr;
}

/** quadratic part of local potential for one phi in cubic anisotropy model */
double local_quadratic_potential_1 (double phi, couplings *c) {
    return c->Zphi * c->Zm * ((c->msq + c->dmsq) / 2.0) * phi * phi;
}

/** quadratic part of local potential for phi1/phi2 in cubic anisotropy model */
double local_quadratic_potential (double phi1, double phi2, couplings *c) {
    return c->Zphi * c->Zm * ((c->msq + c->dmsq) / 2.0) *
           (phi1 * phi1 + phi2 * phi2);
}

/** self quartic part of local potential in cubic anisotropy model */
double local_quartic_potential_1 (double phi, couplings *c) {
    double phisqr = c->Zphi * phi * phi;
    return ((c->l1 + c->dl1) / 24.0) * phisqr * phisqr;
}

/** cross quartic part of local potential in cubic anisotropy model */
double local_quartic_potential_2 (double phi1, double phi2, couplings *c) {
    double phi1sqr = c->Zphi * phi1 * phi1;
    double phi2sqr = c->Zphi * phi2 * phi2;
    return ((c->l2 + c->dl2) / 4.0) * phi1sqr * phi2sqr;
}

/** quartic part of local potential for phi1/phi2 in cubic anisotropy model */
double local_quartic_potential (double phi1, double phi2, couplings *c) {
    double phi1sqr = c->Zphi * phi1 * phi1;
    double phi2sqr = c->Zphi * phi2 * phi2;
    return ((c->l1 + c->dl1) / 24.0) * (phi1sqr * phi1sqr + phi2sqr * phi2sqr)
           + ((c->l2 + c->dl2) / 4.0) * phi1sqr * phi2sqr;
}


/*
 *		Hamiltonians
 */

/** total hamiltonian in cubic anisotropy model using n2*/
double hamiltonian (fields f, params p, couplings c) {
    double h = 0.0;
    int x, y, z;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                h += -0.5 * c.Zphi * f.phi1[x][y][z] * laplacian(f.phi1, &p, x,
                                                                 y, z)
                     - 0.5 * c.Zphi * f.phi2[x][y][z] * laplacian(f.phi2, &p, x,
                                                                  y, z)
                     + local_potential(f.phi1[x][y][z], f.phi2[x][y][z], &c);
            }
        }
    }

    return h;
}


/** local change to hamiltonian due to change in phi1
 *                      in cubic anisotropy model                                               */
double delta_hamiltonian (double*** phiA, double*** phiB, params *p,
                          couplings *c, int x, int y, int z, double dphiA) {
#ifdef IMPROVED
    return -0.5 * c->Zphi * dphiA *
           (2.0 * laplacian(phiA, p, x, y, z) - (15.0 / 2.0) * dphiA)
           + local_potential(phiA[x][y][z] + dphiA, phiB[x][y][z], c)
           - local_potential(phiA[x][y][z], phiB[x][y][z], c);
#else
    return -0.5 * c->Zphi * dphiA *
           (2.0 * laplacian(phiA, p, x, y, z) - 6.0 * dphiA)
           + local_potential(phiA[x][y][z] + dphiA, phiB[x][y][z], c)
           - local_potential(phiA[x][y][z], phiB[x][y][z], c);
#endif
}


/** Functional derivative of the Hamiltonian with respect to phiA, excluding
   laplacian part */
/*double functional_hamiltonian(double phiA, double phiB, params *p) {
            double phiAsqr = p->Zphi*p->Zphi*phiA*phiA;
            double phiBsqr = p->Zphi*p->Zphi*phiB*phiB;
            return
               p->Zm*p->Zphi*(p->msq+p->dmsq)*phiA+1.0/6.0*(p->l1+p->dl1)*phiAsqr*phiA
 +0.5*(p->l2+p->dl2)*phiA*phiBsqr;

   }*/

/** functional derivative of hamiltonian */
double functional_hamiltonian(double*** phiA, double*** phiB, params *p,
                              couplings *c, int x, int y, int z) {
    double phiAsqr = c->Zphi * c->Zphi * phiA[x][y][z] * phiA[x][y][z];
    double phiBsqr = c->Zphi * c->Zphi * phiB[x][y][z] * phiB[x][y][z];

    return -c->Zphi * laplacian(phiA, p, x, y, z)
           + c->Zm * c->Zphi * (c->msq + c->dmsq) * phiA[x][y][z]
           + 1.0 / 6.0 * (c->l1 + c->dl1) * phiAsqr * phiA[x][y][z]
           + 0.5 * (c->l2 + c->dl2) * phiA[x][y][z] * phiBsqr;
}

/** Hamiltonian stochastic equation for momentum, gaussian noise term not
   included */
double partial_mom(double*** momA, double*** phiA, double*** phiB, params *p,
                   couplings *c, int x, int y, int z) {
    return c->Zphi * laplacian(phiA, p, x, y, z)
           - functional_hamiltonian(phiA, phiB, p, c, x, y, z)
           + (p->gamma) * momA[x][y][z];
}

/** kinetic energy in momentum field */
double momentum_energy(fields m, params p, couplings c) {
    int x, y, z;
    double res = 0;

    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                res += m.phi1[x][y][z] * m.phi1[x][y][z] + m.phi2[x][y][z] *
                       m.phi2[x][y][z];
            }
        }
    }

    return 0.5 * res;
}

/** sum of momentum and field energy */
double energy(fields f, fields m, params p, couplings c) {
    return momentum_energy(m, p, c) + hamiltonian(f, p, c);
}
