/** @file singlet_order_parameter.c
 * order parameter stuff for singlet scalar model
 */
#include "common.h"
#include "singlet.h"

/** local order parameter */
double order_parameter_local(fields f, int x, int y, int z,
                             params *p, couplings *c) {
    /* n.b. b4 = 1 */
    return f.phi[x][y][z]*f.phi[x][y][z] - 2.0*p->OPsym*f.phi[x][y][z];
}

/** local change to order parameter due to change in phi */
double delta_order_parameter(double*** phi, params *p, couplings *c,
                             int x, int y, int z, double dphi) {
    /* n.b. b4 = 1 */
    /* (phi0 + dphi) - phi0 */
    return ((phi[x][y][z]+dphi)*(phi[x][y][z]+dphi)-phi[x][y][z]*phi[x][y][z]- 2.0*p->OPsym*dphi)/ (double)(p->Nx * p->Ny * p->Nz);
}

/** scales field to ensure order paramter at some fixed value */
void scale_field_to_order_parameter(fields *f, params p, couplings c) {
    int x, y, z;
    double op, op_new, phi_new, fluct_scale;

    /* calculating order parameter */
    op = order_parameter(*f, p, c);

    /* determining how much to scale the field */
    op_new = 0.5 * (p.OPmin + p.OPmax);
    phi_new = p.OPsym + sqrt(op_new + p.OPsym*p.OPsym);
    fluct_scale = 0.05 * p.OPsym / op;

    /* scaling the field */
    for(x = 0; x < (p.Nx); x++) {
        for(y = 0; y < (p.Ny); y++) {
            for(z = 0; z < (p.Nz); z++) {
                f->phi[x][y][z] = phi_new + fluct_scale * (f->phi[x][y][z] - p.OPsym);
            }
        }
    }
}
